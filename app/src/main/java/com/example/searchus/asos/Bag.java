package com.example.searchus.asos;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Impl.Main_impl;
import com.example.searchus.asos.MODELS.ADDRESS_MODEL;
import com.example.searchus.asos.MODELS.mycart;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Bag extends AppCompatActivity
{
    ArrayList<mycart> listcart = new ArrayList<mycart>();

    LinearLayout parentlo,empty_bag_lo,main_lo,nonet_lo;
    RecyclerView Bag_RecyclerView;
    TextView totalitem,subtotal;
    View view;
    Button chechoutbt,empty_lo_add_addr_bt,checkaddrbt;
    Toolbar toolbar;

    PreferenceManager_ASOS PreferenceManager_ASOS;
    public static final int MY_PERMISSION_LOCATION = 001;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_bag);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

        TOOLBAR();
        BINDING();


        chechoutbt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CHECK_LOGIN();
            }
        });

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
    }

    public void TOOLBAR()
    {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
                finish();
            }
        });

    }

    public void BINDING()
    {
        Bag_RecyclerView = (RecyclerView) findViewById(R.id.bag_recycler_viewid);
        view=findViewById(R.id.view_id);
        parentlo=findViewById(R.id.parentlo_id);
        empty_bag_lo=findViewById(R.id.empty_bag_loid);
        main_lo=findViewById(R.id.main_lo_id);
        totalitem=findViewById(R.id.totalitemid);
        subtotal=findViewById(R.id.subtotalid);
        chechoutbt=findViewById(R.id.chechoutbtid);
        empty_lo_add_addr_bt=findViewById(R.id.empty_lo_add_addr_btid);
        checkaddrbt=findViewById(R.id.checkaddrbtid);

        main_lo=findViewById(R.id.main_lo_id);
        nonet_lo=findViewById(R.id.nonet_lo_id);
    }

    // Checking Internet connection function
    private BroadcastReceiver networkReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
            {
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED)
                {
//                    "We have internet connection. Good to go."

                    toolbar.setVisibility(View.VISIBLE);
                    nonet_lo.setVisibility(View.GONE);
                    main_lo.setVisibility(View.VISIBLE);

                    ADDRESS_IF_AVAILABLE_GETDATA();
                    CART_GETDATA();

                }
                else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED)
                {
//                  "We have lost internet connection"

                    toolbar.setVisibility(View.GONE);
                    nonet_lo.setVisibility(View.VISIBLE);
                    main_lo.setVisibility(View.GONE);
                }
            }
        }
    };

    public void CART_GETDATA()
    {
        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        Bag_RecyclerView.setLayoutManager(trending_LayoutManager);
        Bag_RecyclerView.setNestedScrollingEnabled(false);

        Main_impl impl = new Main_impl(Bag.this);
        listcart = impl.getUser();

        double sum = 0;
        for (int i=0;i<listcart.size();i++)
        {
            sum = sum + Double.parseDouble(listcart.get(i).getAmount());
        }
        Log.e("ALL_AMOUNT:", String.valueOf(sum));

        subtotal.setText("₹ "+String.valueOf(sum));

        if(listcart.size()<2)
        {
            totalitem.setText("( "+listcart.size()+" item"+" )");

        }
        else
        {
            totalitem.setText("( "+listcart.size()+" items"+" )");

        }

        //IF CART IS EMPTY THEN SAD LAYOUT
        if(listcart.size()==0)
        {
            main_lo.setVisibility(View.GONE);
            empty_bag_lo.setVisibility(View.VISIBLE);
            empty_lo_add_addr_bt.setVisibility(View.GONE);
        }
        else
        {
            empty_bag_lo.setVisibility(View.GONE);
            main_lo.setVisibility(View.VISIBLE);
            Bag_RecyclerView.setAdapter(new BAG_ADP(getApplicationContext(),listcart,subtotal,totalitem));
        }


    }

    public class BAG_ADP extends RecyclerView.Adapter<BAG_ADP.ViewHolder>
    {
        ArrayList<mycart> cartModelArrayList;
        Context context;
        private RequestQueue rq;
        private PreferenceManager_ASOS PreferenceManager_ASOS;
        TextView subtotal;
        TextView totalitem;

        public BAG_ADP(Context context, ArrayList<mycart> cartModelArrayList,TextView subtotal,TextView totalitem)
        {
            super();
            this.context = context;
            this.cartModelArrayList = cartModelArrayList;
            rq= Volley.newRequestQueue(context);
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
            this.subtotal=subtotal;
            this.totalitem=totalitem;
        }


        @Override
        public BAG_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_bag_lvitems, parent, false);
            BAG_ADP.ViewHolder viewHolder = new BAG_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final BAG_ADP.ViewHolder holder, final int position)
        {


            holder.cart_pname.setText(cartModelArrayList.get(position).getProduct_name());
            holder.cart_p_price.setText(cartModelArrayList.get(position).getPrice());
            holder.tv_Qty.setText(cartModelArrayList.get(position).getQty());
            holder.tvqtyprice.setText(cartModelArrayList.get(position).getAmount());



            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH+""+cartModelArrayList.get(position).getImg())
                    .into(holder.cart_p_img);

            Log.e("lol", PaperCart_CoNNectioN.IMGPATH+""+cartModelArrayList.get(position).getImg());

            //ADD CODE
            holder.tv_cart_addqty.setOnClickListener(new View.OnClickListener() //ADD
            {
                @Override
                public void onClick(View v)
                {


//                    Log.e("SETTEXT_QTY", holder.tv_Qty.getText().toString());


                    if(!PreferenceManager_ASOS.getQTY_Status())
                    {
                        int getQTY= Integer.parseInt(holder.tv_Qty.getText().toString());
                        getQTY++;
                        holder.tv_Qty.setText(String.valueOf(getQTY));
                        int qty=getQTY;
                        double qtyprice= Double.parseDouble(holder.cart_p_price.getText().toString());
                        double ans=qty*qtyprice;
//                    Log.e("Add", String.valueOf(ans));
                        holder.tvqtyprice.setText(Double.toString(ans));

                        cartModelArrayList.get(position).setAmount(String.valueOf(ans));

                        try
                        {
                            Main_impl main_impl=new Main_impl(context);
                            main_impl.update(new mycart(cartModelArrayList.get(position).getId(),
                                    cartModelArrayList.get(position).getProduct_id(),
                                    cartModelArrayList.get(position).getProduct_name(),
                                    cartModelArrayList.get(position).getPrice(),
                                    cartModelArrayList.get(position).getDescription(),
                                    cartModelArrayList.get(position).getSpecification(),
                                    cartModelArrayList.get(position).getDiscount(),
                                    cartModelArrayList.get(position).getAvailability(),
                                    cartModelArrayList.get(position).getImg(),
                                    String.valueOf(qty),
                                    String.valueOf(ans),
                                    "",
                                    cartModelArrayList.get(position).getAvailqty(),
                                    cartModelArrayList.get(position).getVariant(),
                                    cartModelArrayList.get(position).getNote()));

                        }
                        catch (Exception e)
                        {

                        }
                        double sum = 0;
                        for (int i=0;i<cartModelArrayList.size();i++)
                        {
                            sum = sum + Double.parseDouble(cartModelArrayList.get(i).getAmount());
                        }
                        Log.e("ADD_ALL_AMOUNT:", String.valueOf(sum));

                        subtotal.setText("₹ "+String.valueOf(sum));
//                        grandtotal.setText("₹ "+String.valueOf(sum));
//                        toptotal.setText("₹ "+String.valueOf(sum));
                    }
                    else
                    {

                        int availableQTY= Integer.parseInt(cartModelArrayList.get(position).getAvailqty());
                        int SETTEXT_QTY= Integer.parseInt(holder.tv_Qty.getText().toString());


                        if(SETTEXT_QTY==availableQTY)
                        {
                            Snackbar snackbar = Snackbar.make(parentlo, "Opps, we have only this much quantity left now....!!!", Snackbar.LENGTH_SHORT);
                            View snackBarView = snackbar.getView();
                            snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                            snackbar.show();
                        }
                        else
                        {
                            int getQTY= Integer.parseInt(holder.tv_Qty.getText().toString());
                            getQTY++;
                            holder.tv_Qty.setText(String.valueOf(getQTY));
                            int qty=getQTY;
                            double qtyprice= Double.parseDouble(holder.cart_p_price.getText().toString());
                            double ans=qty*qtyprice;
//                    Log.e("Add", String.valueOf(ans));
                            holder.tvqtyprice.setText(Double.toString(ans));

                            cartModelArrayList.get(position).setAmount(String.valueOf(ans));

                            try
                            {
                                Main_impl main_impl=new Main_impl(context);
                                main_impl.update(new mycart(cartModelArrayList.get(position).getId(),
                                        cartModelArrayList.get(position).getProduct_id(),
                                        cartModelArrayList.get(position).getProduct_name(),
                                        cartModelArrayList.get(position).getPrice(),
                                        cartModelArrayList.get(position).getDescription(),
                                        cartModelArrayList.get(position).getSpecification(),
                                        cartModelArrayList.get(position).getDiscount(),
                                        cartModelArrayList.get(position).getAvailability(),
                                        cartModelArrayList.get(position).getImg(),
                                        String.valueOf(qty),
                                        String.valueOf(ans),
                                        "",
                                        cartModelArrayList.get(position).getAvailqty(),
                                        cartModelArrayList.get(position).getVariant(),
                                        cartModelArrayList.get(position).getNote()));

                            }
                            catch (Exception e)
                            {

                            }


                            double sum = 0;
                            for (int i=0;i<cartModelArrayList.size();i++)
                            {
                                sum = sum + Double.parseDouble(cartModelArrayList.get(i).getAmount());
                            }
                            Log.e("ADD_ALL_AMOUNT:", String.valueOf(sum));

                            subtotal.setText("₹ "+String.valueOf(sum));
//                            grandtotal.setText("₹ "+String.valueOf(sum));
//                            toptotal.setText("₹ "+String.valueOf(sum));
                        }
                    }
                }
            });


//            holder.tv_cart_addqty.setOnClickListener(new View.OnClickListener() //ADD
//            {
//                @Override
//                public void onClick(View v)
//                {
//                    int getQTY= Integer.parseInt(holder.tv_Qty.getText().toString());
//                    getQTY++;
//                    holder.tv_Qty.setText(String.valueOf(getQTY));
//                    int qty=getQTY;
//                    double qtyprice= Double.parseDouble(holder.cart_p_price.getText().toString());
//                    double ans=qty*qtyprice;
//
//                    holder.tvqtyprice.setText(Double.toString(ans));
//
//                    cartModelArrayList.get(position).setAmount(String.valueOf(ans));
//
//                    Main_impl main_impl=new Main_impl(context);
//                    main_impl.update(new mycart(cartModelArrayList.get(position).getId(),
//                            cartModelArrayList.get(position).getProduct_id(),
//                            cartModelArrayList.get(position).getProduct_name(),
//                            cartModelArrayList.get(position).getPrice(),
//                            cartModelArrayList.get(position).getDescription(),
//                            cartModelArrayList.get(position).getSpecification(),
//                            cartModelArrayList.get(position).getDiscount(),
//                            cartModelArrayList.get(position).getAvailability(),
//                            cartModelArrayList.get(position).getImg(),
//                            String.valueOf(qty),
//                            String.valueOf(ans),
//                            "",
//                            cartModelArrayList.get(position).getAvailqty(),
//                            cartModelArrayList.get(position).getVariant(),
//                            cartModelArrayList.get(position).getNote()));
//
//
//                    double sum = 0;
//                    for (int i=0;i<cartModelArrayList.size();i++)
//                    {
//                        sum = sum + Double.parseDouble(cartModelArrayList.get(i).getAmount());
//                    }
//                    Log.e("ADD_ALL_AMOUNT:", String.valueOf(sum));
//
//                    subtotal.setText("₹ "+String.valueOf(sum));
//                }
//            });

            //SUB CODE
            holder.tv_cart_subqty.setOnClickListener(new View.OnClickListener() //SUB
            {
                @Override
                public void onClick(View v) {
                    int qty;
                    double ans;

                    int getQTY= Integer.parseInt(holder.tv_Qty.getText().toString());
                    getQTY--;
                    if(getQTY<1)
                    {
                        holder.tv_Qty.setText("1");
                        qty=1;
                        double qtyprice=Double.parseDouble(holder.cart_p_price.getText().toString());;
                        ans=qty*qtyprice;
//                        Log.e("Sub", String.valueOf(ans));
                        holder.tvqtyprice.setText(Double.toString(ans));
                    }
                    else
                    {
                        holder.tv_Qty.setText(String.valueOf(getQTY));
                        qty=getQTY;
                        double qtyprice= Double.parseDouble(holder.cart_p_price.getText().toString());
                        ans=qty*qtyprice;
//                        Log.e("Sub", String.valueOf(ans));
                        holder.tvqtyprice.setText(Double.toString(ans));
                    }

                    cartModelArrayList.get(position).setAmount(String.valueOf(ans));


                    Main_impl main_impl=new Main_impl(context);
                    main_impl.update(new mycart(cartModelArrayList.get(position).getId(),
                            cartModelArrayList.get(position).getProduct_id(),
                            cartModelArrayList.get(position).getProduct_name(),
                            cartModelArrayList.get(position).getPrice(),
                            cartModelArrayList.get(position).getDescription(),
                            cartModelArrayList.get(position).getSpecification(),
                            cartModelArrayList.get(position).getDiscount(),
                            cartModelArrayList.get(position).getAvailability(),
                            cartModelArrayList.get(position).getImg(),
                            String.valueOf(qty),
                            String.valueOf(ans),
                            "",
                            cartModelArrayList.get(position).getAvailqty(),
                            cartModelArrayList.get(position).getVariant(),
                            cartModelArrayList.get(position).getNote()));

                    double sum = 0;
                    for (int i=0;i<cartModelArrayList.size();i++)
                    {
                        sum = sum + Double.parseDouble(cartModelArrayList.get(i).getAmount());
                    }
                    Log.e("SUB_ALL_AMOUNT:", String.valueOf(sum));

                    subtotal.setText("₹ "+String.valueOf(sum));
                }
            });

            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    if (!isLongClick)
                    {
                        Intent intent=new Intent(Bag.this,Product_Detail.class);
                        intent.putExtra("prodid",cartModelArrayList.get(position).getProduct_id());
                        intent.putExtra("getnote",cartModelArrayList.get(position).getNote());
                        PreferenceManager_ASOS.setcamefrom("yesbag");
                        startActivity(intent);
                        finish();
                    }
                }
            });

            //DELETE FROM BAG CODE
            holder.deletewishlist.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    String title="Remove product";
                    String message="Are you sure you want to remove this product?";

                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Bag.this);

                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

                    // Initialize a new spannable string builder instance
                    SpannableStringBuilder ssBuilder = new SpannableStringBuilder(title);

                    // Apply the text color span
                    ssBuilder.setSpan(
                            foregroundColorSpan,
                            0,
                            title.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    );

                    dialogBuilder.setTitle(ssBuilder);
                    dialogBuilder.setMessage(message);
                    dialogBuilder.setIcon(R.drawable.delete_bin);


                    dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();

                            int getproduct_id= cartModelArrayList.get(position).getId();
                            Main_impl main_impl=new Main_impl(context);
                            main_impl.delete_record(getproduct_id);

                            Snackbar snackbar = Snackbar.make(parentlo, "Product removed successfully from Bag....!!!", Snackbar.LENGTH_SHORT);
                            View snackBarView = snackbar.getView();
                            snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                            snackbar.show();

                            cartModelArrayList.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, cartModelArrayList.size());

                            double sum = 0;
                            for (int i=0;i<cartModelArrayList.size();i++)
                            {
                                sum = sum + Double.parseDouble(cartModelArrayList.get(i).getAmount());
                            }
                            Log.e("DELETE_ALL_AMOUNT:", String.valueOf(sum));

                            subtotal.setText("₹ "+String.valueOf(sum));


                            if(cartModelArrayList.size()<2)
                            {
                                totalitem.setText("( "+listcart.size()+" item"+" )");
                            }
                            else
                            {
                                totalitem.setText("( "+listcart.size()+" items"+" )");
                            }

                            //IF CART IS EMPTY THEN SAD LAYOUT
                            if(cartModelArrayList.size()==0)
                            {
                                main_lo.setVisibility(View.GONE);
                                empty_bag_lo.setVisibility(View.VISIBLE);
                                empty_lo_add_addr_bt.setVisibility(View.GONE);

                            }
                        }
                    });
                    dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblue));
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.mblue));
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.white));

                }
            });

        }

        @Override
        public int getItemCount()
        {
            return cartModelArrayList.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            ImageView cart_p_img;
            private TextView cart_pname,cart_p_price,tvqtyprice,tv_Qty,tv_cart_addqty,tv_cart_subqty;
            private ItemClickListener clickListener;
            LinearLayout deletewishlist;

            public ViewHolder(View itemView)
            {
                super(itemView);
                cart_p_img = (ImageView) itemView.findViewById(R.id.cart_p_imgid);
                cart_pname = (TextView) itemView.findViewById(R.id.cart_pnameid);
                cart_p_price = (TextView) itemView.findViewById(R.id.cart_p_priceid);
                tvqtyprice = (TextView) itemView.findViewById(R.id.tvqtypriceid);
                tv_Qty = (TextView) itemView.findViewById(R.id.tv_Qtyid);
                tv_cart_addqty = (TextView) itemView.findViewById(R.id.tv_cart_addqtyid);
                tv_cart_subqty = (TextView) itemView.findViewById(R.id.tv_cart_subqtyid);
                deletewishlist = (LinearLayout) itemView.findViewById(R.id.deletecartid);

                itemView.setOnClickListener(this);

            }


            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view)
            {
                clickListener.onClick(view,getAdapterPosition(), false);
            }
        }

    }

    public void CHECK_LOGIN()
    {
        String check_user=PreferenceManager_ASOS.GetUserID();

        if(check_user.equals(""))
        {
            Intent intent=new Intent(Bag.this,Enter_OTP.class);
//            intent.putExtra("from_intent","Bag");
            startActivity(intent);
//            finish();
        }
        else
        {
            Intent intent=new Intent(Bag.this,CheckOut.class);
            PreferenceManager_ASOS.setcamefrom("CART");//--
//            intent.putExtra("from_intent","CART");
            startActivity(intent);
            finish();
        }
    }

    public void ADDRESS_IF_AVAILABLE_GETDATA()
    {
       try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "user_address_process");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            object.put("action", "get_user_address");

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(Bag.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading your bag products");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("ADDR_IF_AVAILABLE_res", "BLANK DATA");

                        chechoutbt.setVisibility(View.GONE);
                        checkaddrbt.setVisibility(View.VISIBLE);
                        checkaddrbt.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                {
                                    if (checkSelfPermission(
                                            android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                            && getApplicationContext().checkSelfPermission(
                                            android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                                    {
                                        requestPermissions(
                                                new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                                                        Manifest.permission.ACCESS_FINE_LOCATION},
                                                MY_PERMISSION_LOCATION);
                                    }
                                    else
                                    {
                                        Intent intent=new Intent(Bag.this,Add_New_Addr.class);
                                        PreferenceManager_ASOS.setcamefrom("BAG");//--
//                                PreferenceManager_ASOS.setcamefrom("checking_addr_yes");
                                        startActivity(intent);
                                    }
                                }


                            }
                        });

                    }
                    else
                    {
                        Log.e("ADDR_IF_AVAILABLE_res", response);

                        chechoutbt.setVisibility(View.VISIBLE);
                        checkaddrbt.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("ADDR_IF_AVAILABLE_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults)
    {
        if (requestCode == MY_PERMISSION_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            final ProgressDialog progress = new ProgressDialog(Bag.this);
            progress.setTitle("Loading");
            progress.setMessage("Please wait...");
            progress.show();

            Thread timer = new Thread()
            {
                public void run()
                {
                    try
                    {
                        sleep(3000);
                        progress.cancel();
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        //  gps functionality
                        Intent intent=new Intent(Bag.this,Add_New_Addr.class);
                        PreferenceManager_ASOS.setcamefrom("BAG");//--
//                                PreferenceManager_ASOS.setcamefrom("checking_addr_yes");
                        startActivity(intent);
                    }

                }

            };
            timer.start();

        }
        else
        {
            Toast.makeText(Bag.this, "GPS permission required",
                    Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    protected void onResume()
    {
        super.onResume();

        BINDING();

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (networkReceiver != null)
        {
            unregisterReceiver(networkReceiver);
        }
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
