package com.example.searchus.asos.MODELS;

import java.util.ArrayList;

public class GETSET_PRODLIST
{

    String product_id;
    String product_name;
    String product_price,final_price,note;
    boolean favourite_status;
    ArrayList<String>img_path;
    ArrayList<String>variant_img;
    ArrayList<Pop_PROD_Final_Varient_MODEL> varientArray;

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getFinal_price() {
        return final_price;
    }

    public void setFinal_price(String final_price) {
        this.final_price = final_price;
    }

    public boolean isFavourite_status() {
        return favourite_status;
    }

    public void setFavourite_status(boolean favourite_status) {
        this.favourite_status = favourite_status;
    }

    public ArrayList<String> getImg_path() {
        return img_path;
    }

    public void setImg_path(ArrayList<String> img_path) {
        this.img_path = img_path;
    }

    public ArrayList<String> getVariant_img() {
        return variant_img;
    }

    public void setVariant_img(ArrayList<String> variant_img) {
        this.variant_img = variant_img;
    }

    public ArrayList<Pop_PROD_Final_Varient_MODEL> getVarientArray() {
        return varientArray;
    }

    public void setVarientArray(ArrayList<Pop_PROD_Final_Varient_MODEL> varientArray) {
        this.varientArray = varientArray;
    }
}
