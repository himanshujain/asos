package com.example.searchus.asos;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;


public class Splash_Screen extends AppCompatActivity {

    ImageView imageView;
    TextView tv_businessname;
    PreferenceManager_ASOS PreferenceManager_ASOS;

    String version="0";
    int update_type=0;
    String currentdate;

    boolean emergency_closer_status;
    String emergency_closer_notes;

    ArrayList<String> ArrayList_Keyword = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_splash__screen);
        PreferenceManager_ASOS = new PreferenceManager_ASOS(getApplicationContext());
        imageView = findViewById(R.id.ivsplashid);
//        Picasso.with(getApplicationContext())
//                .load(PreferenceManager_ASOS.GetBusiness_Logo())
//                .into(imageView);

        tv_businessname = findViewById(R.id.tv_businessnameid);
//        tv_businessname.setText(PreferenceManager_ASOS.GetBusiness_Name());

//        {
//            public void run()
//            {
//                try
//                {
//                    sleep(800);
//                }
//                catch (InterruptedException e)
//                {
//                    e.printStackTrace();
//                }
//                finally
//                {
//                    Intent i = new Intent(Splash_Screen.this, Home.class);
//                    startActivity(i);
//                    finish();
//                }
//
//            }
//
//        };
//        timer.start();


        //CHECK LOGIN CODE
//        if(!PreferenceManager_ASOS.GetUserID().equals(""))
//        {
//            Thread timer = new Thread()
//            {
//                public void run()
//                {
//                    try
//                    {
//                        sleep(800);
//                    } catch (InterruptedException e)
//                    {
//                        e.printStackTrace();
//                    }
//                    finally
//                    {
//                        Intent i = new Intent(Splash_Screen.this, Home.class);
//                        startActivity(i);
//                        finish();
//                    }
//
//                }
//
//            };
//            timer.start();
//        }
//        else
//        {
//            Thread timer = new Thread()
//            {
//                public void run()
//                {
//                    try
//                    {
//                        sleep(800);
//                    } catch (InterruptedException e)
//                    {
//                        e.printStackTrace();
//                    }
//                    finally
//                    {
//                        Intent i = new Intent(Splash_Screen.this, Enter_OTP.class);
//                        startActivity(i);
//                        finish();
//                    }
//
//                }
//
//            };
//            timer.start();
//        }

        GET_COMBINED_API();
        currentdate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
    }


    public void GET_COMBINED_API()
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            ArrayList<String>arrayList=new ArrayList<>();

            JSONObject object_1=new JSONObject() ;
            JSONObject object_2=new JSONObject() ;
            object_1.put("options", object_2);

            object_2.put("action", "status");

            JSONObject object_3=new JSONObject() ;
            object_2.put("input", object_3);
            arrayList.add("quanity_status");
            arrayList.add("app_version");
            arrayList.add("emergency_closer");
            arrayList.add("auto_search_keyword");
            arrayList.add("shipping_type");
            arrayList.add("emergency_note");
            arrayList.add("api_settings");

            JSONArray jsonArray=new JSONArray(arrayList);

            object_3.put("status_list", jsonArray);

            final String requestBody = object_1.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.COMBINED_API_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {

                    String getresponce = response.replace("\"", "");


                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("COMBINED_RES ", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("COMBINED_RES", response);

//                        Toast.makeText(getApplicationContext(),"COMBINED_RES_Done",Toast.LENGTH_SHORT).show();

                        try
                        {
                            JSONObject main_object=new JSONObject(String.valueOf(response));
                            String code=main_object.getString("code");
                            String message=main_object.getString("message");

                            JSONObject data_object=main_object.getJSONObject("data");

                            Object json1 = data_object.get("quanity");//QTY
                            if (json1 instanceof Boolean)
                            {
                                boolean quanity_status_main_boolean=data_object.getBoolean("quanity");
                                PreferenceManager_ASOS.setQTY_Status(quanity_status_main_boolean);
                            }
                            else
                            {
                                JSONObject quanity_object=data_object.getJSONObject("quanity");
                                boolean quanity_status=quanity_object.getBoolean("status");
                                PreferenceManager_ASOS.setQTY_Status(quanity_status);
                            }

                            Object json2 = data_object.get("app_version");//APP_VERSION
                            if (json2 instanceof Boolean)
                            {
                                boolean app_version_status=data_object.getBoolean("app_version");
                                PreferenceManager_ASOS.setapp_version_status(app_version_status);

                                Object json3 = data_object.get("emergency_closer");//CLOSER
                                if (json3 instanceof Boolean)
                                {
                                    boolean emergency_closer_main_status=data_object.getBoolean("emergency_closer");
                                    PreferenceManager_ASOS.setemergency_closer_status_main(emergency_closer_main_status);

                                    Intent i = new Intent(Splash_Screen.this, Home.class);
                                    startActivity(i);
                                    finish();
                                }
                                else
                                {
                                    JSONObject emergency_closer_object=data_object.getJSONObject("emergency_closer");
                                    emergency_closer_status=emergency_closer_object.getBoolean("status");
                                    PreferenceManager_ASOS.setemergency_closer_status(emergency_closer_status);

                                    emergency_closer_notes=emergency_closer_object.getString("notes");
                                    PreferenceManager_ASOS.setemergency_closer_note(emergency_closer_notes);

                                    Intent i = new Intent(Splash_Screen.this, Emergency_Closer.class);
                                    i.putExtra("emergency_closer_notes",emergency_closer_notes);
                                    startActivity(i);
                                }
                            }
                            else
                            {
                                JSONObject app_version_object=data_object.getJSONObject("app_version");
                                version=app_version_object.getString("version");
                                update_type=app_version_object.getInt("update_type");
                                PreferenceManager_ASOS.setversion(version);
                                PreferenceManager_ASOS.setupdate_type(update_type);

                                Object json3 = data_object.get("emergency_closer");//CLOSER
                                if (json3 instanceof Boolean)
                                {
                                    boolean emergency_closer_main_status=data_object.getBoolean("emergency_closer");
                                    PreferenceManager_ASOS.setemergency_closer_status_main(emergency_closer_main_status);

                                    UPDATE_LOGIC(version,update_type);
                                }
                                else
                                {
                                    JSONObject emergency_closer_object=data_object.getJSONObject("emergency_closer");
                                    emergency_closer_status=emergency_closer_object.getBoolean("status");
                                    PreferenceManager_ASOS.setemergency_closer_status(emergency_closer_status);

                                    emergency_closer_notes=emergency_closer_object.getString("notes");
                                    PreferenceManager_ASOS.setemergency_closer_note(emergency_closer_notes);

                                    Intent i = new Intent(Splash_Screen.this, Emergency_Closer.class);
                                    i.putExtra("emergency_closer_notes",emergency_closer_notes);
                                    startActivity(i);
                                }
                            }



                            Object json4 = data_object.get("auto_search_keyword");//SEARCHKEYWORD
                            if (json4 instanceof Boolean)
                            {
                                boolean auto_search_keyword_main_status=data_object.getBoolean("auto_search_keyword");
                                PreferenceManager_ASOS.setemergency_closer_status_main(auto_search_keyword_main_status);
                            }
                            else
                            {
                                JSONObject auto_search_keyword_object=data_object.getJSONObject("auto_search_keyword");
                                boolean auto_search_keyword_status=auto_search_keyword_object.getBoolean("status");
                                PreferenceManager_ASOS.setauto_search_keyword_status(auto_search_keyword_status);


                                GET_SEARCH_KEYWORDS_LOGIC();
                            }



                            Object json5 = data_object.get("shipping_type");//SHIPPING TYPE
                            if (json5 instanceof Boolean)
                            {
                                boolean shipping_type_main_status=data_object.getBoolean("shipping_type");
                                PreferenceManager_ASOS.setshipping_type_main_status(shipping_type_main_status);
                            }
                            else
                            {
                                JSONObject shipping_type_object=data_object.getJSONObject("shipping_type");
                                String shipping_method=shipping_type_object.getString("shipping_method");
                                PreferenceManager_ASOS.setshipping_method(shipping_method);
                            }


                            Object json6 = data_object.get("emergency_note");//EMERGENCY NOTE
                            if (json6 instanceof Boolean)
                            {
                                boolean emergency_note_main_status=data_object.getBoolean("emergency_note");
                                PreferenceManager_ASOS.setemergency_note_status(emergency_note_main_status);
                            }
                            else
                            {
                                JSONObject emergency_note_object=data_object.getJSONObject("emergency_note");
                                String emergency_note=emergency_note_object.getString("notes");
                                PreferenceManager_ASOS.setemergency_note(emergency_note);
                            }

                            JSONObject api_settings_object=data_object.getJSONObject("api_settings");
                            String type=api_settings_object.getString("type");
                            PreferenceManager_ASOS.settype(type);
                            String image_path=api_settings_object.getString("image_path");
                            PreferenceManager_ASOS.setimage_path(image_path+"/");
                            String web_path=api_settings_object.getString("web_path");




                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("COMBINED_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy
                    (10000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    private void UPDATE_LOGIC(String version, int update_type)
    {
        int current_versionCode = BuildConfig.VERSION_CODE;
        int new_versionCode = Integer.parseInt(version);

        if (current_versionCode < new_versionCode)
        {
            if (update_type == 0)
            {
                //code for checking only onetime popup in one day
                if (!PreferenceManager_ASOS.getcheckToday_date().equals(currentdate) || PreferenceManager_ASOS.getcheckToday_date().equals(""))
                {
                    PreferenceManager_ASOS.setcheckToday_date(currentdate);

                    POPUP_DIALOGUE_SOFT_UPDATE();
                    Suggestion_GETDATA();
                }
                else
                {
                    Intent i = new Intent(Splash_Screen.this, Home.class);
                    startActivity(i);
                    finish();
                }

                Log.e("P1",PreferenceManager_ASOS.getcheckToday_date());
                Log.e("currentdate1",currentdate);

            } else {
                POPUP_DIALOGUE_HARD_UPDATE();
            }
        } else {
            Intent i = new Intent(Splash_Screen.this, Home.class);
            startActivity(i);
            finish();
        }
    }

    public void POPUP_DIALOGUE_SOFT_UPDATE() {
        String title = "Update Require";
        String message = "Opps, it seems you need to update...!!!";

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Splash_Screen.this);

        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

        // Initialize a new spannable string builder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(title);

        // Apply the text color span
        ssBuilder.setSpan(
                foregroundColorSpan,
                0,
                title.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        dialogBuilder.setTitle(ssBuilder);
        dialogBuilder.setMessage(message);
        dialogBuilder.setIcon(R.drawable.appnewlogo);
        dialogBuilder.setCancelable(true);


        dialogBuilder.setPositiveButton("Update Now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.searchus.Ira_The_Fashion_Store_SA5b4f3e8713080B")));
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                Intent i = new Intent(Splash_Screen.this, Home.class);
                startActivity(i);
                finish();
            }
        });

        dialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Intent i = new Intent(Splash_Screen.this, Home.class);
                startActivity(i);
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblack));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.mgray));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.white));
    }

    public void POPUP_DIALOGUE_HARD_UPDATE() {
        String title = "Update Require";
        String message = "Opps, it seems app got major update...!!!";

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Splash_Screen.this);

        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

        // Initialize a new spannable string builder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(title);

        // Apply the text color span
        ssBuilder.setSpan(
                foregroundColorSpan,
                0,
                title.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        dialogBuilder.setTitle(ssBuilder);
        dialogBuilder.setMessage(message);
        dialogBuilder.setIcon(R.drawable.appnewlogo);
        dialogBuilder.setCancelable(true);


        dialogBuilder.setPositiveButton("Update Now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.searchus.Ira_The_Fashion_Store_SA5b4f3e8713080B")));
            }
        });


        dialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //Exit app code
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });

//        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
//        {
//            @Override
//            public void onClick(DialogInterface dialog, int which)
//            {
//                dialog.dismiss();
//            }
//        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblack));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.myellow));
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.white));
    }

    public void GET_SEARCH_KEYWORDS_LOGIC()
    {
        Log.e("P",PreferenceManager_ASOS.getcheckToday_date());
        Log.e("currentdate",currentdate);

        if (!PreferenceManager_ASOS.getcheckToday_date().equals(currentdate) || PreferenceManager_ASOS.getcheckToday_date().equals(""))
        {
            Suggestion_GETDATA();
        }
    }

    public void Suggestion_GETDATA()
    {

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = "http://api.searchus.in/API/ecommerce/auto_complete";
            JSONObject object = new JSONObject();
            JSONObject jobj = new JSONObject();
            JSONObject jobj2 = new JSONObject();
            jobj2.put("keyword", "ALL");
            jobj.put("action", "product");
            jobj.put("input", jobj2);
            try {
                object.put("options", jobj);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            final String requestBody = object.toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {

                    String getresponce = response.replace("\"", "");

                    if (getresponce.equals("No Data Found")) {
                        Log.e("Searchsuggestionres", "BLANK DATA");
                    } else {
                        Log.e("Searchsuggestionres", response);


                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String message = jsonObject.getString("message");

                            if (message.equals("Done")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    ArrayList_Keyword.add(jsonArray.getString(i));
                                }

                                for (int i = 0; i < ArrayList_Keyword.size(); i++) {
                                    Log.i("Arraylist", ArrayList_Keyword.get(i));
                                }

                                PreferenceManager_ASOS.setkeywords(null);
                                Set<String> foo = new HashSet<String>(ArrayList_Keyword);
                                PreferenceManager_ASOS.setkeywords(foo);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Searchsuggestion_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onPause() {
        //TODO Auto-generated method stub
        super.onPause();
        finish();
    }

}
