package com.example.searchus.asos.Classes;

import android.view.View;

public interface ItemClickListener
{

    void onClick(View view, int position, boolean isLongClick);
}
