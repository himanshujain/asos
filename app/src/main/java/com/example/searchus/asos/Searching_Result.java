package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.MODELS.SearchResult_Varient_MODEL;
import com.example.searchus.asos.MODELS.Search_Result_MODEL;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Searching_Result extends AppCompatActivity
{
    LinearLayout parentlo,empty_lo,main_lo;
    TextView empty_msg;
    Button empty_lo_add_addr_bt;
    EditText search_ed;
    RecyclerView category_wise_prod_models_rv;

    PreferenceManager_ASOS PreferenceManager_ASOS;
    ArrayList<SearchResult_Varient_MODEL> varientArray;
    ArrayList<String> V_imgarray;
    Search_Result_MODEL getset_prodlist;

    ArrayList<Search_Result_MODEL> gather_getset_prodlistArrayList=new ArrayList<>();
    ProgressBar loader;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    int IsFatchingData = 0;
    int offset = 0;
    int limit = 6;
    Product_Listing_Adapter product_listing_adapter;
    boolean cancel_progress=true;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_searching__result);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        TOOLBAR();
        BINDING();

        search_ed.setText("Search result for "+getIntent().getStringExtra("getsearchingword").toString());

        category_wise_prod_models_rv.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        category_wise_prod_models_rv.setLayoutManager(gaggeredGridLayoutManager);
        product_listing_adapter=new Product_Listing_Adapter(getApplicationContext(),gather_getset_prodlistArrayList);
        category_wise_prod_models_rv.setAdapter(product_listing_adapter);

        category_wise_prod_models_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                totalItemCount = gaggeredGridLayoutManager.getItemCount();
                Log.e("totalItemCount", String.valueOf(totalItemCount));

                firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                int lastInScreen = firstVisibleItem + visibleItemCount;
                Log.e("lastInScreen", String.valueOf(lastInScreen));

                if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                {
                    loader.setVisibility(View.VISIBLE);
                    PROD_LISTING_GETDATA();
                }
            }
        });

        PROD_LISTING_GETDATA();
    }

    public void TOOLBAR()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
                finish();
            }
        });
    }

    public void BINDING()
    {
        parentlo= (LinearLayout) findViewById(R.id.parentlo_id);
        loader= (ProgressBar) findViewById(R.id.loaderid);
        category_wise_prod_models_rv = findViewById(R.id.prodlist_recycler_view);
        search_ed = findViewById(R.id.search_ed_id);
        empty_lo=findViewById(R.id.empty_loid);
        main_lo=findViewById(R.id.main_lo_id);
        empty_msg=findViewById(R.id.empty_msg_id);
        empty_lo_add_addr_bt=findViewById(R.id.empty_lo_add_addr_btid);
    }

    public void PROD_LISTING_GETDATA()
    {
        IsFatchingData = 1;

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "search_products");
            object.put("keyword",getIntent().getStringExtra("getsearchingword").toString());
            JSONObject jobj = new JSONObject();//options
            jobj.put("row_offset", offset);
            jobj.put("row_count", limit);
            jobj.put("user_id", PreferenceManager_ASOS.GetUserID());
            try
            {
                object.put("options", jobj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            final String requestBody = object.toString();


            if(cancel_progress)
            {
                progressDialog=new ProgressDialog(Searching_Result.this,R.style.ProgressDialogStyle);
                progressDialog.setTitle("Loading your searching result");
                progressDialog.setMessage("Please wait ...");
                progressDialog.show();
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    loader.setVisibility(View.GONE);
                    ArrayList<Search_Result_MODEL> getset_prodlistArrayList=new ArrayList<>();


                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("Search_result_res", "BLANK DATA");

                        if(offset==0)
                        {
                            main_lo.setVisibility(View.GONE);
                            empty_lo.setVisibility(View.VISIBLE);
                            empty_lo_add_addr_bt.setVisibility(View.GONE);
                            empty_msg.setText("No result found...!!!");
                        }
                        else
                        {
                            empty_lo.setVisibility(View.GONE);
                            main_lo.setVisibility(View.VISIBLE);
                        }

                    }
                    else
                    {
                        Log.e("Search_result_res", response);



                        try
                        {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));

                            JSONArray prodlistarray=jsonObject.getJSONArray("data");

                            for(int k=0;k<prodlistarray.length();k++)
                            {
                                JSONObject jsonObject1 = prodlistarray.getJSONObject(k); //loop obj

                                String pid = jsonObject1.getString("product_id");
                                String product_spec = jsonObject1.getString("product_spec");
                                String product_name = jsonObject1.getString("product_name");
                                String product_price = jsonObject1.getString("product_price");
                                String product_desc = jsonObject1.getString("product_desc");
                                String quantity = jsonObject1.getString("quantity");
                                String final_price = jsonObject1.getString("Final_Price");
                                ArrayList<String> Img = new ArrayList<>();

                                JSONArray jsonArray = jsonObject1.getJSONArray("img");
                                if(jsonArray!=null)
                                {
                                    for (int i = 0; i < jsonArray.length(); i++)
                                    {
                                        Img.add(jsonArray.getString(i));
                                    }
                                }
                                Log.e("Img", String.valueOf(Img));

                                String avalibility = jsonObject1.getString("avalibility");
                                String favourite_status = jsonObject1.getString("Product_Favourite_Status");
                                String purchase_price = jsonObject1.getString("purchase_price");
                                String product_code = jsonObject1.getString("product_code");
                                String notes = jsonObject1.getString("notes");
                                String extra_1 = jsonObject1.getString("extra_1");
                                String extra_2 = jsonObject1.getString("extra_2");


                                varientArray=new ArrayList<>();
                                JSONArray jsonArray2 = jsonObject1.getJSONArray("variant");
                                for (int i = 0; i < jsonArray2.length(); i++)
                                {
                                    JSONObject jsonObject3 = jsonArray2.getJSONObject(i);
                                    String V_variant_name=jsonObject3.getString("name");
                                    String V_purchase_price=jsonObject3.getString("purchase_price");
                                    String V_mrp=jsonObject3.getString("product_price");
                                    String V_final_price=jsonObject3.getString("Final_Price");
                                    String V_quantity=jsonObject3.getString("Quantity");

//                                        Object json = jsonObject3.get("images");
//                                        JSONArray imagevar = null;
//                                        V_imgarray = new ArrayList<String>();
//
//                                        if (json instanceof JSONArray)
//                                        {
//                                            imagevar = jsonObject3.getJSONArray("images");
//                                            for (int j = 0; j < imagevar.length(); j++)
//                                            {
//                                                V_imgarray.add(String.valueOf(imagevar.get(j)));
//                                            }
//
//                                        }
//                                        else
//                                        {
//                                        }

                                    SearchResult_Varient_MODEL searchResult_varient_model = new
                                            SearchResult_Varient_MODEL(V_variant_name,V_purchase_price, V_mrp, V_final_price,V_quantity);
                                    varientArray.add(searchResult_varient_model);
                                }


                                getset_prodlist=new Search_Result_MODEL();
                                getset_prodlist.setProduct_id(pid);
                                getset_prodlist.setProduct_name(product_name);
                                getset_prodlist.setProduct_price(product_price);
                                getset_prodlist.setFinal_price(final_price);
                                getset_prodlist.setFavourite_status(favourite_status);
                                getset_prodlist.setNote(notes);
                                getset_prodlist.setImg_path(Img);
                                getset_prodlist.setVariant_img(V_imgarray);
                                getset_prodlist.setVarientArray(varientArray);

                                getset_prodlistArrayList.add(getset_prodlist);
                            }

                            Log.e("Search_result_res_size", String.valueOf(getset_prodlistArrayList.size()));

//                            category_wise_prod_models_rv.setAdapter(new Product_Listing_Adapter(getApplicationContext(),getset_prodlistArrayList));

                            //Loader code
                            int oldcount = gather_getset_prodlistArrayList.size();
                            for (int i = 0; i < getset_prodlistArrayList.size(); i++)
                            {
                                offset++;
                                product_listing_adapter.add(getset_prodlistArrayList.get(i));
                            }


                            product_listing_adapter.notifyDataSetChanged();
                            IsFatchingData = 0;

                            if (getset_prodlistArrayList.size() % limit != 0 || oldcount == gather_getset_prodlistArrayList.size())
                            {
                                loader.setVisibility(View.GONE);
                            }

                            cancel_progress=false;
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Log.e("e", String.valueOf(e));
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Search result  error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public class Product_Listing_Adapter extends RecyclerView.Adapter<Product_Listing_Adapter.ViewHolder>
    {
        ArrayList<Search_Result_MODEL> Product_Listing;
        Context context;
        private PreferenceManager_ASOS PreferenceManager_ASOS;
        String prodid;
        boolean status;
        String STATUS;

        public Product_Listing_Adapter(Context context, ArrayList<Search_Result_MODEL> Product_Listing)
        {
            super();
            this.context = context;
            this.Product_Listing = Product_Listing;
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
        }

        //Loader code
        public void add(Search_Result_MODEL country) {
            this.Product_Listing.add(country);
        }


        @Override
        public Product_Listing_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_product_list_lvitem, parent, false);
            Product_Listing_Adapter.ViewHolder viewHolder = new Product_Listing_Adapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final Product_Listing_Adapter.ViewHolder holder, final int position)
        {

            holder.prodname.setText(Product_Listing.get(position).getProduct_name());

            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH + "" + Product_Listing.get(position).getImg_path().get(0).toString())
                    .into(holder.imgThumbnail);

//           // img code
//            if(Product_Listing.get(position).getVarientArray().isEmpty())
//            {
//                if (Product_Listing.get(position).getImg_path().size()>0)
//                {
//                    Picasso.with(context)
//                            .load(PaperCart_CoNNectioN.IMGPATH + "" + Product_Listing.get(position).getImg_path().get(0).toString())
//                            .into(holder.imgThumbnail);
//                }
//            }
//            else
//            {
//                if (Product_Listing.get(position).getVariant_img().size()>0)
//                {
//                    Picasso.with(context)
//                            .load(PaperCart_CoNNectioN.IMGPATH + "" + Product_Listing.get(position).getVariant_img().get(0).toString())
//                            .into(holder.imgThumbnail);
//                }
//            }


            if(Product_Listing.get(position).getVarientArray().isEmpty())
            {
                holder.mrp.setText("₹ "+Product_Listing.get(position).getProduct_price());
                holder.finalprice.setText("₹ "+Product_Listing.get(position).getFinal_price());
            }
            else
            {
                holder.mrp.setText("₹ "+Product_Listing.get(position).getVarientArray().get(0).getMrp());
                holder.finalprice.setText("₹ "+Product_Listing.get(position).getVarientArray().get(0).getMrp());
            }


            //disc calculating starts
            if(Product_Listing.get(position).getVarientArray().isEmpty())
            {
                if(Product_Listing.get(position).getProduct_price().equals("")||Product_Listing.get(position).getProduct_price().equals("0"))
                {
                    holder.mrp.setVisibility(View.GONE);
                    holder.disc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    holder.mrp.setVisibility(View.VISIBLE);
                    holder.disc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(Product_Listing.get(position).getFinal_price());
                    double mrp_rate= Double.parseDouble(Product_Listing.get(position).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    holder.disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                holder.finalprice.setText("₹ "+Product_Listing.get(position).getFinal_price());
            }
            else
            {

                if(Product_Listing.get(position).getVarientArray().get(0).getMrp().equals("")||Product_Listing.get(position).getVarientArray().get(0).getMrp().equals("0"))
                {
                    holder.mrp.setVisibility(View.GONE);
                    holder.disc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    holder.mrp.setVisibility(View.VISIBLE);
                    holder.disc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(Product_Listing.get(position).getVarientArray().get(0).getFinal_price());
                    double mrp_rate= Double.parseDouble(Product_Listing.get(position).getVarientArray().get(0).getMrp());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    holder.disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                holder.finalprice.setText("₹ "+Product_Listing.get(position).getVarientArray().get(0).getFinal_price());
            }


            //fav code
            final String check_user=PreferenceManager_ASOS.GetUserID();
            Log.e("check_user",check_user);
            if(check_user.equals(""))
            {
                holder.ivfav.setBackgroundResource(R.drawable.unfilllike);

            }
            else
            {
                //CHNGING ITEM SAVED BG

                if(Product_Listing.get(position).getFavourite_status().equals("None"))
                {
                    holder.ivfav.setBackgroundResource(R.drawable.unfilllike);
                }
                else
                {
                    holder.ivfav.setBackgroundResource(R.drawable.filllike);
                }
            }


            //fav onclick code
            holder.ivfav.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
//                    boolean abc=recentView_array.get(i).getProduct_Favourite_Status();

                    if(check_user.equals(""))
                    {
                        Intent intent=new Intent(Searching_Result.this,Enter_OTP.class);
//                        intent.putExtra("from_intent","Searching_Result");
                        startActivity(intent);
                    }
                    else
                    {
                        if(Product_Listing.get(position).getFavourite_status().equals("None"))
                        {
                            prodid= String.valueOf(Product_Listing.get(position).getProduct_id());
                            STATUS="FAVOURITE";
                            Product_Listing.get(position).setFavourite_status("FAVOURITE");
                            ADD_FAVOURITE(holder.ivfav,prodid);
                        }
                        else
                        {
                            prodid= String.valueOf(Product_Listing.get(position).getProduct_id());
                            STATUS="UNFAVOURITE";
                            Product_Listing.get(position).setFavourite_status("None");
                            ADD_FAVOURITE(holder.ivfav,prodid);

                        }
                    }


                }

            });



            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        prodid= String.valueOf(Product_Listing.get(position).getProduct_id());
                        String getnote= String.valueOf(Product_Listing.get(position).getNote());
                        Intent intent=new Intent(Searching_Result.this,Product_Detail.class);
                        intent.putExtra("prodid",prodid);
                        intent.putExtra("getnote",getnote);
                        startActivity(intent);
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return Product_Listing.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            public ImageView imgThumbnail,ivfav;
            public TextView prodname,disc,mrp,finalprice;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                prodname = (TextView) itemView.findViewById(R.id.prodnameid);
                disc = (TextView) itemView.findViewById(R.id.discid);
                mrp = (TextView) itemView.findViewById(R.id.mrpid);
                finalprice = (TextView) itemView.findViewById(R.id.finalpriceid);
                ivfav = (ImageView) itemView.findViewById(R.id.ivfavid);
                itemView.setOnClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

        }

        public void ADD_FAVOURITE(final ImageView ivfav,String p_id)
        {
            try
            {
                RequestQueue requestQueue = Volley.newRequestQueue(context);

                JSONObject object=new JSONObject() ;
                object.put("type","business_favourite_process");
                object.put("user_id",PreferenceManager_ASOS.GetUserID());
                object.put("action","add_product");
                JSONObject jsonObject2=new JSONObject();
                jsonObject2.put("product_id",p_id);
                jsonObject2.put("status",STATUS);

                object.put("options",jsonObject2);


                final String requestBody = object.toString();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {

                        String getresponce = response.replace("\"", "");

                        if(getresponce.equals("No Data Found"))
                        {
                            Log.e("FAV res", "BLANK DATA");
                        }
                        else
                        {
                            Log.e("FAV res", response);

                            try
                            {
                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
                                String status = jsonObject.getString("status");

                                if(status.equals("UNFAVOURITE"))
                                {
                                    ivfav.setBackgroundResource(R.drawable.unfilllike);

                                    Snackbar snackbar = Snackbar
                                            .make(parentlo, "Product removed from saved items....!!!", Snackbar.LENGTH_SHORT);
                                    View snackBarView = snackbar.getView();
                                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                    snackbar.show();
                                }
                                else
                                {
                                    ivfav.setBackgroundResource(R.drawable.filllike);

                                    Snackbar snackbar = Snackbar
                                            .make(parentlo, "Product successfully saved....!!!", Snackbar.LENGTH_SHORT);
                                    View snackBarView = snackbar.getView();
                                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                    snackbar.show();
                                }

                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                                Log.e("e", String.valueOf(e));

                            }

                        }
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("FAV error", error.toString());
                    }
                })

                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type","application/json");
                        params.put("authentication", PaperCart_HEADER.KEY);

                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError
                    {
                        try
                        {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        }
                        catch (UnsupportedEncodingException uee)
                        {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                requestQueue.add(stringRequest);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }

    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }


}
