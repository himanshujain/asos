package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Classes.No_Internet;
import com.example.searchus.asos.MODELS.LAYER_3_MODEL;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Category_Layer_3 extends AppCompatActivity
{
    RecyclerView SUB_Category_L3_RecyclerView;
    TextView tvtoolbarcatname,note;
    Toolbar toolbar;

    LinearLayout nonet_lo;
    ScrollView mainlo;

    ArrayList<LAYER_3_MODEL> Layer_3_array=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_category__layer_3);

        //Calling Checking Internet connection function
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        nonet_lo=findViewById(R.id.nonet_lo_id);
        mainlo=findViewById(R.id.mainlo_id);

        tvtoolbarcatname=findViewById(R.id.tvtoolbarcatnameid);
        tvtoolbarcatname.setText( getIntent().getStringExtra("subcatname").toString().trim());

        note=findViewById(R.id.noteid);
        String getnote=getIntent().getStringExtra("getnote").toString();
        Log.e("getnote", getnote);
        if(getnote.equals(""))
        {
            note.setVisibility(View.GONE);
        }
        else
        {
            note.setVisibility(View.VISIBLE);
            note.setText(getnote);
        }

        LAYER_3__GETDATA();
    }

    // Checking Internet connection function
    public class NetworkChangeReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(final Context context, final Intent intent)
        {
            if(No_Internet.getInstance(getApplicationContext()).isNetworkAvailable(context))
            {
//                Toast.makeText(getApplicationContext(),"YES",Toast.LENGTH_SHORT).show();
                toolbar.setVisibility(View.VISIBLE);
                nonet_lo.setVisibility(View.GONE);
                mainlo.setVisibility(View.VISIBLE);


                LAYER_3__GETDATA();

            }
            else
            {
//                Toast.makeText(getApplicationContext(),"NO",Toast.LENGTH_SHORT).show();
                toolbar.setVisibility(View.GONE);
                nonet_lo.setVisibility(View.VISIBLE);
                mainlo.setVisibility(View.GONE);

            }

        }


    }

    private void LAYER_3__GETDATA()
    {

        SUB_Category_L3_RecyclerView = (RecyclerView) findViewById(R.id.subcategory_layer_3_recycler_view);
        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        SUB_Category_L3_RecyclerView.setLayoutManager(trending_LayoutManager);
        SUB_Category_L3_RecyclerView.setNestedScrollingEnabled(false);


        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "get_product_category");
            object.put("category", "levelwise");
            JSONObject object1=new JSONObject() ;
            object.put("options", object1);
            object1.put("level", "3");
            object1.put("category_id",  getIntent().getStringExtra("subcatid").toString().trim());
            object1.put("row_offset", "0");
            object1.put("row_count", "10");

            final String requestBody = object.toString();


            final ProgressDialog progressDialog=new ProgressDialog(Category_Layer_3.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();
                    Layer_3_array.clear();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("LAYER_3 res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("LAYER_3 res", response);

                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONArray jsonArray=jsonObject.getJSONArray("data");
                            Gson gson = new Gson();
                            for (int j = 0; j < jsonArray.length(); j++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                Layer_3_array.add(gson.fromJson(String.valueOf(jsonObject1), LAYER_3_MODEL.class));
                            }

                            SUB_Category_L3_RecyclerView.setAdapter(new SUB_Category_L3_ADP(getApplicationContext(),Layer_3_array));

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("LAYER_3 error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public class SUB_Category_L3_ADP extends RecyclerView.Adapter<SUB_Category_L3_ADP.ViewHolder>
    {
        ArrayList<LAYER_3_MODEL> Layer_3_array;
        Context context;

        public SUB_Category_L3_ADP(Context context, ArrayList<LAYER_3_MODEL> Layer_3_array)
        {
            super();
            this.context = context;
            this.Layer_3_array = Layer_3_array;
        }

        @Override
        public SUB_Category_L3_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_sub_category_lvitems, parent, false);
            SUB_Category_L3_ADP.ViewHolder viewHolder = new SUB_Category_L3_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(SUB_Category_L3_ADP.ViewHolder holder, int position)
        {
            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH+Layer_3_array.get(position).getImage_path())
                    .into(holder.imgThumbnail);

            holder.Layer_3_name.setText(Layer_3_array.get(position).getCategory_name());

            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        String catid=Layer_3_array.get(position).getId();
                        String layer3catname=Layer_3_array.get(position).getCategory_name();
                        String getnote=Layer_3_array.get(position).getNotes();
                        Intent intent=new Intent(Category_Layer_3.this,Product_Listing.class);
                        intent.putExtra("layer3catname",layer3catname);
                        intent.putExtra("CATEGORY_NAME",layer3catname);
                        intent.putExtra("catid",catid);
                        intent.putExtra("from_intent","from_category");
                        intent.putExtra("getnote",getnote);
                        startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount()
        {
            return Layer_3_array.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            public ImageView imgThumbnail;
            public TextView Layer_3_name;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                Layer_3_name = (TextView) itemView.findViewById(R.id.tvproductnameid);
                itemView.setOnClickListener(this);
            }


            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        //Calling Checking Internet connection function
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
       NetworkChangeReceiver receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

}
