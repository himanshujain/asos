package com.example.searchus.asos.MODELS;



public class CATEGORY_GETSET
{


    private String sliderimg,catid,catimgpath,catname,child,pop_prod_name;
    String link,category_name,note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSliderimgpath() {
        return sliderimg;
    }

    public void setSliderimgpath(String sliderimg) {
        this.sliderimg = sliderimg;
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatimgpath() {
        return catimgpath;
    }

    public void setCatimgpath(String catimgpath) {
        this.catimgpath = catimgpath;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getPop_prod_name() {
        return pop_prod_name;
    }

    public void setPop_prod_name(String pop_prod_name) {
        this.pop_prod_name = pop_prod_name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }


}
