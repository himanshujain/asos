package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Impl.Main_impl;
import com.example.searchus.asos.MODELS.WISHLIST_MODEL;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Saved_Item extends AppCompatActivity
{
    CoordinatorLayout parentlo;
    LinearLayout empty_bag_lo,main_lo;
    TextView empty_msg;
    RecyclerView Wishlist_RecyclerView;
    Button empty_lo_add_addr_bt;

    PreferenceManager_ASOS PreferenceManager_ASOS;

    ArrayList<WISHLIST_MODEL> gather_getset_prodlistArrayList=new ArrayList<>();
    ProgressBar loader;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    int IsFatchingData = 0;
    int offset = 0;
    int limit = 6;
    MyWishlist_ADP product_listing_adapter;
    boolean cancel_progress=true;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_saved_item);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

        TOOLBAR();

        parentlo=findViewById(R.id.parentlo_id);
        empty_bag_lo=findViewById(R.id.empty_bag_loid);
        main_lo=findViewById(R.id.main_lo_id);
        loader= (ProgressBar) findViewById(R.id.loaderid);
        empty_lo_add_addr_bt=findViewById(R.id.empty_lo_add_addr_btid);
        empty_msg=findViewById(R.id.empty_msg_id);

//        tv_bagcount=findViewById(R.id.tv_bagcount_id);
//        floatingActionButton=findViewById(R.id.floatingActionButtonid);
//
//        FLOATING_BT();

//        Cart_RecyclerView = (RecyclerView) findViewById(R.id.cart_recycler_view);
//
//        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
//        trending_LayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        Cart_RecyclerView.setLayoutManager(trending_LayoutManager);
//        Cart_RecyclerView.setNestedScrollingEnabled(false);

        Wishlist_RecyclerView = findViewById(R.id.wishlist_recycler_view);
        Wishlist_RecyclerView.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        Wishlist_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
        product_listing_adapter=new MyWishlist_ADP(getApplicationContext(),gather_getset_prodlistArrayList);
        Wishlist_RecyclerView.setAdapter(product_listing_adapter);

        Wishlist_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                totalItemCount = gaggeredGridLayoutManager.getItemCount();
                Log.e("totalItemCount", String.valueOf(totalItemCount));

                firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                int lastInScreen = firstVisibleItem + visibleItemCount;
                Log.e("lastInScreen", String.valueOf(lastInScreen));

                if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                {
                    loader.setVisibility(View.VISIBLE);
                    FAVOURITE_LISTING_GETDATA();
                }
            }
        });

        FAVOURITE_LISTING_GETDATA();

    }

    public void FAVOURITE_LISTING_GETDATA()
    {
        IsFatchingData = 1;

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            JSONObject object=new JSONObject() ;
            object.put("type", "business_favourite_process");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            object.put("action", "get_list");
            JSONObject object1=new JSONObject() ;
            object.put("options", object1);
            object1.put("row_offset", offset);
            object1.put("row_count", limit);

            final String requestBody = object.toString();

            if(cancel_progress)
            {
                progressDialog=new ProgressDialog(Saved_Item.this,R.style.ProgressDialogStyle);
                progressDialog.setTitle("Loading your saved items");
                progressDialog.setMessage("Please wait ...");
                progressDialog.show();
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    loader.setVisibility(View.GONE);
                    ArrayList<WISHLIST_MODEL> wishlist_modelArrayList=new ArrayList<>();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("WISHLIST res", "BLANK DATA");

                        if(offset==0)
                        {
                            main_lo.setVisibility(View.GONE);
                            empty_bag_lo.setVisibility(View.VISIBLE);
                            empty_lo_add_addr_bt.setVisibility(View.GONE);
                        }
                        else
                        {
                            empty_bag_lo.setVisibility(View.GONE);
                            main_lo.setVisibility(View.VISIBLE);
                            empty_msg.setText("Opps, it seems you haven't saved any product...!!!");
                        }

                    }
                    else
                    {
                        Log.e("WISHLIST res", response);


                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONArray jsonArray=jsonObject.getJSONArray("data");

                            Gson gson = new Gson();
                            for (int j = 0; j < jsonArray.length(); j++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                wishlist_modelArrayList.add(gson.fromJson(String.valueOf(jsonObject1), WISHLIST_MODEL.class));
                            }

                            Log.e("wishlist SIZE", String.valueOf(wishlist_modelArrayList.size()));


                            //Loader code
                            int oldcount = gather_getset_prodlistArrayList.size();
                            for (int i = 0; i < wishlist_modelArrayList.size(); i++)
                            {
                                offset++;
                                product_listing_adapter.add(wishlist_modelArrayList.get(i));
                            }


                            product_listing_adapter.notifyDataSetChanged();
                            IsFatchingData = 0;

                            if (wishlist_modelArrayList.size() % limit != 0 || oldcount == gather_getset_prodlistArrayList.size())
                            {
                                loader.setVisibility(View.GONE);
                            }

                            cancel_progress=false;

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }


                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("WISHLIST error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public class MyWishlist_ADP extends RecyclerView.Adapter<MyWishlist_ADP.ViewHolder>
    {
        ArrayList<WISHLIST_MODEL> wishlist_modelArrayList;
        Context context;

        private RequestQueue rq;
        private PreferenceManager_ASOS PreferenceManager_ASOS;
        String prodid,status;

        public MyWishlist_ADP(Context context, ArrayList<WISHLIST_MODEL> wishlist_modelArrayList)
        {
            super();
            this.context = context;
            this.wishlist_modelArrayList = wishlist_modelArrayList;
            rq= Volley.newRequestQueue(context);
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
        }

        //Loader code
        public void add(WISHLIST_MODEL country) {
            this.wishlist_modelArrayList.add(country);
        }

        @Override
        public MyWishlist_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_saveditems_lvitems, parent, false);
            MyWishlist_ADP.ViewHolder viewHolder = new MyWishlist_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final MyWishlist_ADP.ViewHolder holder, final int position)
        {
            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH+""+wishlist_modelArrayList.get(position).getImg().get(0))
                    .into(holder.wishlist_iv);

            holder.wishlistproductname.setText(wishlist_modelArrayList.get(position).getProduct_name());


            if(wishlist_modelArrayList.get(position).getVariant().isEmpty())
            {
                holder.wishlist_prodprice.setText("₹ "+wishlist_modelArrayList.get(position).getProduct_price());
                holder.wishlist_finalprice.setText("₹ "+wishlist_modelArrayList.get(position).getFinal_Price());
            }
            else
            {
                holder.wishlist_prodprice.setText("₹ "+wishlist_modelArrayList.get(position).getVariant().get(0).getProduct_price());
                holder.wishlist_finalprice.setText("₹ "+wishlist_modelArrayList.get(position).getVariant().get(0).getFinal_Price());
            }

            if(wishlist_modelArrayList.get(position).getVariant().isEmpty())   //disc calculating starts
            {
                if(wishlist_modelArrayList.get(position).getProduct_price().equals("")||wishlist_modelArrayList.get(position).getProduct_price().equals("0"))
                {
                    holder.wishlist_prodprice.setVisibility(View.GONE);
                    holder.wishlist_disc.setVisibility(View.GONE);
                }
                else
                {
                    holder.wishlist_prodprice.setVisibility(View.VISIBLE);
                    holder.wishlist_disc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(wishlist_modelArrayList.get(position).getFinal_Price());
                    double mrp_rate= Double.parseDouble(wishlist_modelArrayList.get(position).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    holder.wishlist_disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                holder.wishlist_finalprice.setText("₹ "+wishlist_modelArrayList.get(position).getFinal_Price());
            }
            else
            {

                if(wishlist_modelArrayList.get(position).getVariant().get(0).getProduct_price().equals("")||wishlist_modelArrayList.get(position).getVariant().get(0).getProduct_price().equals("0"))
                {
                    holder.wishlist_prodprice.setVisibility(View.GONE);
                    holder.wishlist_disc.setVisibility(View.GONE);
                }
                else
                {
                    holder.wishlist_prodprice.setVisibility(View.VISIBLE);
                    holder.wishlist_disc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(wishlist_modelArrayList.get(position).getVariant().get(0).getFinal_Price());
                    double mrp_rate= Double.parseDouble(wishlist_modelArrayList.get(position).getVariant().get(0).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    holder.wishlist_disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                holder.wishlist_finalprice.setText("₹ "+wishlist_modelArrayList.get(position).getVariant().get(0).getFinal_Price());
            }




            holder.deletewishlist.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    String title="Remove product";
                    String message="Are you sure you want to remove this product?";

                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Saved_Item.this);

                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

                    // Initialize a new spannable string builder instance
                    SpannableStringBuilder ssBuilder = new SpannableStringBuilder(title);

                    // Apply the text color span
                    ssBuilder.setSpan(
                            foregroundColorSpan,
                            0,
                            title.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    );

                    dialogBuilder.setTitle(ssBuilder);
                    dialogBuilder.setMessage(message);
                    dialogBuilder.setIcon(R.drawable.delete_bin);


                    dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();

                            String getprod_id=wishlist_modelArrayList.get(position).getProduct_id();
                            REMOVE_WISHLIST(getprod_id,position);

                        }
                    });
                    dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblue));
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.mblue));
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.white));

                }
            });

            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        Intent intent=new Intent(Saved_Item.this,Product_Detail.class);
                        intent.putExtra("prodid",wishlist_modelArrayList.get(position).getProduct_id());
                        intent.putExtra("getnote",wishlist_modelArrayList.get(position).getNotes());
                        PreferenceManager_ASOS.setcamefrom("yes");
                        startActivity(intent);
                        finish();
                    }
                }
            });
        }

        @Override
        public int getItemCount()
        {
            return wishlist_modelArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            public ImageView wishlist_iv;
            public TextView wishlistproductname,wishlist_finalprice,wishlist_prodprice,wishlist_disc;
            ImageView deletewishlist;
//            LinearLayout mainlayout;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                wishlist_iv = itemView.findViewById(R.id.wishlist_ivid);
                wishlistproductname = itemView.findViewById(R.id.wishlistproductnameid);
                wishlist_finalprice = itemView.findViewById(R.id.wishlist_finalprice);
                wishlist_prodprice = itemView.findViewById(R.id.wishlist_prodpriceid);
                wishlist_disc = itemView.findViewById(R.id.wishlist_discid);
                deletewishlist = itemView.findViewById(R.id.deletewishlistid);
//                mainlayout = itemView.findViewById(R.id.mainlayoutid);
                itemView.setOnClickListener(this);
            }
            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }



        }


        public void REMOVE_WISHLIST(String p_id, final int pos)
        {
            JSONObject object=new JSONObject() ;
            try
            {
                object.put("type","business_favourite_process");
                object.put("user_id",PreferenceManager_ASOS.GetUserID());
                object.put("action","add_product");
                JSONObject jsonObject2=new JSONObject();
                jsonObject2.put("product_id",p_id);
                jsonObject2.put("status","UNFAVOURITE");

                object.put("options",jsonObject2);



                final ProgressDialog progressDialog=new ProgressDialog(Saved_Item.this,R.style.ProgressDialogStyle);
                progressDialog.setTitle("Removing");
                progressDialog.setMessage("Please wait ...");
                progressDialog.show();

                JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, object, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Log.e("Un_Fav", String.valueOf(response));

                        try
                        {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String Action = jsonObject.getString("Action");

                            if(Action.equals("Done"))
                            {
                                wishlist_modelArrayList.remove(pos);
                                notifyItemRemoved(pos);
                                notifyItemRangeChanged(pos, wishlist_modelArrayList.size());

                                Snackbar snackbar = Snackbar.make(parentlo, "Product removed successfully from Saved Item....!!!", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                snackbar.show();


                                if(wishlist_modelArrayList.size()==0)
                                {
                                    main_lo.setVisibility(View.GONE);
                                    empty_bag_lo.setVisibility(View.VISIBLE);
                                    empty_lo_add_addr_bt.setVisibility(View.GONE);
                                }
                                else
                                {
                                    empty_bag_lo.setVisibility(View.GONE);
                                    main_lo.setVisibility(View.VISIBLE);
                                    empty_msg.setText("Opps, it seems you haven't saved any product...!!!");

                                }
                            }


                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Log.e("e", String.valueOf(e));

                        }

                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("UN_Fav_error", String.valueOf(error));
                    }
                })
                {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put(PaperCart_HEADER.Contenttype,PaperCart_HEADER.Appjson);
                        params.put(PaperCart_HEADER.Auth, PaperCart_HEADER.KEY);

                        return params;
                    }
                };
                rq.add(jsonObjectRequest);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void TOOLBAR()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
    }


    @Override
    protected void onResume()
    {
        super.onResume();

//        //BAG COUNT CODE
//        ArrayList<mycart> cart_count = new ArrayList<mycart>();
//        Main_impl impl = new Main_impl(Saved_Item.this);
//        cart_count = impl.getUser();
//        tv_bagcount.setText(String.valueOf(cart_count.size()));

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
