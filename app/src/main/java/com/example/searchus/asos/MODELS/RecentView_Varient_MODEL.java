package com.example.searchus.asos.MODELS;

public class RecentView_Varient_MODEL
{
    public RecentView_Varient_MODEL(String name, String product_price, String final_Price, String purchase_price, String quantity) {
        this.name = name;
        this.product_price = product_price;
        Final_Price = final_Price;
        this.purchase_price = purchase_price;
        Quantity = quantity;
    }

    String name;
   String product_price,Final_Price,purchase_price;
   String Quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getFinal_Price() {
        return Final_Price;
    }

    public void setFinal_Price(String final_Price) {
        Final_Price = final_Price;
    }

    public String getPurchase_price() {
        return purchase_price;
    }

    public void setPurchase_price(String purchase_price) {
        this.purchase_price = purchase_price;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }
}
