package com.example.searchus.asos.MODELS;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DELL on 09/25/2016.
 */
public class mycart
{
    int id;
    String product_id,
            product_name,
            price,
            description,
            specification,
            discount,
            availability,
            img,
            qty,
            amount,
            stts,
            availqty,
            variant,
            note;

    public mycart(int id, String product_id, String product_name, String price, String description, String specification, String discount, String availability, String img, String qty, String amount, String stts, String availqty, String variant,String note) {
        this.id = id;
        this.product_id = product_id;
        this.product_name = product_name;
        this.price = price;
        this.description = description;
        this.specification = specification;
        this.discount = discount;
        this.availability = availability;
        this.img = img;
        this.qty = qty;
        this.amount = amount;
        this.stts = stts;
        this.availqty = availqty;
        this.variant = variant;
        this.note = note;
    }

    public mycart() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStts() {
        return stts;
    }

    public void setStts(String stts) {
        this.stts = stts;
    }

    public String getAvailqty() {
        return availqty;
    }

    public void setAvailqty(String availqty) {
        this.availqty = availqty;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String TableName() {
        return "Details";
    }


    public JSONObject getJSONObject()
    {
        JSONObject jsonobj = new JSONObject();
        try
        {

            jsonobj.put("id", product_id);
            jsonobj.put("quantity", qty);
            jsonobj.put("variant", stts);
            jsonobj.put("variant_name", variant);

            //            if (!variant.equals("")) {
//                jsonobj.put("variant_name", variant);
//            } else {
//                jsonobj.put("variant_name", "none");
//            }
        }
        catch (JSONException e)
        {

        }
        return jsonobj;
    }


}
