package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import studio.carbonylgroup.textfieldboxes.ExtendedEditText;
import studio.carbonylgroup.textfieldboxes.TextFieldBoxes;

public class Edit_Address extends AppCompatActivity
{
    LinearLayout parentlo,nonet_lo;
    RelativeLayout main_lo;
    TextFieldBoxes tfname,tfaddr,tfcity,tfstate,tfnearby,tfpincode,tfmobno;
    ExtendedEditText edname,edaddr,edcity,edstate,ednearby,edpincode,edmobno;
    Button edit_addrbt_bt;
    Toolbar toolbar;

    String getaddrname,getaddr,getaddrcity,getaddrpincode,getaddrstate,getaddrmobno,getaddrid,getnearby;
    String geaddrname,geaddr,geaddrcity,geaddrpincode,geaddrstate,geaddrmobno,geaddrid,genearby;

    RequestQueue rq;
    JsonObjectRequest jsonObjectRequest;
    PreferenceManager_ASOS PreferenceManager_ASOS;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_edit__address);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());


          TOOLBAR();
          BINDING();
          CHECKING_PREF();
          SETTEXT();
          DISABLE_UPDATE_BUTTON();
          EDIT_BT_ONCLICK();

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);

    }

    public void TOOLBAR()
    {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(PreferenceManager_ASOS.getcamefrom().equals("yes"))
                {
                    Intent intent=new Intent(Edit_Address.this,My_Address.class);
                    intent.putExtra("from intent", "CheckOut");
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Intent intent=new Intent(Edit_Address.this,My_Address.class);
                    intent.putExtra("from intent", "Edit_Address");
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    public void BINDING()
    {
        parentlo=findViewById(R.id.parentlo_id);

        tfname=findViewById(R.id.tfnameid);
        tfaddr=findViewById(R.id.tfaddrid);
        tfcity=findViewById(R.id.tfcityid);
        tfstate=findViewById(R.id.tfstateid);
        tfnearby=findViewById(R.id.tfnearbyid);
        tfpincode=findViewById(R.id.tfpincodeid);
        tfmobno=findViewById(R.id.tfmobnoid);

        edname=findViewById(R.id.ednameid);
        edaddr=findViewById(R.id.edaddrid);
        edcity=findViewById(R.id.edcityid);
        edstate=findViewById(R.id.edstateid);
        ednearby=findViewById(R.id.ednearbyid);
        edpincode=findViewById(R.id.edpincodeid);
        edpincode.setRawInputType(Configuration.KEYBOARD_12KEY);
        edmobno=findViewById(R.id.edmobnoid);
        edmobno.setRawInputType(Configuration.KEYBOARD_12KEY);

        main_lo=findViewById(R.id.main_lo_id);
        nonet_lo=findViewById(R.id.nonet_lo_id);

        edit_addrbt_bt=findViewById(R.id.edit_addrbt_btid);

    }

    // Checking Internet connection function
    private BroadcastReceiver networkReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
            {
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED)
                {
//                    "We have internet connection. Good to go."

                    toolbar.setVisibility(View.VISIBLE);
                    nonet_lo.setVisibility(View.GONE);
                    main_lo.setVisibility(View.VISIBLE);

                }
                else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED)
                {
//                  "We have lost internet connection"

                    toolbar.setVisibility(View.GONE);
                    nonet_lo.setVisibility(View.VISIBLE);
                    main_lo.setVisibility(View.GONE);
                }
            }
        }
    };


    public void SETTEXT()
    {
        getaddrname=getIntent().getStringExtra("getaddrname").toString();
        getaddr=getIntent().getStringExtra("getaddr").toString();
        getaddrcity=getIntent().getStringExtra("getaddrcity").toString();
        getaddrpincode=getIntent().getStringExtra("getaddrpincode").toString();
        getaddrstate=getIntent().getStringExtra("getaddrstate").toString();
        getaddrmobno=getIntent().getStringExtra("getaddrmobno").toString();
        getaddrid=getIntent().getStringExtra("getaddrid").toString();
        getnearby=getIntent().getStringExtra("getnearby").toString();

        edname.setText(getaddrname);
        edaddr.setText(getaddr);
        edcity.setText(getaddrcity);
        edstate.setText(getaddrstate);
        ednearby.setText(getnearby);
        edpincode.setText(getaddrpincode);
        edmobno.setText(getaddrmobno);

    }

    public void EDIT_BT_ONCLICK()
    {
        edit_addrbt_bt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(edname.getText().toString().equals(""))
                {
                    tfname.setError("Name should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Name should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edname.setFocusable(true);
                }
                else if(edaddr.getText().toString().equals(""))
                {
                    tfaddr.setError("Address should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Address should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edaddr.setFocusable(true);

                }
                else if(edcity.getText().toString().equals(""))
                {
                    tfcity.setError("City should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "City should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edcity.setFocusable(true);
                }
                else if(edstate.getText().toString().equals(""))
                {
                    tfstate.setError("State should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "State should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edstate.setFocusable(true);

                }
                else if(ednearby.getText().toString().equals(""))
                {
                    tfnearby.setError("Nearby should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Nearby should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    ednearby.setFocusable(true);
                }
                else if(edpincode.getText().toString().equals(""))
                {
                    tfpincode.setError("Pincode should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Pincode number should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edpincode.setFocusable(true);

                }
                else if(edmobno.getText().toString().equals(""))
                {
                    tfmobno.setError("Mobile number should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Mobile number should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edmobno.setFocusable(true);

                }
                else if(edmobno.length()!=10)
                {
                    tfmobno.setError("Mobile number must be of 10 digits",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Mobile number must be of 10 digits....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edmobno.setFocusable(true);
                }
                else
                {
                    EDIT_ADDRESS();
                }
            }
        });

    }

    public void EDIT_ADDRESS()
    {
        geaddrname=edname.getText().toString();
        geaddr=edaddr.getText().toString();
        geaddrcity=edcity.getText().toString();
        geaddrstate=edstate.getText().toString();
        genearby=ednearby.getText().toString();
        geaddrpincode=edpincode.getText().toString();
        geaddrmobno=edmobno.getText().toString();
        geaddrid=getaddrid;

        rq= Volley.newRequestQueue(getApplicationContext());

        JSONObject object=new JSONObject() ;
        try
        {
            object.put("type", "user_address_process");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            object.put("action", "update_address");

            JSONObject object1=new JSONObject() ;
            object.put("options", object1);

            object1.put("name", geaddrname);
            object1.put("address", geaddr);
            object1.put("state", geaddrstate);
            object1.put("city", geaddrcity);
            object1.put("pincode", geaddrpincode);
            object1.put("phone", geaddrmobno);
            object1.put("near_by", genearby);
            object1.put("address_id", geaddrid);

            final ProgressDialog progressDialog=new ProgressDialog(Edit_Address.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Making changes");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response)
                {
                    Log.e("Edit_Addr_res", String.valueOf(response));

                    try
                    {
                        JSONObject jsonObject=new JSONObject(String.valueOf(response));
                        String status=jsonObject.getString("status");

                        if(status.equals("Address Details Updated successfully"))
                        {

                            final Snackbar snackbar = Snackbar.make(parentlo, "Address Updated Successfully....!!!", Snackbar.LENGTH_SHORT);
                            View snackBarView = snackbar.getView();
                            snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                            snackbar.show();

                            Thread timer = new Thread()
                            {
                                public void run()
                                {
                                    try
                                    {
                                        sleep(1000);
                                        snackbar.dismiss();
                                    }
                                    catch (InterruptedException e)
                                    {
                                        e.printStackTrace();
                                    }
                                    finally
                                    {
                                        if(PreferenceManager_ASOS.getcamefrom().equals("CheckOut")) //IF CAME FROM CHECKOUT
                                        {
                                            PreferenceManager_ASOS.setcamefrom("ADD_EDIT_ADDR");
                                            Intent intent = new Intent(Edit_Address.this, CheckOut.class);
//                                            intent.putExtra("from_intent", "from_add_editaddr");

                                            intent.putExtra("getAddress_id", geaddrid);
                                            intent.putExtra("getName", geaddrname);
                                            intent.putExtra("getAddress", geaddr);
                                            intent.putExtra("getNear_by", genearby);
                                            intent.putExtra("getCity",geaddrcity);
                                            intent.putExtra("getPincode", geaddrpincode);
                                            intent.putExtra("getState", geaddrstate);
                                            intent.putExtra("getCountry", "India");
                                            intent.putExtra("getPhone", geaddrmobno);

                                            startActivity(intent);
                                            finish();
                                        }
                                        else//--- if simple edit addr
                                        {
//                                            Intent intent=new Intent(getApplicationContext(),My_Address.class);
//                                            intent.putExtra("from intent","Edit_Address");
//                                            startActivity(intent);
                                            onBackPressed();
                                            finish();
                                        }

                                    }

                                }

                            };
                            timer.start();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Something went wrong, try again",Toast.LENGTH_SHORT).show();
                        }


                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Edit_Addr_error", String.valueOf(error));

                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put(PaperCart_HEADER.Contenttype, PaperCart_HEADER.Appjson);
                    params.put(PaperCart_HEADER.Auth,PaperCart_HEADER.KEY);

                    return params;
                }
            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rq.add(jsonObjectRequest);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void DISABLE_UPDATE_BUTTON()
    {
        edname.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                edit_addrbt_bt.setEnabled(true);
                edit_addrbt_bt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });


        edaddr.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                edit_addrbt_bt.setEnabled(true);
                edit_addrbt_bt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });


        edcity.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                edit_addrbt_bt.setEnabled(true);
                edit_addrbt_bt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        edstate.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                edit_addrbt_bt.setEnabled(true);
                edit_addrbt_bt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        ednearby.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                edit_addrbt_bt.setEnabled(true);
                edit_addrbt_bt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        edpincode.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                edit_addrbt_bt.setEnabled(true);
                edit_addrbt_bt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        edmobno.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                edit_addrbt_bt.setEnabled(true);
                edit_addrbt_bt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });


    }

    public void CHECKING_PREF()
    {
        //IF CAME FROM CHECKOUT
        if(PreferenceManager_ASOS.getcamefrom().equals("yes"))
        {
            edit_addrbt_bt.setText("DELIVER TO THIS ADDRESS");
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (networkReceiver != null)
        {
            unregisterReceiver(networkReceiver);
        }
    }

    @Override
    public void onBackPressed()
    {
        if(PreferenceManager_ASOS.getcamefrom().equals("yes"))
        {
            Intent intent=new Intent(Edit_Address.this,My_Address.class);
            intent.putExtra("from intent", "CheckOut");
            startActivity(intent);
            finish();
        }
        else
        {
            Intent intent=new Intent(Edit_Address.this,My_Address.class);
            intent.putExtra("from intent", "Edit_Address");
            startActivity(intent);
            finish();
        }
    }
}
