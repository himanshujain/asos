package com.example.searchus.asos.MODELS;

import java.io.Serializable;

public class RATING_PEOPLE_LIST implements Serializable
{
    public RATING_PEOPLE_LIST(String user_id, String name, String img_path, String rate, String review, String date_time)
    {
        this.user_id = user_id;
        this.name = name;
        this.img_path = img_path;
        this.rate = rate;
        this.review = review;
        this.date_time = date_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_path() {
        return img_path;
    }

    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    String user_id,
            name,
            img_path,
            rate,
            review,
            date_time;


}
