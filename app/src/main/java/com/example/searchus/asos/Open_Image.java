package com.example.searchus.asos;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Classes.TouchImageView;

import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class Open_Image extends AppCompatActivity
{

    TouchImageView imageView;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    ArrayList<String> getimg;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_open__image);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
                finish();
            }
        });

        imageView=findViewById(R.id.ivid);

        getimg = new ArrayList<String>();
        getimg = (ArrayList<String>) getIntent().getSerializableExtra("getimgs");

        try
        {
            Log.e("ARRAY", String.valueOf(getimg));
        }
        catch(Exception e)
        {

        }

        // Calling the RecyclerView

        recyclerView = (RecyclerView) findViewById(R.id.recycler_viewid);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager horizontalLayoutManager_three = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager_three);
        adapter = new ADP(getApplicationContext(), getimg);
        recyclerView.setAdapter(adapter);
    }


    public class ADP extends RecyclerView.Adapter<ADP.ViewHolder>
    {
        ArrayList<String> arrayList;
        Context context;
        private PreferenceManager_ASOS PreferenceManager_ASOS;

        public ADP(Context context, ArrayList<String> alImage)
        {
            super();
            this.context = context;
            this.arrayList = alImage;
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_openimg_lvitems, parent, false);
            ADP.ViewHolder viewHolder = new ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position)
        {
            Picasso.with(context)
                    .load(PreferenceManager_ASOS.getimage_path()+""+arrayList.get(position))
                    .into(holder.imageView);

            Picasso.with(context)
                    .load(PreferenceManager_ASOS.getimage_path()+""+arrayList.get(0))
                    .into(imageView);

            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        Picasso.with(context)
                                .load(PreferenceManager_ASOS.getimage_path()+""+arrayList.get(position))
                                .into(imageView);
                    }
                   
                }
            });


        }

        @Override
        public int getItemCount()
        {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            public ImageView imageView;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imageView = (ImageView) itemView.findViewById(R.id.ivvid);
                itemView.setOnClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

        }

    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
