package com.example.searchus.asos;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Impl.Main_impl;
import com.example.searchus.asos.MODELS.GETSET_PRODLIST;
import com.example.searchus.asos.MODELS.Pop_PROD_Final_Varient_MODEL;
import com.example.searchus.asos.MODELS.RATING_PEOPLE_LIST;
import com.example.searchus.asos.MODELS.mycart;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Product_Detail extends AppCompatActivity
{
    private ViewPager vp_slider;
    private LinearLayout ll_dots;
    Product_detail_Slider_Image_Adapter sliderPagerAdapter;
    private TextView[] dots;

    RatingBar ratingbar;
    TextView give_ratingtv,tvrating;

    RecyclerView Relatedlist_RecyclerView;
    RecyclerView.Adapter relatedlist_Adapter;
    ArrayList<Integer> Relatedlist_Img;


    TextView prodname,mrp,finalprice,tv_discount,mrprupee,note,tvproddetail;
    LinearLayout p_detail_add_to_wishlist,hide_spinner_lo;
    FrameLayout sharelayout;
    ScrollView parentlo;
    Button add_to_cart_bt;
    ImageView iv_saveditem;
    Spinner SortSpinner;

    String productid,catid,getnote;

    ArrayList<String> Img = new ArrayList<>();
    ArrayList<Pop_PROD_Final_Varient_MODEL> varientArray=new ArrayList<>();

    PreferenceManager_ASOS PreferenceManager_ASOS;
    String pid,product_name,available_varient,varient_name;
    String product_price,product_desc,quantity,product_spec,purchase_price,final_price,avalibility,varient_status,notes;
    String getfinalprice,getprodname;
     boolean getfavourite_status;

    //Recommended prods
    ArrayList<String> V_imgarray;
    GETSET_PRODLIST getset_prodlist;
    String getrecommended_id;
    LinearLayout recc_lo,prod_detail_lo;

    String path;
    Uri uri,shared_uri;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_product__detail);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

        TOOLBAR();
        BINDING();
        ONCLICKS();

        recc_lo.setVisibility(View.GONE);

        Intent intent = getIntent();
        shared_uri = intent.getData ();
        Log.e("datas",shared_uri+"");

        if (shared_uri != null && !shared_uri.equals(Uri.EMPTY))  //if frm shared link
        {
            note.setVisibility(View.GONE);
            TRIMMING_URL();
        }
        else  //if normal intent
        {
            productid = getIntent().getStringExtra("prodid");
//            catid = intent.getStringExtra("CatId");
            getnote=getIntent().getStringExtra("getnote").toString();

            note.setVisibility(View.VISIBLE);
            GET_NOTE_INFO();
            PROD_Detail__GETDATA();
            GET_RATING();
            SHARE_PROD();
        }
    }

    public void TOOLBAR()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(PreferenceManager_ASOS.getcamefrom().equals("yes"))
                {
                    Intent intent=new Intent(Product_Detail.this,Saved_Item.class);
                    startActivity(intent);
                    PreferenceManager_ASOS.setcamefrom("no");
                }
                else
                {
                    PreferenceManager_ASOS.setcamefrom("no");
                    onBackPressed();

                }
            }
        });
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void BINDING()
    {
        note=findViewById(R.id.noteid);
        prodname=findViewById(R.id.prod_nameid);
        mrp=findViewById(R.id.mrpprice_id);
        finalprice=findViewById(R.id.final_priceid);
        tv_discount=findViewById(R.id.tv_discountid);
        mrprupee=findViewById(R.id.mrprupee_id);
        add_to_cart_bt=findViewById(R.id.add_to_cart_btid);
        p_detail_add_to_wishlist=findViewById(R.id.p_detail_add_to_wishlist_id);
        sharelayout=findViewById(R.id.sharelayoutid);
        tvproddetail=findViewById(R.id.tvproddetailid);
        SortSpinner=findViewById(R.id.sortspinnerid);
        iv_saveditem=findViewById(R.id.iv_saveditem_id);
        ll_dots = findViewById(R.id.ll_dots);
        parentlo = findViewById(R.id.parentlo_id);
        hide_spinner_lo = findViewById(R.id.hide_spinner_lo_id);
        recc_lo = findViewById(R.id.recc_lo_id);
        prod_detail_lo = findViewById(R.id.prod_detail_lo_id);
        ratingbar = findViewById(R.id.ratingbarid);
        tvrating=findViewById(R.id.tvratingid);
        give_ratingtv=findViewById(R.id.givetvratingdialogueid);
    }

    public void ONCLICKS()
    {
        ratingbar.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {

                if(tvrating.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Not rated yet",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    String getpid=getIntent().getStringExtra("prodid");

                    Intent intent=new Intent(Product_Detail.this,VIEW_PROD_REVIEW.class);
                    intent.putExtra("pid",getpid);
                    startActivity(intent);
                }


                return false;
            }
        });

        give_ratingtv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(PreferenceManager_ASOS.GetUserID().equals(""))
                {
                    Intent intent=new Intent(Product_Detail.this,Enter_OTP.class);
//                    intent.putExtra("from_intent","Product_Detail");
                    startActivity(intent);
                }
                else
                {
                    Rating_Dialog();
                }
            }
        });


        LayerDrawable layerDrawable = (LayerDrawable) ratingbar.getProgressDrawable();
        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(0)), getResources().getColor(R.color.lightgray));   // Empty star
        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(1)), getResources().getColor(R.color.lightgray)); // Partial star
        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(2)),  getResources().getColor(R.color.gold));  // Full star
    }

    public void GET_NOTE_INFO()
    {
        Log.e("getnote", getnote);
        if(getnote.equals("") || getnote.equals(null))
        {
            note.setVisibility(View.GONE);
        }
        else
        {
            note.setVisibility(View.VISIBLE);
            note.setText(getnote);
        }

    }

    public void PROD_Detail__GETDATA()
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String NEW_PROD_DETAIL_API_URL = "http://api.searchus.in/API/ecommerce/product_details";

            JSONObject object=new JSONObject() ;
            JSONObject jsonObject2=new JSONObject();
            object.put("options",jsonObject2);
            JSONObject jsonObject3=new JSONObject();
            jsonObject2.put("input",jsonObject3);
            jsonObject3.put("user_id",PreferenceManager_ASOS.GetUserID());
            jsonObject3.put("keyword",productid);

            final String requestBody = object.toString();


            final ProgressDialog progressDialog=new ProgressDialog(Product_Detail.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading product details");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, NEW_PROD_DETAIL_API_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");
                    Img.clear();
                    varientArray.clear();

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("Prod_Detail_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("Prod_Detail_res", response);

                        try
                        {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String code = jsonObject.getString("code");
                            String message = jsonObject.getString("message");

                            if(code.equals("000200"))
                            {

                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                pid = jsonObject1.getString("product_id");
                                product_name = jsonObject1.getString("product_name");
                                product_price = jsonObject1.getString("product_price");
                                product_desc = jsonObject1.getString("product_desc");
                                quantity = jsonObject1.getString("quantity");
                                product_spec = jsonObject1.getString("product_spec");
                                String tags = jsonObject1.getString("tags");

                                //ADDING IMG TO SLIDER
                                JSONArray jsonArray = jsonObject1.getJSONArray("img_path");
                                if(jsonArray!=null)
                                {
                                    for (int i = 0; i < jsonArray.length(); i++)
                                    {
                                        Img.add(jsonArray.getString(i));
                                    }

                                }

                                //if no img then
                                if(Img.isEmpty())
                                {
                                    Img.add("/images/recent.jpg");//noimg url
                                    init();
                                }
                                else
                                {
                                    init();
                                }

                                if(Img.size()<2)
                                {
                                    ll_dots.setVisibility(View.GONE);
                                }
                                else
                                {
                                    ll_dots.setVisibility(View.VISIBLE);
                                }
                                addBottomDots(0);


                                purchase_price = jsonObject1.getString("purchase_price");
                                avalibility = jsonObject1.getString("avalibility");
                                String product_code = jsonObject1.getString("product_code");
                                notes = jsonObject1.getString("notes");
                                String extra_1 = jsonObject1.getString("extra_1");
                                String extra_2 = jsonObject1.getString("extra_2");
                                String category_id = jsonObject1.getString("category_id");
                                String category_name = jsonObject1.getString("category_name");
                                String category_notes = jsonObject1.getString("category_notes");
                                String categroy_path = jsonObject1.getString("categroy_path");
                                getfavourite_status = jsonObject1.getBoolean("favourite_status");
                                Log.e("favourite_status", String.valueOf(getfavourite_status));

                                //ADD TO FAV CODE....
                                FAV_CODE(getfavourite_status);


                                //VARIENT PARSING
                                JSONArray jsonArray2 = jsonObject1.getJSONArray("variant_details");
                                for (int i = 0; i < jsonArray2.length(); i++)
                                {
                                    JSONObject jsonObject3 = jsonArray2.getJSONObject(i);
                                    int V_sr_no=jsonObject3.getInt("sr_no");
                                    String V_variant_name=jsonObject3.getString("variant_name");
                                    int V_purchase_price=jsonObject3.getInt("purchase_price");
                                    int V_mrp=jsonObject3.getInt("mrp");
                                    int V_final_price=jsonObject3.getInt("final_price");
                                    int V_quantity=jsonObject3.getInt("quantity");

                                    Object json = jsonObject3.get("images");
                                    JSONArray imagevar = null;
                                    ArrayList<String> V_imgarray = new ArrayList<String>();

                                    if (json instanceof JSONArray)
                                    {
                                        imagevar = jsonObject3.getJSONArray("images");
                                        for (int j = 0; j < imagevar.length(); j++)
                                        {
                                            V_imgarray.add(String.valueOf(imagevar.get(j)));
                                        }

                                    }

                                    Pop_PROD_Final_Varient_MODEL pop_prod_varient_model = new
                                            Pop_PROD_Final_Varient_MODEL(V_variant_name,V_sr_no,V_purchase_price, V_mrp, V_final_price,V_quantity,V_imgarray);
                                    varientArray.add(pop_prod_varient_model);
                                }

                                final_price = jsonObject1.getString("final_price");

                                VARIENT_ONCREATE_SETETXT_LOGIC();

                            }

                            //ADD TO CART CODE...
                            add_to_cart_bt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v)
                                {
                                    String GET_Varient_Img=PreferenceManager_ASOS.getV_IMG();
                                    String GET_Varient_QTY= String.valueOf(PreferenceManager_ASOS.getV_QTY());

                                    getfinalprice=finalprice.getText().toString();

                                    if(varientArray.isEmpty())
                                    {
                                        varient_name="none";
                                        getprodname=prodname.getText().toString();
                                        varient_status="false";
                                    }
                                    else
                                    {
                                        varient_name = (prodname.getText().toString().substring(prodname.getText().toString().indexOf(" - ") + 3));
//                                    String abc=prodname.getText().toString();
//                                    String response = (abc.substring(abc.lastIndexOf("-") + 2));
                                        getprodname=prodname.getText().toString();
                                        varient_status="true";
                                    }

                                    Main_impl impl = new Main_impl(Product_Detail.this);
                                    ArrayList<mycart> getpnamearraylist = new ArrayList<mycart>();
                                    ArrayList<mycart> getidarraylist = new ArrayList<mycart>();

                                    ArrayList<String>chk_pname_arraylist=new ArrayList<>();
                                    ArrayList<String>chk_ID_arraylist=new ArrayList<>();

                                    getidarraylist = impl.getUser();
                                    getpnamearraylist = impl.getUser();

                                    for (int i=0;i<getidarraylist.size();i++)
                                    {
                                        getidarraylist.get(i).getId();
                                        getpnamearraylist.get(i).getProduct_name();

                                        Log.e("getProduct_name", prodname.getText().toString());

                                        chk_ID_arraylist.add(getidarraylist.get(i).getProduct_id());
                                        chk_pname_arraylist.add(getpnamearraylist.get(i).getProduct_name());
                                    }

//                                Log.e("chk_ID_arraylist", String.valueOf(chk_ID_arraylist));
//                                Log.e("chk_pname_arraylist", String.valueOf(chk_pname_arraylist));

                                    if(chk_pname_arraylist.contains(prodname.getText().toString()))
                                    {
                                        if(chk_ID_arraylist.contains(pid))
                                        {
                                            Snackbar snackbar = Snackbar.make(parentlo, "Product is already available in Bag....!!!", Snackbar.LENGTH_SHORT);
                                            View snackBarView = snackbar.getView();
                                            snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                                            snackbar.show();
                                        }
                                    }
                                    else
                                    {
                                        if(Img.isEmpty())
                                        {

                                            long det = impl.insert(new mycart(0,
                                                    pid,
                                                    getprodname,
                                                    getfinalprice,
                                                    product_desc,
                                                    product_spec,
                                                    tv_discount.getText().toString(),
                                                    avalibility,
                                                    GET_Varient_Img,
                                                    "1",
                                                    finalprice.getText().toString(),
                                                    varient_status,
                                                    GET_Varient_QTY,
                                                    varient_name,
                                                    getnote));

                                        }
                                        else
                                        {
                                            long det = impl.insert(new mycart(0,
                                                    pid,
                                                    getprodname,
                                                    getfinalprice,
                                                    product_desc,
                                                    product_spec,
                                                    tv_discount.getText().toString(),
                                                    avalibility,
                                                    Img.get(0),
                                                    "1",
                                                    finalprice.getText().toString(),
                                                    varient_status,
                                                    quantity,
                                                    varient_name,
                                                    getnote));

                                        }

                                            Snackbar snackbar = Snackbar.make(parentlo, "Product added successfully to Bag....!!!", Snackbar.LENGTH_SHORT);
                                            View snackBarView = snackbar.getView();
                                            snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mgreen));
                                            snackbar.show();
                                    }
//                                Log.e("varient_name", varient_name);
                                }
                            });

                            tvproddetail.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    Intent intent=new Intent(Product_Detail.this,Product_Info.class);
                                    intent.putExtra("product_name",product_name);
                                    intent.putExtra("product_desc",product_desc);
                                    intent.putExtra("product_spec",product_spec);
                                    startActivity(intent);
                                }
                            });

                            SHOW_HIDE_PROD_DETAIL();

                            //IF NO IMG AVAILABLE
                            if(Img.isEmpty() && varientArray.isEmpty())
                            {
                                 //DO NOTHING
                            }
                            else
                            {
                                ARRAYPASSING();
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Log.e("PROD_DETAIL_Exception", String.valueOf(e));

                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Prod_Detail_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public class CustomAdapter extends BaseAdapter
    {
        Context context;
        ArrayList<Pop_PROD_Final_Varient_MODEL> varientArray;
        LayoutInflater inflter;

        public CustomAdapter(Context applicationContext,ArrayList<Pop_PROD_Final_Varient_MODEL> varientArray)
        {
            this.context = applicationContext;
            this.varientArray = varientArray;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount()
        {
            return varientArray.size();
        }

        @Override
        public Object getItem(int i)
        {
            return null;
        }

        @Override
        public long getItemId(int i)
        {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            view = inflter.inflate(R.layout.asos_custom_spinner_items, null);
            TextView varient_names = (TextView) view.findViewById(R.id.varient_nameid);
            varient_names.setText(varientArray.get(i).getVariant_name());
            return view;
        }
    }

    public void SPINNER_ONCLICK()
    {
        SortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Log.e("QTY",String.valueOf(varientArray.get(position).getQuantity()));
                PreferenceManager_ASOS.setV_QTY(varientArray.get(position).getQuantity());

                prodname.setText(product_name+" - "+varientArray.get(position).getVariant_name());

                finalprice.setText(Double.toString(varientArray.get(position).getFinal_price()));

                if(varientArray.get(position).getMrp()==0)
                {
                    tv_discount.setVisibility(View.GONE);
                    mrp.setVisibility(View.GONE);
                    mrprupee.setVisibility(View.GONE);
                }
                else
                {
                    tv_discount.setVisibility(View.VISIBLE);
                    mrp.setVisibility(View.VISIBLE);
                    mrprupee.setVisibility(View.VISIBLE);

                    mrp.setText(Double.toString(varientArray.get(position).getMrp()));

                    double f_price;
                    double mrp_price;

                    f_price=varientArray.get(position).getFinal_price();
                    mrp_price=varientArray.get(position).getMrp();
                    double ans=f_price*100/mrp_price;
                    double price_perc_off=100-ans;

                    tv_discount.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");
                }

                sliderPagerAdapter = new Product_detail_Slider_Image_Adapter(Product_Detail.this, varientArray.get(position).getImages());
                vp_slider.setAdapter(sliderPagerAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

    }

    public void VARIENT_ONCREATE_SETETXT_LOGIC()
    {
        //VARIENT LOGIC CODE
        if(varientArray.isEmpty())
        {
            hide_spinner_lo.setVisibility(View.GONE);

            prodname.setText(product_name);
            mrp.setText(product_price);
            finalprice.setText(final_price);
            available_varient="false";


            if(product_price.equals("")||product_price.equals("0"))
            {
                tv_discount.setVisibility(View.GONE);
                mrp.setVisibility(View.GONE);
                mrprupee.setVisibility(View.GONE);

            }
            else
            {
                tv_discount.setVisibility(View.VISIBLE);
                mrp.setVisibility(View.VISIBLE);
                mrprupee.setVisibility(View.VISIBLE);

                double final_rate= Double.parseDouble(finalprice.getText().toString());
                double mrp_rate= Double.parseDouble(mrp.getText().toString());
                double ans=final_rate*100/mrp_rate;
                double price_perc_off=100-ans;
                tv_discount.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");
            }


        }
        else
        {
            hide_spinner_lo.setVisibility(View.VISIBLE);

            CustomAdapter customAdapter=new CustomAdapter(getApplicationContext(),varientArray);
            SortSpinner.setAdapter(customAdapter);

            SPINNER_ONCLICK();


            if(varientArray.get(0).getMrp()==0)
            {
                tv_discount.setVisibility(View.GONE);
                mrp.setVisibility(View.GONE);
                mrprupee.setVisibility(View.GONE);

            }
            else
            {
                tv_discount.setVisibility(View.VISIBLE);
                mrp.setVisibility(View.VISIBLE);
                mrprupee.setVisibility(View.VISIBLE);

            }



            prodname.setText(product_name+" - "+varientArray.get(0).getVariant_name());
            mrp.setText(Double.toString(varientArray.get(0).getMrp()));
            finalprice.setText(Double.toString(varientArray.get(0).getFinal_price()));
            available_varient="true";

            double final_rate= Double.parseDouble(String.valueOf(varientArray.get(0).getFinal_price()));
            double mrp_rate= Double.parseDouble(String.valueOf(varientArray.get(0).getMrp()));
            double ans=final_rate*100/mrp_rate;
            double price_perc_off=100-ans;
            tv_discount.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");
        }
    }

    public void SHOW_HIDE_PROD_DETAIL()
    {
        if(product_desc.equals("") && product_spec.equals(""))
        {
            prod_detail_lo.setVisibility(View.GONE);
        }
        else
        {
            prod_detail_lo.setVisibility(View.VISIBLE);
        }
    }

    public void FAV_CODE(boolean favourite_status)
    {
        if(PreferenceManager_ASOS.GetUserID().equals(""))
        {
            iv_saveditem.setBackgroundResource(R.drawable.unfilllike);
        }
        else
        {
            //CHNGING ITEM SAVED BG
            if(favourite_status)
            {
                iv_saveditem.setBackgroundResource(R.drawable.filllike);
            }
            else
            {
                iv_saveditem.setBackgroundResource(R.drawable.unfilllike);
            }
        }


        //fav onclick code
        p_detail_add_to_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(PreferenceManager_ASOS.GetUserID().equals(""))
                {
                    Intent intent=new Intent(Product_Detail.this,Enter_OTP.class);
//                    intent.putExtra("from_intent","Product_Detail");
                    startActivity(intent);
                }
                else
                {
                    String status;
                    if(!getfavourite_status)//false
                    {
                        String prodid=pid;
                        status="FAVOURITE";
                        ADD_FAVOURITE(iv_saveditem,prodid,status);
                    }
                    else
                    {
                        String prodid=pid;
                        status="UNFAVOURITE";
                        ADD_FAVOURITE(iv_saveditem,prodid,status);

                    }
                }


            }
        });

    }

    public void init()
    {
        vp_slider = (ViewPager) findViewById(R.id.proddetail_slider);


        Log.e("IMG", String.valueOf(Img.size()));


        sliderPagerAdapter = new Product_detail_Slider_Image_Adapter(Product_Detail.this, Img);
        vp_slider.setAdapter(sliderPagerAdapter);


        vp_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void addBottomDots(int currentPage)
    {
        dots = new TextView[Img.size()];

        ll_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#FFFFFF"));//other dots color
            ll_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#212121"));//oncurrent img dot color
    }

    public class Product_detail_Slider_Image_Adapter extends PagerAdapter
    {
        private LayoutInflater layoutInflater;
        Activity activity;
        ArrayList<String> image_arraylist;

        public Product_detail_Slider_Image_Adapter(Activity activity, ArrayList<String> image_arraylist)
        {
            this.activity = activity;
            this.image_arraylist = image_arraylist;
        }



        @Override
        public Object instantiateItem(ViewGroup container, final int position)
        {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.asos_product_detail_slider_img, container, false);
            final ImageView im_slider = (ImageView) view.findViewById(R.id.im_slider);


                //normal img set code
                Picasso.with(activity.getApplicationContext())
                        .load(PaperCart_CoNNectioN.IMGPATH+""+image_arraylist.get(position))
                        .into(im_slider, new Callback()
                        {
                            @Override
                            public void onSuccess()
                            {
                                Drawable drawable;
                                drawable = im_slider.getDrawable();
                                Bitmap mB = ((BitmapDrawable) drawable).getBitmap();
                                path = MediaStore.Images.Media.insertImage(getContentResolver(), mB, "Image", null);
                            }

                            @Override
                            public void onError()
                            {
//                            Toast.makeText(activity,"Sorry image cannot be load, try again",Toast.LENGTH_SHORT).show();
                            }
                        });

                String V_IMG = image_arraylist.get(position);
                PreferenceManager_ASOS.setV_IMG(V_IMG);

            //img set low to high code
//            String img_responce=image_arraylist.get(position);
//            final String aftersubstring_img_response = (img_responce.substring(img_responce.lastIndexOf("/") + 1));
//            final String final_img8_path=PreferenceManager_ASOS.getimage_path() +""+PaperCart_CoNNectioN.imgpath_8+""+aftersubstring_img_response;
//            final String final_img4_path=PreferenceManager_ASOS.getimage_path() +""+PaperCart_CoNNectioN.imgpath_4+""+aftersubstring_img_response;
//
//            Picasso.with(activity)
//                    .load(final_img8_path)
//                    .into(im_slider, new Callback()
//                    {
//                        @Override
//                        public void onSuccess()
//                        {
//                            Drawable drawable;
//                            drawable = im_slider.getDrawable();
//                            Bitmap mB = ((BitmapDrawable) drawable).getBitmap();
//                            path = MediaStore.Images.Media.insertImage(getContentResolver(), mB, "Image", null);
//                        }
//
//                        @Override
//                        public void onError()
//                        {
//                            Toast.makeText(activity,"Sorry image cannot be load, try again",Toast.LENGTH_SHORT).show();
//                        }
//                    });
//
//            new Handler().postDelayed(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    // This method will be executed once the timer is over
//                    // Start your app main activity
//                    Picasso.with(activity)
//                            .load(final_img4_path)
//                            .into(im_slider, new Callback()
//                            {
//                                @Override
//                                public void onSuccess()
//                                {
//                                    Drawable drawable;
//                                    drawable = im_slider.getDrawable();
//                                    Bitmap mB = ((BitmapDrawable) drawable).getBitmap();
//                                    path = MediaStore.Images.Media.insertImage(getContentResolver(), mB, "Image", null);
//                                }
//
//                                @Override
//                                public void onError()
//                                {
//                                    Toast.makeText(activity,"Sorry image cannot be load, try again",Toast.LENGTH_SHORT).show();
//                                }
//                            });
//
//                }
//            }, 3000);



//            im_slider.setOnClickListener(new View.OnClickListener()
//            {
//                @Override
//                public void onClick(View v)
//                {
//                    String getimgpath=image_arraylist.get(position);
//                    Intent intent=new Intent(activity, Open_Image.class);
//                    intent.putExtra("imgpath",getimgpath);
//                    activity.startActivity(intent);
//                    Log.e("IMG",image_arraylist.get(position));
//                }
//            });

            container.addView(view);

            return view;
        }


        @Override
        public int getCount() {
            return image_arraylist.size();
        }


        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            View view = (View) object;
            container.removeView(view);
        }

    }

    public void ADD_FAVOURITE(final ImageView iv_saveditem, final String p_id, String status)
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type","business_favourite_process");
            object.put("user_id",PreferenceManager_ASOS.GetUserID());
            object.put("action","add_product");
            JSONObject jsonObject2=new JSONObject();
            jsonObject2.put("product_id",p_id);
            jsonObject2.put("status",status);

            object.put("options",jsonObject2);

            final String requestBody = object.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
//                    progressDialog.dismiss();
                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("Fav_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("Fav_res", response);
                        try
                        {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String status = jsonObject.getString("status");

                            if(status.equals("UNFAVOURITE"))
                            {
                                getfavourite_status=false;
                                iv_saveditem.setBackgroundResource(R.drawable.unfilllike);

                                Snackbar snackbar = Snackbar
                                        .make(parentlo, "Product removed from saved items....!!!", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                snackbar.show();

                                //fav resume logic
                                PreferenceManager_ASOS.setis_favchanged(true);
                                PreferenceManager_ASOS.setfavpid(p_id);
                                PreferenceManager_ASOS.setchange_fav_status(false);
                            }
                            else
                            {
                                getfavourite_status=true;
                                iv_saveditem.setBackgroundResource(R.drawable.filllike);

                                Snackbar snackbar = Snackbar
                                        .make(parentlo, "Product successfully saved....!!!", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                snackbar.show();

                                //fav resume logic
                                PreferenceManager_ASOS.setis_favchanged(true);
                                PreferenceManager_ASOS.setfavpid(p_id);
                                PreferenceManager_ASOS.setchange_fav_status(true);
                            }


                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Log.e("e", String.valueOf(e));

                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Fav_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void GET_RATING()
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type","product_rate_reviews");
            object.put("product_id",productid);
            JSONObject jsonObject2=new JSONObject();
            jsonObject2.put("action","get_rate_review");
            object.put("options",jsonObject2);

            final String requestBody = object.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
//                    progressDialog.dismiss();
                    String getresponce = response.replace("\"", "");

                    ArrayList<RATING_PEOPLE_LIST> ratingpeople_list=new ArrayList();

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("GET_RATING_res", "BLANK DATA");

                        tvrating.setText("");
                        ratingbar.setRating(Float.parseFloat("0"));
                    }
                    else
                    {
                        Log.e("GET_RATING_res", response);
                        try
                        {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            JSONObject average_infosObject = dataObject.getJSONObject("average_infos");

                            String avg=average_infosObject.getString("avg");
                            String review=average_infosObject.getString("review");
                            String rating=average_infosObject.getString("rating");
                            String one_rate=average_infosObject.getString("one_rate");
                            String two_rate=average_infosObject.getString("two_rate");
                            String three_rate=average_infosObject.getString("three_rate");
                            String four_rate=average_infosObject.getString("four_rate");
                            String five_rate=average_infosObject.getString("five_rate");

                            JSONArray listArray=dataObject.getJSONArray("list");
                            Gson gson = new Gson();
                            for(int i=0;i<listArray.length();i++)
                            {
                                JSONObject Object = listArray.getJSONObject(i);
                                ratingpeople_list.add(gson.fromJson(String.valueOf(Object), RATING_PEOPLE_LIST.class));
                            }
                            Log.e("ratingpeople_list", String.valueOf(ratingpeople_list));

                            ratingbar.setRating(Float.parseFloat(avg));

                            if(avg.equals("1"))
                            {
                                tvrating.setText("("+avg+") star");
                            }
                            else
                            {
                                tvrating.setText("("+avg+") stars");
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Log.e("e", String.valueOf(e));
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("GET_RATING_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication",PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }
//    public void RELATED_PROD_LIST() //NOT USED
//    {
//        Relatedlist_Img = new ArrayList<>(Arrays.asList(R.drawable.no_image,R.drawable.no_image));
//
//        Relatedlist_RecyclerView = (RecyclerView) findViewById(R.id.relatedlist_recycler_view);
//
//        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
//        trending_LayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        Relatedlist_RecyclerView.setLayoutManager(trending_LayoutManager);
//        Relatedlist_RecyclerView.setNestedScrollingEnabled(false);
//        relatedlist_Adapter = new Related_List_ADP(getApplicationContext(), Relatedlist_Img);
//        Relatedlist_RecyclerView.setAdapter(relatedlist_Adapter);
//
//    }
//
//    public class Related_List_ADP extends RecyclerView.Adapter<Related_List_ADP.ViewHolder> //NOT USED
//    {
//        ArrayList<Integer> alImage;
//        Context context;
//
//        public Related_List_ADP(Context context, ArrayList<Integer> alImage)
//        {
//            super();
//            this.context = context;
//            this.alImage = alImage;
//        }
//
//        @Override
//        public Related_List_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
//        {
//            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_relatedlist_lvitems, parent, false);
//            Related_List_ADP.ViewHolder viewHolder = new Related_List_ADP.ViewHolder(v);
//            return viewHolder;
//        }
//
//        @Override
//        public void onBindViewHolder(Related_List_ADP.ViewHolder holder, int position)
//        {
//
//            holder.imgThumbnail.setImageResource(alImage.get(position));
//
//
//
//        }
//
//        @Override
//        public int getItemCount()
//        {
//            return alImage.size();
//        }
//
//        public  class ViewHolder extends RecyclerView.ViewHolder
//        {
//
//            public ImageView imgThumbnail;
//
//            public ViewHolder(View itemView)
//            {
//                super(itemView);
//                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
//
//            }
//
//
//        }
//
//    }

    private void Rating_Dialog()
    {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.asos_rating_dialogue, null);
        dialogBuilder.setView(view);

        dialogBuilder.setPositiveButton("OK", null);

        final RatingBar dialogueratingbar=view.findViewById(R.id.dialogueratingbarid);
        final EditText edreview=view.findViewById(R.id.edreviewid);

        LayerDrawable layerDrawable = (LayerDrawable) dialogueratingbar.getProgressDrawable();
        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(0)), getResources().getColor(R.color.mgray));   // Empty star
        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(1)), getResources().getColor(R.color.mgray)); // Partial star
        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(2)),  getResources().getColor(R.color.gold));  // Full star

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

//                if(edreview.length()==0)
//                {
//                    edreview.setFocusable(true);
//                    edreview.setError("Please add review");
//                }
//                else
//                {
//                    String getreviewnumber=String.valueOf(dialogueratingbar.getRating());
//                    String getreviewtext=edreview.getText().toString();
//
//                    GETREVIEW(getreviewnumber,getreviewtext);
//
//                    alertDialog.dismiss();
//                }

                String getreviewnumber=String.valueOf(dialogueratingbar.getRating());
                String getreviewtext=edreview.getText().toString();

                if(getreviewtext.equals(""))
                {
                    getreviewtext="-";
                }

                GETREVIEW(getreviewnumber,getreviewtext);
                alertDialog.dismiss();
            }
        });



        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblack));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.mblack));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.white));

    }


    public void GETREVIEW(String getreviewnumber,String getreviewtext)
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type","product_rate_reviews");
            object.put("product_id",getIntent().getStringExtra("prodid"));
            JSONObject jsonObject2=new JSONObject();
            jsonObject2.put("action","add_rate_review");
            jsonObject2.put("user_id",PreferenceManager_ASOS.GetUserID());
            jsonObject2.put("rate",getreviewnumber);
            jsonObject2.put("review",getreviewtext);
            object.put("options",jsonObject2);

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(Product_Detail.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Just a minute ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();
                    String getresponce = response.replace("\"", "");

                    Log.e("GIVE_RATING_res", response);

                    try
                    {
                        JSONObject jsonObject=new JSONObject(response);
                        String status=jsonObject.getString("status");
                        Log.e("status", status);

                        if(status.equals("success"))
                        {
                            Snackbar snackbar = Snackbar.make(parentlo, "Thanks for the review....!!!", Snackbar.LENGTH_SHORT);
                            View snackBarView = snackbar.getView();
                            snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                            snackbar.show();
                        }

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("GIVE_RATING_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication",PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void SHARE_PROD()
    {
        sharelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                    if (checkPermissionREAD_EXTERNAL_STORAGE(Product_Detail.this))
                    {
                        // do your stuff..
                        uri=Uri.parse(path);

                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
                        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, " Hey,\n" +
                                "Checkout the details for " + product_name + " on "+getResources().getString(R.string.app_name)+" app.\n" +
                                "\n" +
                                "I have been using " +getResources().getString(R.string.app_name)+ " to buy all the daily essentials like Groceries, Vegetables, Fruits, Garments, Etc.\n" +
                                "\n" +
                                "Give it a try!\n" +
                                "\n" +
                                "https://www.searchus.in/"+getResources().getString(R.string.app_name)+"/"+pid);

                        sharingIntent.setType("image/*");
                        sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }

                }
//                String GET_Varient_Img=PreferenceManager_ASOS.getV_IMG();
//
//                if(Img.isEmpty()) //if VARIENT
//                {
//                    String imgpath=PreferenceManager_ASOS.getimage_path()+GET_Varient_Img;
//
//                }
//                else
//                {
//                    String imgpath=PreferenceManager_ASOS.getimage_path()+Img.get(0);
//
        });

    }

    public boolean checkPermissionREAD_EXTERNAL_STORAGE
            (
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage required", context,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[] { permission },
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults)
    {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    // do your stuff
//                    Toast.makeText(getApplicationContext(),"path="+path,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(Product_Detail.this, "Permission required",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions,
                        grantResults);
        }
    }

    public void TRIMMING_URL()
    {
        String SHARE_URL=shared_uri.toString();
        Log.e("SHARE_URL",SHARE_URL);

//        String after_cutting_productid = SHARE_URL.substring(0, path.lastIndexOf("/"));

//        productid = after_cutting_productid.substring(after_cutting_productid.lastIndexOf("/") + 1);
        productid = SHARE_URL.substring(SHARE_URL.lastIndexOf("/") + 1);

        Log.e("get_productid",productid);

        PROD_Detail__GETDATA();
        GET_RATING();
        SHARE_PROD();
    }

    @SuppressLint("ClickableViewAccessibility")
    public void ARRAYPASSING()
    {
        if(Img.isEmpty())
        {
            final ArrayList imgarray = new ArrayList<Object>(V_imgarray);

            vp_slider.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    Intent intent = new Intent(Product_Detail.this, Open_Image.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("getimgs", imgarray);
                    intent.putExtras(bundle);
                    startActivity(intent);

                    return false;
                }
            });
        }
        else if(varientArray.isEmpty())
        {
            final ArrayList imgarray = new ArrayList<Object>(Img);

            vp_slider.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    Intent intent = new Intent(Product_Detail.this, Open_Image.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("getimgs", imgarray);
                    intent.putExtras(bundle);
                    startActivity(intent);

                    return false;
                }
            });
        }

    }

    @Override
    public void onBackPressed()
    {
        if(PreferenceManager_ASOS.getcamefrom().equals("yes"))
        {
            Intent intent=new Intent(Product_Detail.this,Saved_Item.class);
            startActivity(intent);
            finish();
            PreferenceManager_ASOS.setcamefrom("no");
        }
        else if(PreferenceManager_ASOS.getcamefrom().equals("yesbag"))
        {
            Intent intent=new Intent(Product_Detail.this,Bag.class);
            startActivity(intent);
            finish();
            PreferenceManager_ASOS.setcamefrom("nobag");
        }
        else
        {
            PreferenceManager_ASOS.setcamefrom("nobag");
            PreferenceManager_ASOS.setcamefrom("no");
            super.onBackPressed();
            finish();
        }

    }


    @Override
    protected void onResume()
    {
        super.onResume();
        PROD_Detail__GETDATA();
    }
}
