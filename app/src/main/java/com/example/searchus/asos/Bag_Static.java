package com.example.searchus.asos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Impl.Main_impl;
import com.example.searchus.asos.MODELS.mycart;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Bag_Static extends AppCompatActivity
{
    private ArrayList<mycart> listcart = new ArrayList<mycart>();

    LinearLayout parentlo;
    RecyclerView Bag_RecyclerView;
    TextView totalitem,subtotal;
    View view;
    Button chechoutbt;

    PreferenceManager_ASOS PreferenceManager_ASOS;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_bag_static);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

        TOOLBAR();
        BINDING();
        CART_GETDATA();

    }

    public void TOOLBAR()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

    }

    public void BINDING()
    {
        Bag_RecyclerView = (RecyclerView) findViewById(R.id.bag_recycler_viewid);
        view=findViewById(R.id.view_id);
        parentlo=findViewById(R.id.parentlo_id);
        totalitem=findViewById(R.id.totalitemid);
        subtotal=findViewById(R.id.subtotalid);
        chechoutbt=findViewById(R.id.chechoutbtid);
    }

    public void CART_GETDATA()
    {
        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        Bag_RecyclerView.setLayoutManager(trending_LayoutManager);
        Bag_RecyclerView.setNestedScrollingEnabled(false);

        Main_impl impl = new Main_impl(Bag_Static.this);
        listcart = impl.getUser();

        double sum = 0;
        for (int i=0;i<listcart.size();i++)
        {
            sum = sum + Double.parseDouble(listcart.get(i).getAmount());
        }
        Log.e("ALL_AMOUNT:", String.valueOf(sum));

        subtotal.setText("₹ "+String.valueOf(sum));

        if(listcart.size()<2)
        {
            totalitem.setText("( "+listcart.size()+" item"+" )");

        }
        else
        {
            totalitem.setText("( "+listcart.size()+" items"+" )");

        }

        Bag_RecyclerView.setAdapter(new BAG_STATIC_ADP(getApplicationContext(),listcart,subtotal,totalitem));

    }


    public class BAG_STATIC_ADP extends RecyclerView.Adapter<BAG_STATIC_ADP.ViewHolder>
    {
        ArrayList<mycart> cartModelArrayList;
        Context context;
        private RequestQueue rq;
        private PreferenceManager_ASOS PreferenceManager_ASOS;
        TextView subtotal;
        TextView totalitem;

        public BAG_STATIC_ADP(Context context, ArrayList<mycart> cartModelArrayList,TextView subtotal,TextView totalitem)
        {
            super();
            this.context = context;
            this.cartModelArrayList = cartModelArrayList;
            rq= Volley.newRequestQueue(context);
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
            this.subtotal=subtotal;
            this.totalitem=totalitem;
        }


        @Override
        public BAG_STATIC_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_bag_static_lvitems, parent, false);
            BAG_STATIC_ADP.ViewHolder viewHolder = new BAG_STATIC_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final BAG_STATIC_ADP.ViewHolder holder, final int position)
        {


            holder.cart_pname.setText(cartModelArrayList.get(position).getProduct_name());
            holder.cart_p_price.setText("₹ "+cartModelArrayList.get(position).getPrice());
            holder.cart_p_price_2.setText("₹ "+cartModelArrayList.get(position).getAmount());
            holder.tv_Qty.setText(cartModelArrayList.get(position).getQty());



            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH+""+cartModelArrayList.get(position).getImg())
                    .into(holder.cart_p_img);



            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    if (!isLongClick)
                    {
                        Intent intent=new Intent(Bag_Static.this,Product_Detail.class);
                        intent.putExtra("prodid",cartModelArrayList.get(position).getProduct_id());
                        intent.putExtra("getnote",cartModelArrayList.get(position).getNote());
                        startActivity(intent);
                    }
                }
            });

        }

        @Override
        public int getItemCount()
        {
            return cartModelArrayList.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            ImageView cart_p_img;
            private TextView cart_pname,cart_p_price,cart_p_price_2,tv_Qty;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                cart_p_img = (ImageView) itemView.findViewById(R.id.cart_p_imgid);
                cart_pname = (TextView) itemView.findViewById(R.id.cart_pnameid);
                cart_p_price = (TextView) itemView.findViewById(R.id.cart_p_priceid);
                cart_p_price_2 = (TextView) itemView.findViewById(R.id.cart_p_priceid_2_id);
                tv_Qty = (TextView) itemView.findViewById(R.id.tv_Qtyid);

                itemView.setOnClickListener(this);

            }


            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view)
            {
                clickListener.onClick(view,getAdapterPosition(), false);
            }
        }

    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
}
