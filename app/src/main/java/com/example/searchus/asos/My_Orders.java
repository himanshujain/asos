package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.MODELS.GETSET_PRODLIST;
import com.example.searchus.asos.MODELS.My_Order_GETSET;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class My_Orders extends AppCompatActivity
{
    RecyclerView MyOrder_RecyclerView;
    LinearLayout empty_lo,main_lo,nonet_lo;
    Button empty_lo_add_addr_bt;
    TextView empty_msg;
    Toolbar toolbar;

    PreferenceManager_ASOS PreferenceManager_ASOS;

    ArrayList<My_Order_GETSET> gather_getset_prodlistArrayList=new ArrayList<>();
    ProgressBar loader;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    int IsFatchingData = 0;
    int offset = 0;
    int limit = 6;
    MyOrder_ADP product_listing_adapter;
    boolean cancel_progress=true;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_my__orders);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

        TOOLBAR();

        loader= (ProgressBar) findViewById(R.id.loaderid);
        empty_lo=findViewById(R.id.empty_loid);
        empty_msg=findViewById(R.id.empty_msg_id);
        main_lo=findViewById(R.id.main_lo_id);
        nonet_lo=findViewById(R.id.nonet_lo_id);
        empty_lo_add_addr_bt=findViewById(R.id.empty_lo_add_addr_btid);

        MyOrder_RecyclerView = (RecyclerView) findViewById(R.id.myorderrecycler_view);

//        //Calling Checking Internet connection function
//        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        registerReceiver(networkReceiver, intentFilter);

        MyOrder_RecyclerView.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        MyOrder_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
        product_listing_adapter=new MyOrder_ADP(getApplicationContext(),gather_getset_prodlistArrayList);
        MyOrder_RecyclerView.setAdapter(product_listing_adapter);

        MyOrder_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                totalItemCount = gaggeredGridLayoutManager.getItemCount();
                Log.e("totalItemCount", String.valueOf(totalItemCount));

                firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                int lastInScreen = firstVisibleItem + visibleItemCount;
                Log.e("lastInScreen", String.valueOf(lastInScreen));

                if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                {
                    loader.setVisibility(View.VISIBLE);
                    MY_ORDER_GETDATA();
                }
            }
        });

        MY_ORDER_GETDATA();

    }

    public void TOOLBAR()
    {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
                finish();
            }
        });
    }
    // Checking Internet connection function
    private BroadcastReceiver networkReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
            {
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED)
                {
//                    "We have internet connection. Good to go."

                    toolbar.setVisibility(View.VISIBLE);
                    nonet_lo.setVisibility(View.GONE);
                    main_lo.setVisibility(View.VISIBLE);

                    MyOrder_RecyclerView.setHasFixedSize(true);
                    gaggeredGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                    MyOrder_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
                    product_listing_adapter=new MyOrder_ADP(getApplicationContext(),gather_getset_prodlistArrayList);
                    MyOrder_RecyclerView.setAdapter(product_listing_adapter);

                    MyOrder_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                            Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                            totalItemCount = gaggeredGridLayoutManager.getItemCount();
                            Log.e("totalItemCount", String.valueOf(totalItemCount));

                            firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                            Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                            int lastInScreen = firstVisibleItem + visibleItemCount;
                            Log.e("lastInScreen", String.valueOf(lastInScreen));

                            if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                            {
                                loader.setVisibility(View.VISIBLE);
                                MY_ORDER_GETDATA();
                            }
                        }
                    });

                    MY_ORDER_GETDATA();

                }
                else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED)
                {
//                  "We have lost internet connection"

                    toolbar.setVisibility(View.GONE);
                    nonet_lo.setVisibility(View.VISIBLE);
                    main_lo.setVisibility(View.GONE);
                }
            }
        }
    };

    public void MY_ORDER_GETDATA()
    {
        IsFatchingData = 1;

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            JSONObject object=new JSONObject();
            object.put("type", "get_user_orders");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            JSONObject object2=new JSONObject() ;
            object.put("options", object2);
            object2.put("row_offset", offset);
            object2.put("row_count", limit);

            final String requestBody = object.toString();

            if(cancel_progress)
            {
                progressDialog=new ProgressDialog(My_Orders.this,R.style.ProgressDialogStyle);
                progressDialog.setTitle("Loading your orderlist");
                progressDialog.setMessage("Please wait ...");
                progressDialog.show();
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    loader.setVisibility(View.GONE);
                    ArrayList<My_Order_GETSET> dataArrayList=new ArrayList<>();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("MY_ORDER_res", "BLANK DATA");

                        if(offset==0)
                        {
                            main_lo.setVisibility(View.GONE);
                            empty_lo.setVisibility(View.VISIBLE);
                            empty_lo_add_addr_bt.setVisibility(View.GONE);
                            empty_msg.setText("Opps, it seems you haven't order any product...!!!");
                        }
                        else
                        {
                            empty_lo.setVisibility(View.GONE);
                            main_lo.setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        Log.e("MY_ORDER_res", response);

                        String product_id = null,product_name = null,variant_name=null,Quantity = null,Price = null,Amount = null,Saler_status = null,Customer_status = null;
                        ArrayList<String> listdata = new ArrayList<String>();
                        String status = null,time=null,notes=null;

                        //----------Parsing Starts------------//

                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONArray mainjsonArray=jsonObject.getJSONArray("data");
                            for (int a=0;a<mainjsonArray.length();a++)
                            {
                                JSONObject mainObj = mainjsonArray.getJSONObject(a);

                                String Order_ID = mainObj.getString("Order ID");
                                double Total_Amount = mainObj.getDouble("Total Amount");
                                String Payment_Type = mainObj.getString("Payment Type");
                                String Order_Date = mainObj.getString("Order Date");

                                JSONObject addrObj = mainObj.getJSONObject("Address");//New Address Array

                                String name = addrObj.getString("name");
                                String address = addrObj.getString("address");
                                String country = addrObj.getString("country");
                                String state = addrObj.getString("state");
                                String city = addrObj.getString("city");
                                String pincode = addrObj.getString("pincode");
                                String phone = addrObj.getString("phone");
                                String alternate_phone_no = addrObj.getString("alternate_phone_no");
                                String near_by = addrObj.getString("near_by");

                                JSONArray Prodlist_jsonArray = mainObj.getJSONArray("Product LIST");//New Product LIST Array
                                for (int b = 0; b < Prodlist_jsonArray.length(); b++)
                                {
                                    JSONObject Prodlist_mainObj = Prodlist_jsonArray.getJSONObject(b); //New Obj

                                    product_id = Prodlist_mainObj.getString("Product_id");
                                    product_name = Prodlist_mainObj.getString("Product_name");
                                    variant_name = Prodlist_mainObj.getString("variant_name");
                                    Quantity = Prodlist_mainObj.getString("Quantity");
                                    Price = Prodlist_mainObj.getString("Price");
                                    Amount = Prodlist_mainObj.getString("Amount");
                                    Saler_status = Prodlist_mainObj.getString("Saler_status");

                                    JSONArray Prods_img_jsonArray = Prodlist_mainObj.getJSONArray("Product_image");//New ProductImg Array
                                    for (int i = 0; i < Prods_img_jsonArray.length(); i++)
                                    {
                                        listdata.add(Prods_img_jsonArray.getString(i));
                                    }

                                    Customer_status = Prodlist_mainObj.getString("Customer_status");
                                }
                                String Prodlist_jsonArray_size= String.valueOf(Prodlist_jsonArray.length());

                                String current_status = mainObj.getString("current_status");
                                String current_time = mainObj.getString("current_time");
                                String current_notes = mainObj.getString("current_notes");

                                JSONArray Order_Tracking_jsonArray = mainObj.getJSONArray("Order_Tracking"); //New Order_Tracking Array
                                for (int b = 0; b < Order_Tracking_jsonArray.length(); b++)
                                {
                                    JSONObject Order_Tracking_mainObj = Order_Tracking_jsonArray.getJSONObject(b);
                                    status = Order_Tracking_mainObj.getString("status");
                                    time = Order_Tracking_mainObj.getString("time");
                                    notes = Order_Tracking_mainObj.getString("notes");
                                }

                                String shipping_charge = mainObj.getString("shipping_charge");

                                JSONObject extra_mainObj = mainObj.getJSONObject("extra"); //New extra Array

                                String discount = null;
                                String discount_amount=null;
                                boolean promo_code = false;

                                Object json = extra_mainObj.get("promo_code"); //Checking promo_code
                                if (json instanceof JSONObject) //if true and have inner Obj
                                {
                                    JSONObject promo_code_mainObj = extra_mainObj.getJSONObject("promo_code");

                                    discount = promo_code_mainObj.getString("discount");
                                    discount_amount= promo_code_mainObj.getString("discount_amount");
                                }
                                else //if false
                                {
                                    discount=null;
                                    discount_amount=null;
                                    promo_code = extra_mainObj.getBoolean("promo_code");
                                }

//                                JSONObject amount_mainObj = extra_mainObj.getJSONObject("amount");
//                                String total_amount = amount_mainObj.getString("total_amount");


                                String wallet_use = null;
                                String usable_amount = null;
                                boolean wallet_infos=false;

                                Object json1 =  extra_mainObj.get("wallet_infos"); //Checking wallet
                                if (json1 instanceof JSONObject)  //if true and have inner Obj
                                {
                                    JSONObject wallet_infos_mainObj = extra_mainObj.getJSONObject("wallet_infos");
                                    usable_amount = wallet_infos_mainObj.getString("usable_amount");
                                    wallet_use = wallet_infos_mainObj.getString("wallet_use");
                                }
                                else //if false
                                {
                                    wallet_use=null;
                                    usable_amount=null;
                                    wallet_infos = extra_mainObj.getBoolean("wallet_infos");
                                }

                                //----------Parsing Ends------------//

                                My_Order_GETSET my_order_getset = new My_Order_GETSET(Order_ID, Total_Amount, Payment_Type, Order_Date, name, address, country, state, city, pincode, phone, alternate_phone_no, near_by, product_id, product_name, variant_name, Quantity, Price, Amount, Saler_status, Customer_status, listdata, current_status, current_time, current_notes, status, time, notes, shipping_charge, discount, discount_amount, usable_amount, wallet_use,promo_code,wallet_infos,Prodlist_jsonArray_size);
                                dataArrayList.add(my_order_getset);
                            }

                            Log.e("My_Order size", String.valueOf(dataArrayList.size()));

                            //Loader code
                            int oldcount = gather_getset_prodlistArrayList.size();
                            for (int i = 0; i < dataArrayList.size(); i++)
                            {
                                offset++;
                                product_listing_adapter.add(dataArrayList.get(i));
                            }


                            product_listing_adapter.notifyDataSetChanged();
                            IsFatchingData = 0;

                            if (dataArrayList.size() % limit != 0 || oldcount == gather_getset_prodlistArrayList.size())
                            {
                                loader.setVisibility(View.GONE);
                            }

                            cancel_progress=false;
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }


                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("MY_ORDER error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


    }

    public class MyOrder_ADP extends RecyclerView.Adapter<MyOrder_ADP.ViewHolder>
    {
        ArrayList<My_Order_GETSET> arrayList;
        Context context;

        public MyOrder_ADP(Context context, ArrayList<My_Order_GETSET> arrayList)
        {
            super();
            this.context = context;
            this.arrayList = arrayList;
        }
        public void add(My_Order_GETSET country) {
            this.arrayList.add(country);
        }

        @Override
        public MyOrder_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_myorder_lvitems, parent, false);
            MyOrder_ADP.ViewHolder viewHolder = new MyOrder_ADP.ViewHolder(v);
            return viewHolder;
        }

        //Loader code

        @Override
        public void onBindViewHolder(MyOrder_ADP.ViewHolder holder, int position)
        {

            holder.tv_orderid.setText(arrayList.get(position).getOrder_ID());
//            Log.e("Order: ",arrayList.get(position).getOrder_ID());
            holder.tv_date.setText(arrayList.get(position).getOrder_Date());
            holder.tv_no_of_item.setText(arrayList.get(position).getProdlist_jsonArray_size());

            //settext of Current_status
            if(PreferenceManager_ASOS.get_isordercancel())
            {
                if(PreferenceManager_ASOS.getcancel_orderid().equals(arrayList.get(position).getOrder_ID()))
                {
                    holder.tv_status.setTextColor(context.getResources().getColor(R.color.red));
                    holder.tv_status.setTypeface(null, Typeface.BOLD);
                    holder.tv_status.setText(PreferenceManager_ASOS.getcancel_orderstatus());
                    arrayList.get(position).setCurrent_status(PreferenceManager_ASOS.getcancel_orderstatus());
                }
            }
            else
            {
                if(arrayList.get(position).getCurrent_status().equals("Decline")||arrayList.get(position).getCurrent_status().equals("Cancelled"))
                {
                    holder.tv_status.setTextColor(context.getResources().getColor(R.color.red));
                    holder.tv_status.setTypeface(null, Typeface.BOLD);
                }
                else
                {
                    holder.tv_status.setTextColor(context.getResources().getColor(R.color.mblue));
                    holder.tv_status.setTypeface(null, Typeface.BOLD);
                }
                holder.tv_status.setText(arrayList.get(position).getCurrent_status());
            }


//            if(arrayList.get(position).getCurrent_status().equals("Decline")||arrayList.get(position).getCurrent_status().equals("Cancelled"))
//            {
//                holder.tv_status.setTextColor(context.getResources().getColor(R.color.red));
//                holder.tv_status.setTypeface(null, Typeface.BOLD);
//            }
//            else
//            {
//                holder.tv_status.setTextColor(context.getResources().getColor(R.color.mblue));
//                holder.tv_status.setTypeface(null, Typeface.BOLD);
//            }
//            holder.tv_status.setText(arrayList.get(position).getCurrent_status());
            holder.tv_total.setText("₹ "+String.valueOf(arrayList.get(position).getTotal_Amount()));

            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        String getnote=arrayList.get(position).getCurrent_notes();
                        Log.e("Order_getnote: ",getnote);
                        Intent intent=new Intent(My_Orders.this,My_Order_Detail.class);
                        intent.putExtra("orderid",arrayList.get(position).getOrder_ID());
                        intent.putExtra("getnote",getnote);
                        startActivity(intent);
//                        finish();
                    }

                }
            });


        }

        @Override
        public int getItemCount()
        {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            public TextView tv_orderid,tv_date,tv_no_of_item,tv_status,tv_total;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);

                tv_orderid = (TextView) itemView.findViewById(R.id.tv_orderid_id);
                tv_date = (TextView) itemView.findViewById(R.id.tv_date_id);
                tv_no_of_item = (TextView) itemView.findViewById(R.id.tv_no_of_item_id);
                tv_status = (TextView) itemView.findViewById(R.id.tv_status_id);
                tv_total = (TextView) itemView.findViewById(R.id.tv_total_id);

                itemView.setOnClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view)
            {
                clickListener.onClick(view, getPosition(), false);
            }

        }

    }

//    @Override
//    protected void onDestroy()
//    {
//        super.onDestroy();
//
//        if (networkReceiver != null)
//        {
//            unregisterReceiver(networkReceiver);
//        }
//    }

    @Override
    protected void onResume()
    {
        super.onResume();
        product_listing_adapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        PreferenceManager_ASOS.set_isordercancel(false);
        finish();
    }
}
