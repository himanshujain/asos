package com.example.searchus.asos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class No_Net extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_no_net);
    }
}
