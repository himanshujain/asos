package com.example.searchus.asos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



import java.util.ArrayList;
import java.util.Arrays;

public class Product_Info extends AppCompatActivity
{
    TextView prod_name,tv_detail,tv_spec;
    CardView cv_desc_static,cv_spec_static;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_product__info);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               onBackPressed();
               finish();
            }
        });


        prod_name=findViewById(R.id.prod_nameid);
        tv_detail=findViewById(R.id.tv_detail_id);
        tv_spec=findViewById(R.id.tv_spec_id);
        cv_desc_static=findViewById(R.id.cv_desc_static_id);
        cv_spec_static=findViewById(R.id.cv_spec_static_id);

        String getproduct_name=getIntent().getStringExtra("product_name").toString();
        String getproduct_desc=getIntent().getStringExtra("product_desc").toString();
        String getproduct_spec=getIntent().getStringExtra("product_spec").toString();

        prod_name.setText(getproduct_name);

        if(getproduct_desc.equals("") || getproduct_desc.equals(null))
        {
            cv_desc_static.setVisibility(View.GONE);
            tv_detail.setVisibility(View.GONE);
        }
        else
        {
            cv_desc_static.setVisibility(View.VISIBLE);
            tv_detail.setVisibility(View.VISIBLE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
            {
                tv_detail.setText(Html.fromHtml(getproduct_desc,Html.FROM_HTML_MODE_LEGACY));
            }
            else
            {
                tv_detail.setText(Html.fromHtml(getproduct_desc));
            }
        }

        if(getproduct_spec.equals("") || getproduct_spec.equals(null))
        {
            cv_spec_static.setVisibility(View.GONE);
            tv_spec.setVisibility(View.GONE);
        }
        else
        {
            cv_spec_static.setVisibility(View.VISIBLE);
            tv_spec.setVisibility(View.VISIBLE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
            {
                tv_spec.setText(Html.fromHtml(getproduct_spec,Html.FROM_HTML_MODE_LEGACY));

            }
            else
            {
                tv_spec.setText(Html.fromHtml(getproduct_spec));
            }
        }

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }



}
