package com.example.searchus.asos;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.MODELS.MyOrder_Prod_Detail_Model;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class My_Order_Detail extends AppCompatActivity
{
    RecyclerView Orderdet_prodlist_RecyclerView;
    TextView tv_orderid,tv_addrname,tv_addr,tv_addrnearby,tv_addrcity,tv_addrpincode,tv_addrstate,tv_addrcountry,tv_addrphno;
    TextView tv_current_status,tv_Payment_Type,tv_date,tv_total_amount,tv_shipping_charge,tv_promo_code,tv_walletinfo,tv_totalAmount;
    ImageView ic_info;
    LinearLayout linearlayout,nonet_lo,layout_promocode,layout_wallet,disclo;
    RelativeLayout main_lo;
    Button cancelorder_bt;
    Toolbar toolbar;
    View view;

    String getnote;
    String current_notes;
    AlertDialog alertDialog;

    PreferenceManager_ASOS PreferenceManager_ASOS;
    ArrayList<MyOrder_Prod_Detail_Model>myOrder_prod_detail_modelArrayList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_my__order__detail);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

        TOOLBAR();
        BINDING();
        PreferenceManager_ASOS.set_isordercancel(false);

        cancelorder_bt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CANCEL_ORDER_DIALOGUE();
            }
        });

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);

    }

    // Checking Internet connection function
    private BroadcastReceiver networkReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
            {
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED)
                {
//                    "We have internet connection. Good to go."

                    toolbar.setVisibility(View.VISIBLE);
                    nonet_lo.setVisibility(View.GONE);
                    main_lo.setVisibility(View.VISIBLE);

                    MY_ORDER_DETAILS_GETDATA();

                }
                else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED)
                {
//                  "We have lost internet connection"

                    toolbar.setVisibility(View.GONE);
                    nonet_lo.setVisibility(View.VISIBLE);
                    main_lo.setVisibility(View.GONE);
                }
            }
        }
    };


    public void MY_ORDER_DETAILS_GETDATA()
    {
        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        Orderdet_prodlist_RecyclerView.setLayoutManager(trending_LayoutManager);
        Orderdet_prodlist_RecyclerView.setNestedScrollingEnabled(false);

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "get_user_orders");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            JSONObject jsonObject1=new JSONObject();
            JSONObject jsonObject2=new JSONObject();

            jsonObject2.put("order_id",getIntent().getStringExtra("orderid").toString());

            jsonObject1.put("filter", jsonObject2);

            object.put("options", jsonObject1);

            final String requestBody = object.toString();


            final ProgressDialog progressDialog=new ProgressDialog(My_Order_Detail.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading order details");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("ORDER Detail", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("ORDER Detail res", response);

                        String product_id = null,product_name = null,variant_name=null,Quantity = null,Price = null,Amount = null,Saler_status = null,Customer_status = null;
                        ArrayList<String> listdata = new ArrayList<String>();
                        listdata.clear();
                        myOrder_prod_detail_modelArrayList.clear();
                        String status = null,time=null,notes=null;

                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONObject mainObj=jsonObject.getJSONObject("data");

                            String Order_ID = mainObj.getString("Order ID");
                            double Total_Amount = mainObj.getDouble("Total Amount");
                            String Payment_Type = mainObj.getString("Payment Type");
                            String Order_Date = mainObj.getString("Order Date");

                            JSONObject addrObj = mainObj.getJSONObject("Address");
                            String name = addrObj.getString("name");
                            String address = addrObj.getString("address");
                            String country = addrObj.getString("country");
                            String state = addrObj.getString("state");
                            String city = addrObj.getString("city");
                            String pincode = addrObj.getString("pincode");
                            String phone = addrObj.getString("phone");
                            String alternate_phone_no = addrObj.getString("alternate_phone_no");
                            String near_by = addrObj.getString("near_by");

                            JSONArray Prodlist_jsonArray = mainObj.getJSONArray("Product LIST");
                            for (int b = 0; b < Prodlist_jsonArray.length(); b++)
                            {
                                JSONObject Prodlist_mainObj = Prodlist_jsonArray.getJSONObject(b);
                                product_id = Prodlist_mainObj.getString("Product_id");
                                product_name = Prodlist_mainObj.getString("Product_name");
                                variant_name = Prodlist_mainObj.getString("variant_name");
                                Quantity = Prodlist_mainObj.getString("Quantity");
                                Price = Prodlist_mainObj.getString("Price");
                                Amount = Prodlist_mainObj.getString("Amount");
                                Saler_status = Prodlist_mainObj.getString("Saler_status");
                                JSONArray Prods_img_jsonArray = Prodlist_mainObj.getJSONArray("Product_image");
                                for (int i = 0; i < Prods_img_jsonArray.length(); i++)
                                {
                                    listdata.add(Prods_img_jsonArray.getString(i));
                                }

                                Customer_status = Prodlist_mainObj.getString("Customer_status");

                                MyOrder_Prod_Detail_Model myOrder_prod_detail_model=new MyOrder_Prod_Detail_Model(product_id,product_name,variant_name,Quantity,Price,Amount,Saler_status,Customer_status,listdata);
                                myOrder_prod_detail_modelArrayList.add(myOrder_prod_detail_model);
                            }

                            String Prodlist_jsonArray_size= String.valueOf(Prodlist_jsonArray.length());

                            String current_status = mainObj.getString("current_status");
                            String current_time = mainObj.getString("current_time");
                            current_notes = mainObj.getString("current_notes");

                            JSONArray Order_Tracking_jsonArray = mainObj.getJSONArray("Order_Tracking");
                            for (int b = 0; b < Order_Tracking_jsonArray.length(); b++)
                            {
                                JSONObject Order_Tracking_mainObj = Order_Tracking_jsonArray.getJSONObject(b);
                                status = Order_Tracking_mainObj.getString("status");
                                time = Order_Tracking_mainObj.getString("time");
                                notes = Order_Tracking_mainObj.getString("notes");
                            }

                            String shipping_charge = mainObj.getString("shipping_charge");

                            JSONObject extra_mainObj = mainObj.getJSONObject("extra");

                            String discount = null;
                            String discount_amount="";
                            boolean promo_code = false;

                            Object json = extra_mainObj.get("promo_code");
                            if (json instanceof JSONObject)
                            {
                                JSONObject promo_code_mainObj = extra_mainObj.getJSONObject("promo_code");

                                discount = promo_code_mainObj.getString("discount");
                                discount_amount= promo_code_mainObj.getString("discount_amount");//



                                layout_promocode.setVisibility(View.VISIBLE);
                                tv_promo_code.setText("₹ "+discount_amount);
                            }
                            else
                            {
                                discount=null;
                                discount_amount="";
                                promo_code = extra_mainObj.getBoolean("promo_code");

                                layout_promocode.setVisibility(View.GONE);
                            }

                            JSONObject amount_mainObj = extra_mainObj.getJSONObject("amount");
                            String total_amount = amount_mainObj.getString("total_amount");


                            String wallet_use = null;
                            String usable_amount = "";
                            boolean wallet_infos=false;

                            Object json1 =  extra_mainObj.get("wallet_infos");
                            if (json1 instanceof JSONObject)
                            {
                                JSONObject wallet_infos_mainObj = extra_mainObj.getJSONObject("wallet_infos");
                                usable_amount = wallet_infos_mainObj.getString("usable_amount");
                                wallet_use = wallet_infos_mainObj.getString("wallet_use");//



                                layout_wallet.setVisibility(View.VISIBLE);
                                tv_walletinfo.setText("₹ "+usable_amount);
                            }
                            else
                            {
                                wallet_use=null;
                                usable_amount="";
                                wallet_infos = extra_mainObj.getBoolean("wallet_infos");

                                layout_wallet.setVisibility(View.GONE);
                            }


                            Orderdet_prodlist_RecyclerView.setAdapter(new Order_detail_prod_ADP(getApplicationContext(),myOrder_prod_detail_modelArrayList));


                            tv_orderid.setText(Order_ID);
                            tv_addrname.setText(name);
                            tv_addr.setText(address);
                            tv_addrnearby.setText(near_by);
                            tv_addrcity.setText(city);
                            tv_addrpincode.setText(pincode);
                            tv_addrstate.setText(state);
                            tv_addrcountry.setText(country);
                            tv_addrphno.setText(phone);

                            COLOR_CHANGE_LOGIC(current_status);
                            CANCEL_ORDER_BT_LOGIC(current_status);


                            tv_Payment_Type.setText(Payment_Type);
                            tv_date.setText(Order_Date);
                            tv_total_amount.setText("₹ "+total_amount);
                            tv_shipping_charge.setText("₹ "+shipping_charge);

                            tv_totalAmount.setText(String.valueOf("₹ "+Total_Amount));


                            HIDELOGIC(discount_amount,usable_amount);
                            GETNOTE();
                        }

                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }


                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("ORDER Detail error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void HIDELOGIC(String discount_amount,String usable_amount)
    {
        if(discount_amount.equals("") && usable_amount.equals(""))
        {
            disclo.setVisibility(View.GONE);
        }
        else
        {
            disclo.setVisibility(View.VISIBLE);
        }
    }

    public void COLOR_CHANGE_LOGIC(String current_status)
    {
        if(current_status.equals("Decline")||current_status.equals("Cancelled"))
        {
            tv_current_status.setTextColor(getApplicationContext().getResources().getColor(R.color.mred));
            tv_current_status.setTypeface(null, Typeface.BOLD);
            tv_current_status.setText(current_status);

        }
        else
        {
            tv_current_status.setTextColor(getApplicationContext().getResources().getColor(R.color.mblue));
            tv_current_status.setTypeface(null, Typeface.BOLD);
            tv_current_status.setText(current_status);

        }
    }


    public void TOOLBAR()
    {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
                finish();
            }
        });
    }

    public void BINDING()
    {
        tv_orderid=findViewById(R.id.tv_orderid_idid);
        tv_addrname=findViewById(R.id.tv_addrnameid);
        tv_addr=findViewById(R.id.tv_addrid);
        tv_addrnearby=findViewById(R.id.tv_addrnearbyid);
        tv_addrcity=findViewById(R.id.tv_addrcityid);
        tv_addrpincode=findViewById(R.id.tv_addrpincodeid);
        tv_addrstate=findViewById(R.id.tv_addrstateid);
        tv_addrcountry=findViewById(R.id.tv_addrcountryid);
        tv_addrphno=findViewById(R.id.tv_addrphnoid);

        tv_current_status=findViewById(R.id.tv_current_statusiid);
        tv_Payment_Type=findViewById(R.id.tv_Payment_Typeid);
        tv_date=findViewById(R.id.tv_date_id);
        tv_total_amount=findViewById(R.id.tv_total_amountid);
        tv_shipping_charge=findViewById(R.id.tv_shipping_chargeid);
        tv_promo_code=findViewById(R.id.tv_promo_codeid);
        tv_walletinfo=findViewById(R.id.tv_walletinfoid);
        tv_totalAmount=findViewById(R.id.tv_totalAmountid);
        linearlayout=findViewById(R.id.linearlayoutid);
        cancelorder_bt=findViewById(R.id.cancelorder_btid);

        main_lo=findViewById(R.id.main_lo_id);
        nonet_lo=findViewById(R.id.nonet_lo_id);

        ic_info=findViewById(R.id.ic_info_id);

        layout_promocode=findViewById(R.id.layout_promocodeid);
        layout_wallet=findViewById(R.id.layout_walletid);
        disclo=findViewById(R.id.discloid);

        view=findViewById(R.id.viewid);

        Orderdet_prodlist_RecyclerView = (RecyclerView) findViewById(R.id.orderdetail_prodlist_recycler_view);
    }

    public void GETNOTE()
    {
        getnote=current_notes;
        if(getnote.equals(""))
        {
            ic_info.setVisibility(View.GONE);
        }
        else
        {
            ic_info.setVisibility(View.VISIBLE);
            ic_info.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    CALL_SNAKEBAR(getnote);
                }
            });
        }

    }

    public void CALL_SNAKEBAR(String getnote)
    {
        // Create the Snackbar
        Snackbar snackbar = Snackbar.make(linearlayout, "", Snackbar.LENGTH_LONG);
        // Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();

        // Inflate our custom view
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View snackView = inflater.inflate(R.layout.asos_snakebar_view, null);

        // Configure the view
        TextView textViewTop = (TextView) snackView.findViewById(R.id.tv_ordernote_id);
        textViewTop.setText(getnote);

        //If the view is not covering the whole snackbar layout, add this line
        layout.setPadding(0,0,0,0);

        // Add the view to the Snackbar's layout
        layout.addView(snackView, 0);

        // Show the Snackbar
        snackbar.show();
    }

    public void CANCEL_ORDER_BT_LOGIC(String current_status)
    {
        if(current_status.equals("Pending")||current_status.equals("Approve")||current_status.equals("Processing"))
        {
            cancelorder_bt.setVisibility(View.VISIBLE);
        }
        else
        {
            cancelorder_bt.setVisibility(View.GONE);
        }

    }

    private void CANCEL_ORDER_DIALOGUE()
    {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.asos_cancel_order_detail_dialogue, null);
        dialogBuilder.setCancelable(true);

        final EditText edcancelnote=view.findViewById(R.id.edcancelnote_id);
        final Button cancelorder_dialoguebt=view.findViewById(R.id.cancelorder_dialoguebtid);

        cancelorder_dialoguebt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(edcancelnote.length()==0)
                {
                    edcancelnote.requestFocus();
                    edcancelnote.setError("Please enter your reason for cancelling...");
                }
                else
                {
                    String getorderid=tv_orderid.getText().toString();
                    String getnote=edcancelnote.getText().toString();

                    CANCEL_ORDER_GETDATA(getorderid,getnote);
                    alertDialog.dismiss();
                }

            }
        });

        dialogBuilder.setView(view);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public void CANCEL_ORDER_GETDATA(final String getorderid, String getnote)
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "get_user_orders");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            JSONObject jsonObject1=new JSONObject();
            object.put("options", jsonObject1);
            jsonObject1.put("order_id", getorderid);
            jsonObject1.put("action", "cancel_order");
            jsonObject1.put("description", getnote);

            final String requestBody = object.toString();


            final ProgressDialog progressDialog=new ProgressDialog(My_Order_Detail.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Cancelling your order");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");

                    try
                    {
                        JSONObject jsonObject=new JSONObject(response);
                        String status=jsonObject.getString("status");
                        if(status.equals("Done"))
                        {
                           Simple_Dialog("Order Cancelled","Your order has been cancelled successfully",R.drawable.ic_tick);
                            ic_info.setVisibility(View.VISIBLE);

                            PreferenceManager_ASOS.set_isordercancel(true);
                            PreferenceManager_ASOS.setcancel_orderid(getorderid);
                            PreferenceManager_ASOS.setcancel_orderstatus("Cancelled");}
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("CANCEL_ORDER_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    private void Simple_Dialog(String title, String message, int icon)
    {

        final android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);

        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

        // Initialize a new spannable string builder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(title);

        // Apply the text color span
        ssBuilder.setSpan(
                foregroundColorSpan,
                0,
                title.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        dialogBuilder.setTitle(ssBuilder);
        dialogBuilder.setMessage(message);
        dialogBuilder.setIcon(icon);
        dialogBuilder.setCancelable(false);


        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();


//                Intent intent=new Intent(My_Order_Detail.this,My_Orders.class);
//                startActivity(intent);
//                finish();
                myOrder_prod_detail_modelArrayList.clear();
                MY_ORDER_DETAILS_GETDATA();
        }
        });


        android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblue));
        alertDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
    }

    public class Order_detail_prod_ADP extends RecyclerView.Adapter<Order_detail_prod_ADP.ViewHolder>
    {
        ArrayList<MyOrder_Prod_Detail_Model> myOrder_prod_detail_modelArrayList;
        Context context;

        public Order_detail_prod_ADP(Context context, ArrayList<MyOrder_Prod_Detail_Model> myOrder_prod_detail_modelArrayList)
        {
            super();
            this.context = context;
            this.myOrder_prod_detail_modelArrayList = myOrder_prod_detail_modelArrayList;
        }

        @Override
        public Order_detail_prod_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_orderdetail_prodlist_lvitems, parent, false);
            Order_detail_prod_ADP.ViewHolder viewHolder = new Order_detail_prod_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(Order_detail_prod_ADP.ViewHolder holder, int position)
        {
            holder.tvproductname.setText(myOrder_prod_detail_modelArrayList.get(position).getProduct_name());
            holder.tv_price.setText("₹ "+myOrder_prod_detail_modelArrayList.get(position).getPrice());
            holder.tv_Qty.setText(myOrder_prod_detail_modelArrayList.get(position).getQuantity());
            Log.e("QTY",myOrder_prod_detail_modelArrayList.get(position).getQuantity());

            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH + "" + myOrder_prod_detail_modelArrayList.get(position).getProds_img_jsonArray().get(position))
                    .into(holder.prod_img);


            holder.setClickListener(new ItemClickListener()
            {

                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        Intent intent=new Intent(My_Order_Detail.this,Product_Detail.class);
                        intent.putExtra("prodid",myOrder_prod_detail_modelArrayList.get(position).getProduct_id());
                        intent.putExtra("getnote","");
                        startActivity(intent);
                    }
                }
            });


        }

        @Override
        public int getItemCount()
        {
            return myOrder_prod_detail_modelArrayList.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
        {

            public ImageView prod_img;
            public TextView tvproductname,tv_price,tv_Qty;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                prod_img = (ImageView) itemView.findViewById(R.id.prod_img_id);
                tvproductname = (TextView) itemView.findViewById(R.id.tvproductnameid);
                tv_price = (TextView) itemView.findViewById(R.id.tv_price_id);
                tv_Qty = (TextView) itemView.findViewById(R.id.tv_Qty_id);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

            @Override
            public boolean onLongClick(View view)
            {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }
        }

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (networkReceiver != null)
        {
            unregisterReceiver(networkReceiver);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
//        finish();
//        Intent intent=new Intent(My_Order_Detail.this,My_Orders.class);
//        startActivity(intent);
        finish();
    }
}
