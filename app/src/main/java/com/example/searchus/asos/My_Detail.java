package com.example.searchus.asos;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import studio.carbonylgroup.textfieldboxes.ExtendedEditText;

public class My_Detail extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    ExtendedEditText name, contnumb, alternatenumb, emailid, dob;
    RadioGroup radiogroup_gender;
    RadioButton radiobt_girl, radiobt_guy;
    RadioButton radiobt_gender;
    Button update_profilebt, logout_googlesignin;
    LinearLayout parentlo;

    LinearLayout nonet_lo, emailidlo;
    ScrollView mainlo;
    Toolbar toolbar;

    PreferenceManager_ASOS PreferenceManager_ASOS;
    RequestQueue rq;
    int index;

    private SignInButton signInButton;
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    String google_emailid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_my__detail);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        PreferenceManager_ASOS = new PreferenceManager_ASOS(getApplicationContext());

        TOOLBAR();
        BINDING();
        GOOGLESIGNIN();
        DISABLE_UPDATE_BUTTON();
        DATE_PICKER();
        UPDATE_PROFILE_CHECK();

        update_profilebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UPDATE_PROFILE();
            }
        });

        logout_googlesignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LOGOUT_GOOGLESIGNIN();
            }
        });

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);

    }



    // Checking Internet connection function
    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
//                    "We have internet connection. Good to go."

                    parentlo.setBackgroundColor(getResources().getColor(R.color.lightgray));
                    toolbar.setVisibility(View.VISIBLE);
                    nonet_lo.setVisibility(View.GONE);
                    mainlo.setVisibility(View.VISIBLE);
                } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
//                  "We have lost internet connection"

                    parentlo.setBackgroundColor(getResources().getColor(R.color.white));
                    toolbar.setVisibility(View.GONE);
                    nonet_lo.setVisibility(View.VISIBLE);
                    mainlo.setVisibility(View.GONE);
                }
            }
        }
    };

    public void BINDING() {
        name = findViewById(R.id.name_id);
        contnumb = findViewById(R.id.contnumb_id);
        contnumb.setRawInputType(Configuration.KEYBOARD_12KEY);
        alternatenumb = findViewById(R.id.alternatenumb_id);
        alternatenumb.setRawInputType(Configuration.KEYBOARD_12KEY);
        emailid = findViewById(R.id.emailid_id);
        dob = findViewById(R.id.dob_id);
        parentlo = findViewById(R.id.parentlo_id);
        update_profilebt = findViewById(R.id.update_profilebt_id);
        radiogroup_gender = findViewById(R.id.radiogroup_gender_id);
        radiobt_girl = findViewById(R.id.radiobt_girl_id);
        radiobt_guy = findViewById(R.id.radiobt_guy_id);
        logout_googlesignin = findViewById(R.id.logout_googlesignin_id);
        nonet_lo = findViewById(R.id.nonet_lo_id);
        mainlo = findViewById(R.id.mainlo_id);
        emailidlo = findViewById(R.id.emailidloid);
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);

        SETTEXT();
    }

    public void TOOLBAR() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    public void SETTEXT() {
        name.setText(PreferenceManager_ASOS.GetUser_NAME());
        contnumb.setText(PreferenceManager_ASOS.getprimary_number());
        alternatenumb.setText(PreferenceManager_ASOS.getalternate_no());
        emailid.setText(PreferenceManager_ASOS.getemail());
        dob.setText(PreferenceManager_ASOS.getbirthdate());

        if (PreferenceManager_ASOS.getgender().equals("Girl")) {
            radiobt_girl.setChecked(true);
            radiobt_guy.setChecked(false);
        } else if (PreferenceManager_ASOS.getgender().equals("Guy")) {
            radiobt_girl.setChecked(false);
            radiobt_guy.setChecked(true);
        }
    }

    public void DISABLE_UPDATE_BUTTON() {
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                update_profilebt.setEnabled(true);
                update_profilebt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        alternatenumb.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                update_profilebt.setEnabled(true);
                update_profilebt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        emailid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                update_profilebt.setEnabled(true);
                update_profilebt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                update_profilebt.setEnabled(true);
                update_profilebt.setBackgroundColor(getResources().getColor(R.color.mblack));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //checking any radiobt is checked(selected)
        radiogroup_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                View radioButton = radioGroup.findViewById(i);
                index = radioGroup.indexOfChild(radioButton);

                if (radiobt_guy.isChecked() || radiobt_girl.isChecked()) {
                    update_profilebt.setEnabled(true);
                    update_profilebt.setBackgroundColor(getResources().getColor(R.color.mblack));
                }
            }
        });

    }

    public void DATE_PICKER() {
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mYear, mMonth, mDay;

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(My_Detail.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                dob.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //        gotoCheckPermissions();
        Bitmap photo = null;
        if (requestCode == RC_SIGN_IN)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }

    }

    private void handleSignInResult(GoogleSignInResult result)
    {
        //If the login succeed
        Log.e("result", String.valueOf(result));
        if (result.isSuccess())
        {
            //Getting google account
            final GoogleSignInAccount acct = result.getSignInAccount();

            // Displaying name and email
            String name = acct.getDisplayName();
            google_emailid = acct.getEmail();

            Log.e("DETAILS_GMAIL", "Name: " + name + " Email: " + google_emailid);

            UPDATE_PROFILE();
        }
        else
        {
            google_emailid = "";
            //If login fails
        }
    }


    public void UPDATE_PROFILE()
    {
        //getText of Radio button code
        int selectedId = radiogroup_gender.getCheckedRadioButtonId();
        radiobt_gender = (RadioButton) findViewById(selectedId);
        final String getgender = String.valueOf(radiobt_gender.getText());


        String getname = name.getText().toString();
        String getalternatenumb = alternatenumb.getText().toString();
        String getemailid = emailid.getText().toString();
        String getdob = dob.getText().toString();
        ;

        rq = Volley.newRequestQueue(this);

        try {
            JSONObject object = new JSONObject();
            object.put("type", "user_management_process");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            object.put("action", "update_info");

            JSONObject object1 = new JSONObject();
            object.put("info", object1);
            object1.put("alternate_no", getalternatenumb);
            object1.put("profile_img", "");
            object1.put("name", getname);
            object1.put("email_id", google_emailid);
            object1.put("birthdate", getdob);
            object1.put("gender", getgender);
            object1.put("city", "");

            final ProgressDialog progressDialog = new ProgressDialog(My_Detail.this, R.style.ProgressDialogStyle);
            progressDialog.setTitle("Saving changes");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Update_Profile_res", String.valueOf(response));

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        if (jsonObject.getString("status").equals("Success")) {
                            String user_id = String.valueOf(jsonObject.getString("user_id"));
                            String email = String.valueOf(jsonObject.getString("email_id"));
                            String birthdate = String.valueOf(jsonObject.getString("birthdate"));
                            String username = String.valueOf(jsonObject.getString("name"));
                            String alternate_no = String.valueOf(jsonObject.getString("alternate_no"));
                            String gender = String.valueOf(jsonObject.getString("gender"));
                            String primary_number = String.valueOf(jsonObject.getString("primary_number"));

                            PreferenceManager_ASOS.SetUserID(user_id);
                            PreferenceManager_ASOS.setprimary_number(primary_number);
                            PreferenceManager_ASOS.SetUser_NAME(username);
                            PreferenceManager_ASOS.setemail(email);
                            PreferenceManager_ASOS.setalternate_no(alternate_no);
                            PreferenceManager_ASOS.setgender(gender);
                            if (birthdate.equals(null))
                            {
                                PreferenceManager_ASOS.setbirthdate("");

                            }
                            else
                            {
                                PreferenceManager_ASOS.setbirthdate(birthdate);

                            }

                            name.setText(username);
                            contnumb.setText(primary_number);
                            alternatenumb.setText(alternate_no);
                            emailid.setText(email);
                            dob.setText(birthdate);

                            Snackbar snackbar = Snackbar
                                    .make(parentlo, "Updated successfully...!!!", Snackbar.LENGTH_LONG);
                            View snackBarView = snackbar.getView();
                            snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                            snackbar.show();

                            if (email.equals("") || email.equals(null))
                            {
                                emailidlo.setVisibility(View.GONE);
                                PreferenceManager_ASOS.setemail("");
                                TextView textView = (TextView) signInButton.getChildAt(0);
                                textView.setText("Sign in");
                            }
                            else
                            {
                                emailidlo.setVisibility(View.VISIBLE);
                                TextView textView = (TextView) signInButton.getChildAt(0);
                                textView.setText("Change");
                                GMAIL_VERIFY();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    Log.e("Update_Profile_error", String.valueOf(error));

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(PaperCart_HEADER.Contenttype, PaperCart_HEADER.Appjson);
                    params.put(PaperCart_HEADER.Auth, PaperCart_HEADER.KEY);

                    return params;
                }
            };

            rq.add(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void UPDATE_PROFILE_CHECK()
    {
        if(PreferenceManager_ASOS.getemail().equals(""))
        {
            emailidlo.setVisibility(View.GONE);
            PreferenceManager_ASOS.setemail("");
            TextView textView = (TextView) signInButton.getChildAt(0);
            textView.setText("Sign in");
        }
        else
        {
            emailidlo.setVisibility(View.VISIBLE);
            TextView textView = (TextView) signInButton.getChildAt(0);
            textView.setText("Change");
        }

    }

    public void GMAIL_VERIFY()
    {
        String URL="http://api.searchus.in/API/ecommerce/email/validation";

        JSONObject object = new JSONObject();

        try
        {
            JSONObject jobj = new JSONObject();
            JSONObject jobj3 = new JSONObject();

            jobj3.put("user_id", PreferenceManager_ASOS.GetUserID());
            jobj3.put("media_type", "1");
            jobj3.put("media_value", google_emailid);
            jobj3.put("category", "1");
            jobj.put("input", jobj3);
            object.put("options", jobj);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final ProgressDialog progressDialog = new ProgressDialog(My_Detail.this, R.style.ProgressDialogStyle);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait ...");
        progressDialog.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                progressDialog.dismiss();
                Log.e("VERIFY_GMAIL_res", String.valueOf(response));

                try
                {
                    JSONObject  jObj = new JSONObject(String.valueOf(response));
                    String arrobj = jObj.getString("code");
                    String arrmsg = jObj.getString("message");
                    if (arrobj.equals("000200"))
                    {
//                        Toast.makeText(getApplicationContext(), "VERIFIED"+google_emailid, Toast.LENGTH_LONG).show();
                    }
                    else
                    {
//                        Toast.makeText(getApplicationContext(), "UN-VERIFIED"+google_emailid, Toast.LENGTH_LONG).show();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();

                Log.e("VERIFY_GMAIL_error", String.valueOf(error));

            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(PaperCart_HEADER.Contenttype, PaperCart_HEADER.Appjson);
                params.put(PaperCart_HEADER.Auth, PaperCart_HEADER.KEY);

                return params;
            }
        };

        rq.add(jsonObjectRequest);
        }

    private void LOGOUT_GOOGLESIGNIN()
    {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>()
                {
                    @Override
                    public void onResult(Status status)
                    {
                        Snackbar snackbar = Snackbar
                                .make(parentlo, "Done successfully...!!!", Snackbar.LENGTH_LONG);
                        View snackBarView = snackbar.getView();
                        snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                        snackbar.show();
                    }
                });

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (networkReceiver != null)
        {
            unregisterReceiver(networkReceiver);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {

    }

    public void GOOGLESIGNIN()
    {
        //request email
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        //api
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();




        //signin
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                LOGOUT_GOOGLESIGNIN();

                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
    }
}
