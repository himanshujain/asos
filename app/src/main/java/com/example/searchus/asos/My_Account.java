package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.searchus.asos.Classes.BlurBuilder;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;

public class My_Account extends AppCompatActivity
{

    TextView mydetail,addrbook,tvmyorder,signout;
    PreferenceManager_ASOS PreferenceManager_ASOS;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_my__account);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

        TOOLBAR();
        BLUR_IMG();
        ON_CLICKS();
        SIGNOUT();
    }

    public void TOOLBAR()
    {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               onBackPressed();
//        Intent intent=new Intent(getApplicationContext(),Home.class);
//        startActivity(intent);
                finish();
            }
        });

    }

    public void BLUR_IMG()
    {
        ImageView imageView=findViewById(R.id.imageView);
        Bitmap resultBmp = BlurBuilder.blur(this, BitmapFactory.decodeResource(getResources(), R.drawable.cover_img));
        imageView.setImageBitmap(resultBmp);
    }

    public void ON_CLICKS()
    {
        tvmyorder=findViewById(R.id.tvmyorderid);
        tvmyorder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(My_Account.this,My_Orders.class);
                startActivity(intent);
//                finish();
            }
        });

        mydetail=findViewById(R.id.mydetailid);
        mydetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(My_Account.this,My_Detail.class);
                startActivity(intent);
            }
        });

        addrbook=findViewById(R.id.tvaddrbookid);
        addrbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(My_Account.this,My_Address.class);
//                PreferenceManager_ASOS.setcamefrom("My_Account");
//                intent.putExtra("from intent","My_Account");
                startActivity(intent);
            }
        });
    }

    public void SIGNOUT()
    {
        signout=findViewById(R.id.signout_id);

        signout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                final ProgressDialog progress = new ProgressDialog(My_Account.this);
                progress.setTitle("Signing out");
                progress.setMessage("Please wait...");
                progress.show();

                Thread timer = new Thread()
                {
                    public void run()
                    {
                        try
                        {
                            sleep(1000);
                            progress.cancel();
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                        finally
                        {
                            Intent i = new Intent(My_Account.this, Home.class);
//                            i.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                            startActivity(i);
                            finish();
                            PreferenceManager_ASOS.SetUserID("");
                        }

                    }

                };
                timer.start();
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
//        Intent intent=new Intent(getApplicationContext(),Home.class);
//        startActivity(intent);
        finish();
    }

}
