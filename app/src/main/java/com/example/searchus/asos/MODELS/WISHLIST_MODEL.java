package com.example.searchus.asos.MODELS;

import java.io.Serializable;
import java.util.ArrayList;

public class WISHLIST_MODEL implements Serializable
{
    public WISHLIST_MODEL(String product_id, String product_spec, String product_name, String product_price, String product_desc, String product_keywords, String quantity, String final_Price, ArrayList<Pop_PROD_Varient_MODEL> variant, ArrayList<String> img, String folder_name, String folder_img, String avalibility, String purchase_price, String product_code, String notes, String extra_1, String extra_2) {
        this.product_id = product_id;
        this.product_spec = product_spec;
        this.product_name = product_name;
        this.product_price = product_price;
        this.product_desc = product_desc;
        this.product_keywords = product_keywords;
        this.quantity = quantity;
        Final_Price = final_Price;
        this.variant = variant;
        this.img = img;
        this.folder_name = folder_name;
        this.folder_img = folder_img;
        this.avalibility = avalibility;
        this.purchase_price = purchase_price;
        this.product_code = product_code;
        this.notes = notes;
        this.extra_1 = extra_1;
        this.extra_2 = extra_2;
    }

    String product_id,
            product_spec,
            product_name,
            product_price,
            product_desc,
            product_keywords,
            quantity,
            Final_Price;
    private ArrayList<Pop_PROD_Varient_MODEL> variant;
    private ArrayList<String> img;
    String folder_name,
            folder_img,
            avalibility,
            purchase_price,
            product_code,
            notes,
            extra_1,
            extra_2;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_spec() {
        return product_spec;
    }

    public void setProduct_spec(String product_spec) {
        this.product_spec = product_spec;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_desc() {
        return product_desc;
    }

    public void setProduct_desc(String product_desc) {
        this.product_desc = product_desc;
    }

    public String getProduct_keywords() {
        return product_keywords;
    }

    public void setProduct_keywords(String product_keywords) {
        this.product_keywords = product_keywords;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getFinal_Price() {
        return Final_Price;
    }

    public void setFinal_Price(String final_Price) {
        Final_Price = final_Price;
    }

    public ArrayList<Pop_PROD_Varient_MODEL> getVariant() {
        return variant;
    }

    public void setVariant(ArrayList<Pop_PROD_Varient_MODEL> variant) {
        this.variant = variant;
    }

    public ArrayList<String> getImg() {
        return img;
    }

    public void setImg(ArrayList<String> img) {
        this.img = img;
    }

    public String getFolder_name() {
        return folder_name;
    }

    public void setFolder_name(String folder_name) {
        this.folder_name = folder_name;
    }

    public String getFolder_img() {
        return folder_img;
    }

    public void setFolder_img(String folder_img) {
        this.folder_img = folder_img;
    }

    public String getAvalibility() {
        return avalibility;
    }

    public void setAvalibility(String avalibility) {
        this.avalibility = avalibility;
    }

    public String getPurchase_price() {
        return purchase_price;
    }

    public void setPurchase_price(String purchase_price) {
        this.purchase_price = purchase_price;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getExtra_1() {
        return extra_1;
    }

    public void setExtra_1(String extra_1) {
        this.extra_1 = extra_1;
    }

    public String getExtra_2() {
        return extra_2;
    }

    public void setExtra_2(String extra_2) {
        this.extra_2 = extra_2;
    }
}
