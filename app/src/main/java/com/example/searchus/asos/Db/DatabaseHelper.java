package com.example.searchus.asos.Db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.searchus.asos.Impl.Main_impl;


/**
 * Created by DELL on 03/24/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Login.db";
    public static final int DATABASE_VERSION = 2;
    public static final String TABLE_NAME = "Details";
    public static final String TABLE_NAME1 = "Details2";
    public static final String TABLE_NAME2 = "Details3";
    public static final String COLUMN_VARIANT = "variant";
    private Context context;
    private static DatabaseHelper databaseHelper;


    public static final String DATABASE_CREATE = "Create table "
            + TABLE_NAME
            + "(id integer primary key autoincrement, "
            + "product_id text,product_name text,product_price text,product_desc text, "
            + "product_spec text, "
            + "discount text,avalibility text,img text,qty text,amount text,stts text,availqty text,variant text,note text, "
            + " UNIQUE (id) ON CONFLICT REPLACE)";

//    public static final String DATABASE_CREATE1 = "Create table " + TABLE_NAME1
//            + " (id integer primary key autoincrement, "
//            + "Name Text,Address Text,Number Text,Country Text,State Text,City Text,Nearby Text,Zipcode Text,Alternate Text,AddId Text, "
//            + " UNIQUE (id) ON CONFLICT REPLACE)";
//
//    public static final String DATABASE_CREATE2 = "Create table "
//            + TABLE_NAME2
//            + "(id integer primary key autoincrement, "
//            + "product_id text,product_name text,product_price text,product_desc text, "
//            + "product_spec text, "
//            + "discount text,avalibility text,status text,img text,qty text,amount text,stts text, "
//            + " UNIQUE (id) ON CONFLICT REPLACE)";
//
//    private static final String DATABASE_ALTER_CART_1 = "ALTER TABLE "
//            + TABLE_NAME + " ADD COLUMN " + COLUMN_VARIANT + " text;";
//


    public static DatabaseHelper getInstance(Context context) {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context);
        }
        return databaseHelper;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
//        db.execSQL(DATABASE_CREATE1);
//        db.execSQL(DATABASE_CREATE2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2){
            Main_impl impl=new Main_impl(context);
            impl.delete_table();
    }}
}
