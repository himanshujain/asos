package com.example.searchus.asos;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.GPSTracker;
import com.example.searchus.asos.MODELS.ADDRESS_MODEL;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import studio.carbonylgroup.textfieldboxes.ExtendedEditText;
import studio.carbonylgroup.textfieldboxes.TextFieldBoxes;

public class Add_New_Addr extends AppCompatActivity
{
    LinearLayout parentlo,nonet_lo;
    RelativeLayout main_lo;
    TextFieldBoxes tfname,tfaddr,tfcity,tfstate,tfnearby,tfpincode,tfmobno;
    ExtendedEditText edname,edaddr,edcity,edstate,ednearby,edpincode,edmobno;
    Button add_addr_bt;
    Toolbar toolbar;

    PreferenceManager_ASOS PreferenceManager_ASOS;

    GPSTracker gps;
    double latitude;
    double longitude;
    List<Address> addresses;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_add__new__addr);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());
//a
        TOOLBAR();
        BINDING();
        CHECKING_PREF();
        ADD_ADDR_ONCLICK();
        GETLAT_LONG();

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
    }

    public void TOOLBAR()
    {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(PreferenceManager_ASOS.getcamefrom().equals("yes"))//IF CAME FROM CHECKOUT
                {
                    Intent intent=new Intent(Add_New_Addr.this,My_Address.class);
                    intent.putExtra("from intent", "CheckOut");
                    startActivity(intent);
                    finish();
                }
                else if(PreferenceManager_ASOS.getcamefrom().equals("checking_addr_yes")) //IF CAME FROM BAG(CHECKING ADDR IF AVAILABLE)
                {
//            Intent intent=new Intent(Add_New_Addr.this,Bag.class);
//            startActivity(intent);
                    onBackPressed();
                    finish();
                    PreferenceManager_ASOS.setcamefrom("checking_addr_no");
                }
                else
                {
                    Intent intent=new Intent(Add_New_Addr.this,My_Address.class);
                    intent.putExtra("from intent", "Add_New_Addr");
                    startActivity(intent);
//            super.onBackPressed();
                    finish();
                }
            }
        });

    }

    public void BINDING()
    {
        parentlo=findViewById(R.id.parentlo_id);

        tfname=findViewById(R.id.tfnameid);
        tfaddr=findViewById(R.id.tfaddrid);
        tfcity=findViewById(R.id.tfcityid);
        tfstate=findViewById(R.id.tfstateid);
        tfnearby=findViewById(R.id.tfnearbyid);
        tfpincode=findViewById(R.id.tfpincodeid);
        tfmobno=findViewById(R.id.tfmobnoid);

        edname=findViewById(R.id.ednameid);
        edaddr=findViewById(R.id.edaddrid);
        edcity=findViewById(R.id.edcityid);
        edstate=findViewById(R.id.edstateid);
        ednearby=findViewById(R.id.ednearbyid);
        edpincode=findViewById(R.id.edpincodeid);
        edpincode.setRawInputType(Configuration.KEYBOARD_12KEY);
        edmobno=findViewById(R.id.edmobnoid);
        edmobno.setRawInputType(Configuration.KEYBOARD_12KEY);

        main_lo=findViewById(R.id.main_lo_id);
        nonet_lo=findViewById(R.id.nonet_lo_id);

        add_addr_bt=findViewById(R.id.add_addr_btid);

    }

    // Checking Internet connection function
    private BroadcastReceiver networkReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
            {
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED)
                {
//                    "We have internet connection. Good to go."

                    toolbar.setVisibility(View.VISIBLE);
                    nonet_lo.setVisibility(View.GONE);
                    main_lo.setVisibility(View.VISIBLE);

                }
                else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED)
                {
//                  "We have lost internet connection"

                    toolbar.setVisibility(View.GONE);
                    nonet_lo.setVisibility(View.VISIBLE);
                    main_lo.setVisibility(View.GONE);
                }
            }
        }
    };


    public void ADD_ADDRESS()
    {
        final String getedname=edname.getText().toString();
        final String getedaddr=edaddr.getText().toString();
        final String getedcity=edcity.getText().toString();
        final String getedstate=edstate.getText().toString();
        final String getednearby=ednearby.getText().toString();
        final String getedpincode=edpincode.getText().toString();
        final String getedmobno=edmobno.getText().toString();

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "user_address_process");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            object.put("action", "new_address");

            JSONObject object1=new JSONObject() ;
            object.put("options", object1);

            object1.put("name", getedname);
            object1.put("address", getedaddr);
            object1.put("state", getedstate);
            object1.put("city", getedcity);
            object1.put("pincode", getedpincode);
            object1.put("phone", getedmobno);
            object1.put("near_by", getednearby);

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(Add_New_Addr.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Adding new address");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();
                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("Add_Addr_res ", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("Add_Addr_res", response);

                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            String status=jsonObject.getString("status");

                            if(status.equals("Address  Details Added successfully"))
                            {
                                final Snackbar snackbar = Snackbar.make(parentlo, "New Address Added Successfully....!!!", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mgreen));
                                snackbar.show();

                                if(PreferenceManager_ASOS.getcamefrom().equals("CheckOut")) //IF CAME FROM CHECKOUT
                                {
                                    ADDRESS_GETDATA();
                                }
                                else if(PreferenceManager_ASOS.getcamefrom().equals("BAG")) //IF CAME FROM BAG(CHECKING ADDR IF AVAILABLE)
                                {
                                    ADDRESS_GETDATA();
                                }
                                else
                                {
//                                                Intent intent=new Intent(getApplicationContext(),My_Address.class);
//                                                intent.putExtra("from intent","Add_New_Addr");
//                                                startActivity(intent);
                                    onBackPressed();//--if simple add addr
                                    finish();
                                }

//                                Thread timer = new Thread()
//                                {
//                                    public void run()
//                                    {
//                                        try
//                                        {
//                                            sleep(2000);
//                                            snackbar.dismiss();
//                                        }
//                                        catch (InterruptedException e)
//                                        {
//                                            e.printStackTrace();
//                                        }
//                                        finally
//                                        {
//                                            if(PreferenceManager_ASOS.getcamefrom().equals("yes")) //IF CAME FROM CHECKOUT
//                                            {
//                                                PreferenceManager_ASOS.setcamefrom("no");
//
//                                                ADDRESS_GETDATA();
//                                            }
//                                            else if(PreferenceManager_ASOS.getcamefrom().equals("BAG")) //IF CAME FROM BAG(CHECKING ADDR IF AVAILABLE)
//                                            {
//                                                PreferenceManager_ASOS.setcamefrom("");
//
//                                                ADDRESS_GETDATA();
//                                            }
//                                            else
//                                            {
////                                                Intent intent=new Intent(getApplicationContext(),My_Address.class);
////                                                intent.putExtra("from intent","Add_New_Addr");
////                                                startActivity(intent);
//                                                onBackPressed();//--if simple add addr
//                                                finish();
//                                            }
//
//                                        }
//
//                                    }
//
//                                };
//                                timer.start();
                            }
                            else
                            {
                                Snackbar snackbar = Snackbar.make(parentlo, "Address Already Exists....!!!", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                                snackbar.show();
                            }


                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Add_Addr error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void ADD_ADDR_ONCLICK()
    {
        add_addr_bt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(edname.getText().toString().equals(""))
                {
                    tfname.setError("Name should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Name should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edname.setFocusable(true);
                }
                else if(edaddr.getText().toString().equals(""))
                {
                    tfaddr.setError("Address should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Address should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edaddr.setFocusable(true);

                }
                else if(edcity.getText().toString().equals(""))
                {
                    tfcity.setError("City should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "City should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edcity.setFocusable(true);
                }
                else if(edstate.getText().toString().equals(""))
                {
                    tfstate.setError("State should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "State should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edstate.setFocusable(true);

                }
                else if(ednearby.getText().toString().equals(""))
                {
                    tfnearby.setError("Nearby should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Nearby should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    ednearby.setFocusable(true);
                }
                else if(edpincode.getText().toString().equals(""))
                {
                    tfpincode.setError("Pincode should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Pincode number should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edpincode.setFocusable(true);

                }
                else if(edmobno.getText().toString().equals(""))
                {
                    tfmobno.setError("Mobile number should not be blank",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Mobile number should not be blank....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edmobno.setFocusable(true);

                }
                else if(edmobno.length()!=10)
                {
                    tfmobno.setError("Mobile number must be of 10 digits",true);
                    final Snackbar snackbar = Snackbar.make(parentlo, "Mobile number must be of 10 digits....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                    snackbar.show();
                    edmobno.setFocusable(true);
                }
                else
                {
                    ADD_ADDRESS();
                }
            }
        });

    }

    public void ADDRESS_GETDATA()
    {

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "user_address_process");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            object.put("action", "get_user_address");

            final String requestBody = object.toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    ArrayList<ADDRESS_MODEL> address_modelArrayList=new ArrayList<>();

                    address_modelArrayList.clear();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("ADDRESS_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("ADDRESS_res", response);
                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONArray jsonArray=jsonObject.getJSONArray("data");

                            Gson gson = new Gson();
                            for (int j = 0; j < jsonArray.length(); j++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                address_modelArrayList.add(gson.fromJson(String.valueOf(jsonObject1), ADDRESS_MODEL.class));
                            }
//                            address_modelArrayList.get(jsonArray.length()-1).getAddress_id();
//                            address_modelArrayList.get(jsonArray.length()-1).getName();
//                            address_modelArrayList.get(jsonArray.length()-1).getAddress();
//                            address_modelArrayList.get(jsonArray.length()-1).getNear_by();
//                            address_modelArrayList.get(jsonArray.length()-1).getCity();
//                            address_modelArrayList.get(jsonArray.length()-1).getPincode();
//                            address_modelArrayList.get(jsonArray.length()-1).getState();
//                            address_modelArrayList.get(jsonArray.length()-1).getCountry();
//                            address_modelArrayList.get(jsonArray.length()-1).getPhone();
                            PreferenceManager_ASOS.setcamefrom("ADD_EDIT_ADDR");//--//FOR ADD OR EDIT ADDR

                            Intent intent = new Intent(Add_New_Addr.this, CheckOut.class);
//                            intent.putExtra("from_intent", "from_add_editaddr");

                            intent.putExtra("getAddress_id",  address_modelArrayList.get(jsonArray.length()-1).getAddress_id());
                            intent.putExtra("getName", address_modelArrayList.get(jsonArray.length()-1).getName());
                            intent.putExtra("getAddress", address_modelArrayList.get(jsonArray.length()-1).getAddress());
                            intent.putExtra("getNear_by", address_modelArrayList.get(jsonArray.length()-1).getNear_by());
                            intent.putExtra("getCity",address_modelArrayList.get(jsonArray.length()-1).getCity());
                            intent.putExtra("getPincode", address_modelArrayList.get(jsonArray.length()-1).getPincode());
                            intent.putExtra("getState", address_modelArrayList.get(jsonArray.length()-1).getState());
                            intent.putExtra("getCountry", address_modelArrayList.get(jsonArray.length()-1).getCountry());
                            intent.putExtra("getPhone", address_modelArrayList.get(jsonArray.length()-1).getPhone());

                            startActivity(intent);
                            finish();
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }



                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("ADDRESS_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }


    public void CHECKING_PREF()
    {
        //IF CAME FROM CHECKOUT
        if(PreferenceManager_ASOS.getcamefrom().equals("yes"))
        {
            add_addr_bt.setText("DELIVER TO THIS ADDRESS");
        }
    }


    public void GET_ADDRESS_FROM_GPS(double latitude,double longitude)
    {
        Geocoder geocoder;
        geocoder = new Geocoder(this, Locale.getDefault());

        try
        {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();

        Log.e("ADDR_IMP","addr:"+address+"\n"+"city:"+city+"\n"+"state:"+state+"\n"+"country:"+country+"\n"+"postalCode:"+postalCode+"\n"+"knownName:"+knownName);

        edaddr.setText(address);
        edcity.setText(city);
        edstate.setText(state);
        ednearby.setText(knownName);
        edpincode.setText(postalCode);
    }

    public void GETLAT_LONG()
    {
        gps = new GPSTracker(getApplicationContext());
        // check if GPS enabled
        if(gps.canGetLocation())
        {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            // \n is for new line
//            Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();

            GET_ADDRESS_FROM_GPS(latitude,longitude);

        }
        else
        {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (networkReceiver != null)
        {
            unregisterReceiver(networkReceiver);
        }
    }

    @Override
    public void onBackPressed()
    {
        if(PreferenceManager_ASOS.getcamefrom().equals("CheckOut"))//IF CAME FROM CHECKOUT
        {
            super.onBackPressed();
            finish();
            PreferenceManager_ASOS.setcamefrom("BAG");
        }
        else if(PreferenceManager_ASOS.getcamefrom().equals("BAG")) //--IF CAME FROM BAG(CHECKING ADDR IF AVAILABLE)
        {
//            Intent intent=new Intent(Add_New_Addr.this,Bag.class);
//            startActivity(intent);
            super.onBackPressed();
            finish();
            PreferenceManager_ASOS.setcamefrom("");
        }
        else
        {
//            Intent intent=new Intent(Add_New_Addr.this,My_Address.class);
//            intent.putExtra("from intent", "Add_New_Addr");
//            startActivity(intent);
            super.onBackPressed();//--if simple add addr
            finish();
        }
    }
}
