package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Search_Keyword_Page extends AppCompatActivity
{

    RecyclerView Search_suggestion_RV;
    EditText search_ed;
    FrameLayout carticon_layout,searchclick;
    LinearLayout parentlo;

    PreferenceManager_ASOS PreferenceManager_ASOS;
    Search_Suggestion_ADP search_suggestion_adp;
    ArrayList<String> suggestion = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_search__keyword__page);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        TOOLBAR();


        suggestion = new ArrayList<>();

        parentlo = (LinearLayout) findViewById(R.id.parentlo_id);
        search_ed = (EditText) findViewById(R.id.search_ed_id);
        searchclick=findViewById(R.id.searchclickid);

//        Suggestion_GETDATA();
        GETKEYWORDS();

        searchclick.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(search_ed.length()==0)
                {
                    Snackbar snackbar = Snackbar.make(parentlo, "Please enter the productname to search...!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                    snackbar.show();
                }
                else
                {
                    Intent intent=new Intent(Search_Keyword_Page.this,Searching_Result.class);
                    intent.putExtra("getsearchingword",search_ed.getText().toString());
                    startActivity(intent);
                }
            }
        });

        search_ed.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    Intent intent=new Intent(Search_Keyword_Page.this,Searching_Result.class);
                    intent.putExtra("getsearchingword",search_ed.getText().toString());
                    startActivity(intent);

                    return true;
                }
                return false;
            }
        });

    }

    public void TOOLBAR()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
                finish();
            }
        });
    }

    public void Suggestion_GETDATA()
    {

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL="http://api.searchus.in/API/ecommerce/auto_complete";
            JSONObject object = new JSONObject();
            JSONObject jobj = new JSONObject();
            JSONObject jobj2 = new JSONObject();
            jobj2.put("keyword", "ALL");
            jobj.put("action", "product");
            jobj.put("input", jobj2);
            try
            {
                object.put("options", jobj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }


            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(Search_Keyword_Page.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();



            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("Searchsuggestionres", "BLANK DATA");
                    }
                    else {
                        Log.e("Searchsuggestionres", response);


                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String message = jsonObject.getString("message");

                            if (message.equals("Done"))
                            {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++)
                                {
                                    suggestion.add(jsonArray.getString(i));
                                    Log.e("suggestion", String.valueOf(suggestion));
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Search_suggestion_RV = (RecyclerView) findViewById(R.id.searchsuggestion_recycler_view);
                        Search_suggestion_RV.setHasFixedSize(true);
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
                        Search_suggestion_RV.setLayoutManager(layoutManager);
                        search_suggestion_adp=new Search_Suggestion_ADP(getApplicationContext(), suggestion,search_ed);
                        Search_suggestion_RV.setAdapter(search_suggestion_adp);

                        search_ed.addTextChangedListener(new TextWatcher()
                        {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                search_suggestion_adp.filter(charSequence);
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {

                            }
                        });

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Searchsuggestion_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


    }

    public void GETKEYWORDS()
    {
        ArrayList<String> suggestion = new ArrayList<String>(PreferenceManager_ASOS.getkeywords());
        for (int i = 0; i<suggestion.size(); i++)
        {
            Log.e("FETCH", suggestion.get(i));
        }

        Search_suggestion_RV = (RecyclerView) findViewById(R.id.searchsuggestion_recycler_view);
        Search_suggestion_RV.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        Search_suggestion_RV.setLayoutManager(layoutManager);
        search_suggestion_adp=new Search_Suggestion_ADP(getApplicationContext(), suggestion,search_ed);
        Search_suggestion_RV.setAdapter(search_suggestion_adp);

        search_ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                search_suggestion_adp.filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    public class Search_Suggestion_ADP extends RecyclerView.Adapter<Search_Suggestion_ADP.ViewHolder>
    {
        private List<String> suggestionarraylist= null;
        private ArrayList<String> countriesCopy;
        Context context;
        EditText search_ed;

        public Search_Suggestion_ADP(Context context, ArrayList<String> suggestionarraylist,EditText search_ed)
        {
            super();
            this.context = context;
            this.suggestionarraylist = suggestionarraylist;
            this.search_ed = search_ed;

            this.countriesCopy = new ArrayList<>();
            countriesCopy.addAll(suggestionarraylist);
        }

        @Override
        public Search_Suggestion_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_search_suggestion_lvitem, parent, false);
            Search_Suggestion_ADP.ViewHolder viewHolder = new Search_Suggestion_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(Search_Suggestion_ADP.ViewHolder holder, final int position)
        {

            holder.tv_suggestion.setText(suggestionarraylist.get(position).toString());
            holder.tv_suggestion.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Log.e("GET SUGGESTION", String.valueOf(suggestionarraylist.get(position).toString()));
                    search_ed.setText(String.valueOf(suggestionarraylist.get(position).toString()));


                    Intent intent=new Intent(Search_Keyword_Page.this,Searching_Result.class);
                    intent.putExtra("getsearchingword",search_ed.getText().toString());
                    startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount()
        {
            return suggestionarraylist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder
        {

            public TextView tv_suggestion;


            public ViewHolder(View itemView)
            {
                super(itemView);
                tv_suggestion = (TextView) itemView.findViewById(R.id.tv_suggestionid);

            }


        }

        public void filter(CharSequence sequence)
        {
            ArrayList<String> temp = new ArrayList<>();
            if (!TextUtils.isEmpty(sequence)) {
                for (String s : suggestion) {
                    if (s.toLowerCase().contains(sequence)) {
                        temp.add(s);
                    }
                }
            } else {
                temp.addAll(countriesCopy);
            }
            suggestion.clear();
            suggestion.addAll(temp);
            notifyDataSetChanged();
            temp.clear();
        }


    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
