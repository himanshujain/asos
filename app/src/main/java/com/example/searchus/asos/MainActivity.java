package com.example.searchus.asos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity
{
    RecyclerView Category_RecyclerView;
    RecyclerView.Adapter category_Adapter;
    ArrayList<String> Category_Name;
    ArrayList<Integer> Category_Img;

    ImageView ivsave,ivbag;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Category_Name = new ArrayList<>(Arrays.asList("Jersey", "Denim"));
        Category_Img = new ArrayList<>(Arrays.asList(R.mipmap.ic_launcher, R.mipmap.ic_launcher));

        Category_RecyclerView = (RecyclerView) findViewById(R.id.category_recycler_view);

        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        Category_RecyclerView.setLayoutManager(trending_LayoutManager);
        Category_RecyclerView.setNestedScrollingEnabled(false);
        category_Adapter = new Category_ADP(getApplicationContext(),Category_Img,Category_Name);
        Category_RecyclerView.setAdapter(category_Adapter);

        ivsave=findViewById(R.id.saveivid);

        ivsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Saved_Item.class);
                startActivity(intent);
            }
        });
    }


    public class Category_ADP extends RecyclerView.Adapter<Category_ADP.ViewHolder>
    {
        ArrayList<Integer> alImage;
        ArrayList<String> prodname;
        Context context;

        public Category_ADP(Context context, ArrayList<Integer> alImage, ArrayList<String> prodname)
        {
            super();
            this.context = context;
            this.alImage = alImage;
            this.prodname = prodname;
        }

        @Override
        public Category_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_category_lvitems, parent, false);
            Category_ADP.ViewHolder viewHolder = new Category_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(Category_ADP.ViewHolder holder, final int position)
        {

            holder.imgThumbnail.setImageResource(alImage.get(position));
            holder.tvprodname.setText(prodname.get(position));

            holder.imgThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    String getpname=prodname.get(position).toString();
                    Intent intent=new Intent(MainActivity.this, SubCategory_Layer_2.class);
                    intent.putExtra("getpname",getpname);
                    startActivity(intent);
                    Toast.makeText(context,""+getpname,Toast.LENGTH_SHORT).show();
                }
            });

        }

        @Override
        public int getItemCount()
        {
            return alImage.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder
        {

            public ImageView imgThumbnail;
            public TextView tvprodname;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                tvprodname = (TextView) itemView.findViewById(R.id.tvproductnameid);
            }


        }


    }



}
