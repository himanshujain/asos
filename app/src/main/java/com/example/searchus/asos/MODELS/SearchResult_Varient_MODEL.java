package com.example.searchus.asos.MODELS;

import java.io.Serializable;
import java.util.ArrayList;

public class SearchResult_Varient_MODEL implements Serializable
{
    public SearchResult_Varient_MODEL(String variant_name, String purchase_price, String mrp, String final_price, String quantity) {
        this.variant_name = variant_name;
        this.purchase_price = purchase_price;
        this.mrp = mrp;
        this.final_price = final_price;
        this.quantity = quantity;
    }

    private String variant_name,purchase_price,mrp,final_price,quantity;

    ArrayList<String>images;


    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public String getPurchase_price() {
        return purchase_price;
    }

    public void setPurchase_price(String purchase_price) {
        this.purchase_price = purchase_price;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getFinal_price() {
        return final_price;
    }

    public void setFinal_price(String final_price) {
        this.final_price = final_price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }


}
