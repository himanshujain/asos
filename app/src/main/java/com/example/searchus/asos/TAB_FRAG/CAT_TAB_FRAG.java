package com.example.searchus.asos.TAB_FRAG;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Classes.No_Internet;
import com.example.searchus.asos.MODELS.CATEGORY_GETSET;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.example.searchus.asos.Product_Listing;
import com.example.searchus.asos.R;
import com.example.searchus.asos.SubCategory_Layer_2;
import com.example.searchus.asos.Verify_OTP;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
* Created by jauhar xlr on 18/4/2016.
        */
public class CAT_TAB_FRAG extends Fragment
{

    RecyclerView Category_RecyclerView;

    LinearLayout nonet_lo;
    LinearLayout mainlo;

    PreferenceManager_ASOS PreferenceManager_ASOS;
    ArrayList<CATEGORY_GETSET> Category=new ArrayList<>();
    CATEGORY_GETSET getset;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.asos_cat_tab_layout,null);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getContext());

//        //Calling Checking Internet connection function
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
//        getActivity().registerReceiver(receiver, filter);

        nonet_lo=rootView.findViewById(R.id.nonet_lo_id);
        mainlo=rootView.findViewById(R.id.mainlo_id);

        Category_RecyclerView = (RecyclerView) rootView.findViewById(R.id.category_recycler_view);

        CATEGORY_GETDATA();

        return rootView;
    }

    // Checking Internet connection function
    public class NetworkChangeReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(final Context context, final Intent intent)
        {
            if(No_Internet.getInstance(getActivity()).isNetworkAvailable(context))
            {
//                Toast.makeText(getApplicationContext(),"YES",Toast.LENGTH_SHORT).show();
                nonet_lo.setVisibility(View.GONE);
                mainlo.setVisibility(View.VISIBLE);

//                CATEGORY_GETDATA();

            }
            else
            {
//                Toast.makeText(getApplicationContext(),"NO",Toast.LENGTH_SHORT).show();
                nonet_lo.setVisibility(View.VISIBLE);
                mainlo.setVisibility(View.GONE);
            }

        }


    }

    private void CATEGORY_GETDATA()
    {
        Category_RecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),1);
        Category_RecyclerView.setLayoutManager(layoutManager);
        Category_RecyclerView.setNestedScrollingEnabled(false);

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());

            JSONObject object=new JSONObject() ;
            object.put("type", "get_product_category");
            object.put("category", "levelwise");
            JSONObject object1=new JSONObject() ;
            object.put("options", object1);
            object1.put("level", "1");
            object1.put("parent_id", "0");
            object1.put("row_count", "10000000");

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(getContext(),R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading Categories");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();
                    Category.clear();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("CATEGORY_res", "CATEGORY BLANK DATA");
                    }
                    else
                    {
                        Log.e("CATEGORY_res", response);

                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONArray jsonArray=jsonObject.getJSONArray("data");
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject object1=jsonArray.getJSONObject(i);

                                String path=object1.getString("image_path");
                                String id=object1.getString("id");
                                String parent_id = object1.getString("parent_id");
                                String category_name = object1.getString("category_name");
                                String child = object1.getString("child");
                                String notes = object1.getString("notes");

                                getset=new CATEGORY_GETSET();
                                getset.setCatid(id);
                                getset.setCatimgpath(PaperCart_CoNNectioN.IMGPATH+path);
                                getset.setCatname(category_name);
                                getset.setChild(child);
                                getset.setNote(notes);

                                Category.add(getset);
                            }

                            Category_RecyclerView.setAdapter(new Category_Adapter(getContext(),Category));
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }


                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("CATEGORY_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }


    public class Category_Adapter extends RecyclerView.Adapter<Category_Adapter.ViewHolder>
    {
        ArrayList<CATEGORY_GETSET> Category;
        Context context;


        public Category_Adapter(Context context, ArrayList<CATEGORY_GETSET> Category)
        {
            super();
            this.context = context;
            this.Category = Category;
        }


        @Override
        public Category_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_category_lvitems, parent, false);
            Category_Adapter.ViewHolder viewHolder = new Category_Adapter.ViewHolder(v);
            return viewHolder;
        }

        @Override


        public void onBindViewHolder(final ViewHolder holder, int position)
        {
            holder.catname.setText(Category.get(position).getCatname());

            Picasso.with(context)
                    .load(Category.get(position).getCatimgpath())
                    .into(holder.imgThumbnail);

            Log.e("CAT_IMG_1",Category.get(position).getCatimgpath());
//
//            String img_responce=Category.get(position).getCatimgpath();
//            final String aftersubstring_img_response = (img_responce.substring(img_responce.lastIndexOf("/") + 1));
//            final String final_img8_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_8+""+aftersubstring_img_response;
//            final String final_img4_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_4+""+aftersubstring_img_response;
//
//            Picasso.with(context)
//                    .load(final_img8_path)
//                    .into(holder.imgThumbnail);
//
//            Log.e("CAT_IMG_2",final_img8_path);
//
//
//            new Handler().postDelayed(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    // This method will be executed once the timer is over
//                    // Start your app main activity
//                    Picasso.with(context)
//                            .load(final_img4_path)
//                            .into(holder.imgThumbnail);
//
//                }
//            }, 3000);

            holder.setClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if(Category.get(position).getChild().equals("0"))
                    {
                        String catid=Category.get(position).getCatid();
                        String getnote=Category.get(position).getNote();
                        Log.e("catid", Category.get(position).getChild());
                        Intent intent=new Intent(getActivity(),Product_Listing.class);
                        intent.putExtra("catid",catid);
                        intent.putExtra("from_intent","from_category");
                        intent.putExtra("CATEGORY_NAME",Category.get(position).getCatname());
                        intent.putExtra("getnote",getnote);
                        context.startActivity(intent);

                    }
                    else
                    {
                        String catid=Category.get(position).getCatid();
                        String catname=Category.get(position).getCatname();
                        String getnote=Category.get(position).getNote();
                        Intent intent=new Intent(getActivity(),SubCategory_Layer_2.class);
                        intent.putExtra("catid",catid);
                        intent.putExtra("catname",catname);
                        intent.putExtra("getnote",getnote);
                        context.startActivity(intent);
                    }

                }
            });
        }

        @Override
        public int getItemCount()
        {
            return Category.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            public ImageView imgThumbnail;
            public TextView catname;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                catname = (TextView) itemView.findViewById(R.id.catnameid);
                itemView.setOnClickListener(this);

            }

            public void setClickListener(ItemClickListener clickListener) {
                this.clickListener = clickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

//        //Calling Checking Internet connection function
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
//        getActivity().registerReceiver(receiver, filter);
    }
}
