package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.No_Internet;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import studio.carbonylgroup.textfieldboxes.ExtendedEditText;
import studio.carbonylgroup.textfieldboxes.TextFieldBoxes;

public class Enter_OTP extends AppCompatActivity
{
    private TextFieldBoxes tfmobno;
    private ExtendedEditText edmobno;
    private Button submitbt;
    ImageView logo;
    TextView tv_businessname;

    LinearLayout nonet_lo,parentlo;
    ScrollView enter_otp_mainlo;


    RequestQueue rq;
    String sentnumber;
    JsonObjectRequest jsonObjectRequest;
    PreferenceManager_ASOS PreferenceManager_ASOS;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_enter__otp);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

//        //Calling Checking Internet connection function
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
//        registerReceiver(receiver, filter);


        nonet_lo=findViewById(R.id.nonet_lo_id);
        enter_otp_mainlo=findViewById(R.id.enter_otp_mainlo_id);

        parentlo=findViewById(R.id.parentlo_id);
        tfmobno=findViewById(R.id.tfmobnoid);
        edmobno=findViewById(R.id.edmobnoid);
        edmobno.setRawInputType(Configuration.KEYBOARD_12KEY);
        logo=findViewById(R.id.logoid);
//        Picasso.with(getApplicationContext())
//                .load(PreferenceManager_ASOS.GetBusiness_Logo())
//                .into(logo);

        tv_businessname=findViewById(R.id.tv_businessnameid);
//        tv_businessname.setText(PreferenceManager_ASOS.GetBusiness_Name());

        submitbt=findViewById(R.id.submitotpbtid);
        submitbt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(edmobno.getText().toString().equals(""))
                {
                    tfmobno.setError("Please enter your mobile number",true);
                }
                else if(edmobno.length()!=10)
                {
                    tfmobno.setError("Mobile number must be 10 digits",true);
                }
                else
                {
                    SENT_OTP();
                }
            }
        });

//        if(getIntent().getStringExtra("from_intent").equals("Verify_OTP"))
//        {
//           String getnumb=getIntent().getStringExtra("getnumber").toString();
//           edmobno.setText(getnumb);
//        }


    }


    // Checking Internet connection function
    public class NetworkChangeReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(final Context context, final Intent intent)
        {
            if(No_Internet.getInstance(getApplicationContext()).isNetworkAvailable(context))
            {
//                Toast.makeText(getApplicationContext(),"YES",Toast.LENGTH_SHORT).show();
                nonet_lo.setVisibility(View.GONE);
                enter_otp_mainlo.setVisibility(View.VISIBLE);
            }
            else
            {
//                Toast.makeText(getApplicationContext(),"NO",Toast.LENGTH_SHORT).show();
                nonet_lo.setVisibility(View.VISIBLE);
                enter_otp_mainlo.setVisibility(View.GONE);
            }

        }


    }


    public void  SENT_OTP()
    {

        rq= Volley.newRequestQueue(this);

        sentnumber=edmobno.getText().toString().trim();
        JSONObject object=new JSONObject() ;
        try
        {
            object.put("type", "send_otp");
            object.put("category", "login");
            object.put("user_id", sentnumber);

            final ProgressDialog progressDialog=new ProgressDialog(Enter_OTP.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Sending OTP");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response)
                {
                    progressDialog.dismiss();


                    Log.e("SentSMSresponse", String.valueOf(response));
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        if(jsonObject.getString("status").equals("SMS_Sended"))
                        {
                            Intent intent=new Intent(getApplicationContext(),Verify_OTP.class);
                            intent.putExtra("sentnumber",sentnumber);
//                            intent.putExtra("from_intent","Enter_OTP");
                            startActivity(intent);
                            finish();
                        }
                        else if(jsonObject.getString("status").equals("OTP is limited to only 5 times a day"))
                        {
                            Toast.makeText(getApplicationContext(),"LIMITED SMS",Toast.LENGTH_LONG).show();
                        }

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    if (error instanceof NetworkError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ParseError) {
                    } else if (error instanceof NoConnectionError) {
                    } else if (error instanceof TimeoutError) {
                    }
                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put(PaperCart_HEADER.Contenttype,PaperCart_HEADER.Appjson);
                    params.put(PaperCart_HEADER.Auth, PaperCart_HEADER.KEY);

                    return params;
                }
            };

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rq.add(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
//        if(getIntent().getStringExtra("from_intent").equals("Home"))
//        {
//            super.onBackPressed();
//            finish();
//        }
//        else  if(getIntent().getStringExtra("from_intent").equals("Product_Listing"))
//        {
//            super.onBackPressed();
//        }
//        else  if(getIntent().getStringExtra("from_intent").equals("Bag"))
//        {
//            super.onBackPressed();
//        }
//        else  if(getIntent().getStringExtra("from_intent").equals("Product_Detail"))
//        {
//            super.onBackPressed();
//        }
//        else  if(getIntent().getStringExtra("from_intent").equals("Searching_Result"))
//        {
//            super.onBackPressed();
//        }
//        else
//        {
//            super.onBackPressed();
//        }
    }

//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//
//        //Calling Checking Internet connection function
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        Enter_OTP.NetworkChangeReceiver receiver = new Enter_OTP.NetworkChangeReceiver();
//        registerReceiver(receiver, filter);
//
//    }


}
