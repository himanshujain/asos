package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Classes.No_Internet;
import com.example.searchus.asos.MODELS.GETSET_PRODLIST;
import com.example.searchus.asos.MODELS.Pop_PROD_Final_Varient_MODEL;
import com.example.searchus.asos.MODELS.RecentView_Varient_MODEL;
import com.example.searchus.asos.MODELS.SUBSECTIONS_MODEL;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Product_Listing extends AppCompatActivity
{
    LinearLayout nonet_lo;
    LinearLayout mainlo;

    RecyclerView Prod_list_RecyclerView;
    Spinner SortSpinner;
    TextView note;
    LinearLayout parentlo;
    Toolbar toolbar;

    String getintent;
    String sort_type="";
    boolean favourite_status;

    ArrayList<GETSET_PRODLIST> gather_getset_prodlistArrayList=new ArrayList<>();
    ArrayList<SUBSECTIONS_MODEL> gather_getset2_prodlistArrayList=new ArrayList<>();
    ArrayList<Pop_PROD_Final_Varient_MODEL> varientArray;
    ArrayList<String> V_imgarray;
    GETSET_PRODLIST getset_prodlist;
    PreferenceManager_ASOS PreferenceManager_ASOS;

    ProgressBar loader;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    int IsFatchingData = 0;
    int offset = 0;
    int limit = 6;
    Product_Listing_Adapter product_listing_adapter;
    SUBSECTIONS_Adapter subsections_adapter;
    boolean cancel_progress=true;
    ProgressDialog progressDialog;
    StringRequest stringRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_product__listing);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

//        //Calling Checking Internet connection function
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
//        registerReceiver(receiver, filter);



        nonet_lo=findViewById(R.id.nonet_lo_id);
        mainlo=findViewById(R.id.mainlo_id);
        SortSpinner=findViewById(R.id.sortspinnerid);

        parentlo=findViewById(R.id.parentlo_id);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //fav onresume logic
                PreferenceManager_ASOS.setis_favchanged(false);
                onBackPressed();
                finish();
            }
        });


        note=findViewById(R.id.noteid);
        String getnote=getIntent().getStringExtra("getnote").toString();
        Log.e("getnote", getnote);
        if(getnote.equals(""))
        {
            note.setVisibility(View.GONE);
        }
        else
        {
            note.setVisibility(View.VISIBLE);
            note.setText(getnote);
        }

        loader= (ProgressBar) findViewById(R.id.loaderid);


        //--------------- GENERAL
        toolbar.setVisibility(View.VISIBLE);
        nonet_lo.setVisibility(View.GONE);
        mainlo.setVisibility(View.VISIBLE);

        getintent= getIntent().getStringExtra("from_intent");

        if(getintent.equals("from_category"))  //IF CAME FROM CATEGORY....
        {
            String getCATEGORY_NAME= getIntent().getStringExtra("CATEGORY_NAME");
            getSupportActionBar().setTitle(getCATEGORY_NAME);
//            feature_cardview.setVisibility(View.VISIBLE);

            Prod_list_RecyclerView = (RecyclerView) findViewById(R.id.prodlist_recycler_view);
            Prod_list_RecyclerView.setHasFixedSize(true);
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            Prod_list_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
            product_listing_adapter=new Product_Listing_Adapter(getApplicationContext(),gather_getset_prodlistArrayList);
            Prod_list_RecyclerView.setAdapter(product_listing_adapter);

            Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
            {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy)
                {
                    super.onScrolled(recyclerView, dx, dy);

                    visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                    Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                    totalItemCount = gaggeredGridLayoutManager.getItemCount();
                    Log.e("totalItemCount", String.valueOf(totalItemCount));

                    firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                    Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    Log.e("lastInScreen", String.valueOf(lastInScreen));

                    // Scrolling up
                    if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                    {
                        loader.setVisibility(View.VISIBLE);
                        PROD_LISTING_FROM_CAT_GETDATA();
                    }

                }
            });

            Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    if (!recyclerView.canScrollVertically(1))
                    {
                        Snackbar snackbar = Snackbar
                                .make(parentlo, "Reached at the bottom...!!!", Snackbar.LENGTH_SHORT);
                        View snackBarView = snackbar.getView();
                        snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                        snackbar.show();
                    }

                }
            });

            PROD_LISTING_FROM_CAT_GETDATA();
        }
        else if(getintent.equals("from_subsec_prod")) //IF CAME FROM SECTIONS....
        {
            final String getsecid=getIntent().getStringExtra("getsecid").toString();
            Log.e("getsecid",getsecid);
            String getsecname=getIntent().getStringExtra("getsecname").toString();

            getSupportActionBar().setTitle(getsecname);
//            feature_cardview.setVisibility(View.GONE);

            Prod_list_RecyclerView = (RecyclerView) findViewById(R.id.prodlist_recycler_view);
            Prod_list_RecyclerView.setHasFixedSize(true);
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            Prod_list_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
            subsections_adapter=new SUBSECTIONS_Adapter(getApplicationContext(),gather_getset2_prodlistArrayList);
            Prod_list_RecyclerView.setAdapter(subsections_adapter);

            Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
            {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy)
                {
                    super.onScrolled(recyclerView, dx, dy);

                    visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                    Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                    totalItemCount = gaggeredGridLayoutManager.getItemCount();
                    Log.e("totalItemCount", String.valueOf(totalItemCount));

                    firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                    Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    Log.e("lastInScreen", String.valueOf(lastInScreen));

                    if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                    {
                        loader.setVisibility(View.VISIBLE);
                        SUB_SECTION_PROD_GETDATA(getsecid);
                    }
                }
            });

            // DETECT RECYCLER REACHED AT BOTTOM...
            Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    if (!recyclerView.canScrollVertically(1))
                    {
                        Snackbar snackbar = Snackbar
                                .make(parentlo, "Reached at the bottom...!!!", Snackbar.LENGTH_SHORT);
                        View snackBarView = snackbar.getView();
                        snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                        snackbar.show();
                    }
                }
            });


            SUB_SECTION_PROD_GETDATA(getsecid);
        }

        //---------------

        SPINNER_ONCLICK();
    }

    // Checking Internet connection function
//    public class NetworkChangeReceiver extends BroadcastReceiver
//    {
//        @Override
//        public void onReceive(final Context context, final Intent intent)
//        {
//            if(No_Internet.getInstance(getApplicationContext()).isNetworkAvailable(context))
//            {
////                Toast.makeText(getApplicationContext(),"YES",Toast.LENGTH_SHORT).show();
//                toolbar.setVisibility(View.VISIBLE);
//                nonet_lo.setVisibility(View.GONE);
//                mainlo.setVisibility(View.VISIBLE);
//
//
////                getintent= getIntent().getStringExtra("from_intent");
////
////                if(getintent.equals("from_category"))  //IF CAME FROM CATEGORY....
////                {
////                    String getCATEGORY_NAME= getIntent().getStringExtra("CATEGORY_NAME");
////                    getSupportActionBar().setTitle(getCATEGORY_NAME);
//////            feature_cardview.setVisibility(View.VISIBLE);
////
////                    Prod_list_RecyclerView = (RecyclerView) findViewById(R.id.prodlist_recycler_view);
////                    Prod_list_RecyclerView.setHasFixedSize(true);
////                    gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
////                    Prod_list_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
////                    product_listing_adapter=new Product_Listing_Adapter(getApplicationContext(),gather_getset_prodlistArrayList);
////                    Prod_list_RecyclerView.setAdapter(product_listing_adapter);
////
////                    Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
////                    {
////                        @Override
////                        public void onScrolled(RecyclerView recyclerView, int dx, int dy)
////                        {
////                            super.onScrolled(recyclerView, dx, dy);
////
////                            visibleItemCount = gaggeredGridLayoutManager.getChildCount();
////                            Log.e("visibleItemCount", String.valueOf(visibleItemCount));
////
////                            totalItemCount = gaggeredGridLayoutManager.getItemCount();
////                            Log.e("totalItemCount", String.valueOf(totalItemCount));
////
////                            firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
////                            Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));
////
////
////                            int lastInScreen = firstVisibleItem + visibleItemCount;
////                            Log.e("lastInScreen", String.valueOf(lastInScreen));
////
////                                // Scrolling up
////                                if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
////                                {
////                                    loader.setVisibility(View.VISIBLE);
////                                    PROD_LISTING_FROM_CAT_GETDATA();
////                                }
////
////                        }
////                    });
////
////                    Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
////                        @Override
////                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
////                            super.onScrollStateChanged(recyclerView, newState);
////
////                            if (!recyclerView.canScrollVertically(1))
////                            {
////                                Snackbar snackbar = Snackbar
////                                        .make(parentlo, "Reached at the bottom...!!!", Snackbar.LENGTH_SHORT);
////                                View snackBarView = snackbar.getView();
////                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
////                                snackbar.show();
////                            }
////
////                        }
////                    });
////
////                    PROD_LISTING_FROM_CAT_GETDATA();
////                }
////                else if(getintent.equals("from_subsec_prod")) //IF CAME FROM SECTIONS....
////                {
////                    final String getsecid=getIntent().getStringExtra("getsecid").toString();
////                    Log.e("getsecid",getsecid);
////                    String getsecname=getIntent().getStringExtra("getsecname").toString();
////
////                    getSupportActionBar().setTitle(getsecname);
//////            feature_cardview.setVisibility(View.GONE);
////
////                    Prod_list_RecyclerView = (RecyclerView) findViewById(R.id.prodlist_recycler_view);
////                    Prod_list_RecyclerView.setHasFixedSize(true);
////                    gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
////                    Prod_list_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
////                    subsections_adapter=new SUBSECTIONS_Adapter(getApplicationContext(),gather_getset2_prodlistArrayList);
////                    Prod_list_RecyclerView.setAdapter(subsections_adapter);
////
////                    Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
////                    {
////                        @Override
////                        public void onScrolled(RecyclerView recyclerView, int dx, int dy)
////                        {
////                            super.onScrolled(recyclerView, dx, dy);
////
////                            visibleItemCount = gaggeredGridLayoutManager.getChildCount();
////                            Log.e("visibleItemCount", String.valueOf(visibleItemCount));
////
////                            totalItemCount = gaggeredGridLayoutManager.getItemCount();
////                            Log.e("totalItemCount", String.valueOf(totalItemCount));
////
////                            firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
////                            Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));
////
////
////                            int lastInScreen = firstVisibleItem + visibleItemCount;
////                            Log.e("lastInScreen", String.valueOf(lastInScreen));
////
////                                if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
////                                {
////                                    loader.setVisibility(View.VISIBLE);
////                                    SUB_SECTION_PROD_GETDATA(getsecid);
////                                }
////                        }
////                    });
////
////                    // DETECT RECYCLER REACHED AT BOTTOM...
////                    Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
////                        @Override
////                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
////                            super.onScrollStateChanged(recyclerView, newState);
////
////                            if (!recyclerView.canScrollVertically(1))
////                            {
////                                Snackbar snackbar = Snackbar
////                                        .make(parentlo, "Reached at the bottom...!!!", Snackbar.LENGTH_SHORT);
////                                View snackBarView = snackbar.getView();
////                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
////                                snackbar.show();
////                            }
////                        }
////                    });
////
////
////                    SUB_SECTION_PROD_GETDATA(getsecid);
////                }
//
//            }
//            else
//            {
////                Toast.makeText(getApplicationContext(),"NO",Toast.LENGTH_SHORT).show();
//                toolbar.setVisibility(View.GONE);
//                nonet_lo.setVisibility(View.VISIBLE);
//                mainlo.setVisibility(View.GONE);
//
//            }
//
//        }
//
//
//    }

    public void SORTBY_GETDATA(String sort_type)
    {
        IsFatchingData = 1;

        String getcid= getIntent().getStringExtra("catid").trim();
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JSONObject object = new JSONObject();
        try
        {
            JSONObject jobj2 = new JSONObject();
            jobj2.put("sorting_by", sort_type);

            JSONObject jobj = new JSONObject();
            jobj.put("user_id", PreferenceManager_ASOS.GetUserID());
            jobj.put("row_offset", String.valueOf(offset));
            jobj.put("row_count", String.valueOf(limit));
            jobj.put("filter", jobj2);

            object.put("type","get_business_folderwise" );
            object.put("keyword", getcid); //cid
            object.put("options", jobj);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final String requestBody = object.toString();

        if(cancel_progress)
        {
            progressDialog=new ProgressDialog(Product_Listing.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                progressDialog.dismiss();

                loader.setVisibility(View.GONE);
                ArrayList<GETSET_PRODLIST> getset_prodlistArrayList=new ArrayList<>();

                String getresponce = response.replace("\"", "");

                if(getresponce.equals("No Data Found"))
                {
                    Log.e("SORTBY_res", "BLANK DATA");
                }
                else
                {
                    Log.e("SORTBY_res", response);

                    try
                    {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        String code = jsonObject.getString("code");
                        String message = jsonObject.getString("message");

                        if(code.equals("000200"))
                        {

                            JSONObject datajsonObject=jsonObject.getJSONObject("data");
                            JSONArray prodlistarray=datajsonObject.getJSONArray("product_list");
                            for(int k=0;k<prodlistarray.length();k++)
                            {
                                JSONObject jsonObject1 = prodlistarray.getJSONObject(k);

                                String pid = jsonObject1.getString("product_id");
                                String product_name = jsonObject1.getString("product_name");
                                String product_price = jsonObject1.getString("product_price");
                                String product_desc = jsonObject1.getString("product_desc");
                                String quantity = jsonObject1.getString("quantity");
                                String product_spec = jsonObject1.getString("product_spec");
//                                String tags = jsonObject1.getString("tags");
                                ArrayList<String> Img = new ArrayList<>();

                                JSONArray jsonArray = jsonObject1.getJSONArray("img_path");
                                if(jsonArray!=null)
                                {
                                    for (int i = 0; i < jsonArray.length(); i++)
                                    {
                                        Img.add(jsonArray.getString(i));
                                    }
                                }
//                                Log.e("Img", String.valueOf(Img));

                                String purchase_price = jsonObject1.getString("purchase_price");
                                String avalibility = jsonObject1.getString("avalibility");
                                String product_code = jsonObject1.getString("product_code");
                                String notes = jsonObject1.getString("notes");
                                String extra_1 = jsonObject1.getString("extra_1");
                                String extra_2 = jsonObject1.getString("extra_2");
                                String favourite_status_String = jsonObject1.getString("favourite_status");
//                                Log.e("favourite_status", String.valueOf(favourite_status));
                                if (favourite_status_String.equals("None"))
                                {
                                    favourite_status=false;
                                }
                                else if(favourite_status_String.equals("FAVOURITE"))
                                {
                                    favourite_status=true;
                                }

                                varientArray=new ArrayList<>();
                                JSONArray jsonArray2 = jsonObject1.getJSONArray("variant_details");
                                for (int i = 0; i < jsonArray2.length(); i++)
                                {
                                    JSONObject jsonObject3 = jsonArray2.getJSONObject(i);
                                    int V_sr_no=jsonObject3.getInt("sr_no");
                                    String V_variant_name=jsonObject3.getString("variant_name");
                                    double V_purchase_price=jsonObject3.getDouble("purchase_price");
                                    double V_mrp=jsonObject3.getDouble("mrp");
                                    double V_final_price=jsonObject3.getDouble("final_price");
                                    int V_quantity=jsonObject3.getInt("quantity");

                                    Object json = jsonObject3.get("images");
                                    JSONArray imagevar = null;
                                    V_imgarray = new ArrayList<String>();

                                    if (json instanceof JSONArray)
                                    {
                                        imagevar = jsonObject3.getJSONArray("images");
                                        for (int j = 0; j < imagevar.length(); j++)
                                        {
                                            V_imgarray.add(String.valueOf(imagevar.get(j)));
                                        }

                                    }
                                    else
                                    {
                                    }

                                    Pop_PROD_Final_Varient_MODEL pop_prod_varient_model = new
                                            Pop_PROD_Final_Varient_MODEL(V_variant_name,V_sr_no,V_purchase_price, V_mrp, V_final_price,V_quantity,V_imgarray);
                                    varientArray.add(pop_prod_varient_model);
                                }

                                String final_price = jsonObject1.getString("final_price");

                                getset_prodlist=new GETSET_PRODLIST();
                                getset_prodlist.setProduct_id(pid);
                                getset_prodlist.setProduct_name(product_name);
                                getset_prodlist.setProduct_price(product_price);
                                getset_prodlist.setFinal_price(final_price);
                                getset_prodlist.setFavourite_status(favourite_status);
                                getset_prodlist.setImg_path(Img);
                                getset_prodlist.setVariant_img(V_imgarray);
                                getset_prodlist.setVarientArray(varientArray);
                                getset_prodlist.setNote(notes);

                                getset_prodlistArrayList.add(getset_prodlist);
                            }
                        }


                        //Loader code
                        int oldcount = gather_getset_prodlistArrayList.size();
                        for (int i = 0; i < getset_prodlistArrayList.size(); i++)
                        {
                            offset++;
                            product_listing_adapter.add(getset_prodlistArrayList.get(i));
                        }


                        product_listing_adapter.notifyDataSetChanged();
                        IsFatchingData = 0;
//

                        if (getset_prodlistArrayList.size() % limit != 0 || oldcount == gather_getset_prodlistArrayList.size())
                        {
                            loader.setVisibility(View.GONE);
                        }

                        cancel_progress=false;
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        Log.e("e", String.valueOf(e));
                    }


                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.e("SORTBY_error", error.toString());
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                params.put("authentication", PaperCart_HEADER.KEY);

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError
            {
                try
                {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                }
                catch (UnsupportedEncodingException uee)
                {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };


        requestQueue.add(stringRequest);

    }


    //CALL WHEN CAME FROM CATEGORY
    public void PROD_LISTING_FROM_CAT_GETDATA()
    {
        IsFatchingData = 1;

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            JSONObject jobj3 = new JSONObject();//parameters
            jobj3.put("offset", offset);
            jobj3.put("limit", limit);

            JSONObject jobj2 = new JSONObject();//input
            jobj2.put("keyword", getIntent().getStringExtra("catid").toString().trim());
            jobj2.put("user_id",  PreferenceManager_ASOS.GetUserID());


            JSONObject jobj = new JSONObject();//options
            jobj.put("input", jobj2);
            jobj.put("parameters", jobj3);
            try
            {
                object.put("options", jobj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            final String requestBody = object.toString();

            if(cancel_progress)
            {
                progressDialog=new ProgressDialog(Product_Listing.this,R.style.ProgressDialogStyle);
                progressDialog.setTitle("Loading");
                progressDialog.setMessage("Please wait ...");
                progressDialog.show();
            }


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.NEW_PRODUCTLIST_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    loader.setVisibility(View.GONE);
                    ArrayList<GETSET_PRODLIST> getset_prodlistArrayList=new ArrayList<>();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("Prodlist_CAT_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("Prodlist_CAT_res", response);
                        try
                        {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String code = jsonObject.getString("code");
                            String message = jsonObject.getString("message");

                            if(code.equals("000200"))
                            {

                                JSONObject datajsonObject=jsonObject.getJSONObject("data");
                                JSONArray prodlistarray=datajsonObject.getJSONArray("product_list");
                                for(int k=0;k<prodlistarray.length();k++)
                                {
                                    JSONObject jsonObject1 = prodlistarray.getJSONObject(k);

                                    String pid = jsonObject1.getString("product_id");
                                    String product_name = jsonObject1.getString("product_name");
                                    String product_price = jsonObject1.getString("product_price");
                                    String product_desc = jsonObject1.getString("product_desc");
                                    String quantity = jsonObject1.getString("quantity");
                                    String product_spec = jsonObject1.getString("product_spec");
                                    String tags = jsonObject1.getString("tags");
                                    ArrayList<String> Img = new ArrayList<>();

                                    JSONArray jsonArray = jsonObject1.getJSONArray("img_path");
                                    if(jsonArray!=null)
                                    {
                                        for (int i = 0; i < jsonArray.length(); i++)
                                        {
                                            Img.add(jsonArray.getString(i));
                                        }
                                    }
//                                Log.e("Img", String.valueOf(Img));

                                    String purchase_price = jsonObject1.getString("purchase_price");
                                    String avalibility = jsonObject1.getString("avalibility");
                                    String product_code = jsonObject1.getString("product_code");
                                    String notes = jsonObject1.getString("notes");
                                    String extra_1 = jsonObject1.getString("extra_1");
                                    String extra_2 = jsonObject1.getString("extra_2");
                                    final boolean favourite_status = jsonObject1.getBoolean("favourite_status");
//                                Log.e("favourite_status", String.valueOf(favourite_status));

                                    varientArray=new ArrayList<>();
                                    JSONArray jsonArray2 = jsonObject1.getJSONArray("variant_details");
                                    for (int i = 0; i < jsonArray2.length(); i++)
                                    {
                                        JSONObject jsonObject3 = jsonArray2.getJSONObject(i);
                                        int V_sr_no=jsonObject3.getInt("sr_no");
                                        String V_variant_name=jsonObject3.getString("variant_name");
                                        double V_purchase_price=jsonObject3.getDouble("purchase_price");
                                        double V_mrp=jsonObject3.getDouble("mrp");
                                        double V_final_price=jsonObject3.getDouble("final_price");
                                        int V_quantity=jsonObject3.getInt("quantity");

                                        Object json = jsonObject3.get("images");
                                        JSONArray imagevar = null;
                                        V_imgarray = new ArrayList<String>();

                                        if (json instanceof JSONArray)
                                        {
                                            imagevar = jsonObject3.getJSONArray("images");
                                            for (int j = 0; j < imagevar.length(); j++)
                                            {
                                                V_imgarray.add(String.valueOf(imagevar.get(j)));
                                            }

                                        }
                                        else
                                        {
                                        }

                                        Pop_PROD_Final_Varient_MODEL pop_prod_varient_model = new
                                                Pop_PROD_Final_Varient_MODEL(V_variant_name,V_sr_no,V_purchase_price, V_mrp, V_final_price,V_quantity,V_imgarray);
                                        varientArray.add(pop_prod_varient_model);
                                    }

                                    String final_price = jsonObject1.getString("final_price");

                                    getset_prodlist=new GETSET_PRODLIST();
                                    getset_prodlist.setProduct_id(pid);
                                    getset_prodlist.setProduct_name(product_name);
                                    getset_prodlist.setProduct_price(product_price);
                                    getset_prodlist.setFinal_price(final_price);
                                    getset_prodlist.setFavourite_status(favourite_status);
                                    getset_prodlist.setImg_path(Img);
                                    getset_prodlist.setVariant_img(V_imgarray);
                                    getset_prodlist.setVarientArray(varientArray);
                                    getset_prodlist.setNote(notes);

                                    getset_prodlistArrayList.add(getset_prodlist);
                                }
                            }


                            //Loader code
                            int oldcount = gather_getset_prodlistArrayList.size();
                            for (int i = 0; i < getset_prodlistArrayList.size(); i++)
                            {
                                offset++;
                                product_listing_adapter.add(getset_prodlistArrayList.get(i));
                            }


                            product_listing_adapter.notifyDataSetChanged();
                            IsFatchingData = 0;
//

                            if (getset_prodlistArrayList.size() % limit != 0 || oldcount == gather_getset_prodlistArrayList.size())
                            {
                                loader.setVisibility(View.GONE);
                            }

                            cancel_progress=false;
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Log.e("e", String.valueOf(e));
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Prodlist_CAT error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public class Product_Listing_Adapter extends RecyclerView.Adapter<Product_Listing_Adapter.ViewHolder>
    {
        ArrayList<GETSET_PRODLIST> Product_Listing;
        Context context;
        private RequestQueue rq;
        private PreferenceManager_ASOS PreferenceManager_ASOS;
        String prodid;
        boolean status;
        String STATUS;


        public Product_Listing_Adapter(Context context, ArrayList<GETSET_PRODLIST> Product_Listing)
        {
            super();
            this.context = context;
            this.Product_Listing = Product_Listing;
            rq= Volley.newRequestQueue(context);
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
        }

        //Loader code
        public void add(GETSET_PRODLIST country)
        {
            this.Product_Listing.add(country);
        }

        @Override
        public Product_Listing_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_product_list_lvitem, parent, false);
            Product_Listing_Adapter.ViewHolder viewHolder = new Product_Listing_Adapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final Product_Listing_Adapter.ViewHolder holder, final int position)
        {

            holder.prodname.setText(Product_Listing.get(position).getProduct_name());
            Log.e("getProduct_id: ",String.valueOf(Product_Listing.get(position).getProduct_id()));


            if(Product_Listing.get(position).getVarientArray().isEmpty())
            {
                holder.mrp_price.setText("₹ "+Product_Listing.get(position).getProduct_price());
                holder.finalprice.setText("₹ "+Product_Listing.get(position).getFinal_price());
            }
            else
            {
                holder.mrp_price.setText("₹ "+Product_Listing.get(position).getVarientArray().get(0).getMrp());
                holder.finalprice.setText("₹ "+Product_Listing.get(position).getVarientArray().get(0).getMrp());
            }


            //disc calculating starts
            if(Product_Listing.get(position).getVarientArray().isEmpty())
            {
                if(Product_Listing.get(position).getProduct_price().equals("")||Product_Listing.get(position).getProduct_price().equals("0"))
                {
                    holder.mrp_price.setVisibility(View.GONE);
                    holder.tvdisc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    holder.mrp_price.setVisibility(View.VISIBLE);
                    holder.tvdisc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(Product_Listing.get(position).getFinal_price());
                    double mrp_rate= Double.parseDouble(Product_Listing.get(position).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    holder.tvdisc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                holder.finalprice.setText("₹ "+Product_Listing.get(position).getFinal_price());
            }
            else
            {

                if(Product_Listing.get(position).getVarientArray().get(0).getMrp()==0)
                {
                    holder.mrp_price.setVisibility(View.GONE);
                    holder.tvdisc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    holder.mrp_price.setVisibility(View.VISIBLE);
                    holder.tvdisc.setVisibility(View.VISIBLE);

                    double final_rate= Product_Listing.get(position).getVarientArray().get(0).getFinal_price();
                    double mrp_rate= Product_Listing.get(position).getVarientArray().get(0).getMrp();
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    holder.tvdisc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                holder.finalprice.setText("₹ "+Product_Listing.get(position).getVarientArray().get(0).getFinal_price());
            }



            //img code
            if(Product_Listing.get(position).getVarientArray().isEmpty())
            {
                if (Product_Listing.get(position).getImg_path().size()>0)
                {
                    //normal img set code
                    Picasso.with(context)
                            .load(PaperCart_CoNNectioN.IMGPATH + "" + Product_Listing.get(position).getImg_path().get(0).toString())
                            .into(holder.imgThumbnail);

                    //img set low to high code
//                    String img_responce=Product_Listing.get(position).getImg_path().get(0).toString();
//                    final String aftersubstring_img_response = (img_responce.substring(img_responce.lastIndexOf("/") + 1));
//                    final String final_img8_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_8+""+aftersubstring_img_response;
//                    final String final_img4_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_4+""+aftersubstring_img_response;
//
//                    Picasso.with(context)
//                            .load(final_img8_path)
//                            .into(holder.imgThumbnail);
//
//
//                    new Handler().postDelayed(new Runnable()
//                    {
//                        @Override
//                        public void run()
//                        {
//                            // This method will be executed once the timer is over
//                            // Start your app main activity
//                            Picasso.with(context)
//                                    .load(final_img4_path)
//                                    .into(holder.imgThumbnail);
//
//                        }
//                    }, 3000);

                }
            }
            else
            {
                if (Product_Listing.get(position).getVariant_img().size()>0)
                {
                    //normal img set code
                    Picasso.with(context)
                            .load(PaperCart_CoNNectioN.IMGPATH + "" + Product_Listing.get(position).getVariant_img().get(0).toString())
                            .into(holder.imgThumbnail);

                    //img set low to high code

//                    String img_responce=Product_Listing.get(position).getVariant_img().get(0).toString();
//                    final String aftersubstring_img_response = (img_responce.substring(img_responce.lastIndexOf("/") + 1));
//                    final String final_img8_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_8+""+aftersubstring_img_response;
//                    final String final_img4_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_4+""+aftersubstring_img_response;
//
//                    Picasso.with(context)
//                            .load(final_img8_path)
//                            .into(holder.imgThumbnail);
//
//
//                    new Handler().postDelayed(new Runnable()
//                    {
//                        @Override
//                        public void run()
//                        {
//                            // This method will be executed once the timer is over
//                            // Start your app main activity
//                            Picasso.with(context)
//                                    .load(final_img4_path)
//                                    .into(holder.imgThumbnail);
//
//                        }
//                    }, 3000);
                }
            }


            //fav code

            //CHNGING ITEM SAVED BG
            if(PreferenceManager_ASOS.GetUserID().equals(""))
            {
                holder.ivfav.setBackgroundResource(R.drawable.unfilllike);
            }
            else
            {
                //fav onresume logic

                if(PreferenceManager_ASOS.getis_favchanged())
                {
                    if(Product_Listing.get(position).getProduct_id().equals(PreferenceManager_ASOS.getfavpid())) //if same
                    {
                        if(PreferenceManager_ASOS.getchange_fav_status())
                        {
                            Product_Listing.get(position).setFavourite_status(true);
                            holder.ivfav.setBackgroundResource(R.drawable.filllike);
                        }
                        else
                        {
                            Product_Listing.get(position).setFavourite_status(false);
                            holder.ivfav.setBackgroundResource(R.drawable.unfilllike);
                        }
                    }
                    else
                    {
                        if(!Product_Listing.get(position).isFavourite_status())
                        {
                            holder.ivfav.setBackgroundResource(R.drawable.unfilllike);
                        }
                        else
                        {
                            holder.ivfav.setBackgroundResource(R.drawable.filllike);
                        }
                    }
                }
                else
                {
                    if(!Product_Listing.get(position).isFavourite_status())
                    {
                        holder.ivfav.setBackgroundResource(R.drawable.unfilllike);
                    }
                    else
                    {
                        holder.ivfav.setBackgroundResource(R.drawable.filllike);
                    }
                }


            }
                //fav onclick code
            holder.ivfav.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(PreferenceManager_ASOS.GetUserID().equals(""))
                    {
                        Intent intent=new Intent(Product_Listing.this,Enter_OTP.class);
//                        intent.putExtra("from_intent","Product_Listing");
                        startActivity(intent);
                    }
                    else if(!PreferenceManager_ASOS.GetUserID().equals(""))
                    {
                        boolean abc=Product_Listing.get(position).isFavourite_status();
                        if(!abc)// if not fav
                        {
                            //doing fav
                            prodid= String.valueOf(Product_Listing.get(position).getProduct_id());
                            status=true;
                            STATUS="FAVOURITE";
                            Product_Listing.get(position).setFavourite_status(true);
                            ADD_FAVOURITE(holder.ivfav,prodid);

                        }
                        else
                        {
                            //doing unfav
                            prodid= String.valueOf(Product_Listing.get(position).getProduct_id());
                            status=false;
                            STATUS="UNFAVOURITE";
                            Product_Listing.get(position).setFavourite_status(false);
                            ADD_FAVOURITE(holder.ivfav,prodid);

                        }
                    }

                }
            });


            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        prodid= String.valueOf(Product_Listing.get(position).getProduct_id());
                        String getnote= String.valueOf(Product_Listing.get(position).getNote());
                        String getrid=getIntent().getStringExtra("catid").toString().trim();
                        Intent intent=new Intent(Product_Listing.this,Product_Detail.class);
                        intent.putExtra("prodid",prodid);
                        intent.putExtra("getnote",getnote);
                        intent.putExtra("getrecommended_id",getrid);
                        startActivity(intent);
                    }

                }
            });

        }


        @Override
        public int getItemCount() {
            return Product_Listing.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            public ImageView imgThumbnail,ivfav;
            public TextView prodname,finalprice,mrp_price,tvdisc;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);

                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                ivfav = (ImageView) itemView.findViewById(R.id.ivfavid);
                finalprice = (TextView) itemView.findViewById(R.id.finalpriceid);
                mrp_price = (TextView) itemView.findViewById(R.id.mrpid);
                prodname = (TextView) itemView.findViewById(R.id.prodnameid);
                tvdisc = (TextView) itemView.findViewById(R.id.discid);
                itemView.setOnClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

        }

        public void ADD_FAVOURITE(final ImageView ivfav,String p_id)
        {
            try
            {
                RequestQueue requestQueue = Volley.newRequestQueue(context);

                JSONObject object=new JSONObject() ;
                object.put("type","business_favourite_process");
                object.put("user_id",PreferenceManager_ASOS.GetUserID());
                object.put("action","add_product");
                JSONObject jsonObject2=new JSONObject();
                jsonObject2.put("product_id",p_id);
                jsonObject2.put("status",STATUS);

                object.put("options",jsonObject2);


                final String requestBody = object.toString();

                 stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {

                        String getresponce = response.replace("\"", "");

                        if(getresponce.equals("No Data Found"))
                        {
                            Log.e("FAV res", "BLANK DATA");
                        }
                        else
                        {
                            Log.e("FAV res", response);

                            try
                            {
                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
                                String status = jsonObject.getString("status");

                                if(status.equals("UNFAVOURITE"))
                                {
                                    ivfav.setBackgroundResource(R.drawable.unfilllike);

                                    Snackbar snackbar = Snackbar
                                            .make(parentlo, "Product removed from saved items....!!!", Snackbar.LENGTH_SHORT);
                                    View snackBarView = snackbar.getView();
                                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                    snackbar.show();
                                }
                                else
                                {
                                    ivfav.setBackgroundResource(R.drawable.filllike);

                                    Snackbar snackbar = Snackbar
                                            .make(parentlo, "Product successfully saved....!!!", Snackbar.LENGTH_SHORT);
                                    View snackBarView = snackbar.getView();
                                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                    snackbar.show();
                                }

                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                                Log.e("FAV exception", String.valueOf(e));

                            }

                        }
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        if (error instanceof NetworkError) {
                        } else if (error instanceof ServerError) {
                        } else if (error instanceof AuthFailureError) {
                        } else if (error instanceof ParseError) {
                        } else if (error instanceof NoConnectionError) {
                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(context,
                                    "Oops. Timeout error!",
                                    Toast.LENGTH_LONG).show();
                            Log.e("FAV error", error.toString());
                        }
                    }
                })

                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type","application/json");
                        params.put("authentication",PaperCart_HEADER.KEY);

                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError
                    {
                        try
                        {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        }
                        catch (UnsupportedEncodingException uee)
                        {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };
                stringRequest.setRetryPolicy
                        (new DefaultRetryPolicy(10000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                requestQueue.add(stringRequest);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }

    }

    //CALL WHEN CAME FROM SECTIONS
    public void SUB_SECTION_PROD_GETDATA(String getsecid)
    {
        IsFatchingData = 1;

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            JSONObject object=new JSONObject() ;
            object.put("type", "app_sections");
            object.put("action", "get_section_details");

            JSONObject object1 = new JSONObject();
            object.put("options", object1);
            object1.put("row_offset", offset);
            object1.put("row_count", limit);
            object1.put("section_id", getsecid);
            object1.put("user_id", PreferenceManager_ASOS.GetUserID());

//            JSONObject jobj = new JSONObject();
//            jobj.put("row_offset", offset);
//            jobj.put("row_count", limit);
//            jobj.put("section_id", getsecid);
//
//            try
//            {
//                object.put("type", "app_sections");
//                object.put("action", "get_section_details");
//                object.put("user_id", PreferenceManager_ASOS.GetUserID());
//                object.put("options", jobj);
//            }
//            catch (JSONException e)
//            {
//                e.printStackTrace();
//            }

            final String requestBody = object.toString();

            if(cancel_progress)
            {
                progressDialog=new ProgressDialog(Product_Listing.this,R.style.ProgressDialogStyle);
                progressDialog.setTitle("Loading");
                progressDialog.setMessage("Please wait ...");
                progressDialog.show();
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {

                    progressDialog.dismiss();

                    loader.setVisibility(View.GONE);
                    ArrayList<SUBSECTIONS_MODEL>subsections_modelArrayList=new ArrayList<>();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("SUB_SECTION_PROD_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("SUB_SECTION_PROD_res", response);
                        try
                        {
                            JSONObject mainObject=new JSONObject(String.valueOf(response));
                            JSONObject jsonObject=mainObject.getJSONObject("data");

                            JSONArray jsonArray=jsonObject.getJSONArray("description");
                            for (int a = 0; a < jsonArray.length(); a++)
                            {
                                ArrayList<RecentView_Varient_MODEL>recentView_varient_modelArrayList = new ArrayList<>();
                                ArrayList<String>Img=new ArrayList<>();


                                JSONObject jsonObject1 = jsonArray.getJSONObject(a);
                                String product_id=jsonObject1.getString("product_id");
                                String product_spec=jsonObject1.getString("product_spec");
                                String  product_desc=jsonObject1.getString("product_desc");
                                String  product_price=jsonObject1.getString("mrp");
                                String product_name=jsonObject1.getString("product_name");
                                String  quantity=jsonObject1.getString("quantity");
                                String  Final_Price=jsonObject1.getString("Final_Price");
                                String avalibility=jsonObject1.getString("avalibility");

                                JSONArray VarientArray=jsonObject1.getJSONArray("variant");
                                for (int b = 0; b < VarientArray.length(); b++)
                                {
                                    JSONObject VarientObj= VarientArray.getJSONObject(b);

                                    String v_name=VarientObj.getString("name");
                                    String v_product_price=VarientObj.getString("product_price");
                                    String v_Final_Price=VarientObj.getString("Final_Price");
                                    String v_Quantity=VarientObj.getString("Quantity");
                                    String v_purchase_price=VarientObj.getString("purchase_price");

                                    RecentView_Varient_MODEL recentView_varient_model=new RecentView_Varient_MODEL(v_name,v_product_price,v_Final_Price,v_purchase_price,v_Quantity);
                                    recentView_varient_modelArrayList.add(recentView_varient_model);
                                }

                                JSONArray Prods_img_jsonArray = jsonObject1.getJSONArray("img");
                                for (int i = 0; i < Prods_img_jsonArray.length(); i++)
                                {

                                    Img.add(Prods_img_jsonArray.getString(i));
                                }

                                String Product_Favourite_Status=jsonObject1.getString("Product_Favourite_Status");
                                String product_keywords=jsonObject1.getString("product_keywords");
                                String purchase_price=jsonObject1.getString("purchase_price");
                                String product_code=jsonObject1.getString("product_code");
                                String notes=jsonObject1.getString("notes");



                                SUBSECTIONS_MODEL subsections_model=new SUBSECTIONS_MODEL
                                        (product_id,
                                                product_spec,
                                                product_name,
                                                product_price,
                                                product_desc,
                                                product_keywords,
                                                quantity,
                                                Final_Price,
                                                recentView_varient_modelArrayList,
                                                Img,
                                                avalibility,
                                                Product_Favourite_Status,
                                                purchase_price,
                                                product_code,
                                                notes);

                                subsections_modelArrayList.add(subsections_model);


                            }

                            //Loader code

                            int oldcount = gather_getset2_prodlistArrayList.size();
                            for (int i = 0; i < subsections_modelArrayList.size(); i++)
                            {
                                offset++;
                                subsections_adapter.add(subsections_modelArrayList.get(i));
                            }


                            subsections_adapter.notifyDataSetChanged();
                            IsFatchingData = 0;

                            if (subsections_modelArrayList.size() % limit != 0 || oldcount == gather_getset2_prodlistArrayList.size())
                            {
                                loader.setVisibility(View.GONE);
                            }

                            cancel_progress=false;
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("SUB_SECTION_PROD error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication",PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            stringRequest.setRetryPolicy
                    (new DefaultRetryPolicy(10000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    //SECTION DETAIL PROD ADAPTER
    public class SUBSECTIONS_Adapter extends RecyclerView.Adapter<SUBSECTIONS_Adapter.ViewHolder>
    {
        ArrayList<SUBSECTIONS_MODEL> recentView_array;
        Context context;
        String prodid;
        String STATUS;
        private PreferenceManager_ASOS PreferenceManager_ASOS;

        public SUBSECTIONS_Adapter(Context context,  ArrayList<SUBSECTIONS_MODEL> recentView_array)
        {
            super();
            this.context = context;
            this.recentView_array = recentView_array;
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
        }

        //Loader code
        public void add(SUBSECTIONS_MODEL country)
        {
            this.recentView_array.add(country);
        }

        @Override
        public SUBSECTIONS_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
        {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.asos_product_list_lvitem, viewGroup, false);
            SUBSECTIONS_Adapter.ViewHolder viewHolder = new SUBSECTIONS_Adapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final SUBSECTIONS_Adapter.ViewHolder viewHolder, final int i)
        {

            //settext

            viewHolder.prodname.setText(recentView_array.get(i).getProduct_name());
            Log.e("prodname: ",recentView_array.get(i).getProduct_name());

            //normal img set code
            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH+""+recentView_array.get(i).getImg().get(0))
                    .into(viewHolder.imgThumbnail);

            //img set low to high code
//            String img_responce=recentView_array.get(i).getImg().get(0);
//            final String aftersubstring_img_response = (img_responce.substring(img_responce.lastIndexOf("/") + 1));
//            final String final_img8_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_8+""+aftersubstring_img_response;
//            final String final_img4_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_4+""+aftersubstring_img_response;
//
//            Picasso.with(context)
//                    .load(final_img8_path)
//                    .into(viewHolder.imgThumbnail);
//
//
//            new Handler().postDelayed(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    // This method will be executed once the timer is over
//                    // Start your app main activity
//                    Picasso.with(context)
//                            .load(final_img4_path)
//                            .into(viewHolder.imgThumbnail);
//
//                }
//            }, 3000);

            if(recentView_array.get(i).getRecentView_varient_models().isEmpty())
            {
                viewHolder.mrp.setText("₹ "+recentView_array.get(i).getProduct_price());
                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getFinal_Price());
            }
            else
            {
                viewHolder.mrp.setText("₹ "+recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price());
                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getRecentView_varient_models().get(0).getFinal_Price());
            }

            //disc calculating starts
            if(recentView_array.get(i).getRecentView_varient_models().isEmpty())
            {
                if(recentView_array.get(i).getProduct_price().equals("")||recentView_array.get(i).getProduct_price().equals("0"))
                {
                    viewHolder.mrp.setVisibility(View.GONE);
                    viewHolder.disc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    viewHolder.mrp.setVisibility(View.VISIBLE);
                    viewHolder.disc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(recentView_array.get(i).getFinal_Price());
                    double mrp_rate= Double.parseDouble(recentView_array.get(i).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    viewHolder.disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getFinal_Price());
            }
            else
            {

                if(recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price().equals("")||recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price().equals("0"))
                {
                    viewHolder.mrp.setVisibility(View.GONE);
                    viewHolder.disc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    viewHolder.mrp.setVisibility(View.VISIBLE);
                    viewHolder.disc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(recentView_array.get(i).getRecentView_varient_models().get(0).getFinal_Price());
                    double mrp_rate= Double.parseDouble(recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    viewHolder.disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getRecentView_varient_models().get(0).getFinal_Price());
            }

           //fav code
            final String check_user=PreferenceManager_ASOS.GetUserID();
            if(check_user.equals(""))
            {
                viewHolder.ivfav.setBackgroundResource(R.drawable.unfilllike);

            }
            else
            {
                //CHNGING ITEM SAVED BG

                if(recentView_array.get(i).getProduct_Favourite_Status().equals("None"))
                {
                    viewHolder.ivfav.setBackgroundResource(R.drawable.unfilllike);
                }
                else
                {
                    viewHolder.ivfav.setBackgroundResource(R.drawable.filllike);
                }
            }

            //fav onclick code
            viewHolder.ivfav.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
//                    boolean abc=recentView_array.get(i).getProduct_Favourite_Status();

                    if(check_user.equals(""))
                    {
                        Intent intent=new Intent(Product_Listing.this,Enter_OTP.class);
//                        intent.putExtra("from_intent","Product_Listing");
                        startActivity(intent);
                    }
                    else
                    {
                        if(recentView_array.get(i).getProduct_Favourite_Status().equals("None"))
                        {
                            prodid= String.valueOf(recentView_array.get(i).getProduct_id());
                            STATUS="FAVOURITE";
                            recentView_array.get(i).setProduct_Favourite_Status("FAVOURITE");
                            ADD_FAVOURITE(viewHolder.ivfav,prodid);
                        }
                        else
                        {
                            prodid= String.valueOf(recentView_array.get(i).getProduct_id());
                            STATUS="UNFAVOURITE";
                            recentView_array.get(i).setProduct_Favourite_Status("None");
                            ADD_FAVOURITE(viewHolder.ivfav,prodid);
                        }
                    }


                }

            });

            viewHolder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        String prodid= recentView_array.get(position).getProduct_id();
                        String getnote= String.valueOf(recentView_array.get(position).getNote());
                        Intent intent=new Intent(Product_Listing.this,Product_Detail.class);
                        intent.putExtra("prodid",prodid);
                        intent.putExtra("getnote",getnote);
                        intent.putExtra("getrecommended_id","");
                        startActivity(intent);
                    }
                }
            });

        }

        @Override
        public int getItemCount()
        {
            return recentView_array.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            public ImageView imgThumbnail,ivfav;
            public TextView prodname,disc,mrp,finalprice;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                prodname = (TextView) itemView.findViewById(R.id.prodnameid);
                disc = (TextView) itemView.findViewById(R.id.discid);
                mrp = (TextView) itemView.findViewById(R.id.mrpid);
                finalprice = (TextView) itemView.findViewById(R.id.finalpriceid);
                ivfav = (ImageView) itemView.findViewById(R.id.ivfavid);
                itemView.setOnClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view)
            {
                clickListener.onClick(view, getPosition(), false);
            }



        }

        public void ADD_FAVOURITE(final ImageView ivfav,String p_id)
        {
            try
            {
                RequestQueue requestQueue = Volley.newRequestQueue(context);

                JSONObject object=new JSONObject() ;
                object.put("type","business_favourite_process");
                object.put("user_id",PreferenceManager_ASOS.GetUserID());
                object.put("action","add_product");
                JSONObject jsonObject2=new JSONObject();
                jsonObject2.put("product_id",p_id);
                jsonObject2.put("status",STATUS);

                object.put("options",jsonObject2);


                final String requestBody = object.toString();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {

                        String getresponce = response.replace("\"", "");

                        if(getresponce.equals("No Data Found"))
                        {
                            Log.e("FAV res", "BLANK DATA");
                        }
                        else
                        {
                            Log.e("FAV res", response);

                            try
                            {
                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
                                String status = jsonObject.getString("status");

                                    if(status.equals("UNFAVOURITE"))
                                    {
                                        ivfav.setBackgroundResource(R.drawable.unfilllike);

                                        Snackbar snackbar = Snackbar
                                                .make(parentlo, "Product removed from saved items....!!!", Snackbar.LENGTH_SHORT);
                                        View snackBarView = snackbar.getView();
                                        snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                        snackbar.show();
                                    }
                                    else
                                    {
                                        ivfav.setBackgroundResource(R.drawable.filllike);

                                        Snackbar snackbar = Snackbar
                                                .make(parentlo, "Product successfully saved....!!!", Snackbar.LENGTH_SHORT);
                                        View snackBarView = snackbar.getView();
                                        snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                        snackbar.show();
                                    }

                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                                Log.e("e", String.valueOf(e));

                            }

                        }
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("FAV error", error.toString());
                    }
                })

                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type","application/json");
                        params.put("authentication", PaperCart_HEADER.KEY);

                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError
                    {
                        try
                        {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        }
                        catch (UnsupportedEncodingException uee)
                        {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                requestQueue.add(stringRequest);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }

    }

    public void SPINNER_ONCLICK()
    {
        SortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            String general="";

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                ((TextView) view).setTextColor(Color.BLACK); //Change selected text color
                ((TextView) view).setTextSize(14); //Change selected text color
                ((TextView) view).setTypeface(Typeface.DEFAULT_BOLD);//change font

                if(position==0)
                {
                    if(general.equals("yes"))
                    {
                        GENERAL();
//                        Toast.makeText(getApplicationContext(),""+position,Toast.LENGTH_SHORT).show();
                    }
                }
                else if(position==1)//LOW TO HIGH
                {
                    general="yes";
                    LOW_TO_HIGH();
                }
                else if(position==2)//HIGN TO LOW
                {
                    general="yes";
                    HIGH_TO_LOW();
//                    Toast.makeText(getApplicationContext(),""+position,Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

    }

    public void GENERAL()
    {
        IsFatchingData = 0;
        offset = 0;
        limit = 6;
        cancel_progress=true;
        gather_getset_prodlistArrayList.clear();

        //---------------
        toolbar.setVisibility(View.VISIBLE);
        nonet_lo.setVisibility(View.GONE);
        mainlo.setVisibility(View.VISIBLE);

        getintent= getIntent().getStringExtra("from_intent");

        if(getintent.equals("from_category"))  //IF CAME FROM CATEGORY....
        {
            String getCATEGORY_NAME= getIntent().getStringExtra("CATEGORY_NAME");
            getSupportActionBar().setTitle(getCATEGORY_NAME);
//            feature_cardview.setVisibility(View.VISIBLE);

            Prod_list_RecyclerView = (RecyclerView) findViewById(R.id.prodlist_recycler_view);
            Prod_list_RecyclerView.setHasFixedSize(true);
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            Prod_list_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
            product_listing_adapter=new Product_Listing_Adapter(getApplicationContext(),gather_getset_prodlistArrayList);
            Prod_list_RecyclerView.setAdapter(product_listing_adapter);

            Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
            {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy)
                {
                    super.onScrolled(recyclerView, dx, dy);

                    visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                    Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                    totalItemCount = gaggeredGridLayoutManager.getItemCount();
                    Log.e("totalItemCount", String.valueOf(totalItemCount));

                    firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                    Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    Log.e("lastInScreen", String.valueOf(lastInScreen));

                    // Scrolling up
                    if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                    {
                        loader.setVisibility(View.VISIBLE);
                        PROD_LISTING_FROM_CAT_GETDATA();
                    }

                }
            });

            Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    if (!recyclerView.canScrollVertically(1))
                    {
                        Snackbar snackbar = Snackbar
                                .make(parentlo, "Reached at the bottom...!!!", Snackbar.LENGTH_SHORT);
                        View snackBarView = snackbar.getView();
                        snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                        snackbar.show();
                    }

                }
            });

            PROD_LISTING_FROM_CAT_GETDATA();
        }
        else if(getintent.equals("from_subsec_prod")) //IF CAME FROM SECTIONS....
        {
            final String getsecid=getIntent().getStringExtra("getsecid").toString();
            Log.e("getsecid",getsecid);
            String getsecname=getIntent().getStringExtra("getsecname").toString();

            getSupportActionBar().setTitle(getsecname);
//            feature_cardview.setVisibility(View.GONE);

            Prod_list_RecyclerView = (RecyclerView) findViewById(R.id.prodlist_recycler_view);
            Prod_list_RecyclerView.setHasFixedSize(true);
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            Prod_list_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
            subsections_adapter=new SUBSECTIONS_Adapter(getApplicationContext(),gather_getset2_prodlistArrayList);
            Prod_list_RecyclerView.setAdapter(subsections_adapter);

            Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
            {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy)
                {
                    super.onScrolled(recyclerView, dx, dy);

                    visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                    Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                    totalItemCount = gaggeredGridLayoutManager.getItemCount();
                    Log.e("totalItemCount", String.valueOf(totalItemCount));

                    firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                    Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    Log.e("lastInScreen", String.valueOf(lastInScreen));

                    if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                    {
                        loader.setVisibility(View.VISIBLE);
                        SUB_SECTION_PROD_GETDATA(getsecid);
                    }
                }
            });

            // DETECT RECYCLER REACHED AT BOTTOM...
            Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    if (!recyclerView.canScrollVertically(1))
                    {
                        Snackbar snackbar = Snackbar
                                .make(parentlo, "Reached at the bottom...!!!", Snackbar.LENGTH_SHORT);
                        View snackBarView = snackbar.getView();
                        snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                        snackbar.show();
                    }
                }
            });


            SUB_SECTION_PROD_GETDATA(getsecid);
        }

        //---------------
    }

    public void HIGH_TO_LOW()
    {
        sort_type="PRICE_HIGH_TO_LOW";

        IsFatchingData = 0;
        offset = 0;
        limit = 6;
        cancel_progress=true;
        gather_getset_prodlistArrayList.clear();

        Prod_list_RecyclerView = (RecyclerView) findViewById(R.id.prodlist_recycler_view);
        Prod_list_RecyclerView.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        Prod_list_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
        product_listing_adapter=new Product_Listing_Adapter(getApplicationContext(),gather_getset_prodlistArrayList);
        Prod_list_RecyclerView.setAdapter(product_listing_adapter);

        Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                totalItemCount = gaggeredGridLayoutManager.getItemCount();
                Log.e("totalItemCount", String.valueOf(totalItemCount));

                firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                int lastInScreen = firstVisibleItem + visibleItemCount;
                Log.e("lastInScreen", String.valueOf(lastInScreen));

                // Scrolling up
                if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                {
                    loader.setVisibility(View.VISIBLE);
                    SORTBY_GETDATA(sort_type);
                }

            }
        });

        Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1))
                {
                    Snackbar snackbar = Snackbar
                            .make(parentlo, "Reached at the bottom...!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                    snackbar.show();
                }

            }
        });

        SORTBY_GETDATA(sort_type);
    }

    public void LOW_TO_HIGH()
    {
        sort_type="PRICE_LOW_TO_HIGH";

        IsFatchingData = 0;
        offset = 0;
        limit = 6;
        cancel_progress=true;
        gather_getset_prodlistArrayList.clear();

        Prod_list_RecyclerView = (RecyclerView) findViewById(R.id.prodlist_recycler_view);
        Prod_list_RecyclerView.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        Prod_list_RecyclerView.setLayoutManager(gaggeredGridLayoutManager);
        product_listing_adapter=new Product_Listing_Adapter(getApplicationContext(),gather_getset_prodlistArrayList);
        Prod_list_RecyclerView.setAdapter(product_listing_adapter);

        Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = gaggeredGridLayoutManager.getChildCount();
                Log.e("visibleItemCount", String.valueOf(visibleItemCount));

                totalItemCount = gaggeredGridLayoutManager.getItemCount();
                Log.e("totalItemCount", String.valueOf(totalItemCount));

                firstVisibleItem = gaggeredGridLayoutManager.findFirstVisibleItemPositions(null)[0];
                Log.e("firstVisibleItem", String.valueOf(firstVisibleItem));


                int lastInScreen = firstVisibleItem + visibleItemCount;
                Log.e("lastInScreen", String.valueOf(lastInScreen));

                // Scrolling up
                if ((lastInScreen == totalItemCount)&& IsFatchingData == 0)
                {
                    loader.setVisibility(View.VISIBLE);
                    SORTBY_GETDATA(sort_type);
                }

            }
        });

        Prod_list_RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1))
                {
                    Snackbar snackbar = Snackbar
                            .make(parentlo, "Reached at the bottom...!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                    snackbar.show();
                }

            }
        });

        SORTBY_GETDATA(sort_type);
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        //fav onresume logic
        product_listing_adapter.notifyDataSetChanged();

    }

    @Override
    public void onBackPressed()
    {
        //fav onresume logic
        PreferenceManager_ASOS.setis_favchanged(false);
        super.onBackPressed();
        finish();
    }

}
