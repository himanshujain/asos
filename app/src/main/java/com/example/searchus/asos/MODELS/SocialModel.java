package com.example.searchus.asos.MODELS;

import java.io.Serializable;

/**
 * Created by DELL on 02/11/2017.
 */
public class SocialModel implements Serializable {
    private String name;
    private String link;
    private String icon;

    public SocialModel(String name, String link, String icon) {
        this.name = name;
        this.link = link;
        this.icon = icon;
    }

    public SocialModel() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
