package com.example.searchus.asos.NAV_FRAG;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.searchus.asos.R;

import com.example.searchus.asos.TAB_FRAG.CAT_TAB_FRAG;
import com.example.searchus.asos.TAB_FRAG.SECTION_TAB_FRAG;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Jauhar xlr on 4/18/2016.
 *  mycreativecodes.in
 */
public class HomeFragment extends Fragment
{

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 2 ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        /**
         *Inflate tab_layout and setup Views.
         */
            View x =  inflater.inflate(R.layout.asos_home_tab_layout,null);
            tabLayout = (TabLayout) x.findViewById(R.id.tabs);
            viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        setViewPager(viewPager);

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

//        tabLayout.post(new Runnable() {
//            @Override
//            public void run() {
                    tabLayout.setupWithViewPager(viewPager);
//                   }
//        });

        return x;

    }
private void setViewPager(ViewPager viewPager){
        MyAdapter myAdapter=new MyAdapter(getFragmentManager());
        myAdapter.addFrag(new SECTION_TAB_FRAG(),"HOME");
    myAdapter.addFrag(new CAT_TAB_FRAG(),"Category");
    viewPager.setAdapter(myAdapter);
}
    class MyAdapter extends FragmentPagerAdapter{
        private final List<Fragment> mfraglist=new ArrayList<>();
        private final List<String> mfragtitlelist=new ArrayList<>();

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
//          switch (position)
//          {
//              case 0 : return new SECTION_TAB_FRAG();
//              case 1 : return new CAT_TAB_FRAG();
////              case 2 : return new ExpoTabFragment();
//          }
        return mfraglist.get(position);
        }

        @Override
        public int getCount()
        {

            return mfraglist.size();

        }

        /**
         * This method returns the title of the tab according to the position.
         */
        public void addFrag(Fragment fragment,String title){
            mfraglist.add(fragment);
            mfragtitlelist.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {

//            switch (position)
//            {
//                case 0 :
//                    return "HOME";
//                case 1 :
//                    return "CATEGORIES";
////                case 2 :
////                    return "Expo";
//            }
                return mfragtitlelist.get(position);
        }
    }

}
