package com.example.searchus.asos.PREFERENCE_MANAGER;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;


@SuppressLint("Registered")
public class PreferenceManager_ASOS extends Application
{
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;

    private static final String SHARED_PREF_NAME="SEARCHUSTHEME";

    @SuppressLint("CommitPrefEdits")
    public PreferenceManager_ASOS(Context context)
    {
        this.context=context;
        preferences=context.getSharedPreferences(SHARED_PREF_NAME,0);
        editor=preferences.edit();
    }

    //pref for profile
    public void SetUserID(String UserID)
    {
        editor.putString("UserID",UserID).commit();
    }
    public String GetUserID()
    {
        return preferences.getString("UserID","");
    }


    public void setprimary_number(String primary_number)
    {
        editor.putString("primary_number",primary_number).commit();
    }
    public String getprimary_number()
    {
        return preferences.getString("primary_number","");
    }


    public void SetUser_NAME(String User_NAME)
    {
        editor.putString("User_NAME",User_NAME).commit();
    }
    public String GetUser_NAME()
    {
        return preferences.getString("User_NAME","");
    }

    public void setemail(String email)
    {
        editor.putString("email",email).commit();
    }
    public String getemail()
    {
        return preferences.getString("email","");
    }

    public void setalternate_no(String alternate_no)
    {
        editor.putString("alternate_no",alternate_no).commit();
    }
    public String getalternate_no()
    {
        return preferences.getString("alternate_no","");
    }

    public void setgender(String gender)
    {
        editor.putString("gender",gender).commit();
    }
    public String getgender()
    {
        return preferences.getString("gender","");
    }

    public void setradiobtindex(String radiobtindex)
    {
        editor.putString("radiobtindex",radiobtindex).commit();
    }
    public String getradiobtindex()
    {
        return preferences.getString("radiobtindex","");
    }


    public void setbirthdate(String birthdate)
    {
        editor.putString("birthdate",birthdate).commit();
    }
    public String getbirthdate()
    {
        return preferences.getString("birthdate","");
    }

    public void setversion(String version)
    {
        editor.putString("version",version).commit();
    }
    public String getversion()
    {
        return preferences.getString("version","");
    }

    public void setshipping_method(String shipping_method)
    {
        editor.putString("shipping_method",shipping_method).commit();
    }
    public String getshipping_method()
    {
        return preferences.getString("shipping_method","");
    }

    public void setemergency_closer_note(String emergency_closer_note)
    {
        editor.putString("emergency_closer_note",emergency_closer_note).commit();
    }
    public String getemergency_closer_note()
    {
        return preferences.getString("emergency_closer_note","");
    }

    public void setemergency_note(String emergency_note)
    {
        editor.putString("emergency_note",emergency_note).commit();
    }
    public String getemergency_note()
    {
        return preferences.getString("emergency_note","");
    }

    public void settype(String type)
    {
        editor.putString("type",type).commit();
    }
    public String gettype()
    {
        return preferences.getString("type","");
    }

    public void setimage_path(String image_path)
    {
        editor.putString("image_path",image_path).commit();
    }
    public String getimage_path()
    {
        return preferences.getString("image_path","");
    }

    public void setcheckToday_date(String checkToday_date)
    {
        editor.putString("checkToday_date",checkToday_date).commit();
    }
    public String getcheckToday_date()
    {
        return preferences.getString("checkToday_date","");
    }

    //IMG PREF
    public void setV_IMG(String V_IMG)
    {
        editor.putString("V_IMG",V_IMG).commit();
    }
    public String getV_IMG()
    {
        return preferences.getString("V_IMG","");
    }


    //imp pref
    public void SetSearchUsID(String SearchUsID)
    {
        editor.putString("SearchUsID",SearchUsID).commit();
    }
    public String GetSearchUsID()
    {
        return preferences.getString("SearchUsID","");
    }


    public void SetBusinessID(String BusinessID)
    {
        editor.putString("BusinessID",BusinessID).commit();
    }
    public String GetBusinessID()
    {
        return preferences.getString("BusinessID","");
    }

    public void SetBusiness_Logo(String Business_Logo)
    {
        editor.putString("Business_Logo",Business_Logo).commit();
    }
    public String GetBusiness_Logo()
    {
        return preferences.getString("Business_Logo","");
    }

    public void SetBusiness_Name(String Business_Name)
    {
        editor.putString("Business_Name",Business_Name).commit();
    }
    public String GetBusiness_Name()
    {
        return preferences.getString("Business_Name","");
    }

    public void SetBusinessOwnerName(String BusinessOwnerName)
    {
        editor.putString("BusinessOwnerName",BusinessOwnerName).commit();
    }
    public String GetBusinessOwnerName()
    {
        return preferences.getString("BusinessOwnerName","");
    }


    public void setAuthKey(String AuthKey)
    {
        editor.putString("AuthKey",AuthKey).commit();
    }
    public String getAuthKey()
    {
        return preferences.getString("AuthKey","");
    }

    //other pref
    public void setcamefrom(String camefrom)
    {
        editor.putString("camefrom",camefrom).commit();
    }
    public String getcamefrom()
    {
        return preferences.getString("camefrom","");
    }

    //Arraylist pref
    public void setkeywords(Set<String> keywords)
    {
        editor.putStringSet("keywords", keywords).commit();
    }
    public Set<String> getkeywords()
    {
        return preferences.getStringSet("keywords",null);
    }

    //int pref
    public void setupdate_type(int update_type)
    {
        editor.putInt("update_type",update_type).commit();
    }
    public int getupdate_type()
    {
        return preferences.getInt("update_type",0);
    }

    public void setV_QTY(int V_QTY)
    {
        editor.putInt("V_QTY",V_QTY).commit();
    }
    public int getV_QTY()
    {
        return preferences.getInt("V_QTY",0);
    }


    //boolean pref
    public void setQTY_Status(Boolean QTY_Status)
    {
        editor.putBoolean("QTY_Status",QTY_Status).commit();
    }

    public boolean getQTY_Status()
    {
        return preferences.getBoolean("QTY_Status", Boolean.parseBoolean(""));

    }

    public void setapp_version_status(Boolean app_version_status)
    {
        editor.putBoolean("app_version_status",app_version_status).commit();
    }

    public boolean getapp_version_status()
    {
        return preferences.getBoolean("app_version_status", Boolean.parseBoolean(""));

    }

    public void setemergency_closer_status(Boolean emergency_closer_status)
    {
        editor.putBoolean("emergency_closer_status",emergency_closer_status).commit();
    }

    public boolean getemergency_closer_status()
    {
        return preferences.getBoolean("emergency_closer_status", Boolean.parseBoolean(""));

    }

    public void setemergency_closer_status_main(Boolean emergency_closer_status_main)
    {
        editor.putBoolean("emergency_closer_status_main",emergency_closer_status_main).commit();
    }

    public boolean getemergency_closer_status_main()
    {
        return preferences.getBoolean("emergency_closer_status_main", Boolean.parseBoolean(""));

    }

    public void setauto_search_keyword_status(Boolean setauto_search_keyword_status)
    {
        editor.putBoolean("setauto_search_keyword_status",setauto_search_keyword_status).commit();
    }

    public boolean getauto_search_keyword_status()
    {
        return preferences.getBoolean("setauto_search_keyword_status", Boolean.parseBoolean(""));
    }

    public void setauto_search_keyword_status_main(Boolean auto_search_keyword_status_main)
    {
        editor.putBoolean("auto_search_keyword_status_main",auto_search_keyword_status_main).commit();
    }

    public boolean getauto_search_keyword_status_main()
    {
        return preferences.getBoolean("auto_search_keyword_status_main", Boolean.parseBoolean(""));
    }

    public void setshipping_type_main_status(Boolean shipping_type_main_status)
    {
        editor.putBoolean("shipping_type_main_status",shipping_type_main_status).commit();
    }

    public boolean getshipping_type_main_status()
    {
        return preferences.getBoolean("shipping_type_main_status", Boolean.parseBoolean(""));
    }



    public void setemergency_note_status(Boolean emergency_note_status)
    {
        editor.putBoolean("emergency_note_status",emergency_note_status).commit();
    }

    public boolean getemergency_note_status()
    {
        return preferences.getBoolean("emergency_note_status", Boolean.parseBoolean(""));

    }


    public void setiF_WALLET(Boolean iF_WALLET)
    {
        editor.putBoolean("iF_WALLET",iF_WALLET).commit();
    }

    public boolean getiF_WALLET()
    {
        return preferences.getBoolean("iF_WALLET", Boolean.parseBoolean(""));

    }

    public void setiF_PROMOCODE(Boolean iF_PROMOCODE)
    {
        editor.putBoolean("iF_PROMOCODE",iF_PROMOCODE).commit();
    }

    public boolean getIF_PROMOCOD()
    {
        return preferences.getBoolean("iF_PROMOCODE", Boolean.parseBoolean(""));

    }

    public void setshopping_amt(String shopping_amt)
    {
        editor.putString("shopping_amt",shopping_amt).commit();
    }
    public String getshopping_amt()
    {
        return preferences.getString("shopping_amt","");
    }

    public void setwallet_disc_amt(String wallet_disc_amt)
    {
        editor.putString("wallet_disc_amt",wallet_disc_amt).commit();
    }
    public String getwallet_disc_amt()
    {
        return preferences.getString("wallet_disc_amt","");
    }

    public void setpromocode_disc_amt(String promocode_disc_amt)
    {
        editor.putString("promocode_disc_amt",promocode_disc_amt).commit();
    }
    public String getpromocode_disc_amt()
    {
        return preferences.getString("promocode_disc_amt","");
    }

    public void setpromocode(String setpromocode)
    {
        editor.putString("setpromocode",setpromocode).commit();
    }
    public String getpromocode()
    {
        return preferences.getString("setpromocode","");
    }

    public void setpromocodeDisc(String promocodeDisc)
    {
        editor.putString("promocodeDisc",promocodeDisc).commit();
    }
    public String getpromocodeDisc()
    {
        return preferences.getString("promocodeDisc","");
    }

    public void setshippingAmt(String shippingAmt)
    {
        editor.putString("shippingAmt",shippingAmt).commit();
    }
    public String getshippingAmt()
    {
        return preferences.getString("shippingAmt","");
    }

    public void setfreedeliveryAmt(String freedeliveryAmt)
    {
        editor.putString("freedeliveryAmt",freedeliveryAmt).commit();
    }
    public String getfreedeliveryAmt()
    {
        return preferences.getString("freedeliveryAmt","");
    }

    public void setWalletAmt(String WalletAmt)
    {
        editor.putString("WalletAmt",WalletAmt).commit();
    }
    public String getWalletAmt()
    {
        return preferences.getString("WalletAmt","");
    }

    //on resume fav logic pref

    public void setis_favchanged(Boolean is_favchanged)
    {
        editor.putBoolean("is_favchanged",is_favchanged).commit();
    }

    public boolean getis_favchanged()
    {
        return preferences.getBoolean("is_favchanged", Boolean.parseBoolean(""));

    }

    public void setfavpid(String favpid)
    {
        editor.putString("favpid",favpid).commit();
    }
    public String getfavpid()
    {
        return preferences.getString("favpid","");
    }

    public void setchange_fav_status(Boolean change_fav_status)
    {
        editor.putBoolean("change_fav_status",change_fav_status).commit();
    }

    public boolean getchange_fav_status()
    {
        return preferences.getBoolean("change_fav_status", Boolean.parseBoolean(""));

    }

    //order cancel pref
    public void set_isordercancel(Boolean _isordercancel)
    {
        editor.putBoolean("_isordercancel",_isordercancel).commit();
    }

    public boolean get_isordercancel()
    {
        return preferences.getBoolean("_isordercancel", Boolean.parseBoolean(""));

    }

    public void setcancel_orderid(String cancel_orderid)
    {
        editor.putString("cancel_orderid",cancel_orderid).commit();
    }
    public String getcancel_orderid()
    {
        return preferences.getString("cancel_orderid","");
    }

    public void setcancel_orderstatus(String cancel_orderstatus)
    {
        editor.putString("cancel_orderstatus",cancel_orderstatus).commit();
    }
    public String getcancel_orderstatus()
    {
        return preferences.getString("cancel_orderstatus","");
    }


}
