package com.example.searchus.asos.MODELS;

import java.util.ArrayList;

public class My_Order_GETSET
{
    public My_Order_GETSET(String order_ID, double total_Amount, String payment_Type, String order_Date, String name, String address, String country, String state, String city, String pincode, String phone, String alternate_phone_no, String near_by, String product_id, String product_name, String variant_name, String quantity, String price, String amount, String saler_status, String customer_status, ArrayList<String> prods_img_jsonArray, String current_status, String current_time, String current_notes, String status, String time, String notes, String shipping_charge, String discount, String discount_amount, String usable_amount, String wallet_use, boolean promo_code, boolean wallet_infos, String Prodlist_jsonArray_size) {
        Order_ID = order_ID;
        Total_Amount = total_Amount;
        Payment_Type = payment_Type;
        Order_Date = order_Date;
        this.name = name;
        this.address = address;
        this.country = country;
        this.state = state;
        this.city = city;
        this.pincode = pincode;
        this.phone = phone;
        this.alternate_phone_no = alternate_phone_no;
        this.near_by = near_by;
        Product_id = product_id;
        Product_name = product_name;
        this.variant_name = variant_name;
        Quantity = quantity;
        Price = price;
        Amount = amount;
        Saler_status = saler_status;
        Customer_status = customer_status;
        Prods_img_jsonArray = prods_img_jsonArray;
        this.current_status = current_status;
        this.current_time = current_time;
        this.current_notes = current_notes;
        this.status = status;
        this.time = time;
        this.notes = notes;
        this.shipping_charge = shipping_charge;
        this.discount = discount;
        this.discount_amount = discount_amount;
        this.usable_amount = usable_amount;
        this.wallet_use = wallet_use;
        this.promo_code = promo_code;
        this.wallet_infos = wallet_infos;
        this.Prodlist_jsonArray_size = Prodlist_jsonArray_size;
    }

    String Order_ID;
    double Total_Amount;
    String Payment_Type;
    String Order_Date;

    String name,address,country,state,city,pincode,phone,alternate_phone_no,near_by;
    String Product_id,Product_name,variant_name,Quantity,Price,Amount,Saler_status,Customer_status;
    ArrayList<String>Prods_img_jsonArray;

    String current_status;
    String current_time;
    String current_notes;

    String status;
    String time;
    String notes;

    String shipping_charge;

    String discount;
    String discount_amount;

    String total_amount;

    String usable_amount;
    String wallet_use;

    boolean promo_code;
    boolean wallet_infos;

    String Prodlist_jsonArray_size;

    public String getOrder_ID() {
        return Order_ID;
    }

    public void setOrder_ID(String order_ID) {
        Order_ID = order_ID;
    }

    public double getTotal_Amount() {
        return Total_Amount;
    }

    public void setTotal_Amount(double total_Amount) {
        Total_Amount = total_Amount;
    }

    public String getPayment_Type() {
        return Payment_Type;
    }

    public void setPayment_Type(String payment_Type) {
        Payment_Type = payment_Type;
    }

    public String getOrder_Date() {
        return Order_Date;
    }

    public void setOrder_Date(String order_Date) {
        Order_Date = order_Date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAlternate_phone_no() {
        return alternate_phone_no;
    }

    public void setAlternate_phone_no(String alternate_phone_no) {
        this.alternate_phone_no = alternate_phone_no;
    }

    public String getNear_by() {
        return near_by;
    }

    public void setNear_by(String near_by) {
        this.near_by = near_by;
    }

    public String getProduct_id() {
        return Product_id;
    }

    public void setProduct_id(String product_id) {
        Product_id = product_id;
    }

    public String getProduct_name() {
        return Product_name;
    }

    public void setProduct_name(String product_name) {
        Product_name = product_name;
    }

    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getSaler_status() {
        return Saler_status;
    }

    public void setSaler_status(String saler_status) {
        Saler_status = saler_status;
    }

    public String getCustomer_status() {
        return Customer_status;
    }

    public void setCustomer_status(String customer_status) {
        Customer_status = customer_status;
    }

    public ArrayList<String> getProds_img_jsonArray() {
        return Prods_img_jsonArray;
    }

    public void setProds_img_jsonArray(ArrayList<String> prods_img_jsonArray) {
        Prods_img_jsonArray = prods_img_jsonArray;
    }

    public String getCurrent_status() {
        return current_status;
    }

    public void setCurrent_status(String current_status) {
        this.current_status = current_status;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(String current_time) {
        this.current_time = current_time;
    }

    public String getCurrent_notes() {
        return current_notes;
    }

    public void setCurrent_notes(String current_notes) {
        this.current_notes = current_notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getShipping_charge() {
        return shipping_charge;
    }

    public void setShipping_charge(String shipping_charge) {
        this.shipping_charge = shipping_charge;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

//    public String getTotal_amount() {
//        return total_amount;
//    }
//
//    public void setTotal_amount(String total_amount) {
//        this.total_amount = total_amount;
//    }

    public String getUsable_amount() {
        return usable_amount;
    }

    public void setUsable_amount(String usable_amount) {
        this.usable_amount = usable_amount;
    }

    public String getWallet_use() {
        return wallet_use;
    }

    public void setWallet_use(String wallet_use) {
        this.wallet_use = wallet_use;
    }

    public boolean isPromo_code() {
        return promo_code;
    }

    public void setPromo_code(boolean promo_code) {
        this.promo_code = promo_code;
    }

    public boolean isWallet_infos() {
        return wallet_infos;
    }

    public void setWallet_infos(boolean wallet_infos) {
        this.wallet_infos = wallet_infos;
    }

    public String getProdlist_jsonArray_size() {
        return Prodlist_jsonArray_size;
    }

    public void setProdlist_jsonArray_size(String prodlist_jsonArray_size) {
        Prodlist_jsonArray_size = prodlist_jsonArray_size;
    }
}
