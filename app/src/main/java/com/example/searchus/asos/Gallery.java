package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.MODELS.Gallerydata;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;

public class Gallery extends AppCompatActivity {
    GridViewWithHeaderAndFooter gridvw;
    public static ProgressDialog progressDialog;
    ArrayList<Gallerydata> arrProduct = new ArrayList<>();
    private static String mResult = "";
    int start = 0;
    int limit = 16;
    View loadMoreView;
    boolean loadingMore = false;
    GalleryAdapter adapter;
    List<String> arrImges= new ArrayList<String>();
    TextView tx_no_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_gallery);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.arrowbackblack);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().hide();
        gridvw = (GridViewWithHeaderAndFooter) findViewById(R.id.gridvw);
        tx_no_data=(TextView)findViewById(R.id.tx_no_data);

        loadMoreView = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.asos_loading_view, null, false);
        gridvw.addFooterView(loadMoreView);
        adapter = new GalleryAdapter(Gallery.this, arrImges);
        gridvw.setAdapter(adapter);

        gridvw.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }


            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == arrImges.size() && arrImges.size() % limit == 0)) {
                    attemptLogin();
                }
            }
        });
    }

    private void attemptLogin() {

        // if (list == null) {
        ConnectivityManager cm = (ConnectivityManager) Gallery.this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()&& !loadingMore) {
            Getwebservice runner = new Getwebservice();
            runner.execute();
        } else {

        }

    }

    private class Getwebservice extends AsyncTask<String, String, String> {

        private String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }



        @Override
        protected String doInBackground(String... params) {
            loadingMore = true;
            try {
                JSONObject object = new JSONObject();
                try {
                    object.put("business_id", "5a23894feca30B");
                    object.put("offset", start);
                    object.put("limit", limit);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                response = Callurl_med("http://api.searchus.in/dashboard/welcome/businessgalleryimages",
                        object.toString(), getApplicationContext());

                Log.d("GetAll", response + "");
            } catch (Exception exception) {
                exception.printStackTrace();
                response = exception.getMessage();
            }
            return response;
        }


        @Override
        protected void onPostExecute(String s) {
            Log.d("respq", s + "");
            super.onPostExecute(s);

            s = response.replaceAll("\\\\", "");
//            s = response.replaceAll("|", "");
            if (s != null) {
                try {
                    JSONArray array = new JSONArray(s);
                    List<String> arrImgvar = new ArrayList<>();

                    if (start == 0){
                        if (array.length()==0) {
                            gridvw.removeFooterView(loadMoreView);
//                        loding.setVisibility(View.GONE);
                            gridvw.setVisibility(View.GONE);
                            tx_no_data.setVisibility(View.VISIBLE);
                        }
                    }
                    if (array.length()==0) {
                        gridvw.removeFooterView(loadMoreView);
//                        loding.setVisibility(View.GONE);
                    } else {
                        for (int m = 0; m < array.length(); m++) {
                            arrImgvar.add(String.valueOf(array.get(m)));
                        }
                        int oldcount = arrImges.size();
                        Log.d("size", arrImgvar.size() + "");
                        for (int i = 0; i < arrImgvar.size(); i++) {
                            start++;
//                            list.add(list5.get(i));
                            adapter.add(arrImgvar.get(i));
//                            adapter5.add(listpro.get(i));
                        }
                        adapter.notifyDataSetChanged();
//                        adapter5.notifyDataSetChanged();
                        loadingMore = false;
                        if (arrImgvar.size() % limit != 0 || oldcount == arrImges.size()) {
                            gridvw.removeFooterView(loadMoreView);
//                            loding.setVisibility(View.GONE);

                        }
                    }



//                    GalleryAdapter adapter = new GalleryAdapter(Gallery.this, arrImgvar);
//                    grid_category.setAdapter(adapter);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }

            }
        }
    }

    public class GalleryAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<Gallerydata> rowlist;
        private List<String> img;

        String pathName;

        public GalleryAdapter(Context context, List<String> img) {
            this.context = context;
            this.rowlist = rowlist;
            this.img = img;
        }
        public void add(String country) {
            this.img.add(country);
        }
        private class viewHolder {
            ImageView imageViewAndroid;

        }

        @Override
        public int getCount() {
            return img.size();
        }

        @Override
        public Object getItem(int position) {
            return img.get(position);
        }

        @Override
        public long getItemId(int position) {
            return img.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            viewHolder holder;
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {

                convertView = inflater.inflate(R.layout.asos_single_gallery_image, null);
                holder = new viewHolder();

                holder.imageViewAndroid = (ImageView) convertView

                        .findViewById(R.id.imgvw_gallery);
                convertView.setTag(holder);

            } else {
                holder = (viewHolder) convertView.getTag();
            }

            Picasso.with(context).load(PaperCart_CoNNectioN.IMGPATH+ img.get(position).toString()).into(holder.imageViewAndroid);
            Log.d("imgg", PaperCart_CoNNectioN.IMGPATH + img.get(position).toString() + "");

            return convertView;
        }



    }

    public String Callurl_med(String url, String body, Context con) {

        URL mUrl;
        try {
            mUrl = new URL(url);
            URLConnection conn = mUrl.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.addRequestProperty("SOAPAction",
                    "http://api.searchus.in/API/user/");
            conn.addRequestProperty("authentication",
                    PaperCart_HEADER.KEY);
            conn.setDoOutput(true);

            if (Build.VERSION.SDK != null && Build.VERSION.SDK_INT > 13) {
                conn.setRequestProperty("Connection", "close");
            }
            OutputStreamWriter wr = new OutputStreamWriter(
                    conn.getOutputStream());
            wr.write(body);
            wr.flush();
            // Get the response

            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            mResult = "";
            String line;
            while ((line = rd.readLine()) != null) {
                mResult += line;
            }

            wr.close();
            rd.close();

        } catch (Exception e) {

            e.printStackTrace();
        }

        return mResult;
    }
}
