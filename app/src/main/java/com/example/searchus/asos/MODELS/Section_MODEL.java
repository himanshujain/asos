package com.example.searchus.asos.MODELS;

public class Section_MODEL
{
    public Section_MODEL(String section_id, String type, String image_path, String date_time, String id, String description, String section_name, String status) {
        this.section_id = section_id;
        this.type = type;
        this.image_path = image_path;
        this.date_time = date_time;
        this.id = id;
        this.description = description;
        this.section_name = section_name;
        this.status = status;
    }

    String section_id,
            type,
            image_path,
            date_time,
            id,
            description,
            section_name,
            status;

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
