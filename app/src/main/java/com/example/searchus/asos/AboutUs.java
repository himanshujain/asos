package com.example.searchus.asos;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ResizableCustomView;
import com.example.searchus.asos.MODELS.MapModel;
import com.example.searchus.asos.MODELS.SocialModel;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;

public class AboutUs extends AppCompatActivity {
    ProgressDialog progressDialog;
    String abtus;
    private static String mResult = "";
    String NameSpaceUrl, ServerUrl, ImgPath;
    ImageView banner_img;
    CircleImageView img_user;
    String lati, lomgi;
    LinearLayout lin_call;
    String unique_id, person_name, person_number, company_name, address,
            near_by, country, state, city, pin_code, corporate_no, email,
            website, product_dealing, main_category, tags, user_remark, status, cover_image, picture_path, about_us, payment;
    private LinearLayout lin_map, lin_message, lin_email;
    TextView txt_address, txt_comp, txt_about, tx_service_provide, tx_addrss, tx_near_by, tx_city_code, tx_state, tx_country, tx_corporate, tx_gallery, tx_keywords,
            tx_time_mon, tx_time_tue, tx_time_wed, tx_time_thu, tx_time_fri, tx_time_sat, tx_time_sun,
            tx_day_mon, tx_day_tue, tx_day_wed, tx_day_thu, tx_day_fri, tx_day_sat, tx_day_sun, tx_lunch, tx_chat;
    String Business, business_id;
    String Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, Lunch, remark;
    List<String> ids;
    List<String> ids_keyword;
    List<String> ids_service;
    List<String> ids_payment;
    List<String> ids_whatsapp;
    List<String> ids_messanger;
    ListView list_call;
    CallAdpater callAdpater;
    RecyclerView lst_keyword, lst_service, lst_payment, lst_follow_us;
    KeywordAdapter keywordAdapter;
    ServiceAdapter serviceAdapter;
    PaymentAdapter paymentAdapter;
    ArrayList<SocialModel> arrProduct = new ArrayList<>();
    SocialAdapter socialAdapter;
    CardView card_social, card_service_provide, card_keywords, card_payment, card_working_hours, card_lunch, card_about;
    WhatsappAdpater whatsappAdpater;
    MessangerAdpater messangerAdpater;
    ChatMessageAdpater chatMessageAdpater;
    RelativeLayout rel_no_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_about_us);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.arrowbackblack);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().hide();
        rel_no_img=(RelativeLayout)findViewById(R.id.rel_no_img);
        banner_img = (ImageView) findViewById(R.id.banner_img);
        img_user = (CircleImageView) findViewById(R.id.img_user);

        txt_comp = findViewById(R.id.txt_comp);
        txt_address = findViewById(R.id.txt_address);
        txt_about = findViewById(R.id.txt_about);
        tx_service_provide = findViewById(R.id.tx_service_provide);
        tx_addrss = findViewById(R.id.tx_addrss);
        tx_near_by = findViewById(R.id.tx_near_by);
        tx_city_code = findViewById(R.id.tx_city_code);
        tx_state = findViewById(R.id.tx_state);
        tx_country = findViewById(R.id.tx_country);
        tx_corporate = findViewById(R.id.tx_corporate);
        tx_gallery = findViewById(R.id.tx_gallery);
        tx_keywords = findViewById(R.id.tx_keywords);
        tx_chat = findViewById(R.id.tx_chat);


        tx_time_mon = (TextView) findViewById(R.id.tx_time_mon);
        tx_time_tue = (TextView) findViewById(R.id.tx_time_tue);
        tx_time_wed = (TextView) findViewById(R.id.tx_time_wed);
        tx_time_thu = (TextView) findViewById(R.id.tx_time_thu);
        tx_time_fri = (TextView) findViewById(R.id.tx_time_fri);
        tx_time_sat = (TextView) findViewById(R.id.tx_time_sat);
        tx_time_sun = (TextView) findViewById(R.id.tx_time_sun);

        tx_day_mon = (TextView) findViewById(R.id.tx_day_mon);
        tx_day_tue = (TextView) findViewById(R.id.tx_day_tue);
        tx_day_wed = (TextView) findViewById(R.id.tx_day_wed);
        tx_day_thu = (TextView) findViewById(R.id.tx_day_thu);
        tx_day_fri = (TextView) findViewById(R.id.tx_day_fri);
        tx_day_sat = (TextView) findViewById(R.id.tx_day_sat);
        tx_day_sun = (TextView) findViewById(R.id.tx_day_sun);

        tx_lunch = (TextView) findViewById(R.id.tx_lunch);

        lin_email = findViewById(R.id.lin_email);
        lin_map = (LinearLayout) findViewById(R.id.lin_map);
        lin_call = (LinearLayout) findViewById(R.id.lin_call);
        lin_message = (LinearLayout) findViewById(R.id.lin_message);

        lst_service = findViewById(R.id.lst_service);
        lst_keyword = (RecyclerView) findViewById(R.id.lst_keyword);
        lst_payment = (RecyclerView) findViewById(R.id.lst_payment);
        lst_follow_us = (RecyclerView) findViewById(R.id.lst_follow_us);

        card_social = (CardView) findViewById(R.id.card_social);
        card_service_provide = (CardView) findViewById(R.id.card_service_provide);
        card_keywords = (CardView) findViewById(R.id.card_keywords);
        card_payment = (CardView) findViewById(R.id.card_payment);
        card_working_hours= (CardView) findViewById(R.id.card_working_hours);
        card_lunch= (CardView) findViewById(R.id.card_lunch);
        card_about = findViewById(R.id.card_about);

        ids = new ArrayList<>();
        ids_keyword = new ArrayList<>();
        ids_service = new ArrayList<>();
        ids_payment = new ArrayList<>();
        ids_whatsapp = new ArrayList<>();
        ids_messanger = new ArrayList<>();

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        LinearLayoutManager llm5 = new LinearLayoutManager(this);
        llm5.setOrientation(LinearLayoutManager.VERTICAL);

        lst_keyword.setLayoutManager(llm);
        lst_keyword.setHasFixedSize(true);
        lst_keyword.setNestedScrollingEnabled(false);

        lst_service.setLayoutManager(llm5);
        lst_service.setHasFixedSize(true);
        lst_service.setNestedScrollingEnabled(false);

        LinearLayoutManager llm2 = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        lst_payment.setLayoutManager(llm2);
        lst_payment.setHasFixedSize(true);
        lst_payment.setNestedScrollingEnabled(false);

        LinearLayoutManager llm3 = new LinearLayoutManager(this);
        llm3.setOrientation(LinearLayoutManager.HORIZONTAL);

        lst_follow_us.setLayoutManager(llm3);
        lst_follow_us.setHasFixedSize(true);
        lst_follow_us.setNestedScrollingEnabled(false);

        attemptLogin();

//        lin_map.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent1 = new Intent(AboutUs.this, MapActivity.class);
//                intent1.putExtra("lati", lati);
//                intent1.putExtra("longi", lomgi);
//                intent1.putExtra("cname", company_name);
//                startActivity(intent1);
//            }
//        });
    }

    private void attemptLogin() {

        // if (list == null) {
        ConnectivityManager cm = (ConnectivityManager) AboutUs.this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            Getwebservice runner = new Getwebservice();
            runner.execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please Check Internet Connection", Toast.LENGTH_SHORT)
                    .show();

        }


    }

    private class Getwebservice extends AsyncTask<String, String, String> {

        private String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AboutUs.this);
            progressDialog.setTitle("Please wait");
            progressDialog.setCancelable(true);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                JSONObject object = new JSONObject();

                JSONObject jobj = new JSONObject();
                jobj.put("business_id", PaperCart_HEADER.Business_id);
                try {
                    object.put("options", jobj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                response = Callurl_med("http://api.searchus.in/API/user/business_details",
                        object.toString(), getApplicationContext());

                Log.d("BisunessDEtails", response + "");
            } catch (Exception exception) {
                exception.printStackTrace();
                response = exception.getMessage();
            }
            return response;
        }


        @Override
        protected void onPostExecute(String s) {
            Log.d("respabout", s + "");
            super.onPostExecute(s);

            if (s != null) {
                progressDialog.dismiss();
                try {
                    JSONObject object = new JSONObject(s);
                    String code = object.getString("code");
                    JSONObject object1 = object.getJSONObject("data");

                    if (code.equals("000200")) {
                        JSONObject object2 = object1.getJSONObject("business_details");
                        cover_image = object2.getString("cover_image");
                        about_us = object2.getString("about_us");
                        payment = object2.getString("payment");
                        person_name = object1.getString("person_name");
                        person_number = object1.getString("person_number");
                        company_name = object1.getString("company_name");
                        address = object1.getString("address");
                        near_by = object1.getString("near_by");
                        city = object1.getString("city");
                        pin_code = object1.getString("pin_code");
                        state = object1.getString("state");
                        country = object1.getString("country");
                        email = object1.getString("email");
                        corporate_no = object1.getString("corporate_no");
                        website = object1.getString("website");
                        product_dealing = object1.getString("product_dealing");
                        picture_path = object1.getString("picture_path");
                        main_category = object1.getString("main_category");
                        tags = object1.getString("tags");
                        user_remark = object1.getString("user_remark");
                        status = object1.getString("status");
                        business_id = object1.getString("business_id");

                        if (!about_us.equals("")) {
                            txt_about.setText(about_us);
                            if (about_us.length() > 260) {
                                ResizableCustomView.doResizeTextView(txt_about, 6, "View More", true);
                            }
                        } else {
                            txt_about.setVisibility(View.GONE);
                            card_about.setVisibility(View.GONE);
                        }
                        if (email.equals("")) {
                            lin_email.setVisibility(View.GONE);
                        } else {
                            lin_email.setVisibility(View.VISIBLE);
                        }

                        if (!product_dealing.equals("")) {
                            tx_service_provide.setText(product_dealing);
                        } else {
                            card_service_provide.setVisibility(View.GONE);
                        }

                        if (!tags.equals("")) {
                        } else {
                            card_keywords.setVisibility(View.GONE);
                        }
                        if (!payment.equals("")) {
                        } else {
                            card_payment.setVisibility(View.GONE);
                        }

                        if (address.equals("")) {
                            tx_addrss.setVisibility(View.GONE);
                        }
                        if (near_by.equals("")) {
                            tx_near_by.setVisibility(View.GONE);
                        }
                        if (state.equals("")) {
                            tx_state.setVisibility(View.GONE);
                        }
                        if (country.equals("")) {
                            tx_country.setVisibility(View.GONE);
                        }
                        if (corporate_no.equals("")) {
                            tx_corporate.setVisibility(View.GONE);
                            lin_call.setVisibility(View.GONE);
                            lin_message.setVisibility(View.GONE);
                        }
                        tx_addrss.setText(address);
                        tx_near_by.setText(near_by);
                        tx_city_code.setText(city + " " + pin_code);
                        tx_state.setText(state);
                        tx_country.setText(country);
                        tx_corporate.setText(corporate_no);
                        tx_keywords.setText(tags);

//                        prodcttool_bar.setText(company_name);
                        Gson gson = new Gson();
                        Object json8 = object1.get("social_links");
                        if (json8 instanceof JSONArray) {
                            JSONArray arraySocial = object1.getJSONArray("social_links");
                            for (int i = 0; i < arraySocial.length(); i++) {
                                JSONObject arrayElement_0 = arraySocial.getJSONObject(i);
                                arrProduct.add(gson.fromJson(String.valueOf(arrayElement_0), SocialModel.class));

                                if (arrayElement_0.getString("name").equals("Whatsapp")) {
                                    ids_whatsapp.add(arrayElement_0.getString("link"));
                                }

                                if (arrayElement_0.getString("name").equals("Facebook Messanger")) {
                                    ids_messanger.add(arrayElement_0.getString("link"));
                                }

                            }


                            if (arraySocial.length() == 0) {
                                card_social.setVisibility(View.GONE);
                            }
                        } else {
                            card_social.setVisibility(View.GONE);
                        }

                        socialAdapter = new SocialAdapter(AboutUs.this, arrProduct);
                        lst_follow_us.setAdapter(socialAdapter);


                        Object json9 = object1.get("working_hours");
                        if (json9 instanceof JSONObject) {
                            JSONObject objhours = object1.getJSONObject("working_hours");
                            Monday = objhours.getString("Monday");
                            Tuesday = objhours.getString("Tuesday");
                            Wednesday = objhours.getString("Wednesday");
                            Thursday = objhours.getString("Thursday");
                            Friday = objhours.getString("Friday");
                            Saturday = objhours.getString("Saturday");
                            Sunday = objhours.getString("Sunday");
                            Lunch = objhours.getString("Lunch");
                            remark = objhours.getString("remark");


                            StringTokenizer tokenlunch = new StringTokenizer(Lunch, "|");
                            String lunch1 = tokenlunch.nextToken();// this will contain "Fruit"
                            String lunch2 = tokenlunch.nextToken();
                            String lunch3 = tokenlunch.nextToken();

                            tx_lunch.setText(" From " + lunch1 + " To " + lunch2);

                            StringTokenizer tokens = new StringTokenizer(Monday, "|");
                            String m1 = tokens.nextToken();// this will contain "Fruit"
                            String m2 = tokens.nextToken();
                            String m3 = tokens.nextToken();

                            tx_time_mon.setText(m1 + " To " + m2);
                            tx_day_mon.setText(m3);
                            if (m3.equals("Full Day")) {
                                tx_day_mon.setTextColor(Color.GREEN);
                                tx_day_mon.setTypeface(null, Typeface.BOLD);
                            } else if (m3.equals("Off Day")) {
                                tx_time_mon.setText("Holiday");
                                tx_day_mon.setTextColor(Color.RED);
                                tx_day_mon.setTypeface(null, Typeface.BOLD);
                            } else {
                                tx_day_mon.setTextColor(Color.parseColor("#f7901d"));
                                tx_day_mon.setTypeface(null, Typeface.BOLD);
                            }


                            StringTokenizer tokens1 = new StringTokenizer(Tuesday, "|");
                            String tue1 = tokens1.nextToken();// this will contain "Fruit"
                            String tue2 = tokens1.nextToken();
                            String tue3 = tokens1.nextToken();

                            tx_time_tue.setText(tue1 + " To " + tue2);
                            tx_day_tue.setText(tue3);
                            if (tue3.equals("Full Day")) {
                                tx_day_tue.setTextColor(Color.GREEN);
                                tx_day_tue.setTypeface(null, Typeface.BOLD);
                            } else if (tue3.equals("Off Day")) {
                                tx_time_tue.setText("Holiday");
                                tx_day_tue.setTextColor(Color.RED);
                                tx_day_tue.setTypeface(null, Typeface.BOLD);
                            } else {
                                tx_day_tue.setTextColor(Color.parseColor("#f7901d"));
                                tx_day_tue.setTypeface(null, Typeface.BOLD);
                            }


                            StringTokenizer tokens2 = new StringTokenizer(Wednesday, "|");
                            String wed1 = tokens2.nextToken();// this will contain "Fruit"
                            String wed2 = tokens2.nextToken();
                            String wed3 = tokens2.nextToken();

                            tx_time_wed.setText(wed1 + " To " + wed2);
                            tx_day_wed.setText(wed3);

                            if (wed3.equals("Full Day")) {
                                tx_day_wed.setTextColor(Color.GREEN);
                                tx_day_wed.setTypeface(null, Typeface.BOLD);
                            } else if (wed3.equals("Off Day")) {
                                tx_time_wed.setText("Holiday");
                                tx_day_wed.setTextColor(Color.RED);
                                tx_day_wed.setTypeface(null, Typeface.BOLD);
                            } else {
                                tx_day_wed.setTextColor(Color.parseColor("#f7901d"));
                                tx_day_wed.setTypeface(null, Typeface.BOLD);
                            }

                            StringTokenizer tokens3 = new StringTokenizer(Thursday, "|");
                            String thu1 = tokens3.nextToken();// this will contain "Fruit"
                            String thu2 = tokens3.nextToken();
                            String thu3 = tokens3.nextToken();

                            tx_time_thu.setText(thu1 + " To " + thu2);
                            tx_day_thu.setText(thu3);

                            if (thu3.equals("Full Day")) {
                                tx_day_thu.setTextColor(Color.GREEN);
                                tx_day_thu.setTypeface(null, Typeface.BOLD);
                            } else if (thu3.equals("Off Day")) {
                                tx_time_thu.setText("Holiday");
                                tx_day_thu.setTextColor(Color.RED);
                                tx_day_thu.setTypeface(null, Typeface.BOLD);
                            } else {
                                tx_day_thu.setTextColor(Color.parseColor("#f7901d"));
                                tx_day_thu.setTypeface(null, Typeface.BOLD);
                            }

                            StringTokenizer tokens4 = new StringTokenizer(Friday, "|");
                            String fri1 = tokens4.nextToken();// this will contain "Fruit"
                            String fri2 = tokens4.nextToken();
                            String fri3 = tokens4.nextToken();

                            tx_time_fri.setText(fri1 + " To " + fri2);
                            tx_day_fri.setText(fri3);

                            if (fri3.equals("Full Day")) {
                                tx_day_fri.setTextColor(Color.GREEN);
                                tx_day_fri.setTypeface(null, Typeface.BOLD);
                            } else if (fri3.equals("Off Day")) {
                                tx_time_fri.setText("Holiday");
                                tx_day_fri.setTextColor(Color.RED);
                                tx_day_fri.setTypeface(null, Typeface.BOLD);
                            } else {
                                tx_day_fri.setTextColor(Color.parseColor("#f7901d"));
                                tx_day_fri.setTypeface(null, Typeface.BOLD);
                            }

                            StringTokenizer tokens5 = new StringTokenizer(Saturday, "|");
                            String sat1 = tokens5.nextToken();// this will contain "Fruit"
                            String sat2 = tokens5.nextToken();
                            String sat3 = tokens5.nextToken();

                            tx_time_sat.setText(sat1 + " To " + sat2);
                            tx_day_sat.setText(sat3);

                            if (sat3.equals("Full Day")) {
                                tx_day_sat.setTextColor(Color.GREEN);
                                tx_day_sat.setTypeface(null, Typeface.BOLD);
                            } else if (sat3.equals("Off Day")) {
                                tx_time_sat.setText("Holiday");
                                tx_day_sat.setTextColor(Color.RED);
                                tx_day_sat.setTypeface(null, Typeface.BOLD);
                            } else {
                                tx_day_sat.setTextColor(Color.parseColor("#f7901d"));
                                tx_day_sat.setTypeface(null, Typeface.BOLD);
                            }

                            StringTokenizer tokens6 = new StringTokenizer(Sunday, "|");
                            String sun1 = tokens6.nextToken();// this will contain "Fruit"
                            String sun2 = tokens6.nextToken();
                            String sun3 = tokens6.nextToken();

                            tx_time_sun.setText(sun1 + " To " + sun2);
                            tx_day_sun.setText(sun3);

                            if (sun3.equals("Full Day")) {
                                tx_day_sun.setTextColor(Color.GREEN);
                                tx_day_sun.setTypeface(null, Typeface.BOLD);
                            } else if (sun3.equals("Off Day")) {
                                tx_time_sun.setText("Holiday");
                                tx_day_sun.setTextColor(Color.RED);
                                tx_day_sun.setTypeface(null, Typeface.BOLD);
                            } else {
                                tx_day_sun.setTextColor(Color.parseColor("#f7901d"));
                                tx_day_sun.setTypeface(null, Typeface.BOLD);
                            }
                        } else {
                            card_lunch.setVisibility(View.GONE);
                            card_working_hours.setVisibility(View.GONE);
                        }

                        Object json5 = object1.get("map_info");
                        if (json5 instanceof JSONArray) {
                            JSONArray objectj = object1.getJSONArray("map_info");
                            ArrayList<MapModel> list = new ArrayList<MapModel>();

                            for (int i = 0; i < objectj.length(); i++) {
                                JSONObject productdata = objectj.getJSONObject(i);
                                MapModel data = new MapModel();
                                data.setLatitude(productdata.getString("latitude"));
                                data.setLongitude(productdata.getString("longitude"));
                                data.setDescription(productdata.getString("description"));

                                list.add(data);
                            }

                            lati = list.get(0).getLatitude().toString();
                            lomgi = list.get(0).getLongitude().toString();


                            lin_map.setVisibility(View.GONE);
                        }
                        //you have an object
                        else {
                            lin_map.setVisibility(View.GONE);
                        }

                        if (!cover_image.equals("")) {
                            Picasso.with(getApplicationContext()).load("https://searchus.in/" + cover_image)
                                    .into(banner_img);
                        } else {
                            rel_no_img.setVisibility(View.VISIBLE);
                            banner_img.setVisibility(View.GONE);
                        }

                        if (!picture_path.equals("")) {
                            Picasso.with(getApplicationContext()).load("https://searchus.in/" + picture_path)
                                    .into(img_user);
                        } else {
                            Picasso.with(getApplicationContext()).load(R.drawable.noimage).into(img_user);
                        }

                        txt_comp.setText(company_name);
                        txt_address.setText(address);

                        tx_gallery.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(AboutUs.this, Gallery.class));
                            }
                        });

                        ids = new LinkedList<>(Arrays.asList(corporate_no.split("\\s*,\\s*")));

                        ids_keyword = new LinkedList<>(Arrays.asList(tags.split("\\s*,\\s*")));

                        keywordAdapter = new KeywordAdapter(AboutUs.this, ids_keyword);
                        lst_keyword.setAdapter(keywordAdapter);


                        ids_service = new LinkedList<>(Arrays.asList(product_dealing.split("\\s*,\\s*")));

                        serviceAdapter = new ServiceAdapter(AboutUs.this, ids_service);
                        lst_service.setAdapter(serviceAdapter);


                        ids_payment = new LinkedList<>(Arrays.asList(payment.split("\\s*,\\s*")));

                        int numberOfColumns = 3;
                        lst_payment.setLayoutManager(new GridLayoutManager(getApplicationContext(), numberOfColumns));
                        paymentAdapter = new PaymentAdapter(AboutUs.this, ids_payment);
                        lst_payment.setAdapter(paymentAdapter);

                        lin_email.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                        "mailto", email, null));
//                                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
//                                emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
                                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                            }
                        });

                        lin_call.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (ids.size() > 1) {
                                    final Dialog dialog6 = new Dialog(AboutUs.this);
                                    dialog6.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog6.setContentView(R.layout.asos_custome_popup_call);
                                    dialog6.show();
                                    dialog6.setCancelable(true);
                                    list_call = (ListView) dialog6.findViewById(R.id.list_call);
                                    callAdpater = new CallAdpater(AboutUs.this, ids);
                                    list_call.setAdapter(callAdpater);
                                } else {
                                    Intent callintent = new Intent(Intent.ACTION_DIAL);
                                    callintent.setData(Uri.parse("tel:" + corporate_no));
                                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        // TODO: Consider calling

                                    }
                                    startActivity(callintent);
                                }
                            }
                        });

                        if (ids_messanger.size() > 0 || ids_whatsapp.size() > 0) {
                            tx_chat.setText("Chat with us");
                        } else {
                            tx_chat.setText("Message");
                        }
                        lin_message.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (ids_messanger.size() > 0 || ids_whatsapp.size() > 0) {
                                    final Dialog dialog = new Dialog(AboutUs.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.asos_custome_chat_popup);
                                    RelativeLayout rel_messanger = (RelativeLayout) dialog.findViewById(R.id.rel_messanger);
                                    RelativeLayout rel_whatsapp = (RelativeLayout) dialog.findViewById(R.id.rel_whatsapp);
                                    RelativeLayout rel_chat_mesage = (RelativeLayout) dialog.findViewById(R.id.rel_chat_mesage);
                                    ImageView img_messanger = (ImageView) dialog.findViewById(R.id.img_messanger);
                                    ImageView img_whatsapp = (ImageView) dialog.findViewById(R.id.img_whatsapp);
                                    ImageView img_msg = (ImageView) dialog.findViewById(R.id.img_msg);

                                    if (ids_messanger.size() == 0) {
                                        rel_messanger.setVisibility(View.GONE);
                                    }
                                    if (ids_whatsapp.size() == 0) {
                                        rel_whatsapp.setVisibility(View.GONE);
                                    }


                                    img_messanger.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            if (ids_messanger.size() > 1) {
                                                final Dialog dialog6 = new Dialog(AboutUs.this);
                                                dialog6.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                dialog6.setContentView(R.layout.asos_custome_popup_chatsus);
                                                dialog6.show();
                                                dialog6.setCancelable(true);
                                                list_call = (ListView) dialog6.findViewById(R.id.list_call);
                                                messangerAdpater = new MessangerAdpater(AboutUs.this, ids_messanger);
                                                list_call.setAdapter(messangerAdpater);
                                            } else {
                                                if (arrProduct.get(0).getLink().contains("http")) {
                                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ids_messanger.get(0)));
                                                    startActivity(browserIntent);
                                                    dialog.dismiss();
                                                } else {
                                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + ids_messanger.get(0)));
                                                    startActivity(browserIntent);
                                                    dialog.dismiss();
                                                }
                                            }
                                        }
                                    });
                                    img_whatsapp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                            String smsNumber = "918866686625";

                                            if (ids_whatsapp.size() > 1) {
                                                final Dialog dialog6 = new Dialog(AboutUs.this);
                                                dialog6.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                dialog6.setContentView(R.layout.asos_custome_popup_chatsus);
                                                dialog6.show();
                                                dialog6.setCancelable(true);
                                                list_call = (ListView) dialog6.findViewById(R.id.list_call);
                                                whatsappAdpater = new WhatsappAdpater(AboutUs.this, ids_whatsapp);
                                                list_call.setAdapter(whatsappAdpater);
                                            } else {
                                                Intent sendIntent = new Intent("android.intent.action.MAIN");
                                                sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                                                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("91" + ids_whatsapp.get(0)) + "@s.whatsapp.net");//phone number without "+" prefix
                                                startActivity(sendIntent);
                                                dialog.dismiss();
                                            }
                                        }
                                    });
                                    img_msg.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                            if (ids.size() > 1) {
                                                final Dialog dialog6 = new Dialog(AboutUs.this);
                                                dialog6.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                dialog6.setContentView(R.layout.asos_custome_popup_message);
                                                dialog6.show();
                                                dialog6.setCancelable(true);
                                                list_call = (ListView) dialog6.findViewById(R.id.list_call);
                                                chatMessageAdpater = new ChatMessageAdpater(AboutUs.this, ids);
                                                list_call.setAdapter(chatMessageAdpater);
                                            } else {
                                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                                smsIntent.setType("vnd.android-dir/mms-sms");
                                                smsIntent.putExtra("address", ids.get(0).toString());
                                                smsIntent.putExtra("sms_body", "");
                                                startActivity(smsIntent);
                                            }

                                        }
                                    });

                                    dialog.show();
                                    dialog.setCancelable(true);
                                } else {
                                    if (ids.size() > 1) {
                                        final Dialog dialog6 = new Dialog(AboutUs.this);
                                        dialog6.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog6.setContentView(R.layout.asos_custome_popup_message);
                                        dialog6.show();
                                        dialog6.setCancelable(true);
                                        list_call = (ListView) dialog6.findViewById(R.id.list_call);
                                        chatMessageAdpater = new ChatMessageAdpater(AboutUs.this, ids);
                                        list_call.setAdapter(chatMessageAdpater);
                                    } else {
                                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                        smsIntent.setType("vnd.android-dir/mms-sms");
                                        smsIntent.putExtra("address", ids.get(0).toString());
                                        smsIntent.putExtra("sms_body", "");
                                        startActivity(smsIntent);
                                    }
                                }

                            }
                        });


                    } else {
                    }
//                    offset = offset+3;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

        }
    }

    public String Callurl_med(String url, String body, Context con) {

        URL mUrl;
        try {
            mUrl = new URL(url);
            URLConnection conn = mUrl.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.addRequestProperty("SOAPAction",
                    "http://api.searchus.in/API/user/");
            conn.addRequestProperty("authentication",
                    "967eea197ef1df56af8f17ee6e27c2389bbcbf9e0464dda5a1af9c540e915f72");
            conn.setDoOutput(true);

            if (Build.VERSION.SDK != null && Build.VERSION.SDK_INT > 13) {
                conn.setRequestProperty("Connection", "close");
            }
            OutputStreamWriter wr = new OutputStreamWriter(
                    conn.getOutputStream());
            wr.write(body);
            wr.flush();
            // Get the response

            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            mResult = "";
            String line;
            while ((line = rd.readLine()) != null) {
                mResult += line;
            }

            wr.close();
            rd.close();

        } catch (Exception e) {

            e.printStackTrace();
        }

        return mResult;
    }

    public class CallAdpater extends BaseAdapter {
        private Context context;
        private List<String> rowlist;
        ArrayAdapter<String> adp_var;
        String url1 = "http://www.searchus.in/";
        String spn_var = "", var_name = "", var_price = "";
        String stts;
        String UserId1, prodctid;
        private ProgressDialog progressDialog;

        public CallAdpater(Context context, List<String> rowlist) {
            this.context = context;
            this.rowlist = rowlist;
        }

        private class viewHolder {
            TextView text_popular;
            RelativeLayout rel_call;

        }


        @Override
        public int getCount() {
            return rowlist.size();
        }

        @Override
        public Object getItem(int position) {
            return rowlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowlist.indexOf(getItem(position));
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            viewHolder holder = null;
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        String name=holder.txt_name.getText().toString();
//        String price=holder.txt_price.getText().toString();
            if (convertView == null) {

                convertView = inflater.inflate(R.layout.asos_single_popular_ss, null);
                holder = new viewHolder();
                holder.text_popular = (TextView) convertView
                        .findViewById(R.id.text_popular);
                holder.rel_call = (RelativeLayout) convertView.findViewById(R.id.rel_call);

                convertView.setTag(holder);

            } else {
                holder = (viewHolder) convertView.getTag();
            }


            holder.text_popular.setText(Html.fromHtml(rowlist.get(position).toString()));

            holder.rel_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callintent = new Intent(Intent.ACTION_DIAL);
                    callintent.setData(Uri.parse("tel:" + Html.fromHtml(rowlist.get(position).toString())));
                    if (ActivityCompat.checkSelfPermission(AboutUs.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling

                    }
                    startActivity(callintent);
                }
            });


            return convertView;
        }

    }

    public class KeywordAdapter extends RecyclerView.Adapter<KeywordAdapter.ViewHolder> {
        private Context context;
        private List<String> rowlist;

        public KeywordAdapter(Context context, List<String> rowlist) {
            this.context = context;
            this.rowlist = rowlist;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView text_keyword;

            public ViewHolder(final View itemView) {
                super(itemView);
                text_keyword =(TextView)itemView
                        .findViewById(R.id.text_keyword);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.asos_single_text, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            holder.text_keyword.setText(rowlist.get(position).toString());

        }

        @Override
        public long getItemId(int position) {
            return rowlist.size();
        }

        @Override
        public int getItemCount() {
            return rowlist.size();
        }


    }

    public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {
        private Context context;
        private List<String> rowlist;

        public ServiceAdapter(Context context, List<String> rowlist) {
            this.context = context;
            this.rowlist = rowlist;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView text_keyword;

            public ViewHolder(final View itemView) {
                super(itemView);
                text_keyword = (TextView) itemView
                        .findViewById(R.id.text_keyword);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.asos_single_text, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            holder.text_keyword.setText(rowlist.get(position).toString());

        }

        @Override
        public long getItemId(int position) {
            return rowlist.size();
        }

        @Override
        public int getItemCount() {
            return rowlist.size();
        }
    }

    public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.ViewHolder> {
        private Context context;
        private List<String> rowlist;

        public PaymentAdapter(Context context, List<String> rowlist) {
            this.context = context;
            this.rowlist = rowlist;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img;

            public ViewHolder(final View itemView) {
                super(itemView);
                img = (ImageView) itemView.findViewById(R.id.img);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.asos_single_payment, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {


            if (rowlist.get(position).toString().equals("Net Banking")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/netbanking.png")
                        .into(holder.img);
            } else if (rowlist.get(position).toString().equals("Cash On Delivery")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/cod.png")
                        .into(holder.img);
            } else if (rowlist.get(position).toString().equals("Master Card")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/mastercard.png")
                        .into(holder.img);
            } else if (rowlist.get(position).toString().equals("Cheque")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/cheque.png")
                        .into(holder.img);
            } else if (rowlist.get(position).toString().equals("Paypal")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/paypal-icon.png")
                        .into(holder.img);
            } else if (rowlist.get(position).toString().equals("Visa")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/visa1.png")
                        .into(holder.img);
            } else if (rowlist.get(position).toString().equals("Rupay")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/rupay.png")
                        .into(holder.img);
            } else if (rowlist.get(position).toString().equals("American Express")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/American-Express-icon.png")
                        .into(holder.img);
            } else if (rowlist.get(position).toString().equals("Maestro")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/mestro.png")
                        .into(holder.img);
            } else if (rowlist.get(position).toString().equals("EMI")) {
                Picasso.with(getApplicationContext()).load("https://www.searchus.in/dashboard/images/template/payment/EMI.jpg")
                        .into(holder.img);
            }else if (rowlist.get(position).toString().equals("Credit Card")) {
                holder.img.setVisibility(View.GONE);
            } else if (rowlist.get(position).toString().equals("Debit Card")) {
                holder.img.setVisibility(View.GONE);
            }

        }

        @Override
        public long getItemId(int position) {
            return rowlist.size();
        }

        @Override
        public int getItemCount() {
            return rowlist.size();
        }


    }

    public class SocialAdapter extends RecyclerView.Adapter<SocialAdapter.ViewHolder> {
        private Context context;
        private List<SocialModel> rowlist;

        public SocialAdapter(Context context, List<SocialModel> rowlist) {
            this.context = context;
            this.rowlist = rowlist;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img;

            public ViewHolder(final View itemView) {
                super(itemView);
                img = (ImageView) itemView.findViewById(R.id.img);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.asos_single_social, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {


            if (rowlist.get(position).getName().toString().equals("Facebook")) {
                Picasso.with(getApplicationContext()).load(R.drawable.facebook).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("Facebook Messanger")) {
                Picasso.with(getApplicationContext()).load(R.drawable.messenger).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("Whatsapp")) {
                Picasso.with(getApplicationContext()).load(R.drawable.whatsapps).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("Google plus")) {
                Picasso.with(getApplicationContext()).load(R.drawable.google_pluss).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("Instagram")) {
                Picasso.with(getApplicationContext()).load(R.drawable.instagramc).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("Linkedin")) {
                Picasso.with(getApplicationContext()).load(R.drawable.linkedin).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("Twitter")) {
                Picasso.with(getApplicationContext()).load(R.drawable.twitter).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("Webiste")) {
                Picasso.with(getApplicationContext()).load(R.drawable.internet).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("Youtube")) {
                Picasso.with(getApplicationContext()).load(R.drawable.youtube).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("Android App")) {
                Picasso.with(getApplicationContext()).load(R.drawable.google_play).into(holder.img);
            } else if (rowlist.get(position).getName().toString().equals("iOS App")) {
                Picasso.with(getApplicationContext()).load(R.drawable.app_store).into(holder.img);
            }

            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (rowlist.get(position).getName().toString().equals("Whatsapp")) {
                        Intent sendIntent = new Intent("android.intent.action.MAIN");
                        sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                        sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("91"+rowlist.get(position).getLink()) + "@s.whatsapp.net");//phone number without "+" prefix
                        AboutUs.this.startActivity(sendIntent);
                    } else {
                        if (rowlist.get(position).getLink().contains("http")) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(rowlist.get(position).getLink()));
                            AboutUs.this.startActivity(browserIntent);
                        } else {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + rowlist.get(position).getLink()));
                            AboutUs.this.startActivity(browserIntent);
                        }

                    }

                }
            });


        }

        @Override
        public long getItemId(int position) {
            return rowlist.size();
        }

        @Override
        public int getItemCount() {
            return rowlist.size();
        }


    }


    public class WhatsappAdpater extends BaseAdapter {
        private Context context;
        private List<String> rowlist;
        ArrayAdapter<String> adp_var;
        String url1 = "http://www.searchus.in/";
        String spn_var = "", var_name = "", var_price = "";
        String stts;
        String UserId1, prodctid;
        private ProgressDialog progressDialog;

        public WhatsappAdpater(Context context, List<String> rowlist) {
            this.context = context;
            this.rowlist = rowlist;
        }

        private class viewHolder {
            TextView text_popular;
            RelativeLayout rel_call;

        }


        @Override
        public int getCount() {
            return rowlist.size();
        }

        @Override
        public Object getItem(int position) {
            return rowlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowlist.indexOf(getItem(position));
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            viewHolder holder = null;
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {

                convertView = inflater.inflate(R.layout.asos_single_popular_ss, null);
                holder = new viewHolder();
                holder.text_popular = (TextView) convertView
                        .findViewById(R.id.text_popular);
                holder.rel_call = (RelativeLayout) convertView.findViewById(R.id.rel_call);
                convertView.setTag(holder);

            } else {
                holder = (viewHolder) convertView.getTag();
            }

            holder.text_popular.setText(rowlist.get(position).toString());

            holder.rel_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent("android.intent.action.MAIN");
                    sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                    sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("91"+rowlist.get(position).toString()) + "@s.whatsapp.net");//phone number without "+" prefix
                    startActivity(sendIntent);
                }
            });
            return convertView;
        }
    }
    public class MessangerAdpater extends BaseAdapter {
        private Context context;
        private List<String> rowlist;
        ArrayAdapter<String> adp_var;
        String url1 = "http://www.searchus.in/";
        String spn_var = "", var_name = "", var_price = "";
        String stts;
        String UserId1, prodctid;
        private ProgressDialog progressDialog;

        public MessangerAdpater(Context context, List<String> rowlist) {
            this.context = context;
            this.rowlist = rowlist;
        }

        private class viewHolder {
            TextView text_popular;
            RelativeLayout rel_call;

        }


        @Override
        public int getCount() {
            return rowlist.size();
        }

        @Override
        public Object getItem(int position) {
            return rowlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowlist.indexOf(getItem(position));
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            viewHolder holder = null;
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {

                convertView = inflater.inflate(R.layout.asos_single_popular_ss, null);
                holder = new viewHolder();
                holder.text_popular = (TextView) convertView
                        .findViewById(R.id.text_popular);
                holder.rel_call = (RelativeLayout) convertView.findViewById(R.id.rel_call);

                convertView.setTag(holder);

            } else {
                holder = (viewHolder) convertView.getTag();
            }


            holder.text_popular.setText(rowlist.get(position).toString());

            holder.rel_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (rowlist.get(position).toString().contains("http")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(rowlist.get(position).toString()));
                        startActivity(browserIntent);
                    }else{
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+rowlist.get(position).toString()));
                        startActivity(browserIntent);
                    }
                }
            });


            return convertView;
        }

    }
    public class ChatMessageAdpater extends BaseAdapter {
        private Context context;
        private List<String> rowlist;
        ArrayAdapter<String> adp_var;
        String url1 = "http://www.searchus.in/";
        String spn_var = "", var_name = "", var_price = "";
        String stts;
        String UserId1, prodctid;
        private ProgressDialog progressDialog;

        public ChatMessageAdpater(Context context, List<String> rowlist) {
            this.context = context;
            this.rowlist = rowlist;
        }

        private class viewHolder {
            TextView text_popular;
            RelativeLayout rel_call;

        }


        @Override
        public int getCount() {
            return rowlist.size();
        }

        @Override
        public Object getItem(int position) {
            return rowlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowlist.indexOf(getItem(position));
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            viewHolder holder = null;
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {

                convertView = inflater.inflate(R.layout.asos_single_popular_ss, null);
                holder = new viewHolder();
                holder.text_popular = (TextView) convertView
                        .findViewById(R.id.text_popular);
                holder.rel_call = (RelativeLayout) convertView.findViewById(R.id.rel_call);
                convertView.setTag(holder);

            } else {
                holder = (viewHolder) convertView.getTag();
            }

            holder.text_popular.setText(rowlist.get(position).toString());

            holder.rel_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address", rowlist.get(position).toString());
                    smsIntent.putExtra("sms_body","");
                    startActivity(smsIntent);
                }
            });
            return convertView;
        }
    }
}
