package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.MODELS.RATING_PEOPLE_LIST;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class VIEW_PROD_REVIEW extends AppCompatActivity
{


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_view__prod__review);

        TOOLBAR();
        GET_RATING();
    }

    public void TOOLBAR()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Product Review");
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
                finish();
            }
        });
    }

    public void GET_RATING()
    {
        final RecyclerView reviewpeople_listrecycler_view = (RecyclerView) findViewById(R.id.reviewpeople_listrecycler_view);
        reviewpeople_listrecycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        reviewpeople_listrecycler_view.setLayoutManager(layoutManager);

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type","product_rate_reviews");
            object.put("product_id",getIntent().getStringExtra("pid"));
            JSONObject jsonObject2=new JSONObject();
            jsonObject2.put("action","get_rate_review");
            object.put("options",jsonObject2);

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(VIEW_PROD_REVIEW.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading Review");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();
                    String getresponce = response.replace("\"", "");

                    ArrayList<RATING_PEOPLE_LIST> ratingpeople_list=new ArrayList();

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("GET_RATING_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("GET_RATING_res", response);
                        try
                        {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            JSONObject average_infosObject = dataObject.getJSONObject("average_infos");

                            String avg=average_infosObject.getString("avg");
                            String review=average_infosObject.getString("review");
                            String rating=average_infosObject.getString("rating");
                            String one_rate=average_infosObject.getString("one_rate");
                            String two_rate=average_infosObject.getString("two_rate");
                            String three_rate=average_infosObject.getString("three_rate");
                            String four_rate=average_infosObject.getString("four_rate");
                            String five_rate=average_infosObject.getString("five_rate");

                            TextView tvonestar=findViewById(R.id.tvonestarid);
                            if(one_rate.equals("1")||one_rate.equals("0"))
                            {
                                tvonestar.setText(one_rate+" user");
                            }
                            else
                            {
                                tvonestar.setText(one_rate+" users");
                            }

                            TextView tvtwostar=findViewById(R.id.tvtwostarid);
                            if(two_rate.equals("1")||two_rate.equals("0"))
                            {
                                tvtwostar.setText(two_rate+" user");
                            }
                            else
                            {
                                tvtwostar.setText(two_rate+" users");
                            }

                            TextView tvthreestar=findViewById(R.id.tvthreestarid);
                            if(three_rate.equals("1")||three_rate.equals("0"))
                            {
                                tvthreestar.setText(three_rate+" user");
                            }
                            else
                            {
                                tvthreestar.setText(three_rate+" users");
                            }

                            TextView tvfourstar=findViewById(R.id.tvfourstarid);
                            if(four_rate.equals("1")||three_rate.equals("0"))
                            {
                                tvfourstar.setText(four_rate+" user");
                            }
                            else
                            {
                                tvfourstar.setText(four_rate+" users");
                            }

                            TextView tvfivestar=findViewById(R.id.tvfivestarid);
                            if(five_rate.equals("1")||four_rate.equals("0"))
                            {
                                tvfivestar.setText(five_rate+" user");
                            }
                            else
                            {
                                tvfivestar.setText(five_rate+" users");
                            }

                            TextView tvoverallstar=findViewById(R.id.tvoverallstarid);
                            if(avg.equals("1"))
                            {
                                tvoverallstar.setText("("+avg+") star");
                            }
                            else
                            {
                                tvoverallstar.setText("("+avg+") stars");
                            }

                            RatingBar onestar=findViewById(R.id.onestarid);
                            RatingBar twostar=findViewById(R.id.twostarid);
                            RatingBar threestar=findViewById(R.id.threestarid);
                            RatingBar fourstar=findViewById(R.id.fourstarid);
                            RatingBar fivestar=findViewById(R.id.fivestarid);

                            DIFFERENT_COLOUR_RATING_BARS(onestar,twostar,threestar,fourstar,fivestar);


                            RatingBar ratingbaroverall=findViewById(R.id.ratingbaroverallid);
                            ratingbaroverall.setRating(Float.parseFloat(avg));
                            LayerDrawable threeld = (LayerDrawable) ratingbaroverall.getProgressDrawable();
                            DrawableCompat.setTint(DrawableCompat.wrap(threeld.getDrawable(0)), getResources().getColor(R.color.lightgray));   // Empty star
                            DrawableCompat.setTint(DrawableCompat.wrap(threeld.getDrawable(1)), getResources().getColor(R.color.lightgray)); // Partial star
                            DrawableCompat.setTint(DrawableCompat.wrap(threeld.getDrawable(2)),  getResources().getColor(R.color.gold));  // Full star


                            JSONArray listArray=dataObject.getJSONArray("list");
                            Gson gson = new Gson();
                            for(int i=0;i<listArray.length();i++)
                            {
                                JSONObject Object = listArray.getJSONObject(i);
                                ratingpeople_list.add(gson.fromJson(String.valueOf(Object), RATING_PEOPLE_LIST.class));
                            }

                            reviewpeople_listrecycler_view.setAdapter(new RATING_PEOPLE_LIST_ADP(getApplicationContext(),ratingpeople_list));
                            Log.e("ratingpeople_list", String.valueOf(ratingpeople_list));
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Log.e("e", String.valueOf(e));
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("GET_RATING_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void DIFFERENT_COLOUR_RATING_BARS(RatingBar one,RatingBar two,RatingBar three,RatingBar four,RatingBar five)
    {
        LayerDrawable oneld = (LayerDrawable) one.getProgressDrawable();
        DrawableCompat.setTint(DrawableCompat.wrap(oneld.getDrawable(0)), getResources().getColor(R.color.lightgray));   // Empty star
        DrawableCompat.setTint(DrawableCompat.wrap(oneld.getDrawable(1)), getResources().getColor(R.color.lightgray)); // Partial star
        DrawableCompat.setTint(DrawableCompat.wrap(oneld.getDrawable(2)),  getResources().getColor(R.color.mred));  // Full star

        LayerDrawable twold = (LayerDrawable) two.getProgressDrawable();
        DrawableCompat.setTint(DrawableCompat.wrap(twold.getDrawable(0)), getResources().getColor(R.color.lightgray));   // Empty star
        DrawableCompat.setTint(DrawableCompat.wrap(twold.getDrawable(1)), getResources().getColor(R.color.lightgray)); // Partial star
        DrawableCompat.setTint(DrawableCompat.wrap(twold.getDrawable(2)),  getResources().getColor(R.color.orange));  // Full star

        LayerDrawable threeld = (LayerDrawable) three.getProgressDrawable();
        DrawableCompat.setTint(DrawableCompat.wrap(threeld.getDrawable(0)), getResources().getColor(R.color.lightgray));   // Empty star
        DrawableCompat.setTint(DrawableCompat.wrap(threeld.getDrawable(1)), getResources().getColor(R.color.lightgray)); // Partial star
        DrawableCompat.setTint(DrawableCompat.wrap(threeld.getDrawable(2)),  getResources().getColor(R.color.yellow));  // Full star

        LayerDrawable fourld = (LayerDrawable) four.getProgressDrawable();
        DrawableCompat.setTint(DrawableCompat.wrap(fourld.getDrawable(0)), getResources().getColor(R.color.lightgray));   // Empty star
        DrawableCompat.setTint(DrawableCompat.wrap(fourld.getDrawable(1)), getResources().getColor(R.color.lightgray)); // Partial star
        DrawableCompat.setTint(DrawableCompat.wrap(fourld.getDrawable(2)),  getResources().getColor(R.color.lightgreen));  // Full star

        LayerDrawable fiveld = (LayerDrawable) five.getProgressDrawable();
        DrawableCompat.setTint(DrawableCompat.wrap(fiveld.getDrawable(0)), getResources().getColor(R.color.lightgray));   // Empty star
        DrawableCompat.setTint(DrawableCompat.wrap(fiveld.getDrawable(1)), getResources().getColor(R.color.lightgray)); // Partial star
        DrawableCompat.setTint(DrawableCompat.wrap(fiveld.getDrawable(2)),  getResources().getColor(R.color.mgreen));  // Full star

    }

    public class RATING_PEOPLE_LIST_ADP extends RecyclerView.Adapter<RATING_PEOPLE_LIST_ADP.ViewHolder>
    {
        Context context;
        ArrayList<RATING_PEOPLE_LIST> reviewlist;

        private RequestQueue rq;
        private PreferenceManager_ASOS PreferenceManager_ASOS;


        public RATING_PEOPLE_LIST_ADP(Context context, ArrayList<RATING_PEOPLE_LIST> reviewlist)
        {
            super();
            this.context = context;
            this.reviewlist = reviewlist;
            rq= Volley.newRequestQueue(context);
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
        }

        @Override
        public RATING_PEOPLE_LIST_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_lv_review_people_item, parent, false);
            RATING_PEOPLE_LIST_ADP.ViewHolder viewHolder = new RATING_PEOPLE_LIST_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final RATING_PEOPLE_LIST_ADP.ViewHolder holder, final int position)
        {
            LayerDrawable layerDrawable = (LayerDrawable) holder.ratingbar.getProgressDrawable();
            DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(0)), getResources().getColor(R.color.lightgray));   // Empty star
            DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(1)), getResources().getColor(R.color.lightgray)); // Partial star
            DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(2)),  getResources().getColor(R.color.gold));  // Full star

            holder.review.setText(reviewlist.get(position).getReview());
            holder.reviewdate.setText(reviewlist.get(position).getDate_time());
            holder.ratingbar.setRating(Float.parseFloat(reviewlist.get(position).getRate()));

            //setIMG
            if(reviewlist.get(position).getImg_path().equals(""))
            {
                holder.people_img.setImageDrawable(getResources().getDrawable(R.drawable.noimage));
            }
            else
            {
                Picasso.with(getApplicationContext())
                        .load(PreferenceManager_ASOS.getimage_path()+""+reviewlist.get(position).getImg_path())
                        .into(holder.people_img);
            }

            //setName
            if(reviewlist.get(position).getName().equals(""))
            {
                holder.name.setText("App User");
            }
            else
            {
                holder.name.setText(reviewlist.get(position).getName());
            }

        }

        @Override
        public int getItemCount()
        {
            return reviewlist.size();
        }


        public  class ViewHolder extends RecyclerView.ViewHolder
        {
            CircleImageView people_img;
            TextView name,review,reviewdate;
            RatingBar ratingbar;

            public ViewHolder(View itemView)
            {
                super(itemView);

                people_img = itemView.findViewById(R.id.people_img_id);
                ratingbar = itemView.findViewById(R.id.ratingbarid);
                name = (TextView) itemView.findViewById(R.id.nameid);
                review = (TextView) itemView.findViewById(R.id.reviewid);
                reviewdate = (TextView) itemView.findViewById(R.id.reviewdateid);

            }

        }

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
