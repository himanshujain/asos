package com.example.searchus.asos;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.MODELS.ADDRESS_MODEL;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class My_Address extends AppCompatActivity
{
    LinearLayout parentlo,empty_lo,main_lo,nonet_lo;
    TextView empty_msg;
    RecyclerView Address_RecyclerView;
    Button add_addr_bt,empty_lo_add_addr_bt;
    Toolbar toolbar;

    ArrayList<ADDRESS_MODEL> address_modelArrayList=new ArrayList<>();
    PreferenceManager_ASOS PreferenceManager_ASOS;
    public static final int MY_PERMISSION_LOCATION = 001;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_my__address);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

        TOOLBAR();
        BINDING();

        add_addr_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (checkSelfPermission(
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && getApplicationContext().checkSelfPermission(
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSION_LOCATION);
                    }
                    else
                    {
                        Intent intent=new Intent(My_Address.this,Add_New_Addr.class);
                        startActivity(intent);
                    }
                }


            }
        });

        empty_lo_add_addr_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (checkSelfPermission(
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && getApplicationContext().checkSelfPermission(
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSION_LOCATION);
                    }
                    else
                    {
                        Intent intent=new Intent(My_Address.this,Add_New_Addr.class);
                        startActivity(intent);
                    }
                }

            }
        });

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
    }


    public void TOOLBAR()
    {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(getIntent().getStringExtra("from intent").equals("Add_New_Addr"))
                {
//            Intent intent=new Intent(getApplicationContext(),My_Account.class);
//            startActivity(intent);
                    onBackPressed();
                    finish();
                }
                else if(PreferenceManager_ASOS.getcamefrom().equals("My_Account"))//---
                {
//            Intent intent=new Intent(getApplicationContext(),My_Account.class);
//            startActivity(intent);
                    onBackPressed();
                    finish();
                }
                else if(getIntent().getStringExtra("from intent").equals("Edit_Address"))
                {
//            Intent intent=new Intent(getApplicationContext(),My_Account.class);
//            startActivity(intent);
                    onBackPressed();
                    finish();;
                }
                else if(getIntent().getStringExtra("from intent").equals("CheckOut"))
                {
                    PreferenceManager_ASOS.setcamefrom("no");

//            Intent intent=new Intent(getApplicationContext(),CheckOut.class);
//            intent.putExtra("from_intent", "fromaddr");
//
//            intent.putExtra("getAddress_id", address_modelArrayList.get(0).getAddress_id());
//            intent.putExtra("getName", address_modelArrayList.get(0).getName());
//            intent.putExtra("getAddress", address_modelArrayList.get(0).getAddress());
//            intent.putExtra("getNear_by", address_modelArrayList.get(0).getNear_by());
//            intent.putExtra("getCity", address_modelArrayList.get(0).getCity());
//            intent.putExtra("getPincode", address_modelArrayList.get(0).getPincode());
//            intent.putExtra("getState", address_modelArrayList.get(0).getState());
//            intent.putExtra("getCountry", address_modelArrayList.get(0).getCountry());
//            intent.putExtra("getPhone", address_modelArrayList.get(0).getPhone());
//
//            startActivity(intent);

                    onBackPressed();
                    finish();
                }
            }
        });
    }

    public void BINDING()
    {
        parentlo=findViewById(R.id.parentlo_id);
        add_addr_bt=findViewById(R.id.add_addr_btid);
        empty_lo=findViewById(R.id.empty_loid);
        main_lo=findViewById(R.id.main_lo_id);
        nonet_lo=findViewById(R.id.nonet_lo_id);
        empty_msg=findViewById(R.id.empty_msg_id);
        empty_lo_add_addr_bt=findViewById(R.id.empty_lo_add_addr_btid);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults)
    {
        if (requestCode == MY_PERMISSION_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            final ProgressDialog progress = new ProgressDialog(My_Address.this);
            progress.setTitle("Loading");
            progress.setMessage("Please wait...");
            progress.show();

            Thread timer = new Thread()
            {
                public void run()
                {
                    try
                    {
                        sleep(3000);
                        progress.cancel();
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        //  gps functionality
                        Intent intent=new Intent(My_Address.this,Add_New_Addr.class);
                        startActivity(intent);
                    }

                }

            };
            timer.start();

        }
        else
        {
            Toast.makeText(My_Address.this, "GPS permission required",
                    Toast.LENGTH_SHORT).show();
        }
    }
    // Checking Internet connection function
    private BroadcastReceiver networkReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
            {
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED)
                {
//                    "We have internet connection. Good to go."

                    toolbar.setVisibility(View.VISIBLE);
                    nonet_lo.setVisibility(View.GONE);
                    main_lo.setVisibility(View.VISIBLE);

                    ADDRESS_GETDATA();

                }
                else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED)
                {
//                  "We have lost internet connection"

                    toolbar.setVisibility(View.GONE);
                    nonet_lo.setVisibility(View.VISIBLE);
                    main_lo.setVisibility(View.GONE);
                }
            }
        }
    };

    public void ADDRESS_GETDATA()
    {
        // Calling the RecyclerView
        Address_RecyclerView = (RecyclerView) findViewById(R.id.addressrecycler_view);
        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        Address_RecyclerView.setLayoutManager(trending_LayoutManager);
        Address_RecyclerView.setNestedScrollingEnabled(false);

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "user_address_process");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            object.put("action", "get_user_address");

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(My_Address.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading your address book");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();
                    address_modelArrayList.clear();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("ADDRESS_res", "BLANK DATA");

                        main_lo.setVisibility(View.GONE);
                        empty_lo.setVisibility(View.VISIBLE);
                        empty_msg.setText("Opps, it seems your ADDRESS BOOK is empty...!!!");
                    }
                    else
                    {
                        Log.e("ADDRESS_res", response);

                        empty_lo.setVisibility(View.GONE);
                        main_lo.setVisibility(View.VISIBLE);

                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONArray jsonArray=jsonObject.getJSONArray("data");

                            Gson gson = new Gson();
                            for (int j = 0; j < jsonArray.length(); j++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                address_modelArrayList.add(gson.fromJson(String.valueOf(jsonObject1), ADDRESS_MODEL.class));
                            }

                            Address_RecyclerView.setAdapter(new Address_ADP(getApplicationContext(),address_modelArrayList));
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }



                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("ADDRESS_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }


    public class Address_ADP extends RecyclerView.Adapter<Address_ADP.ViewHolder>
    {
        Context context;
        private ArrayList<ADDRESS_MODEL> address_modelArrayList;
        private RequestQueue rq;
        private PreferenceManager_ASOS PreferenceManager_ASOS;


        public Address_ADP(Context context, ArrayList<ADDRESS_MODEL> address_modelArrayList)
        {
            super();
            this.context = context;
            this.address_modelArrayList = address_modelArrayList;
            rq= Volley.newRequestQueue(context);
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
        }

        @Override
        public Address_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_addr_lvitems, parent, false);
            Address_ADP.ViewHolder viewHolder = new Address_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final Address_ADP.ViewHolder holder, final int position)
        {
            if(PreferenceManager_ASOS.getcamefrom().equals("CheckOut")) //IF CAME FROM CHECKOUT
            {
                holder.ivdeleteaddr.setVisibility(View.GONE);
            }
            else
            {
                holder.ivdeleteaddr.setVisibility(View.VISIBLE);

            }

            holder.tv_addr_name.setText(address_modelArrayList.get(position).getName());
            holder.tv_addr.setText(address_modelArrayList.get(position).getAddress());
            holder.tv_addr_city.setText(address_modelArrayList.get(position).getCity());
            holder.tv_addr_pincode.setText(address_modelArrayList.get(position).getPincode());
            holder.tv_addr_state.setText(address_modelArrayList.get(position).getState());
            holder.tv_addr_mobno.setText(address_modelArrayList.get(position).getPhone());
            holder.tv_nearby.setText(address_modelArrayList.get(position).getNear_by());


            holder.iveditaddr.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    String getaddrname=holder.tv_addr_name.getText().toString();
                    String getaddr=holder.tv_addr.getText().toString();
                    String getaddrcity=holder.tv_addr_city.getText().toString();
                    String getaddrpincode=holder.tv_addr_pincode.getText().toString();
                    String getaddrstate=holder.tv_addr_state.getText().toString();
                    String getaddrmobno=holder.tv_addr_mobno.getText().toString();
                    String getnearby=holder.tv_nearby.getText().toString();

                    Intent i = new Intent(My_Address.this, Edit_Address.class);
                    i.putExtra("getaddrname",getaddrname);
                    i.putExtra("getaddr",getaddr);
                    i.putExtra("getaddrcity",getaddrcity);
                    i.putExtra("getaddrpincode",getaddrpincode);
                    i.putExtra("getaddrstate",getaddrstate);
                    i.putExtra("getaddrmobno",getaddrmobno);
                    i.putExtra("getaddrid",address_modelArrayList.get(position).getAddress_id());
                    i.putExtra("getnearby",getnearby);
                    startActivity(i);
//                    finish();
                }
            });

            holder.ivdeleteaddr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String title="Remove address";
                    String message="Are you sure you want to remove this address?";

                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(My_Address.this);

                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

                    // Initialize a new spannable string builder instance
                    SpannableStringBuilder ssBuilder = new SpannableStringBuilder(title);

                    // Apply the text color span
                    ssBuilder.setSpan(
                            foregroundColorSpan,
                            0,
                            title.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    );

                    dialogBuilder.setTitle(ssBuilder);
                    dialogBuilder.setMessage(message);
                    dialogBuilder.setIcon(R.drawable.delete_bin);


                    dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();

                            String getaddr_id=address_modelArrayList.get(position).getAddress_id();
                            REMOVE_ADDRESS(getaddr_id,position);

                        }
                    });
                    dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblue));
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.mblue));
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.white));

                }
            });

            holder.cardview.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(PreferenceManager_ASOS.getcamefrom().equals("CheckOut"))//--WHEN ADDR SELECTED
                    {
                        PreferenceManager_ASOS.setcamefrom("SELECT_ADDR");
                        Log.e("PREF",PreferenceManager_ASOS.getcamefrom());
                        Intent intent = new Intent(My_Address.this, CheckOut.class);
//                        intent.putExtra("from_intent", "fromaddr");

                        intent.putExtra("getAddress_id", address_modelArrayList.get(position).getAddress_id());
                        intent.putExtra("getName", address_modelArrayList.get(position).getName());
                        intent.putExtra("getAddress", address_modelArrayList.get(position).getAddress());
                        intent.putExtra("getNear_by", address_modelArrayList.get(position).getNear_by());
                        intent.putExtra("getCity", address_modelArrayList.get(position).getCity());
                        intent.putExtra("getPincode", address_modelArrayList.get(position).getPincode());
                        intent.putExtra("getState", address_modelArrayList.get(position).getState());
                        intent.putExtra("getCountry", address_modelArrayList.get(position).getCountry());
                        intent.putExtra("getPhone", address_modelArrayList.get(position).getPhone());

                        startActivity(intent);
//                        ((Activity) My_Address.this).finish();
                    }

                }
            });

        }
        @Override
        public long getItemId(int position) {
            return address_modelArrayList.size();
        }
        @Override
        public int getItemCount()
        {
            return address_modelArrayList.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder
        {

            public TextView tv_addr_name,tv_addr,tv_addr_city,tv_addr_pincode,tv_addr_state,tv_addr_mobno,tv_nearby;
            private ImageView iveditaddr,ivdeleteaddr;
            CardView cardview;

            public ViewHolder(View itemView)
            {
                super(itemView);

                tv_addr_name = (TextView) itemView.findViewById(R.id.tv_addr_name_id);
                tv_addr = (TextView) itemView.findViewById(R.id.tv_addr_id);
                tv_addr_city = (TextView) itemView.findViewById(R.id.tv_addr_city_id);
                tv_addr_pincode = (TextView) itemView.findViewById(R.id.tv_addr_pincode_id);
                tv_addr_state = (TextView) itemView.findViewById(R.id.tv_addr_state_id);
                tv_addr_mobno = (TextView) itemView.findViewById(R.id.tv_addr_mobno_id);
                tv_nearby = (TextView) itemView.findViewById(R.id.tv_nearby_id);
                iveditaddr = (ImageView) itemView.findViewById(R.id.iveditaddrid);
                ivdeleteaddr = (ImageView) itemView.findViewById(R.id.ivdeleteaddreid);
                cardview=itemView.findViewById(R.id.cardview_id);
            }

        }

        public void REMOVE_ADDRESS(String addr_id, final int pos)
        {
            try
            {
                RequestQueue requestQueue = Volley.newRequestQueue(context);

                JSONObject object=new JSONObject() ;
                object.put("type","user_address_process");
                object.put("action","remove_address");
                object.put("user_id",PreferenceManager_ASOS.GetUserID());
                JSONObject jsonObject2=new JSONObject();
                jsonObject2.put("address_id",addr_id);
                object.put("options",jsonObject2);

                final String requestBody = object.toString();

                final ProgressDialog progressDialog=new ProgressDialog(My_Address.this,R.style.ProgressDialogStyle);
                progressDialog.setTitle("Removing");
                progressDialog.setMessage("Please wait ...");
                progressDialog.show();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        progressDialog.dismiss();
//                        String getresponce = response.replace("\"", "");

                        Log.e("ADD_REMOVE_res", response);

                        try
                        {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String status = jsonObject.getString("status");

                            if(status.equals("Address  Details Removed  successfully"))
                            {
                                address_modelArrayList.remove(pos);
                                notifyItemRemoved(pos);
                                notifyItemRangeChanged(pos, address_modelArrayList.size());

                                Snackbar snackbar = Snackbar.make(parentlo, "Address removed successfully from Address Book....!!!", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                snackbar.show();

                                //IF ADDR BOOK IS EMPTY THEN SAD LAYOUT
                                if(address_modelArrayList.size()==0)
                                {
                                    main_lo.setVisibility(View.GONE);
                                    empty_lo.setVisibility(View.VISIBLE);
                                    empty_msg.setText("Opps, it seems your ADDRESS BOOK is empty...!!!");
                                }
                                else
                                {
                                    main_lo.setVisibility(View.VISIBLE);
                                    empty_lo.setVisibility(View.GONE);
                                }
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Log.e("e", String.valueOf(e));

                        }



                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("Address_remove_error", error.toString());
                    }
                })

                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type","application/json");
                        params.put("authentication", PaperCart_HEADER.KEY);

                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError
                    {
                        try
                        {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        }
                        catch (UnsupportedEncodingException uee)
                        {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                requestQueue.add(stringRequest);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

    }


    @Override
    protected void onResume()
    {
        super.onResume();

        //Calling Checking Internet connection function
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (networkReceiver != null)
        {
            unregisterReceiver(networkReceiver);
        }
    }

    @Override
    public void onBackPressed()
    {
//        if(getIntent().getStringExtra("from intent").equals("Add_New_Addr"))
//        {
////            Intent intent=new Intent(getApplicationContext(),My_Account.class);
////            startActivity(intent);
//            super.onBackPressed();
//            finish();
//        }
////        else if(getIntent().getStringExtra("from intent").equals("My_Account"))//---
////        {
//////            Intent intent=new Intent(getApplicationContext(),My_Account.class);
//////            startActivity(intent);
////            super.onBackPressed();
////            finish();
////        }
//         if(getIntent().getStringExtra("from intent").equals("Edit_Address"))
//        {
////            Intent intent=new Intent(getApplicationContext(),My_Account.class);
////            startActivity(intent);
//            super.onBackPressed();
//            finish();;
//        }
        if(PreferenceManager_ASOS.getcamefrom().equals("CheckOut"))//--.
        {
            PreferenceManager_ASOS.setcamefrom("CART");

//            Intent intent=new Intent(getApplicationContext(),CheckOut.class);
//            intent.putExtra("from_intent", "fromaddr");
//
//            intent.putExtra("getAddress_id", address_modelArrayList.get(0).getAddress_id());
//            intent.putExtra("getName", address_modelArrayList.get(0).getName());
//            intent.putExtra("getAddress", address_modelArrayList.get(0).getAddress());
//            intent.putExtra("getNear_by", address_modelArrayList.get(0).getNear_by());
//            intent.putExtra("getCity", address_modelArrayList.get(0).getCity());
//            intent.putExtra("getPincode", address_modelArrayList.get(0).getPincode());
//            intent.putExtra("getState", address_modelArrayList.get(0).getState());
//            intent.putExtra("getCountry", address_modelArrayList.get(0).getCountry());
//            intent.putExtra("getPhone", address_modelArrayList.get(0).getPhone());
//
//            startActivity(intent);

            super.onBackPressed();
            finish();
        }
        else //--if simple add edit addr
        {
            super.onBackPressed();
            finish();
        }

    }
}
