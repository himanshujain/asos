package com.example.searchus.asos.MODELS;

import java.io.Serializable;
import java.util.ArrayList;

public class Pop_PROD_Final_Varient_MODEL implements Serializable
{

    public Pop_PROD_Final_Varient_MODEL(String variant_name, int sr_no, double purchase_price, double mrp, double final_price, int quantity, ArrayList<String> images) {
        this.variant_name = variant_name;
        this.sr_no = sr_no;
        this.purchase_price = purchase_price;
        this.mrp = mrp;
        this.final_price = final_price;
        this.quantity = quantity;
        this.images = images;
    }

    private String variant_name;
    private int sr_no;
            double purchase_price,
            mrp,
            final_price;
            int  quantity;
    ArrayList<String>images;


    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public int getSr_no() {
        return sr_no;
    }

    public void setSr_no(int sr_no) {
        this.sr_no = sr_no;
    }

    public double getPurchase_price() {
        return purchase_price;
    }

    public void setPurchase_price(double purchase_price) {
        this.purchase_price = purchase_price;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public double getFinal_price() {
        return final_price;
    }

    public void setFinal_price(double final_price) {
        this.final_price = final_price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
