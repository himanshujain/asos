package com.example.searchus.asos.TAB_FRAG;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Classes.No_Internet;
import com.example.searchus.asos.MODELS.GETSET_PRODLIST;
import com.example.searchus.asos.MODELS.HOME_SLIDER_GETSET;
import com.example.searchus.asos.MODELS.Pop_PROD_Final_Varient_MODEL;
import com.example.searchus.asos.MODELS.RECENTVIEW_GETSET_MODEL;
import com.example.searchus.asos.MODELS.RecentView_Varient_MODEL;
import com.example.searchus.asos.MODELS.SUBSECTIONS_MODEL;
import com.example.searchus.asos.MODELS.Section_MODEL;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.example.searchus.asos.Product_Detail;
import com.example.searchus.asos.Product_Listing;
import com.example.searchus.asos.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class SECTION_TAB_FRAG extends Fragment
{

    ArrayList<HOME_SLIDER_GETSET> slider_image_list= new ArrayList<>();;
    ArrayList<RECENTVIEW_GETSET_MODEL> recentview_getset_modelArrayList=new ArrayList<RECENTVIEW_GETSET_MODEL>();

    ViewPager viewPager;
    TabLayout indicator;
    RecyclerView sections_recycler_view;
    RecyclerView Recentlyviewed_RecyclerView;
    TextView tvrecentview;

    LinearLayout nonet_lo;
    NestedScrollView mainlo;

    PreferenceManager_ASOS PreferenceManager_ASOS;
    Timer timer;

    String check_user;

    @SuppressLint("ValidFragment")
    public SECTION_TAB_FRAG()
    {
            // public constructor
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.asos_section_tab_layout,null);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getContext());
        check_user=PreferenceManager_ASOS.GetUserID();

//        //Calling Checking Internet connection function
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
//        getActivity().registerReceiver(receiver, filter);

        nonet_lo=rootView.findViewById(R.id.nonet_lo_id);
        mainlo=rootView.findViewById(R.id.enter_otp_mainlo_id);

        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        indicator = (TabLayout) rootView.findViewById(R.id.indicator);

        tvrecentview=rootView.findViewById(R.id.tvrecentviewid);

        sections_recycler_view = (RecyclerView) rootView.findViewById(R.id.sections_recycler_view);
        Recentlyviewed_RecyclerView = (RecyclerView) rootView.findViewById(R.id.relatedlist_recycler_view);

        init();
        CALL_AUTO_ROTATE();

        if(check_user.equals(""))
        {
            tvrecentview.setVisibility(View.GONE);
            Recentlyviewed_RecyclerView.setVisibility(View.GONE);
            SECTION_GETDATA();
        }
        else
        {
            tvrecentview.setVisibility(View.VISIBLE);
            Recentlyviewed_RecyclerView.setVisibility(View.VISIBLE);
            SECTION_GETDATA();
            RECENTLY_VIEWED_PROD_LIST();

        }
        return rootView;
    }

    // Checking Internet connection function
    public class NetworkChangeReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(final Context context, final Intent intent)
        {
            if(No_Internet.getInstance(getActivity()).isNetworkAvailable(context))
            {
//                Toast.makeText(getApplicationContext(),"YES",Toast.LENGTH_SHORT).show();

                nonet_lo.setVisibility(View.GONE);
                mainlo.setVisibility(View.VISIBLE);



                if(check_user.equals(""))
                {
                    tvrecentview.setVisibility(View.GONE);
                    Recentlyviewed_RecyclerView.setVisibility(View.GONE);
                    SECTION_GETDATA();
                }
                else
                {

                    SECTION_GETDATA();
                    tvrecentview.setVisibility(View.VISIBLE);
                    Recentlyviewed_RecyclerView.setVisibility(View.VISIBLE);
                    RECENTLY_VIEWED_PROD_LIST();

                }


            }
            else
            {
//                Toast.makeText(getApplicationContext(),"NO",Toast.LENGTH_SHORT).show();
                nonet_lo.setVisibility(View.VISIBLE);
                mainlo.setVisibility(View.GONE);
            }

        }


    }

    //Slider GETDATA
    private void init()
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());

            JSONObject object=new JSONObject() ;
            object.put("type", "app_business_infos");
            object.put("category", "cover_images");

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(getContext(),R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();
                    slider_image_list.clear();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("SLIDER RES", "SLIDER BLANK DATA");
                    }
                    else
                    {
                        Log.e("SLIDER RES", response);

                        try
                        {
                            JSONObject mainOBJ=new JSONObject(response);
                            JSONObject jsonObject=mainOBJ.getJSONObject("data");
//                        Log.e("jsonObject", String.valueOf(jsonObject));

                            JSONArray jsonArray=jsonObject.getJSONArray("image");
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject object1=jsonArray.getJSONObject(i);

                                String path=object1.getString("path");
                                String link=object1.getString("link");
                                String category_name = object1.getString("category_name");
                                String notes = object1.getString("notes");


                                HOME_SLIDER_GETSET getset=new HOME_SLIDER_GETSET();
                                getset.setSliderimgpath(path);
                                getset.setLink(link);
                                getset.setCategory_name(category_name);
                                getset.setNote(notes);

                                slider_image_list.add(getset);

                                viewPager.setAdapter(new SliderAdapter(getContext().getApplicationContext(),slider_image_list));

                                //  DOTS CODE
                                indicator.setupWithViewPager(viewPager, true);
                            }
                            progressDialog.dismiss();
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("SLIDER error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public class SliderAdapter extends PagerAdapter
    {
        private LayoutInflater layoutInflater;
        Context context;
        ArrayList<HOME_SLIDER_GETSET> image_arraylist;


        public SliderAdapter(Context context, ArrayList<HOME_SLIDER_GETSET> image_arraylist)
        {
            this.context = context;
            this.image_arraylist = image_arraylist;

        }

        @Override
        public int getCount() {
            return image_arraylist.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final  int position)
        {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.asos_item_slider, container, false);
            final ImageView im_slider = (ImageView) view.findViewById(R.id.im_slider);
            im_slider.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (image_arraylist.get(position).getLink().equals("0") || image_arraylist.get(position).getCategory_name().equals(""))
                    {
                        //DO NOTHING
                    }
                    else
                    {
                        String getnote=image_arraylist.get(position).getNote();

                        Intent intent = new Intent(getActivity().getBaseContext(), Product_Listing.class);
                        intent.putExtra("catid", image_arraylist.get(position).getLink());
                        intent.putExtra("CATEGORY_NAME", image_arraylist.get(position).getCategory_name());
                        intent.putExtra("from_intent","from_category");
                        intent.putExtra("getnote",getnote);
                        getActivity().startActivity(intent);
                    }
                }
            });

            Picasso.with(context).load(PaperCart_CoNNectioN.IMGPATH+""+image_arraylist.get(position).getSliderimgpath()).into(im_slider);
            Log.e("SLIDER_IMG",image_arraylist.get(position).getSliderimgpath());
            container.addView(view);

//            //low to high img
//            String imgpath_8="images/mobile_server/business_cover/image8/";
//            String imgpath_2="images/mobile_server/business_cover/image2/";
//
//            String img_responce=image_arraylist.get(position).getSliderimgpath();
//            final String aftersubstring_img_response = (img_responce.substring(img_responce.lastIndexOf("/") + 1));
//            final String final_img8_path=PaperCart_CoNNectioN.IMGPATH +""+imgpath_8+""+aftersubstring_img_response;
//            final String final_img2_path=PaperCart_CoNNectioN.IMGPATH +""+imgpath_2+""+aftersubstring_img_response;
//
//            Picasso.with(context)
//                    .load(final_img2_path)
//                    .into(im_slider);
//
//            Log.e("SLIDER_IMG_2",final_img2_path);
//
//
////            new Handler().postDelayed(new Runnable()
////            {
////                @Override
////                public void run()
////                {
////                    // This method will be executed once the timer is over
////                    // Start your app main activity
////                    Picasso.with(context)
////                            .load(final_img2_path)
////                            .into(im_slider);
////
////                    Log.e("SLIDER_IMG_2",final_img2_path);
////
////                }
////            }, 3000);
//
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ViewPager viewPager = (ViewPager) container;
            View view = (View) object;
            viewPager.removeView(view);
        }
    }

    //Calling SliderTimer
    private void CALL_AUTO_ROTATE()
    {
        timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 5000, 5000);
    }

    //Slider Auto rotate Timer code
    private class SliderTimer extends TimerTask
    {
        @Override
        public void run()
        {
            if(getActivity() == null)
                return;
            getActivity().runOnUiThread(new Runnable()

            {
                @Override
                public void run()
                {
                    if (viewPager.getCurrentItem() < slider_image_list.size() - 1)
                    {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    }
                    else
                    {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }

    //Section GETDATA
    public void SECTION_GETDATA()
    {
        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        sections_recycler_view.setLayoutManager(trending_LayoutManager);
        sections_recycler_view.setNestedScrollingEnabled(false);
        sections_recycler_view.setHasFixedSize(true);

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            JSONObject object=new JSONObject() ;
            JSONObject jobj = new JSONObject();
            jobj.put("row_offset", "0");
            jobj.put("row_count", "500");
            try
            {
                object.put("type", "app_sections");
                object.put("action", "get_sections");
                object.put("user_id", PreferenceManager_ASOS.GetUserID());
                object.put("options", jobj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();

            }

            final String requestBody = object.toString();


            final ProgressDialog progressDialog=new ProgressDialog(getContext(),R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("SEC_response", "BLANK DATA");
                        sections_recycler_view.setVisibility(View.GONE);
                    }
                    else
                    {
                        Log.e("SEC_response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            ArrayList<Section_MODEL> seactionArrayList = new ArrayList<>();
//                            seactionArrayList.clear();
                            for (int j = 0; j < jsonArray.length(); j++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                String section_id,type,image_path,date_time,id,description,section_name,status;

                                section_id = jsonObject1.getString("section_id");
                                type = jsonObject1.getString("type");
                                image_path = jsonObject1.getString("image_path");
                                date_time = jsonObject1.getString("date_time");
                                id = jsonObject1.getString("id");
                                description = jsonObject1.getString("description");
                                section_name = jsonObject1.getString("section_name");
                                status = jsonObject1.getString("status");


                                Section_MODEL section_model=new Section_MODEL(section_id,type,image_path,date_time,id,description,section_name,status);

                                //add only active status in Arraylist, check size to confirm
                                if(status.equals("active"))
                                {
                                    seactionArrayList.add(section_model);
                                }
                            }
                            sections_recycler_view.setAdapter(new SECTION_Adapter(getContext(), seactionArrayList));
                            Log.e("Seaction_ArrayList SIZE", String.valueOf(seactionArrayList.size()));

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("SEC error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put(PaperCart_HEADER.Contenttype,PaperCart_HEADER.Appjson);
                    params.put(PaperCart_HEADER.Auth, PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public class SECTION_Adapter extends RecyclerView.Adapter<SECTION_Adapter.ViewHolder>
    {
        ArrayList<Section_MODEL> section_array;
        Context context;
        private PreferenceManager_ASOS PreferenceManager_ASOS;
        public RecyclerView subsection_recycler_view;


        public SECTION_Adapter(Context context,  ArrayList<Section_MODEL> section_array)
        {
            super();
            this.context = context;
            this.section_array = section_array;
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
        }

        @Override
        public SECTION_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
        {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.asos_section_lvitems, viewGroup, false);
            SECTION_Adapter.ViewHolder viewHolder = new SECTION_Adapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(SECTION_Adapter.ViewHolder viewHolder, final int i)
        {

            //settext

            viewHolder.tv_sectionname.setText(section_array.get(i).getSection_name());
            Log.e("tv_sectionname", String.valueOf(section_array.get(i).getSection_name()));

            if(section_array.get(i).getType().equals("products"))
            {
                String getsecid=section_array.get(i).getSection_id();
                SUB_SECTION_PROD_GETDATA(getsecid,subsection_recycler_view);
                Log.e("GOPROD", "GOPROD");

            }
            else if(section_array.get(i).getType().equals("category"))
            {
                String getsecid=section_array.get(i).getId();
                PROD_LISTING_GETDATA(getsecid,subsection_recycler_view);
                Log.e("GOCAT", "GOCAT");

            }

            viewHolder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
//                        String prodid= recentView_array.get(position).getProduct_id();
//                        Intent intent=new Intent(context,Product_Detail.class);
//                        intent.putExtra("prodid",prodid);
//                        context.startActivity(intent);
                    }
                }
            });

            //View All onClick
            viewHolder.tv_section_more.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(section_array.get(i).getType().equals("products"))
                    {
                        String getsecid=section_array.get(i).getSection_id();
                        String getsecname=section_array.get(i).getSection_name();
                        String getnote="";

                        Intent intent=new Intent(getActivity(),Product_Listing.class);
                        intent.putExtra("getsecid",getsecid);
                        intent.putExtra("from_intent","from_subsec_prod");
                        intent.putExtra("getsecname",getsecname);
                        intent.putExtra("getnote",getnote);

                        context.startActivity(intent);
                    }
                    else if(section_array.get(i).getType().equals("category"))
                    {
                        String getcatid=section_array.get(i).getId();
                        String getcatname=section_array.get(i).getSection_name();
                        String getnote="";

                        Intent intent=new Intent(getActivity(),Product_Listing.class);
                        intent.putExtra("catid",getcatid);
                        intent.putExtra("from_intent","from_category");
                        intent.putExtra("CATEGORY_NAME",getcatname);
                        intent.putExtra("getnote",getnote);

                        context.startActivity(intent);
                    }
                }
            });


        }

        @Override
        public int getItemCount()
        {
            return section_array.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
        {

            public TextView tv_sectionname,tv_section_more;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                tv_sectionname = (TextView) itemView.findViewById(R.id.tv_sectionnameid);
                tv_section_more = (TextView) itemView.findViewById(R.id.tv_section_moreid);
                subsection_recycler_view = (RecyclerView) itemView.findViewById(R.id.subsection_recycler_view);

                subsection_recycler_view.setHasFixedSize(true);


                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

            @Override
            public boolean onLongClick(View view)
            {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }
        }

        //SECTION DETAIL CALLING ONLY OF PROD GETDATA
        public void SUB_SECTION_PROD_GETDATA(String getsecid, final RecyclerView subsection_recycler_view)
        {


            try
            {
                RequestQueue requestQueue = Volley.newRequestQueue(context);

                JSONObject object=new JSONObject() ;
                JSONObject jobj = new JSONObject();
                jobj.put("row_offset", "0");
                jobj.put("row_count", "500");
                jobj.put("section_id", getsecid);

                try
                {
                    object.put("type", "app_sections");
                    object.put("action", "get_section_details");
                    object.put("user_id", PreferenceManager_ASOS.GetUserID());
                    object.put("options", jobj);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                final String requestBody = object.toString();

                final ProgressDialog progressDialog=new ProgressDialog(context,R.style.ProgressDialogStyle);
                progressDialog.setTitle("Loading");
                progressDialog.setMessage("Please wait ...");
                progressDialog.show();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {

                        progressDialog.dismiss();
                        String getresponce = response.replace("\"", "");

                        if(getresponce.equals("No Data Found"))
                        {
                            Log.e("SUB_SECTION_PROD_res", "BLANK DATA");
                        }
                        else
                        {
                            Log.e("SUB_SECTION_PROD_res", response);
                            try
                            {
                                JSONObject mainObject=new JSONObject(String.valueOf(response));
                                JSONObject jsonObject=mainObject.getJSONObject("data");

                                ArrayList<SUBSECTIONS_MODEL>subsections_modelArrayList=new ArrayList<>();
                                JSONArray jsonArray=jsonObject.getJSONArray("description");
                                for (int a = 0; a < jsonArray.length(); a++)
                                {
                                    ArrayList<RecentView_Varient_MODEL>recentView_varient_modelArrayList = new ArrayList<>();
                                    ArrayList<String>Img=new ArrayList<>();


                                    JSONObject jsonObject1 = jsonArray.getJSONObject(a);
                                    String product_id=jsonObject1.getString("product_id");
                                    String product_spec=jsonObject1.getString("product_spec");
                                    String  product_desc=jsonObject1.getString("product_desc");
                                    String  product_price=jsonObject1.getString("mrp");
                                    String product_name=jsonObject1.getString("product_name");
                                    String  quantity=jsonObject1.getString("quantity");
                                    String  Final_Price=jsonObject1.getString("Final_Price");
                                    String avalibility=jsonObject1.getString("avalibility");

                                    JSONArray VarientArray=jsonObject1.getJSONArray("variant");
                                    for (int b = 0; b < VarientArray.length(); b++)
                                    {
                                        JSONObject VarientObj= VarientArray.getJSONObject(b);

                                        String v_name=VarientObj.getString("name");
                                        String v_product_price=VarientObj.getString("product_price");
                                        String v_Final_Price=VarientObj.getString("Final_Price");
                                        String v_Quantity=VarientObj.getString("Quantity");
                                        String v_purchase_price=VarientObj.getString("purchase_price");

                                        RecentView_Varient_MODEL recentView_varient_model=new RecentView_Varient_MODEL(v_name,v_product_price,v_Final_Price,v_purchase_price,v_Quantity);
                                        recentView_varient_modelArrayList.add(recentView_varient_model);
                                    }

                                    JSONArray Prods_img_jsonArray = jsonObject1.getJSONArray("img");
                                    for (int i = 0; i < Prods_img_jsonArray.length(); i++)
                                    {

                                        Img.add(Prods_img_jsonArray.getString(i));
                                    }

                                    String Product_Favourite_Status=jsonObject1.getString("Product_Favourite_Status");
                                    String  product_keywords=jsonObject1.getString("product_keywords");
                                    String purchase_price=jsonObject1.getString("purchase_price");
                                    String product_code=jsonObject1.getString("product_code");
                                    String notes=jsonObject1.getString("notes");



                                    SUBSECTIONS_MODEL subsections_model=new SUBSECTIONS_MODEL
                                            (product_id,
                                                    product_spec,
                                                    product_name,
                                                    product_price,
                                                    product_desc,
                                                    product_keywords,
                                                    quantity,
                                                    Final_Price,
                                                    recentView_varient_modelArrayList,
                                                    Img,
                                                    avalibility,
                                                    Product_Favourite_Status,
                                                    purchase_price,
                                                    product_code,
                                                    notes);

                                    subsections_modelArrayList.add(subsections_model);


                                }

                                Log.e("size: ", String.valueOf(subsections_modelArrayList.size()));

                                LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                                trending_LayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                                subsection_recycler_view.setLayoutManager(trending_LayoutManager);
                                subsection_recycler_view.setNestedScrollingEnabled(false);

                                subsection_recycler_view.setAdapter(new SUBSECTIONS_Adapter(context,subsections_modelArrayList));

                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }

                        }
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("SUB_SECTION_PROD error", error.toString());
                    }
                })

                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type","application/json");
                        params.put("authentication", PaperCart_HEADER.KEY);

                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError
                    {
                        try
                        {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        }
                        catch (UnsupportedEncodingException uee)
                        {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                requestQueue.add(stringRequest);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }

        //SECTION DETAIL CALLING ONLY OF CATEGORY GETDATA
        public void PROD_LISTING_GETDATA(String getsecid, final RecyclerView subsection_recycler_view)
        {
            // Calling the RecyclerView
            try
            {
                RequestQueue requestQueue = Volley.newRequestQueue(context);

                JSONObject object=new JSONObject() ;
                JSONObject jobj3 = new JSONObject();//parameters
                jobj3.put("offset", "0");
                jobj3.put("limit", "10");

                JSONObject jobj2 = new JSONObject();//input
                jobj2.put("keyword",getsecid);
                jobj2.put("user_id",  PreferenceManager_ASOS.GetUserID());


                JSONObject jobj = new JSONObject();//options
                jobj.put("input", jobj2);
                jobj.put("parameters", jobj3);
                try
                {
                    object.put("options", jobj);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                final String requestBody = object.toString();


                final ProgressDialog progressDialog=new ProgressDialog(context,R.style.ProgressDialogStyle);
                progressDialog.setTitle("Loading");
                progressDialog.setMessage("Please wait ...");
                progressDialog.show();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.NEW_PRODUCTLIST_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        progressDialog.dismiss();

                        String getresponce = response.replace("\"", "");

                        if(getresponce.equals("No Data Found"))
                        {
                            Log.e("SUB_SEC_CAT_res", "BLANK DATA");
                        }
                        else
                        {
                            Log.e("SUB_SEC_CAT_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
                                String code = jsonObject.getString("code");
                                String message = jsonObject.getString("message");

                                ArrayList<GETSET_PRODLIST> getset_prodlistArrayList = null;
                                if (code.equals("000200"))
                                {

                                    JSONObject datajsonObject = jsonObject.getJSONObject("data");
                                    ArrayList<Pop_PROD_Final_Varient_MODEL> varientArray;
                                    ArrayList<String> V_imgarray = null;
                                    GETSET_PRODLIST getset_prodlist;
                                    getset_prodlistArrayList = new ArrayList<>();

                                    JSONArray prodlistarray = datajsonObject.getJSONArray("product_list");
                                    for (int k = 0; k < prodlistarray.length(); k++) {
                                        JSONObject jsonObject1 = prodlistarray.getJSONObject(k);

                                        String pid = jsonObject1.getString("product_id");
                                        String product_name = jsonObject1.getString("product_name");
                                        String product_price = jsonObject1.getString("product_price");
                                        String product_desc = jsonObject1.getString("product_desc");
                                        String quantity = jsonObject1.getString("quantity");
                                        String product_spec = jsonObject1.getString("product_spec");
                                        String tags = jsonObject1.getString("tags");
                                        ArrayList<String> Img = new ArrayList<>();

                                        JSONArray jsonArray = jsonObject1.getJSONArray("img_path");
                                        if (jsonArray != null) {
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                Img.add(jsonArray.getString(i));
                                            }
                                        }
//                                Log.e("Img", String.valueOf(Img));

                                        String purchase_price = jsonObject1.getString("purchase_price");
                                        String avalibility = jsonObject1.getString("avalibility");
                                        String product_code = jsonObject1.getString("product_code");
                                        String notes = jsonObject1.getString("notes");
                                        String extra_1 = jsonObject1.getString("extra_1");
                                        String extra_2 = jsonObject1.getString("extra_2");
                                        final boolean favourite_status = jsonObject1.getBoolean("favourite_status");
//                                Log.e("favourite_status", String.valueOf(favourite_status));

                                        varientArray = new ArrayList<>();
                                        JSONArray jsonArray2 = jsonObject1.getJSONArray("variant_details");
                                        for (int i = 0; i < jsonArray2.length(); i++) {
                                            JSONObject jsonObject3 = jsonArray2.getJSONObject(i);
                                            int V_sr_no = jsonObject3.getInt("sr_no");
                                            String V_variant_name = jsonObject3.getString("variant_name");
                                            double V_purchase_price = jsonObject3.getDouble("purchase_price");
                                            double V_mrp = jsonObject3.getDouble("mrp");
                                            double V_final_price = jsonObject3.getDouble("final_price");
                                            int V_quantity = jsonObject3.getInt("quantity");

                                            Object json = jsonObject3.get("images");
                                            JSONArray imagevar = null;
                                            V_imgarray = new ArrayList<String>();

                                            if (json instanceof JSONArray) {
                                                imagevar = jsonObject3.getJSONArray("images");
                                                for (int j = 0; j < imagevar.length(); j++) {
                                                    V_imgarray.add(String.valueOf(imagevar.get(j)));
                                                }

                                            } else {
                                            }

                                            Pop_PROD_Final_Varient_MODEL pop_prod_varient_model = new
                                                    Pop_PROD_Final_Varient_MODEL(V_variant_name, V_sr_no, V_purchase_price, V_mrp, V_final_price, V_quantity, V_imgarray);
                                            varientArray.add(pop_prod_varient_model);
                                        }

                                        String final_price = jsonObject1.getString("final_price");

                                        getset_prodlist = new GETSET_PRODLIST();
                                        getset_prodlist.setProduct_id(pid);
                                        getset_prodlist.setProduct_name(product_name);
                                        getset_prodlist.setProduct_price(product_price);
                                        getset_prodlist.setFinal_price(final_price);
                                        getset_prodlist.setFavourite_status(favourite_status);
                                        getset_prodlist.setImg_path(Img);
                                        getset_prodlist.setVariant_img(V_imgarray);
                                        getset_prodlist.setVarientArray(varientArray);
                                        getset_prodlist.setNote(notes);

                                        getset_prodlistArrayList.add(getset_prodlist);
                                    }

                                    LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                                    trending_LayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                                    subsection_recycler_view.setLayoutManager(trending_LayoutManager);
                                    subsection_recycler_view.setNestedScrollingEnabled(false);

                                    subsection_recycler_view.setAdapter(new Product_Listing_Adapter(context, getset_prodlistArrayList));

                                }
                                else
                                {
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                }


                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                                Log.e("e", String.valueOf(e));
                            }

                        }
                    }
                }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("SUB_SEC_CAT_error", error.toString());
                    }
                })

                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type","application/json");
                        params.put("authentication",PaperCart_HEADER.KEY);

                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError
                    {
                        try
                        {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        }
                        catch (UnsupportedEncodingException uee)
                        {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                requestQueue.add(stringRequest);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }


    }

    //SECTION DETAIL PROD ADAPTER
    public static class SUBSECTIONS_Adapter extends RecyclerView.Adapter<SUBSECTIONS_Adapter.ViewHolder>
    {
        ArrayList<SUBSECTIONS_MODEL> recentView_array;
        Context context;

        public SUBSECTIONS_Adapter(Context context,  ArrayList<SUBSECTIONS_MODEL> recentView_array)
        {
            super();
            this.context = context;
            this.recentView_array = recentView_array;

        }

        @Override
        public SUBSECTIONS_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
        {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.asos_relatedlist_lvitems, viewGroup, false);
            SUBSECTIONS_Adapter.ViewHolder viewHolder = new SUBSECTIONS_Adapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final SUBSECTIONS_Adapter.ViewHolder viewHolder, int i)
        {

            //settext

            viewHolder.prodname.setText(recentView_array.get(i).getProduct_name());
            Log.e("prodname: ",recentView_array.get(i).getProduct_name());

            //normal img set code
            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH+""+recentView_array.get(i).getImg().get(0))
                    .into(viewHolder.imgThumbnail);

            //img set low to high code
//            String img_responce=recentView_array.get(i).getImg().get(0);
//            final String aftersubstring_img_response = (img_responce.substring(img_responce.lastIndexOf("/") + 1));
//            final String final_img8_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_8+""+aftersubstring_img_response;
//            final String final_img4_path=PaperCart_CoNNectioN.IMGPATH +""+PaperCart_CoNNectioN.imgpath_4+""+aftersubstring_img_response;
//
//            Picasso.with(context)
//                    .load(final_img8_path)
//                    .into(viewHolder.imgThumbnail);
//
//            Log.e("final_img8_path",final_img8_path);
//
//
//            new Handler().postDelayed(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    // This method will be executed once the timer is over
//                    // Start your app main activity
//                    Picasso.with(context)
//                            .load(final_img4_path)
//                            .into(viewHolder.imgThumbnail);
//
//                }
//            }, 3000);



            if(recentView_array.get(i).getRecentView_varient_models().isEmpty())
            {
                viewHolder.mrp.setText("₹ "+recentView_array.get(i).getProduct_price());
                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getFinal_Price());
            }
            else
            {
                viewHolder.mrp.setText("₹ "+recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price());
                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getRecentView_varient_models().get(0).getFinal_Price());
            }

//            for(int v=0;v<recentView_array.get(i).getVariant().size();v++)
//            {
//                Log.e("getVariantPrice: ",String.valueOf(recentView_array.get(i).getVariant().get(v).getProduct_price()));
//            }



            if(recentView_array.get(i).getRecentView_varient_models().isEmpty())   //disc calculating starts
            {
                if(recentView_array.get(i).getProduct_price().equals("")||recentView_array.get(i).getProduct_price().equals("0"))
                {
                    viewHolder.mrp.setVisibility(View.GONE);
                    viewHolder.disc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    viewHolder.mrp.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(recentView_array.get(i).getFinal_Price());
                    double mrp_rate= Double.parseDouble(recentView_array.get(i).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    viewHolder.disc.setVisibility(View.VISIBLE);
                    viewHolder.disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getFinal_Price());
            }
            else
            {

                if(recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price().equals("")||recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price().equals("0"))
                {
                    viewHolder.mrp.setVisibility(View.GONE);
                    viewHolder.disc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    viewHolder.mrp.setVisibility(View.VISIBLE);
                    viewHolder.disc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(recentView_array.get(i).getRecentView_varient_models().get(0).getFinal_Price());
                    double mrp_rate= Double.parseDouble(recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    viewHolder.disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getRecentView_varient_models().get(0).getFinal_Price());
            }


            viewHolder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        String prodid= recentView_array.get(position).getProduct_id();
                        String getnote= String.valueOf(recentView_array.get(position).getNote());
                        Intent intent=new Intent(context,Product_Detail.class);
                        intent.putExtra("prodid",prodid);
                        intent.putExtra("getnote",getnote);
                        context.startActivity(intent);
                    }
                }
            });

        }

        @Override
        public int getItemCount()
        {
            return recentView_array.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
        {

            public ImageView imgThumbnail;
            public TextView prodname,disc,mrp,finalprice;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                prodname = (TextView) itemView.findViewById(R.id.prodnameid);
                disc = (TextView) itemView.findViewById(R.id.discid);
                mrp = (TextView) itemView.findViewById(R.id.mrpid);
                finalprice = (TextView) itemView.findViewById(R.id.finalpriceid);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

            @Override
            public boolean onLongClick(View view)
            {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }
        }

    }

    //SECTION DETAIL CATEGORY ADAPTER
    public static class Product_Listing_Adapter extends RecyclerView.Adapter<Product_Listing_Adapter.ViewHolder>
    {
        ArrayList<GETSET_PRODLIST> Product_Listing;
        Context context;

        private RequestQueue rq;
        private PreferenceManager_ASOS PreferenceManager_ASOS;
        String prodid;
        boolean status;
        String STATUS;

        public Product_Listing_Adapter(Context context, ArrayList<GETSET_PRODLIST> Product_Listing)
        {
            super();
            this.context = context;
            this.Product_Listing = Product_Listing;
            rq= Volley.newRequestQueue(context);
            PreferenceManager_ASOS=new PreferenceManager_ASOS(context);
        }


        @Override
        public Product_Listing_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_relatedlist_lvitems, parent, false);
            Product_Listing_Adapter.ViewHolder viewHolder = new Product_Listing_Adapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final Product_Listing_Adapter.ViewHolder holder, final int position)
        {

            holder.prodname.setText(Product_Listing.get(position).getProduct_name());
            Log.e("getProduct_name: ",Product_Listing.get(position).getProduct_name());


            if(Product_Listing.get(position).getVarientArray().isEmpty())
            {
                holder.mrp_price.setText("₹ "+Product_Listing.get(position).getProduct_price());
                holder.finalprice.setText("₹ "+Product_Listing.get(position).getFinal_price());
            }
            else
            {
                holder.mrp_price.setText("₹ "+Product_Listing.get(position).getVarientArray().get(0).getMrp());
                holder.finalprice.setText("₹ "+Product_Listing.get(position).getVarientArray().get(0).getMrp());
            }


            //disc calculating starts
            if(Product_Listing.get(position).getVarientArray().isEmpty())
            {
                if(Product_Listing.get(position).getProduct_price().equals("")||Product_Listing.get(position).getProduct_price().equals("0"))
                {
                    holder.mrp_price.setVisibility(View.GONE);
                    holder.tvdisc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    holder.mrp_price.setVisibility(View.VISIBLE);
                    holder.tvdisc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(Product_Listing.get(position).getFinal_price());
                    double mrp_rate= Double.parseDouble(Product_Listing.get(position).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    holder.tvdisc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                holder.finalprice.setText("₹ "+Product_Listing.get(position).getFinal_price());
            }
            else
            {

                if(Product_Listing.get(position).getVarientArray().get(0).getMrp()==0)
                {
                    holder.mrp_price.setVisibility(View.GONE);
                    holder.tvdisc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    holder.mrp_price.setVisibility(View.VISIBLE);
                    holder.tvdisc.setVisibility(View.VISIBLE);

                    double final_rate= Product_Listing.get(position).getVarientArray().get(0).getFinal_price();
                    double mrp_rate= Product_Listing.get(position).getVarientArray().get(0).getMrp();
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    holder.tvdisc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                holder.finalprice.setText("₹ "+Product_Listing.get(position).getVarientArray().get(0).getFinal_price());
            }



            //img code
            if(Product_Listing.get(position).getVarientArray().isEmpty())
            {
                if (Product_Listing.get(position).getImg_path().size()>0)
                {
                    Picasso.with(context)
                            .load(PaperCart_CoNNectioN.IMGPATH + "" + Product_Listing.get(position).getImg_path().get(0).toString())
                            .into(holder.imgThumbnail);
                    Log.e("getIMG",PaperCart_CoNNectioN.IMGPATH + "" + Product_Listing.get(position).getImg_path().get(0).toString());

                }
            }
            else
            {
                if (Product_Listing.get(position).getVariant_img().size()>0)
                {
                    Picasso.with(context)
                            .load(PaperCart_CoNNectioN.IMGPATH + "" + Product_Listing.get(position).getVariant_img().get(0).toString())
                            .into(holder.imgThumbnail);
                }
            }


//            //fav code
//            if(!Product_Listing.get(position).isFavourite_status())
//            {
//                holder.iv_add_to_wishlist.setBackgroundResource(R.drawable.gray_circle);
//            }
//            else
//            {
//                holder.iv_add_to_wishlist.setBackgroundResource(R.drawable.circle_bg);
//            }
//            holder.iv_add_to_wishlist.setOnClickListener(new View.OnClickListener()
//            {
//                @Override
//                public void onClick(View v)
//                {
//                    boolean abc=Product_Listing.get(position).isFavourite_status();
//                    if(!abc)
//                    {
//                        prodid= String.valueOf(Product_Listing.get(position).getProduct_id());
//                        status=true;
//                        STATUS="FAVOURITE";
//                        Product_Listing.get(position).setFavourite_status(true);
//                        ADD_FAVOURITE(holder.iv_add_to_wishlist,prodid);
//                    }
//                    else
//                    {
//                        prodid= String.valueOf(Product_Listing.get(position).getProduct_id());
//                        status=false;
//                        STATUS="UNFAVOURITE";
//                        Product_Listing.get(position).setFavourite_status(false);
//                        ADD_FAVOURITE(holder.iv_add_to_wishlist,prodid);
//
//                    }
//
//                }
//            });


            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        prodid= String.valueOf(Product_Listing.get(position).getProduct_id());
                        String getnote= String.valueOf(Product_Listing.get(position).getNote());
                        Intent intent=new Intent(context,Product_Detail.class);
                        intent.putExtra("prodid",prodid);
                        intent.putExtra("getnote",getnote);
                        context.startActivity(intent);
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return Product_Listing.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
        {

            public ImageView imgThumbnail;
            LinearLayout iv_add_to_wishlist;
            public TextView prodname,finalprice,mrp_price,tvdisc;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
//                iv_add_to_wishlist = (LinearLayout) itemView.findViewById(R.id.add_to_wishlist_id);
                finalprice = (TextView) itemView.findViewById(R.id.finalpriceid);
                mrp_price = (TextView) itemView.findViewById(R.id.mrpid);
                prodname = (TextView) itemView.findViewById(R.id.prodnameid);
                tvdisc = (TextView) itemView.findViewById(R.id.discid);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

            @Override
            public boolean onLongClick(View view)
            {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }
        }

//        public void ADD_FAVOURITE(final LinearLayout iv_add_to_wishlist,String p_id)
//        {
//            try
//            {
//                RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                JSONObject object=new JSONObject() ;
//                object.put("type","business_favourite_process");
//                object.put("user_id",PreferenceManager_ASOS.GetUserID());
//                object.put("action","add_product");
//                JSONObject jsonObject2=new JSONObject();
//                jsonObject2.put("product_id",p_id);
//                jsonObject2.put("status",STATUS);
//
//                object.put("options",jsonObject2);
//
//
//                final String requestBody = object.toString();
//
//                StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response)
//                    {
//
//                        String getresponce = response.replace("\"", "");
//
//                        if(getresponce.equals("No Data Found"))
//                        {
//                            Log.e("FAV res", "BLANK DATA");
//                        }
//                        else
//                        {
//                            Log.e("FAV res", response);
//
//                            try
//                            {
//                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
//                                String status = jsonObject.getString("status");
//
//                                if(status.equals("UNFAVOURITE"))
//                                {
//                                    iv_add_to_wishlist.setBackgroundResource(R.drawable.gray_circle);
//                                }
//                                else
//                                {
//                                    iv_add_to_wishlist.setBackgroundResource(R.drawable.circle_bg);
//                                }
//
//                            }
//                            catch (JSONException e)
//                            {
//                                e.printStackTrace();
//                                Log.e("e", String.valueOf(e));
//
//                            }
//
//                        }
//                    }
//                }, new Response.ErrorListener()
//                {
//                    @Override
//                    public void onErrorResponse(VolleyError error)
//                    {
//                        Log.e("FAV error", error.toString());
//                    }
//                })
//
//                {
//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError
//                    {
//                        Map<String, String>  params = new HashMap<String, String>();
//                        params.put("Content-Type","application/json");
//                        params.put("authentication", PreferenceManager_ASOS.getAuthKey());
//
//                        return params;
//                    }
//
//                    @Override
//                    public byte[] getBody() throws AuthFailureError
//                    {
//                        try
//                        {
//                            return requestBody == null ? null : requestBody.getBytes("utf-8");
//                        }
//                        catch (UnsupportedEncodingException uee)
//                        {
//                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
//                            return null;
//                        }
//                    }
//                };
//
//                requestQueue.add(stringRequest);
//            }
//            catch (JSONException e)
//            {
//                e.printStackTrace();
//            }
//
//        }

    }

    //Recently viewed GETDATA
    public void RECENTLY_VIEWED_PROD_LIST()
    {
        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        Recentlyviewed_RecyclerView.setLayoutManager(trending_LayoutManager);
        Recentlyviewed_RecyclerView.setNestedScrollingEnabled(false);
        Recentlyviewed_RecyclerView.setHasFixedSize(true);

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());

            JSONObject object=new JSONObject() ;
            object.put("type", "recently_viewed_process");
            object.put("action", "get_product_list");
            object.put("user_id",check_user);
            JSONObject object1=new JSONObject() ;
            object1.put("row_offset", "0");
            object1.put("row_count", "10");

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(getContext(),R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {


                    progressDialog.dismiss();
                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("Recent_List", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("Recent_List", response);
                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONArray jsonArray=jsonObject.getJSONArray("data");
                            for (int a = 0; a < jsonArray.length(); a++)
                            {
                                ArrayList<RecentView_Varient_MODEL>recentView_varient_modelArrayList = new ArrayList<>();
                                ArrayList<String>Img=new ArrayList<>();


                                JSONObject jsonObject1 = jsonArray.getJSONObject(a);
                                String product_id=jsonObject1.getString("product_id");
                                String product_spec=jsonObject1.getString("product_spec");
                                String product_name=jsonObject1.getString("product_name");
                                String  product_price=jsonObject1.getString("product_price");
                                String  product_desc=jsonObject1.getString("product_desc");
                                String  product_keywords=jsonObject1.getString("product_keywords");
                                String  quantity=jsonObject1.getString("quantity");
                                String  Final_Price=jsonObject1.getString("Final_Price");

                                JSONArray VarientArray=jsonObject1.getJSONArray("variant");
                                for (int b = 0; b < VarientArray.length(); b++)
                                {
                                    JSONObject VarientObj= VarientArray.getJSONObject(b);

                                    String v_name=VarientObj.getString("name");
                                    String v_product_price=VarientObj.getString("product_price");
                                    String v_Final_Price=VarientObj.getString("Final_Price");
                                    String v_purchase_price=VarientObj.getString("purchase_price");
                                    String v_Quantity=VarientObj.getString("Quantity");

                                    RecentView_Varient_MODEL recentView_varient_model=new RecentView_Varient_MODEL(v_name,v_product_price,v_Final_Price,v_purchase_price,v_Quantity);
                                    recentView_varient_modelArrayList.add(recentView_varient_model);
                                }

                                JSONArray Prods_img_jsonArray = jsonObject1.getJSONArray("img");
                                for (int i = 0; i < Prods_img_jsonArray.length(); i++)
                                {

                                    Img.add(Prods_img_jsonArray.getString(i));
                                }

                                String avalibility=jsonObject1.getString("avalibility");
                                String Product_Favourite_Status=jsonObject1.getString("Product_Favourite_Status");
                                String purchase_price=jsonObject1.getString("purchase_price");
                                String product_code=jsonObject1.getString("product_code");
                                String notes=jsonObject1.getString("notes");



                                RECENTVIEW_GETSET_MODEL recentview_getset_model=new RECENTVIEW_GETSET_MODEL

                                        (product_id,
                                                product_spec,
                                                product_name,
                                                product_price,
                                                product_desc,
                                                product_keywords,
                                                quantity,
                                                Final_Price,
                                                recentView_varient_modelArrayList,
                                                Img,
                                                avalibility,
                                                Product_Favourite_Status,
                                                purchase_price,
                                                product_code,
                                                notes);

                                recentview_getset_modelArrayList.add(recentview_getset_model);


                            }

                            Log.e("RECENT VIEW HOME SIZE:", String.valueOf(recentview_getset_modelArrayList.size()));
                            Recentlyviewed_RecyclerView.setAdapter(new RECENT_VIEW_Adapter(getActivity(),recentview_getset_modelArrayList));

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Recent_List error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public static class RECENT_VIEW_Adapter extends RecyclerView.Adapter<RECENT_VIEW_Adapter.ViewHolder>
    {
        ArrayList<RECENTVIEW_GETSET_MODEL> recentView_array;
        Context context;

        public RECENT_VIEW_Adapter(Context context,  ArrayList<RECENTVIEW_GETSET_MODEL> recentView_array)
        {
            super();
            this.context = context;
            this.recentView_array = recentView_array;

        }

        @Override
        public RECENT_VIEW_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
        {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.asos_relatedlist_lvitems, viewGroup, false);
            RECENT_VIEW_Adapter.ViewHolder viewHolder = new RECENT_VIEW_Adapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RECENT_VIEW_Adapter.ViewHolder viewHolder, int i)
        {

            //settext
            viewHolder.prodname.setText(recentView_array.get(i).getProduct_name());
            Log.e("prodname: ",recentView_array.get(i).getProduct_name());

            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH+""+recentView_array.get(i).getImg().get(0))
                    .into(viewHolder.imgThumbnail);


            if(recentView_array.get(i).getRecentView_varient_models().isEmpty())
            {
                viewHolder.mrp.setText("₹ "+recentView_array.get(i).getProduct_price());
                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getFinal_Price());
            }
            else
            {
                viewHolder.mrp.setText("₹ "+recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price());
                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getRecentView_varient_models().get(0).getFinal_Price());
            }

            //disc calculating starts
            if(recentView_array.get(i).getRecentView_varient_models().isEmpty())
            {
                if(recentView_array.get(i).getProduct_price().equals("")||recentView_array.get(i).getProduct_price().equals("0"))
                {
                    viewHolder.mrp.setVisibility(View.INVISIBLE);
                    viewHolder.disc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    viewHolder.mrp.setVisibility(View.VISIBLE);
                    viewHolder.disc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(recentView_array.get(i).getFinal_Price());
                    double mrp_rate= Double.parseDouble(recentView_array.get(i).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    viewHolder.disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");
                   // viewHolder.disc.setText("-"+new DecimalFormat("##.##").format(price_perc_off)+"%");

                }

                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getFinal_Price());
            }
            else
            {

                if(recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price().equals("")||recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price().equals("0"))
                {
                    viewHolder.mrp.setVisibility(View.INVISIBLE);
                    viewHolder.disc.setVisibility(View.INVISIBLE);
                }
                else
                {
                    viewHolder.mrp.setVisibility(View.VISIBLE);
                    viewHolder.disc.setVisibility(View.VISIBLE);

                    double final_rate= Double.parseDouble(recentView_array.get(i).getRecentView_varient_models().get(0).getFinal_Price());
                    double mrp_rate= Double.parseDouble(recentView_array.get(i).getRecentView_varient_models().get(0).getProduct_price());
                    double ans=final_rate*100/mrp_rate;
                    double price_perc_off=100-ans;

                    viewHolder.disc.setText(new DecimalFormat("##.##").format(price_perc_off)+"% OFF");

                }

                viewHolder.finalprice.setText("₹ "+recentView_array.get(i).getRecentView_varient_models().get(0).getFinal_Price());
            }


            viewHolder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        String prodid= recentView_array.get(position).getProduct_id();
                        String getnote= String.valueOf(recentView_array.get(position).getNotes());
                        Intent intent=new Intent(context,Product_Detail.class);
                        intent.putExtra("prodid",prodid);
                        intent.putExtra("getnote",getnote);
                        context.startActivity(intent);
                    }
                }
            });

        }

        @Override
        public int getItemCount()
        {
            return recentView_array.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
        {

            public ImageView imgThumbnail;
            public TextView prodname,disc,mrp,finalprice;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                prodname = (TextView) itemView.findViewById(R.id.prodnameid);
                disc = (TextView) itemView.findViewById(R.id.discid);
                mrp = (TextView) itemView.findViewById(R.id.mrpid);
                finalprice = (TextView) itemView.findViewById(R.id.finalpriceid);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

            @Override
            public boolean onLongClick(View view)
            {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        //pause or cancel timer
        Timer timer=new Timer();
        timer.cancel();
        timer.purge();

//        //Calling Checking Internet connection function
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
//        getActivity().registerReceiver(receiver, filter);

    }
}
