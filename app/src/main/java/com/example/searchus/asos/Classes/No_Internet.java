package com.example.searchus.asos.Classes;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class No_Internet
{

    private static No_Internet instance = new No_Internet();
    static Context context;

    private static final String LOG_TAG = "CheckNetworkStatus";
    private boolean isConnected = false;

    public static No_Internet getInstance(Context ctx)
    {
        context = ctx.getApplicationContext();
        return instance;
    }

    public boolean isNetworkAvailable(Context context)
    {

        ConnectivityManager connectivity = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
            {
                for (int i = 0; i < info.length; i++)
                {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        if(!isConnected)
                        {
                            Log.v(LOG_TAG, "Now you are connected to Internet!");


                            isConnected = true;
                            //do your processing here ---
                            //if you need to post any data to the server or get status
                            //update from the server
                        }
                        return true;
                    }
                }
            }
        }
        Log.v(LOG_TAG, "You are not connected to Internet!");

        isConnected = false;
        return false;
    }


}