package com.example.searchus.asos.MODELS;

/**
 * Created by DELL on 09/13/2016.
 */
public class Gallerydata {
    private String img;
    private String img_id;

    public Gallerydata(String img, String img_id) {
        this.img = img;
        this.img_id = img_id;
    }

    public Gallerydata() {

    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_id() {
        return img_id;
    }

    public void setImg_id(String img_id) {
        this.img_id = img_id;
    }
}
