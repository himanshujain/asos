package com.example.searchus.asos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.No_Internet;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import studio.carbonylgroup.textfieldboxes.ExtendedEditText;
import swarajsaaj.smscodereader.interfaces.OTPListener;
import swarajsaaj.smscodereader.receivers.OtpReader;

public class Verify_OTP extends AppCompatActivity implements OTPListener
{

    private TextView welcomenumb;
    private ExtendedEditText edverifymobno;
    private Button verifyotpbt,resentotpbt,timerbt;
    ImageView logo;
    TextView tv_businessname;

    LinearLayout nonet_lo;
    ScrollView mainlo;

    CountDownTimer cTimer = null;

    RequestQueue rq;
    PreferenceManager_ASOS PreferenceManager_ASOS;
    String verifycode="";
    String getnumber;
    int GET_MY_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_verify__otp);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());

//        //Calling Checking Internet connection function
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
//        registerReceiver(receiver, filter);

        nonet_lo=findViewById(R.id.nonet_lo_id);
        mainlo=findViewById(R.id.mainlo_id);

        logo=findViewById(R.id.logoid);
//        Picasso.with(getApplicationContext())
//                .load(PreferenceManager_ASOS.GetBusiness_Logo())
//                .into(logo);

        tv_businessname=findViewById(R.id.tv_businessnameid);
//        tv_businessname.setText(PreferenceManager_ASOS.GetBusiness_Name());

        if (ContextCompat.checkSelfPermission(Verify_OTP.this, android.Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Verify_OTP.this,
                    android.Manifest.permission.RECEIVE_SMS))
            {
                /* do nothing*/

            }
            else
            {

                ActivityCompat.requestPermissions(Verify_OTP.this,
                        new String[]{android.Manifest.permission.RECEIVE_SMS}, GET_MY_PERMISSION);
            }
        }

        //Calling OtpReader
        OtpReader.bind(this, "SRCHUS");

        edverifymobno=findViewById(R.id.edverifymobnoid);
        edverifymobno.setRawInputType(Configuration.KEYBOARD_12KEY);

        welcomenumb=findViewById(R.id.welcomenumbertvid);
        getnumber=getIntent().getStringExtra("sentnumber").toString().trim();

        welcomenumb.setText("Welcome, "+getnumber+" !!!");

        verifyotpbt=findViewById(R.id.verifyotpbtid);
        verifyotpbt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (!Verify_OTP.this.isFinishing())
                {
                    VERIFY_OTP();
                }
            }
        });

        timerbt=findViewById(R.id.timerbt_id);

        resentotpbt=findViewById(R.id.resentotpbtid);
        resentotpbt.setVisibility(View.GONE);
        resentotpbt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (!Verify_OTP.this.isFinishing())
                {
                    RESENT_OTP();
                    startTimer();
                }
            }
        });

        startTimer();
    }

    public void  VERIFY_OTP()
    {
        verifycode=edverifymobno.getText().toString();

        rq= Volley.newRequestQueue(this);
        try
        {
            JSONObject object=new JSONObject() ;

            JSONObject object1=new JSONObject() ;
            object1.put("login_type", "otp");
            object1.put("verification_code", verifycode);
            object1.put("device_id", "");
            object1.put("mobile",getnumber);


            object.put("type", "check_user");
            object.put("user_infos",object1);

            final ProgressDialog progressDialog=new ProgressDialog(Verify_OTP.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Verifying OTP");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response)
                {
                    Log.e("Verifyresponse", String.valueOf(response));
                    progressDialog.dismiss();

                    try
                    {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        if(jsonObject.getString("status").equals("Done"))
                        {
                            String user_id= String.valueOf(jsonObject.getString("user_id"));
                            String primary_number= String.valueOf(jsonObject.getString("primary_number"));
                            String name= String.valueOf(jsonObject.getString("name"));
                            String email= String.valueOf(jsonObject.getString("email"));
                            String alternate_no= String.valueOf(jsonObject.getString("alternate_no"));
                            String gender= String.valueOf(jsonObject.getString("gender"));
                            String birthdate= String.valueOf(jsonObject.getString("birthdate"));

                            PreferenceManager_ASOS.SetUserID(user_id);
                            PreferenceManager_ASOS.setprimary_number(primary_number);
                            PreferenceManager_ASOS.SetUser_NAME(name);
                            PreferenceManager_ASOS.setemail(email);
                            PreferenceManager_ASOS.setalternate_no(alternate_no);
                            PreferenceManager_ASOS.setgender(gender);
                            PreferenceManager_ASOS.setbirthdate(birthdate);

//                            Intent intent=new Intent(getApplicationContext(),Home.class);
//                            intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
//                            startActivity(intent);
//                            finish();
//                            Toast.makeText(getApplicationContext(),"DONE",Toast.LENGTH_LONG).show();

                            Verify_OTP.this.finish();
                        }
                        else if(jsonObject.getString("status").equals("Invalid OTP Code"))
                        {
                            Toast.makeText(getApplicationContext(),"Invalid OTP Code",Toast.LENGTH_LONG).show();
                        }

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

//                myDialog.dismiss();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    Log.e("Verifyerror", String.valueOf(error));
                    if (error instanceof NetworkError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ParseError) {
                    } else if (error instanceof NoConnectionError) {
                    } else if (error instanceof TimeoutError) {
                    }
                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put(PaperCart_HEADER.Contenttype, PaperCart_HEADER.Appjson);
                    params.put(PaperCart_HEADER.Auth, PaperCart_HEADER.KEY);

                    return params;
                }
            };

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rq.add(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void  RESENT_OTP()
    {
        verifycode=edverifymobno.getText().toString();

        rq= Volley.newRequestQueue(this);
        try
        {
            JSONObject object=new JSONObject() ;
            object.put("type", "send_otp");
            object.put("category","login");
            object.put("user_id",getnumber);

            final ProgressDialog progressDialog=new ProgressDialog(Verify_OTP.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Resending OTP");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response)
                {
                    Log.e("Resending_otp_response", String.valueOf(response));
                    progressDialog.dismiss();

                    try
                    {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                         if(jsonObject.getString("data").equals("invalid parameter"))
                        {
                            Toast.makeText(getApplicationContext(),"Something went wrong, try again...",Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    Log.e("Resending_otp_error", String.valueOf(error));

                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put(PaperCart_HEADER.Contenttype, PaperCart_HEADER.Appjson);
                    params.put(PaperCart_HEADER.Auth, PaperCart_HEADER.KEY);

                    return params;
                }
            };

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rq.add(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // Checking Internet connection function
    public class NetworkChangeReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(final Context context, final Intent intent)
        {
            if(No_Internet.getInstance(getApplicationContext()).isNetworkAvailable(context))
            {
//                Toast.makeText(getApplicationContext(),"YES",Toast.LENGTH_SHORT).show();
                nonet_lo.setVisibility(View.GONE);
                mainlo.setVisibility(View.VISIBLE);
            }
            else
            {
//                Toast.makeText(getApplicationContext(),"NO",Toast.LENGTH_SHORT).show();
                nonet_lo.setVisibility(View.VISIBLE);
                mainlo.setVisibility(View.GONE);

            }

        }

    }


    //Autofetch OTP frm SMS code
    @Override
    public void otpReceived(String messageText)
    {
        if (!Verify_OTP.this.isFinishing())
        {
            String finalstring = messageText.substring(messageText.length() - 6);
            edverifymobno.setText(finalstring);

            if (edverifymobno.getText().toString().length() == 6)
            {
                VERIFY_OTP();
            }
        }

    }

    //start timer
    public void startTimer()
    {

        cTimer = new CountDownTimer(30000, 1000)
        {
            public void onTick(long millisUntilFinished)
            {
                timerbt.setVisibility(View.VISIBLE);
                timerbt.setText(millisUntilFinished / 1000+" seconds remaining, please wait...!!!");
                resentotpbt.setVisibility(View.GONE);

//                Log.e("TIMER", String.valueOf(millisUntilFinished / 1000));
            }
            public void onFinish()
            {
                timerbt.setVisibility(View.GONE);
                resentotpbt.setVisibility(View.VISIBLE);
            }
        };
        cTimer.start();
    }

    //cancel timer
    public void cancelTimer()
    {
        if(cTimer!=null)
            cTimer.cancel();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        cancelTimer();
    }

    @Override
    public void onBackPressed()
    {
//            Intent i = new Intent(Verify_OTP.this, Enter_OTP.class);
//            i.putExtra("from_intent","Verify_OTP");
//            i.putExtra("getnumber",getnumber);
//            startActivity(i);
        super.onBackPressed();
    }

}
