package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Impl.Main_impl;
import com.example.searchus.asos.MODELS.ADDRESS_MODEL;
import com.example.searchus.asos.MODELS.mycart;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CheckOut extends AppCompatActivity
{
    RecyclerView OrderSummary_RecyclerView;
    TextView totalitem,tv_bag;
    TextView addr_person_name,addr,nearby,addr_city,addr_pincode,addr_state,addr_country,addr_phno;
    TextView subtotal,tv_delivery_charges,grand_total;
    TextView wallet_deduction_amt,pc_deduction_amt;
    SwitchCompat cod_switch,netbanking_switch;
    Button change_addrbt,paybt,promocode_applybt,promocode_delete;
    CheckBox walletswitch;
    LinearLayout wallet_deducted_layout,pc_deducted_layout;
    LinearLayout parent_lo;
    EditText ed_promocode;

    private ArrayList<mycart> listcart = new ArrayList<mycart>();
    ArrayList<ADDRESS_MODEL> address_modelArrayList=new ArrayList<>();
    PreferenceManager_ASOS PreferenceManager_ASOS;
    double sum = 0;
    String getAddress_id,getName,getAddress,getNear_by,getCity,getPincode,getState,getCountry,getPhone;
    String amount,free_delivery;
    String cod;
    String api_wallet_disc,api_wallet_amount,promocode_id,api_promocode_discount;
    boolean check_pincode;

    double calculate_wallet_amt;
    double after_wallet_deduction;
    double after_pc_deduction;
    double minusone_wallet;

    double promocode_discount;
    double calculate_promocode_amt;

    boolean wallet;
    boolean promocode;

    double TotalAmt;
    double WalletAmt;
    double PromoCodeAmt;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_check_out);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());



        TOOLBAR();
        BINDING();
        SWITCH_COMPACT_LOGIC();
        CHECKOUT_PRODLIST_GETDATA(); //GETDATA
        WALLET_GETDATA();
        CHECKING_WALLET_PREF();
        CHECKING_PROMOCODE_PREF();
        ADDRESS_GETDATA();

        wallet_deducted_layout.setVisibility(View.GONE);
        pc_deducted_layout.setVisibility(View.GONE);

        tv_bag.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(getApplicationContext(),Bag_Static.class);
                startActivity(intent);
            }
        });

        change_addrbt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PreferenceManager_ASOS.setcamefrom("CheckOut");//--
                Intent intent=new Intent(getApplicationContext(),My_Address.class);
//                intent.putExtra("from intent","CheckOut");
                startActivity(intent);
                Log.e("PREF",PreferenceManager_ASOS.getcamefrom());
            }
        });

        promocode_applybt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(ed_promocode.length()==0)
                {
                    Snackbar snackbar = Snackbar.make(parent_lo, "Please enter the Promo Code....!!!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblack));
                    snackbar.show();
                }
                else
                {
                    PROMOCODE_GETDATA(ed_promocode.getText().toString());
                }
            }
        });

        //WALLET APPLYING
        walletswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    wallet_deducted_layout.setVisibility(View.VISIBLE);
                    wallet_deduction_amt.setText(String.valueOf("0"));

                    //---set pref
                    PreferenceManager_ASOS.setiF_WALLET(true);
                    wallet=PreferenceManager_ASOS.getiF_WALLET();
                    CALCULATION();

                    Snackbar snackbar = Snackbar.make(parent_lo, "Wallet deduction had been applied successfully....!!!", Snackbar.LENGTH_LONG);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mgreen));
                    snackbar.show();

                }
                else
                {
                    wallet_deducted_layout.setVisibility(View.GONE);

                    //---set pref
                    PreferenceManager_ASOS.setiF_WALLET(false);
                    wallet=PreferenceManager_ASOS.getiF_WALLET();
                    CALCULATION();

                    Snackbar snackbar = Snackbar.make(parent_lo, "Wallet deduction had been removed successfully....!!!", Snackbar.LENGTH_LONG);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                    snackbar.show();
                }
            }
        });

        //REMOVING PROMOCODE
        promocode_delete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //setting pref
                PreferenceManager_ASOS.setiF_PROMOCODE(false);
                promocode=PreferenceManager_ASOS.getIF_PROMOCOD();
                CALCULATION();

                promocode_delete.setVisibility(View.GONE);
                promocode_applybt.setVisibility(View.VISIBLE);
                PreferenceManager_ASOS.setpromocode("");
                ed_promocode.setText("");
                pc_deducted_layout.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(parent_lo, "Promo Code had been removed successfully....!!!", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                snackbar.show();
            }
        });

//        WALLET_GETDATA();

        Log.e("W", String.valueOf(PreferenceManager_ASOS.getiF_WALLET()));
        Log.e("P", String.valueOf(PreferenceManager_ASOS.getIF_PROMOCOD()));

        wallet=PreferenceManager_ASOS.getiF_WALLET();
        promocode=PreferenceManager_ASOS.getIF_PROMOCOD();
    }

    public void BINDING()
    {
        totalitem=findViewById(R.id.totalitemid);
        tv_bag=findViewById(R.id.tv_bag_id);
        cod_switch=findViewById(R.id.cod_switchid);
        netbanking_switch=findViewById(R.id.netbanking_switchid);

        addr_person_name=findViewById(R.id.addr_person_name_id);
        addr=findViewById(R.id.addr_id);
        nearby=findViewById(R.id.nearby_id);
        addr_city=findViewById(R.id.addr_city_id);
        addr_pincode=findViewById(R.id.addr_pincode_id);
        addr_state=findViewById(R.id.addr_state_id);
        addr_country=findViewById(R.id.addr_country_id);
        addr_phno=findViewById(R.id.addr_phno_id);

        parent_lo=findViewById(R.id.parent_loid);
        change_addrbt=findViewById(R.id.change_addrbt_id);
        paybt=findViewById(R.id.paybt_id);
        subtotal=findViewById(R.id.subtotalid);
        tv_delivery_charges=findViewById(R.id.tv_delivery_charges_id);
        grand_total=findViewById(R.id.grand_total_id);
        OrderSummary_RecyclerView = findViewById(R.id.viewmybag_recycler_view);
        ed_promocode=findViewById(R.id.ed_promocode_id);
        promocode_applybt=findViewById(R.id.promocode_applybt_id);
        promocode_delete=findViewById(R.id.promocode_delete_id);
        walletswitch=findViewById(R.id.walletswitchid);
        wallet_deducted_layout=findViewById(R.id.wallet_deducted_layoutid);
        wallet_deduction_amt=findViewById(R.id.wallet_deduction_amtid);
        pc_deduction_amt=findViewById(R.id.pc_deduction_amtid);
        pc_deducted_layout=findViewById(R.id.pc_deducted_layoutid);
    }

    public void TOOLBAR()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Exit_Dialog("Exit checkout...?","Are you sure you want to exit checkout ?",R.drawable.appnewlogo);
            }
        });
    }

    public void SWITCH_COMPACT_LOGIC()
    {
        cod_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(cod_switch.isChecked()==true)
                {
                    netbanking_switch.setChecked(false);
                }
            }
        });

        netbanking_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(netbanking_switch.isChecked()==true)
                {
                    cod_switch.setChecked(false);
                }
            }
        });
    }

    public void CHECKOUT_PRODLIST_GETDATA()
    {
        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        OrderSummary_RecyclerView.setLayoutManager(trending_LayoutManager);
        OrderSummary_RecyclerView.setNestedScrollingEnabled(false);

        Main_impl impl = new Main_impl(CheckOut.this);
        listcart = impl.getUser();

        //ALL PROD PRIZE CODE
        for (int i=0;i<listcart.size();i++)
        {
            sum = sum + Double.parseDouble(listcart.get(i).getAmount());
        }

        subtotal.setText("₹ "+String.valueOf(sum));
        PreferenceManager_ASOS.setshopping_amt(String.valueOf(sum));
        TotalAmt= Double.parseDouble(PreferenceManager_ASOS.getshopping_amt());

        //TOTAL ITEM COUNT
        if(listcart.size()<2)
        {
            totalitem.setText("( "+listcart.size()+" item"+" )");
        }
        else
        {
            totalitem.setText("( "+listcart.size()+" items"+" )");
        }

        OrderSummary_RecyclerView.setAdapter(new OrderSummary_ADP(getApplicationContext(),listcart));
    }

    public class OrderSummary_ADP extends RecyclerView.Adapter<OrderSummary_ADP.ViewHolder>
    {
        ArrayList<mycart> arrayList;
        Context context;

        public OrderSummary_ADP(Context context, ArrayList<mycart> arrayList)
        {
            super();
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public OrderSummary_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_bag_horizontal_lvitems, parent, false);
            OrderSummary_ADP.ViewHolder viewHolder = new OrderSummary_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(OrderSummary_ADP.ViewHolder holder, int position)
        {
            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH+""+arrayList.get(position).getImg())
                    .into(holder.cart_p_img);

            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {
                    if (!isLongClick)
                    {
                        Intent intent=new Intent(CheckOut.this,Product_Detail.class);
                        intent.putExtra("prodid",arrayList.get(position).getProduct_id());
                        intent.putExtra("getnote",arrayList.get(position).getNote());

                        startActivity(intent);
                    }
                }
            });


        }

        @Override
        public int getItemCount()
        {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
        {

            ImageView cart_p_img;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);

                cart_p_img = (ImageView) itemView.findViewById(R.id.cart_p_imgid);

                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

            @Override
            public boolean onLongClick(View view)
            {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }
        }

    }

    public void ADDRESS_GETDATA()
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "user_address_process");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            object.put("action", "get_user_address");

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(CheckOut.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("ADDRESS_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("ADDRESS_res", response);

                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONArray jsonArray=jsonObject.getJSONArray("data");

                            Gson gson = new Gson();
                            for (int j = 0; j < jsonArray.length(); j++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                address_modelArrayList.add(gson.fromJson(String.valueOf(jsonObject1), ADDRESS_MODEL.class));
                            }

                            //KNOWING INTENT CODE AND THEN SETEXT...
                            if(PreferenceManager_ASOS.getcamefrom().equals("CART")) //--FROM CART
                            {
                                getAddress_id=(address_modelArrayList.get(0).getAddress_id());
                                addr_person_name.setText(address_modelArrayList.get(0).getName());
                                addr.setText(address_modelArrayList.get(0).getAddress());
                                nearby.setText(address_modelArrayList.get(0).getNear_by());
                                addr_city.setText(address_modelArrayList.get(0).getCity());
                                addr_pincode.setText(address_modelArrayList.get(0).getPincode());
                                addr_state.setText(address_modelArrayList.get(0).getState());
                                addr_country.setText(address_modelArrayList.get(0).getCountry());
                                addr_phno.setText(address_modelArrayList.get(0).getPhone());
                                addr_pincode.setText(address_modelArrayList.get(0).getPincode());

                                Log.e("Pincode cart", address_modelArrayList.get(0).getPincode());

                                PAYBT_ONCLICK(getAddress_id);
                                CHECK_PINCODE(address_modelArrayList.get(0).getPincode());
                            }
                            else if(PreferenceManager_ASOS.getcamefrom().equals("SELECT_ADDR")) //--WHEN ADDR SELECTED
                            {
                                getAddress_id=getIntent().getStringExtra("getAddress_id");
                                getName=getIntent().getStringExtra("getName");
                                getAddress=getIntent().getStringExtra("getAddress");
                                getNear_by=getIntent().getStringExtra("getNear_by");
                                getCity=getIntent().getStringExtra("getCity");
                                getPincode=getIntent().getStringExtra("getPincode");
                                getState=getIntent().getStringExtra("getState");
                                getCountry=getIntent().getStringExtra("getCountry");
                                getPhone=getIntent().getStringExtra("getPhone");

                                addr_person_name.setText(getName);
                                addr.setText(getAddress);
                                nearby.setText(getNear_by);
                                addr_city.setText(getCity);
                                addr_pincode.setText(getPincode);
                                addr_state.setText(getState);
                                addr_country.setText(getCountry);
                                addr_phno.setText(getPhone);

                                Log.e("getName", getName);
                                Log.e("PREF",PreferenceManager_ASOS.getcamefrom());

                                PAYBT_ONCLICK(getAddress_id);
                                CHECK_PINCODE(getPincode);

                            }
                            else if(PreferenceManager_ASOS.getcamefrom().equals("ADD_EDIT_ADDR")) //--FROM ADD OR EDIT ADDR
                            {
                                getAddress_id=getIntent().getStringExtra("getAddress_id").toString();
                                getName=getIntent().getStringExtra("getName").toString();
                                getAddress=getIntent().getStringExtra("getAddress").toString();
                                getNear_by=getIntent().getStringExtra("getNear_by").toString();
                                getCity=getIntent().getStringExtra("getCity").toString();
                                getPincode=getIntent().getStringExtra("getPincode").toString();
                                getState=getIntent().getStringExtra("getState").toString();
                                getCountry=getIntent().getStringExtra("getCountry").toString();
                                getPhone=getIntent().getStringExtra("getPhone").toString();

                                addr_person_name.setText(getName);
                                addr.setText(getAddress);
                                nearby.setText(getNear_by);
                                addr_city.setText(getCity);
                                addr_pincode.setText(getPincode);
                                addr_state.setText(getState);
                                addr_country.setText(getCountry);
                                addr_phno.setText(getPhone);

                                Log.e("Pincode edit/add addr", getPincode);

                                PAYBT_ONCLICK(getAddress_id);
                                CHECK_PINCODE(getPincode);

                            }


                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }


                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("ADDRESS_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void CHECK_PINCODE(String getpincodee)
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "shipping_charge_process");
            object.put("action", "pincodewise");

            JSONObject object1=new JSONObject();
            object.put("options", object1);
            object1.put("pin_code", getpincodee);

            final String requestBody = object.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
//                    progressDialog.dismiss();
                    String getresponce = response.replace("\"", "");


                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("CHECK_PINCODE_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("CHECK_PINCODE_res", response);

                        try
                        {
                            JSONObject mainOBJ=new JSONObject(response);
                            JSONObject dataObj=mainOBJ.getJSONObject("data");
                            String status=dataObj.getString("status");
                            if(status.equals("active"))
                            {
                                JSONObject shipping_detailObj=dataObj.getJSONObject("shipping_details");
                                amount=shipping_detailObj.getString("amount");
                                free_delivery=shipping_detailObj.getString("free_delivery");
                                String pincode=shipping_detailObj.getString("pincode");
                                cod=shipping_detailObj.getString("cod");
                                String notes=shipping_detailObj.getString("notes");

//                                if(sum<=300)
//                                {
//                                    double getsubtotal= sum;
//                                    double add_delivery_charges= getsubtotal+amount;
//                                    tv_delivery_charges.setText(String.valueOf("₹ "+amount));
//                                    grand_total.setText(String.valueOf("₹ "+add_delivery_charges));
//                                }
//                                else
//                                {
//                                    tv_delivery_charges.setText("₹ "+"0.0");
//                                    grand_total.setText("₹ "+String.valueOf(sum));
//                                }

                                check_pincode=true;


                                PreferenceManager_ASOS.setshippingAmt(amount);
                                PreferenceManager_ASOS.setfreedeliveryAmt(free_delivery);

                                CALCULATION();
                            }
                            else if(status.equals("inactive"))
                            {
//                                String message=dataObj.getString("message");
//
//                                tv_delivery_charges.setText("₹ "+"0.0");
//                                grand_total.setText(String.valueOf("₹ "+sum));
//
                                check_pincode=false;
//                                Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);

                                amount= String.valueOf(0.0);
                                free_delivery= String.valueOf(0.0);


                                tv_delivery_charges.setText("₹ "+"0");
                                grand_total.setText("₹ "+String.valueOf(sum));

                                Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);

                                paybt.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
                                    }
                                });

                                PreferenceManager_ASOS.setshippingAmt(amount);
                                PreferenceManager_ASOS.setfreedeliveryAmt(free_delivery);

                                CALCULATION();
                            }

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("CHECK_PINCODE_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication",PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void WALLET_GETDATA()
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "get_wallet_info");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            JSONObject object2=new JSONObject() ;
            object.put("options", object2);
            object2.put("action", "get_info");

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(CheckOut.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("Invalid User ID"))
                    {
                        Log.e("WALLET_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("WALLET_res", response);
                        try
                        {
                            JSONObject jsonObject=new JSONObject(response);
                            JSONObject jsonObject1=jsonObject.getJSONObject("wallet_info");

                            api_wallet_disc=jsonObject1.getString("wallet_limit");
                            api_wallet_amount=jsonObject1.getString("wallet_amount");

                            //CALCULATING WALLET % amt
                            double wallet_discount= Double.parseDouble(api_wallet_disc);//10%
                            double wallet_amount= Double.parseDouble(api_wallet_amount);//1000

                            if(wallet_amount==0)
                            {
                                Log.e("Wallet_Disc_Amt","0");
                            }
                            else
                            {
                                calculate_wallet_amt=wallet_amount*wallet_discount/100;//100
                                PreferenceManager_ASOS.setwallet_disc_amt(String.valueOf(calculate_wallet_amt));
                                WalletAmt= Double.parseDouble(PreferenceManager_ASOS.getwallet_disc_amt());

                                Log.e("Wallet_Disc_Amt",""+WalletAmt);

                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("WALLET_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void PROMOCODE_GETDATA(final String getpromocode)
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "app_promo_code");
            object.put("action", "validate_code");
            JSONObject object2=new JSONObject() ;
            object.put("options", object2);
            object2.put("code", getpromocode);

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(CheckOut.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Please wait");
            progressDialog.setMessage("Applying promo code ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("Invalid Pomo Code"))
                    {
                        Log.e("Pomo_Code_res", "BLANK DATA");

                        Snackbar snackbar = Snackbar.make(parent_lo, "Opps, it seems you had applied invalid Promo Code....!!!", Snackbar.LENGTH_SHORT);
                        View snackBarView = snackbar.getView();
                        snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                        snackbar.show();
                    }
                    else
                    {
                        Log.e("Pomo_Code_res", response);
                        try
                        {
                            JSONObject jsonObject=new JSONObject(response);
                            promocode_id=jsonObject.getString("id");
                            api_promocode_discount=jsonObject.getString("discount");

                            PreferenceManager_ASOS.setpromocodeDisc(api_promocode_discount);
                            promocode_discount= Double.parseDouble(PreferenceManager_ASOS.getpromocodeDisc());

                            calculate_promocode_amt=sum*promocode_discount/100;//10% of sum

                            PreferenceManager_ASOS.setpromocode_disc_amt(String.valueOf(calculate_promocode_amt));
                            Log.e("PromoCode_Disc_Amt",""+calculate_promocode_amt);
                            PromoCodeAmt= Double.parseDouble(PreferenceManager_ASOS.getpromocode_disc_amt());

                            //setting pref
                            PreferenceManager_ASOS.setiF_PROMOCODE(true);
                            PreferenceManager_ASOS.setpromocode(getpromocode);
                            promocode=PreferenceManager_ASOS.getIF_PROMOCOD();

                            Snackbar snackbar = Snackbar.make(parent_lo, getpromocode+" had been applied successfully....!!!", Snackbar.LENGTH_LONG);
                            View snackBarView = snackbar.getView();
                            snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mgreen));
                            snackbar.show();

                            //visible remove promocode bt
                            promocode_delete.setVisibility(View.VISIBLE);
                            promocode_applybt.setVisibility(View.GONE);

                            CALCULATION();

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Pomo_Code_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void CALCULATION()
    {
        if(wallet)
        {
            WalletAmt= Double.parseDouble(PreferenceManager_ASOS.getwallet_disc_amt());
            wallet_deduction_amt.setText("₹ -"+String.valueOf(WalletAmt));

            if(WalletAmt<TotalAmt)
            {
                after_wallet_deduction=TotalAmt-WalletAmt;

                Log.e("after_wallet_deduction",""+after_wallet_deduction);

                //CALCULATE SHIPPING
                if(after_wallet_deduction>Double.valueOf(PreferenceManager_ASOS.getfreedeliveryAmt()))
                {
                    tv_delivery_charges.setText("₹ "+"0");

                    grand_total.setText("₹ "+String.valueOf(after_wallet_deduction));

//                    proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v)
//                        {
//                            if(checkpincode==false)
//                            {
//                                Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                            }
//                            else if(checkpincode)
//                            {
//                                PreferenceManager_ASOS.setcamefrom("");
//                                Intent intent=new Intent(CheckOut.this,Payment_Page.class);
//                                intent.putExtra("addr_id",getAddress_id);
//                                intent.putExtra("sum",String.valueOf(after_wallet_deduction));
//                                startActivity(intent);
//                                finish();
//                            }
//                        }
//                    });


                }
                else
                {
                    tv_delivery_charges.setText("₹ "+Double.valueOf(PreferenceManager_ASOS.getshippingAmt()));

                    final double add_shipping_amount=after_wallet_deduction+Double.valueOf(PreferenceManager_ASOS.getshippingAmt());
                    grand_total.setText("₹ "+String.valueOf(add_shipping_amount));

//                    proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v)
//                        {
//                            if(checkpincode==false)
//                            {
//                                Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                            }
//                            else if(checkpincode)
//                            {
//                                PreferenceManager_ASOS.setcamefrom("");
//                                Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                intent.putExtra("addr_id",getAddress_id);
//                                intent.putExtra("sum",String.valueOf(add_shipping_amount));
//                                startActivity(intent);
//                                finish();
//                            }
//                        }//
//                    });

                }

                if(promocode)
                {
                    calculate_promocode_amt=TotalAmt*Double.valueOf(PreferenceManager_ASOS.getpromocodeDisc())/100;//10% //AGAIN CALCULATING PC FOR CHANGED TotalAmt
                    PreferenceManager_ASOS.setpromocode_disc_amt(String.valueOf(calculate_promocode_amt));

                    PromoCodeAmt= Double.parseDouble(PreferenceManager_ASOS.getpromocode_disc_amt());

                    after_pc_deduction=TotalAmt-PromoCodeAmt-WalletAmt;

                    Log.e("after_pc_deduction",""+after_pc_deduction);

                    //settext
                    pc_deducted_layout.setVisibility(View.VISIBLE);
                    pc_deduction_amt.setText("₹ -"+String.valueOf(PromoCodeAmt));
                    grand_total.setText(String.valueOf(after_pc_deduction));

                    //CALCULATE SHIPPING
                    if(after_pc_deduction>Double.valueOf(PreferenceManager_ASOS.getfreedeliveryAmt()))
                    {
                        tv_delivery_charges.setText("₹ "+"0");

                        grand_total.setText("₹ "+String.valueOf(after_pc_deduction));

//                        proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v)
//                            {
//                                if(checkpincode==false)
//                                {
//                                    Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                                }
//                                else if(checkpincode)
//                                {
//                                    PreferenceManager_ASOS.setcamefrom("");
//                                    Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                    intent.putExtra("addr_id",getAddress_id);
//                                    intent.putExtra("sum",String.valueOf(after_pc_deduction));
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            }
//                        });


                    }
                    else
                    {
                        tv_delivery_charges.setText("₹ "+Double.valueOf(PreferenceManager_ASOS.getshippingAmt()));

                        final double add_shipping_amount=after_pc_deduction+Double.valueOf(PreferenceManager_ASOS.getshippingAmt());
                        grand_total.setText("₹ "+String.valueOf(add_shipping_amount));

//                        proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v)
//                            {
//                                if(checkpincode==false)
//                                {
//                                    Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                                }
//                                else if(checkpincode)
//                                {
//                                    PreferenceManager_ASOS.setcamefrom("");
//                                    Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                    intent.putExtra("addr_id",getAddress_id);
//                                    intent.putExtra("sum",String.valueOf(add_shipping_amount));
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            }
//                        });

                    }

                }
                else if(!promocode)
                {
                    PromoCodeAmt= Double.parseDouble("0");

                    after_pc_deduction=after_wallet_deduction;

                    Log.e("after_pc_deduction",""+after_pc_deduction);

                    //settext
                    pc_deducted_layout.setVisibility(View.GONE);
                    pc_deduction_amt.setText("-");
                    grand_total.setText(String.valueOf(after_pc_deduction));

                    //CALCULATE SHIPPING
                    if(after_pc_deduction>Double.valueOf(PreferenceManager_ASOS.getfreedeliveryAmt()))
                    {
                        tv_delivery_charges.setText("₹ "+"0");

                        grand_total.setText("₹ "+String.valueOf(after_pc_deduction));

//                        proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v)
//                            {
//                                if(checkpincode==false)
//                                {
//                                    Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                                }
//                                else if(checkpincode)
//                                {
//                                    PreferenceManager_ASOS.setcamefrom("");
//                                    Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                    intent.putExtra("addr_id",getAddress_id);
//                                    intent.putExtra("sum",String.valueOf(after_pc_deduction));
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            }
//                        });


                    }
                    else
                    {
                        tv_delivery_charges.setText("₹ "+Double.valueOf(PreferenceManager_ASOS.getshippingAmt()));

                        final double add_shipping_amount=after_pc_deduction+Double.valueOf(PreferenceManager_ASOS.getshippingAmt());
                        grand_total.setText("₹ "+String.valueOf(add_shipping_amount));

//                        proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v)
//                            {
//                                if(checkpincode==false)
//                                {
//                                    Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                                }
//                                else if(checkpincode)
//                                {
//                                    PreferenceManager_ASOS.setcamefrom("");
//                                    Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                    intent.putExtra("addr_id",getAddress_id);
//                                    intent.putExtra("sum",String.valueOf(add_shipping_amount));
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            }
//                        });

                    }
                }

            }
            else
            {
                minusone_wallet=TotalAmt-1;//sum-1
                after_wallet_deduction=TotalAmt-minusone_wallet;//1

                Log.e("after_wallet_deduction",""+after_wallet_deduction);
                wallet_deduction_amt.setText("₹ -"+String.valueOf(after_wallet_deduction));

                if(promocode)
                {
                    calculate_promocode_amt=TotalAmt*Double.valueOf(PreferenceManager_ASOS.getpromocodeDisc())/100;//10% //AGAIN CALCULATING PC FOR CHANGED TotalAmt
                    PreferenceManager_ASOS.setpromocode_disc_amt(String.valueOf(calculate_promocode_amt));

                    PromoCodeAmt= Double.parseDouble(PreferenceManager_ASOS.getpromocode_disc_amt());

                    after_pc_deduction=after_wallet_deduction;

                    Log.e("after_pc_deduction",""+after_pc_deduction);

                    //settext
                    pc_deducted_layout.setVisibility(View.VISIBLE);
                    pc_deduction_amt.setText("₹ -"+String.valueOf(PromoCodeAmt));
                    grand_total.setText(String.valueOf(after_pc_deduction));

                    //CALCULATE SHIPPING
                    if(after_pc_deduction>Double.valueOf(PreferenceManager_ASOS.getfreedeliveryAmt()))
                    {
                        tv_delivery_charges.setText("₹ "+"0");

                        grand_total.setText("₹ "+String.valueOf(after_pc_deduction));

//                        proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v)
//                            {
//                                if(checkpincode==false)
//                                {
//                                    Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                                }
//                                else if(checkpincode)
//                                {
//                                    PreferenceManager_ASOS.setcamefrom("");
//                                    Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                    intent.putExtra("addr_id",getAddress_id);
//                                    intent.putExtra("sum",String.valueOf(after_pc_deduction));
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            }
//                        });


                    }
                    else
                    {
                        tv_delivery_charges.setText("₹ "+Double.valueOf(PreferenceManager_ASOS.getshippingAmt()));

                        final double add_shipping_amount=after_pc_deduction+Double.valueOf(PreferenceManager_ASOS.getshippingAmt());
                        grand_total.setText("₹ "+String.valueOf(add_shipping_amount));

//                        proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v)
//                            {
//                                if(checkpincode==false)
//                                {
//                                    Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                                }
//                                else if(checkpincode)
//                                {
//                                    PreferenceManager_ASOS.setcamefrom("");
//                                    Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                    intent.putExtra("addr_id",getAddress_id);
//                                    intent.putExtra("sum",String.valueOf(add_shipping_amount));
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            }
//                        });

                    }
                }
                else if(!promocode)
                {
                    PromoCodeAmt= Double.parseDouble("0");

                    after_pc_deduction=after_wallet_deduction;

                    Log.e("after_pc_deduction",""+after_pc_deduction);

                    //settext
                    pc_deducted_layout.setVisibility(View.GONE);
                    pc_deduction_amt.setText("-");
                    grand_total.setText(String.valueOf(after_pc_deduction));

                    //CALCULATE SHIPPING
                    if(after_pc_deduction>Double.valueOf(PreferenceManager_ASOS.getfreedeliveryAmt()))
                    {
                        tv_delivery_charges.setText("₹ "+"0");

                        grand_total.setText("₹ "+String.valueOf(after_pc_deduction));

//                        proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v)
//                            {
//                                if(checkpincode==false)
//                                {
//                                    Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                                }
//                                else if(checkpincode)
//                                {
//                                    PreferenceManager_ASOS.setcamefrom("");
//                                    Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                    intent.putExtra("addr_id",getAddress_id);
//                                    intent.putExtra("sum",String.valueOf(after_pc_deduction));
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            }
//                        });


                    }
                    else
                    {
                        tv_delivery_charges.setText("₹ "+Double.valueOf(PreferenceManager_ASOS.getshippingAmt()));

                        final double add_shipping_amount=after_pc_deduction+Double.valueOf(PreferenceManager_ASOS.getshippingAmt());
                        grand_total.setText("₹ "+String.valueOf(add_shipping_amount));

//                        proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v)
//                            {
//                                if(checkpincode==false)
//                                {
//                                    Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                                }
//                                else if(checkpincode)
//                                {
//                                    PreferenceManager_ASOS.setcamefrom("");
//                                    Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                    intent.putExtra("addr_id",getAddress_id);
//                                    intent.putExtra("sum",String.valueOf(add_shipping_amount));
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            }
//                        });

                    }
                }
            }
        }
        else if(!wallet)
        {
            wallet_deduction_amt.setText(String.valueOf("0"));

            if(promocode)
            {
                calculate_promocode_amt=TotalAmt*Double.valueOf(PreferenceManager_ASOS.getpromocodeDisc())/100;//10% //AGAIN CALCULATING PC FOR CHANGED TotalAmt
                PreferenceManager_ASOS.setpromocode_disc_amt(String.valueOf(calculate_promocode_amt));

                PromoCodeAmt= Double.parseDouble(PreferenceManager_ASOS.getpromocode_disc_amt());

                after_pc_deduction=TotalAmt-PromoCodeAmt;

                Log.e("after_pc_deduction",""+after_pc_deduction);

                //settext
                pc_deducted_layout.setVisibility(View.VISIBLE);
                pc_deduction_amt.setText("₹ -"+String.valueOf(PromoCodeAmt));
                grand_total.setText(String.valueOf(after_pc_deduction));

                //CALCULATE SHIPPING
                if(after_pc_deduction>Double.valueOf(PreferenceManager_ASOS.getfreedeliveryAmt()))
                {
                    tv_delivery_charges.setText("₹ "+"0");

                    grand_total.setText("₹ "+String.valueOf(after_pc_deduction));

//                    proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v)
//                        {
//                            if(checkpincode==false)
//                            {
//                                Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                            }
//                            else if(checkpincode)
//                            {
//                                PreferenceManager_ASOS.setcamefrom("");
//                                Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                intent.putExtra("addr_id",getAddress_id);
//                                intent.putExtra("sum",String.valueOf(after_pc_deduction));
//                                startActivity(intent);
//                                finish();
//                            }
//                        }
//                    });


                }
                else
                {
                    tv_delivery_charges.setText("₹ "+Double.valueOf(PreferenceManager_ASOS.getshippingAmt()));

                    final double add_shipping_amount=after_pc_deduction+Double.valueOf(PreferenceManager_ASOS.getshippingAmt());
                    grand_total.setText("₹ "+String.valueOf(add_shipping_amount));

//                    proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v)
//                        {
//                            if(checkpincode==false)
//                            {
//                                Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                            }
//                            else if(checkpincode)
//                            {
//                                PreferenceManager_ASOS.setcamefrom("");
//                                Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                intent.putExtra("addr_id",getAddress_id);
//                                intent.putExtra("sum",String.valueOf(add_shipping_amount));
//                                startActivity(intent);
//                                finish();
//                            }
//                        }//
//                    });

                }
            }
            else if(!promocode)
            {
                PromoCodeAmt= Double.parseDouble("0");

                after_pc_deduction=TotalAmt;

                Log.e("after_pc_deduction",""+after_pc_deduction);

                //settext
                pc_deducted_layout.setVisibility(View.GONE);
                pc_deduction_amt.setText("-");
                grand_total.setText(String.valueOf(after_pc_deduction));

                //CALCULATE SHIPPING
                if(after_pc_deduction>Double.valueOf(PreferenceManager_ASOS.getfreedeliveryAmt()))
                {
                    tv_delivery_charges.setText("₹ "+"0");

                    grand_total.setText("₹ "+String.valueOf(after_pc_deduction));

//                    proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v)
//                        {
//                            if(checkpincode==false)
//                            {
//                                Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                            }
//                            else if(checkpincode)
//                            {
//                                PreferenceManager_ASOS.setcamefrom("");
//                                Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                intent.putExtra("addr_id",getAddress_id);
//                                intent.putExtra("sum",String.valueOf(after_pc_deduction));
//                                startActivity(intent);
//                                finish();
//                            }
//                        }
//                    });


                }
                else
                {
                    tv_delivery_charges.setText("₹ "+Double.valueOf(PreferenceManager_ASOS.getshippingAmt()));

                    final double add_shipping_amount=after_pc_deduction+Double.valueOf(PreferenceManager_ASOS.getshippingAmt());
                    grand_total.setText("₹ "+String.valueOf(add_shipping_amount));

//                    proceedpaymentbt.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v)
//                        {
//                            if(checkpincode==false)
//                            {
//                                Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
//                            }
//                            else if(checkpincode)
//                            {
//                                PreferenceManager_ASOS.setcamefrom("");
//                                Intent intent=new Intent(Delivery_detail.this,Payment_Page.class);
//                                intent.putExtra("addr_id",getAddress_id);
//                                intent.putExtra("sum",String.valueOf(add_shipping_amount));
//                                startActivity(intent);
//                                finish();
//                            }
//                        }
//                    });//

                }
            }

        }

        CHECKING_WALLET_PREF();
        CHECKING_PROMOCODE_PREF();
    }

    public void CHECKING_WALLET_PREF()
    {
        if(wallet)
        {
            walletswitch.setChecked(true);
        }
        else
        {
            walletswitch.setChecked(false);
        }
    }

    public void CHECKING_PROMOCODE_PREF()
    {
        promocode=PreferenceManager_ASOS.getIF_PROMOCOD();

        if(promocode)
        {
            promocode_delete.setVisibility(View.VISIBLE);
            promocode_applybt.setVisibility(View.GONE);
            ed_promocode.setText(PreferenceManager_ASOS.getpromocode());
        }
        else
        {
            promocode_delete.setVisibility(View.GONE);
            promocode_applybt.setVisibility(View.VISIBLE);
            PreferenceManager_ASOS.setpromocode(PreferenceManager_ASOS.getpromocode());
        }

    }

    public void PAYBT_ONCLICK(final String getAddress_id)
    {
        paybt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(cod_switch.isChecked()==true || netbanking_switch.isChecked()==true)
                {

                    if(check_pincode)
                    {
                        PLACE_ORDER(getAddress_id);
                    }
                    else
                    {
                        Error_Dialog("Sorry","Delivery is not available on this pincode",R.drawable.ic_sad);
                    }
                }
                else
                {
                    Error_Dialog("Cannot proceed","Please select the payment option",R.drawable.error);
                }

            }
        });

    }

    public void PLACE_ORDER(final String getAddress_id)
    {
        ArrayList<mycart> arrayList = new ArrayList<mycart>();

        Main_impl main_impl=new Main_impl(getApplicationContext());
        arrayList=main_impl.getUser();

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            JSONObject jobj = new JSONObject();
            jobj.put("payment_type", "Cash_On_Delivery");
            jobj.put("transaction_id", "");
            jobj.put("wallet_status", String.valueOf(PreferenceManager_ASOS.getiF_WALLET()));
            if(ed_promocode.getText().toString().length()!=0)
            {
                jobj.put("promo_code", ed_promocode.getText().toString());
            }

            JSONArray jsonArray = new JSONArray();

            for (int m = 0; m < arrayList.size(); m++)
            {
                jsonArray.put(arrayList.get(m).getJSONObject());
            }
            try
            {
                object.put("type", "shopping_cart_process");
                object.put("user_id", PreferenceManager_ASOS.GetUserID());
                object.put("address_id", getAddress_id);
                object.put("cart_infos", jsonArray);
                object.put("payment_infos", jobj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            final String requestBody = object.toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {


                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("PAYMENT_PAGE_RES", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("PAYMENT_PAGE_RES", response);

                        try
                        {
                            JSONObject mainOBJ=new JSONObject(response);
                            JSONObject jsonObject=mainOBJ.getJSONObject("data");
                            String status=jsonObject.getString("status");
                            if(status.equals("Success"))
                            {

                                Main_impl main_impl1=new Main_impl(getApplicationContext());
                                main_impl1.delete_table();


                                Simple_Dialog("Congratulation","Your order has been confirmed successfully",R.drawable.ic_tick);

                                CLEAR_PREF();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),"Order Fail",Toast.LENGTH_SHORT).show();
                            }
//                    Log.e("jsonObject", String.valueOf(jsonObject));
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("PAYMENT_PAGE_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    public void CLEAR_PREF()
    {
        PreferenceManager_ASOS.setiF_WALLET(false);
        PreferenceManager_ASOS.setiF_PROMOCODE(false);
        PreferenceManager_ASOS.setshopping_amt("");
        PreferenceManager_ASOS.setwallet_disc_amt("");
        PreferenceManager_ASOS.setpromocode_disc_amt("");
        PreferenceManager_ASOS.setpromocode("");
        PreferenceManager_ASOS.setpromocodeDisc("");
        PreferenceManager_ASOS.setshippingAmt("");
        PreferenceManager_ASOS.setfreedeliveryAmt("");
    }

    private void Error_Dialog(String title, String message, int icon)
    {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

        // Initialize a new spannable string builder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(title);

        // Apply the text color span
        ssBuilder.setSpan(
                foregroundColorSpan,
                0,
                title.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        dialogBuilder.setTitle(ssBuilder);
        dialogBuilder.setMessage(message);
        dialogBuilder.setIcon(icon);


        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
//        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
//        {
//            @Override
//            public void onClick(DialogInterface dialog, int which)
//            {
//                dialog.dismiss();
//            }
//        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblue));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.mblue));
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.white));

    }

    private void Simple_Dialog(String title, String message, int icon)
    {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

        // Initialize a new spannable string builder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(title);

        // Apply the text color span
        ssBuilder.setSpan(
                foregroundColorSpan,
                0,
                title.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        dialogBuilder.setTitle(ssBuilder);
        dialogBuilder.setMessage(message);
        dialogBuilder.setIcon(icon);
        dialogBuilder.setCancelable(false);


        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();

                Intent intent=new Intent(getApplicationContext(),Home.class);
                startActivity(intent);
                finish();
            }
        });
//        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
//        {
//            @Override
//            public void onClick(DialogInterface dialog, int which)
//            {
//                dialog.dismiss();
//            }
//        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblue));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.mblue));
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.white));

    }

    private void Exit_Dialog(String title, String message, int icon)
    {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);

        // Initialize a new spannable string builder instance
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(title);

        // Apply the text color span
        ssBuilder.setSpan(
                foregroundColorSpan,
                0,
                title.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        dialogBuilder.setTitle(ssBuilder);
        dialogBuilder.setMessage(message);
        dialogBuilder.setIcon(icon);


        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();

                Intent intent=new Intent(getApplicationContext(),Home.class);
                startActivity(intent);
                finish();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mblue));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.white));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.mblue));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.white));

    }

    @Override
    public void onBackPressed()
    {

        Exit_Dialog("Exit checkout...?","Are you sure you want to exit checkout ?",R.drawable.appnewlogo);

    }
    @Override
    protected void onResume()
    {
        super.onResume();

        //Calling Checking Internet connection function

    }

}
