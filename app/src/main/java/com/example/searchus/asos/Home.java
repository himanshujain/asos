package com.example.searchus.asos;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.BlurBuilder;
import com.example.searchus.asos.Classes.RecyclerItemClickListener;
import com.example.searchus.asos.Impl.Main_impl;
import com.example.searchus.asos.MODELS.mycart;
import com.example.searchus.asos.NAV_FRAG.HomeFragment;
import com.example.searchus.asos.PREFERENCE_MANAGER.PreferenceManager_ASOS;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import studio.carbonylgroup.textfieldboxes.ExtendedEditText;
import studio.carbonylgroup.textfieldboxes.TextFieldBoxes;
import uk.me.hardill.volley.multipart.MultipartRequest;


public class Home extends AppCompatActivity
{
    DrawerLayout myDrawerLayout;
    NavigationView myNavigationView;
    FragmentManager myFragmentManager;
    FragmentTransaction myFragmentTransaction;

    RecyclerView Nav_RecyclerView;
    RecyclerView.Adapter nav_Adapter;
    ArrayList<String> Nav_ItemName;
    ArrayList<Integer> Nav_Img;

    FloatingActionButton floatingActionButton;
    LinearLayout username_lo,signin_lo,signout_lo;
    FrameLayout saveditem_fo,searchclick,wallet;
    TextView tv_username,tv_bagcount,tvrateus;
    TextView tvfeedback,tx_about;
    ImageView iv;
    CoordinatorLayout parentlo;

    PreferenceManager_ASOS PreferenceManager_ASOS;

    String check_user;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    final int CAMERA_REQUEST = 1888;
    private static int RESULT_LOAD_IMAGE = 1;
    final int MY_CAMERA_PERMISSION_CODE = 100;
    Uri CameraUri;
    String picturePath="";
    String msg="";
    String finalpath="";
    Bitmap bitmap;
    String json;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        System.gc();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_home);
        PreferenceManager_ASOS=new PreferenceManager_ASOS(getApplicationContext());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        check_user=PreferenceManager_ASOS.GetUserID();
        Log.e("USERID",check_user);



        myDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        myNavigationView = (NavigationView) findViewById(R.id.nav_drawer);
        parentlo=findViewById(R.id.parentloid);
        username_lo=findViewById(R.id.username_lo_id);
        tv_username=findViewById(R.id.tv_username_id);
        signin_lo=findViewById(R.id.signin_lo_id);
        signout_lo=findViewById(R.id.signout_lo_id);
        searchclick=findViewById(R.id.searchclickid);
        wallet=findViewById(R.id.walletid);
        saveditem_fo=findViewById(R.id.saveditem_fo_id);
        floatingActionButton=findViewById(R.id.floatingActionButtonid);
        tv_bagcount=findViewById(R.id.tv_bagcount_id);
        tvfeedback=findViewById(R.id.tvfeedbackid);
        tvrateus=findViewById(R.id.tvrateusid);
        tx_about=findViewById(R.id.tx_about);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, myDrawerLayout, toolbar,R.string.app_name,
                R.string.app_name);

        myDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();

        Nav_ItemName = new ArrayList<>(Arrays.asList("HOME","MY BAG","SAVED ITEMS","MY ACCOUNT","SHARE & EARN"));
        Nav_Img = new ArrayList<>(Arrays.asList(R.drawable.home,R.drawable.shopping_bag, R.drawable.filllike,R.drawable.myaccount,R.drawable.ic_shareandearn));

        Nav_RecyclerView = (RecyclerView) findViewById(R.id.nav_recycler_view);

        LinearLayoutManager trending_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        trending_LayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        Nav_RecyclerView.setLayoutManager(trending_LayoutManager);
        Nav_RecyclerView.setNestedScrollingEnabled(false);
        nav_Adapter = new NAV_ADP(getApplicationContext(),Nav_Img,Nav_ItemName);
        Nav_RecyclerView.setAdapter(nav_Adapter);



        HomeFragment dashboardFragment = new HomeFragment();
        myFragmentTransaction = getSupportFragmentManager().beginTransaction();
        myFragmentTransaction.add(R.id.containerView, dashboardFragment);
        myFragmentTransaction.commitAllowingStateLoss();

        ImageView imageView=findViewById(R.id.imageView);
        Bitmap resultBmp = BlurBuilder.blur(this, BitmapFactory.decodeResource(getResources(), R.drawable.cover_img));
        imageView.setImageBitmap(resultBmp);
        SHOW_HIDE();
        ONCLICKS_CHK_USERID();
        myNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem selectedMenuItem) {
                myDrawerLayout.closeDrawers();


                if (selectedMenuItem.getItemId() == R.id.nav_item_home)
                {
                    FragmentTransaction fragmentTransaction = myFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerView, new HomeFragment()).commit();
                }

                return false;
            }

        });

        /**
         * Setup Drawer Toggle of the Toolbar
         */

        Nav_RecyclerView.addOnItemTouchListener
                (
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {

                        if(position==0)
                        {
                            if (myDrawerLayout.isDrawerOpen(GravityCompat.START))
                            {
                                myDrawerLayout.closeDrawer(GravityCompat.START);
                            }
                        }
                        else if(position==1)
                        {
                                Intent intent=new Intent(Home.this,Bag.class);
                                startActivity(intent);
                        }
                        else if(position==2)
                        {

                            String check_user=PreferenceManager_ASOS.GetUserID();
                            if(check_user.equals(""))
                            {
                                Intent intent=new Intent(Home.this,Enter_OTP.class);
//                                PreferenceManager_ASOS.setcamefrom("Home");
//                                intent.putExtra("from_intent","Home");
                                startActivity(intent);
//                                finish();
                            }
                            else
                            {
                                Intent intent=new Intent(Home.this,Saved_Item.class);
                                startActivity(intent);
                            }
                        }
                        else if(position==3)
                        {
                            String check_user=PreferenceManager_ASOS.GetUserID();
                            if(check_user.equals(""))
                            {
                                Intent intent=new Intent(Home.this,Enter_OTP.class);
//                                PreferenceManager_ASOS.setcamefrom("Home");
//                                intent.putExtra("from_intent","Home");
                                startActivity(intent);
//                                finish();
                            }
                            else
                            {
                                Intent intent=new Intent(Home.this,My_Account.class);
                                startActivity(intent);
//                                finish();
                            }

                        }
                        else if(position==4)
                        {
                            String check_user=PreferenceManager_ASOS.GetUserID();
                            if(check_user.equals(""))
                            {
                                Intent intent=new Intent(Home.this,Enter_OTP.class);
                                startActivity(intent);
//                                finish();
                            }
                            else
                            {
                                Share_Earn_BottomSheetDialog();
                            }
                        }

                        else
                        CLOSE_NAV_DRAWER();
                    }
                })
        );

        searchclick.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(Home.this,Search_Keyword_Page.class);
                startActivity(intent);
            }
        });

        tvfeedback.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String check_user=PreferenceManager_ASOS.GetUserID();
                if(check_user.equals(""))
                {
                    Intent intent=new Intent(Home.this,Enter_OTP.class);
                    startActivity(intent);
//                                finish();
                }
                else
                {
                    Feedback_BottomSheetDialog();
                }
            }
        });

        tx_about.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Home.this,AboutUs.class));
            }
        });

        tvrateus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.searchus.Ira_The_Fashion_Store_SA5b4f3e8713080B")));
            }
        });


        SIGNOUT();

    }

    public class NAV_ADP extends RecyclerView.Adapter<NAV_ADP.ViewHolder>
    {
        ArrayList<Integer> alImage;
        ArrayList<String> prodname;
        Context context;

        public NAV_ADP(Context context, ArrayList<Integer> alImage, ArrayList<String> prodname)
        {
            super();
            this.context = context;
            this.alImage = alImage;
            this.prodname = prodname;
        }
        @Override
        public NAV_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_nav_lvitems, parent, false);
            NAV_ADP.ViewHolder viewHolder = new NAV_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(NAV_ADP.ViewHolder holder, final int position)
        {
            holder.imgThumbnail.setImageResource(alImage.get(position));
            holder.tvmenuname.setText(prodname.get(position));
        }

        @Override
        public int getItemCount()
        {
            return alImage.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder
        {

            public ImageView imgThumbnail;
            public TextView tvmenuname;
            View viewbg;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                tvmenuname = (TextView) itemView.findViewById(R.id.tvmenunameid);
                viewbg=itemView.findViewById(R.id.viewid);
            }


        }

    }

    public void SHOW_HIDE()
    {
        check_user=PreferenceManager_ASOS.GetUserID();

        if(check_user.equals(""))
        {
            tv_username.setText("User");

            username_lo.setVisibility(View.GONE);//Hello User
            signin_lo.setVisibility(View.VISIBLE);//Sign In>
            signout_lo.setVisibility(View.GONE);//Sign out
            wallet.setVisibility(View.GONE);//Sign out


        }
        else
        {
            tv_username.setText(PreferenceManager_ASOS.GetUser_NAME());

            username_lo.setVisibility(View.VISIBLE);
            signin_lo.setVisibility(View.GONE);
            signout_lo.setVisibility(View.VISIBLE);
            wallet.setVisibility(View.VISIBLE);
        }
    }

    public void ONCLICKS_CHK_USERID()
    {
        saveditem_fo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String check_user=PreferenceManager_ASOS.GetUserID();
                if(check_user.equals(""))
                {
                    Intent intent=new Intent(Home.this,Enter_OTP.class);
//                    PreferenceManager_ASOS.setcamefrom("Home");
//                    intent.putExtra("from_intent","Home");
                    startActivity(intent);
//                    finish();
                }
                else
                {
                    Intent intent=new Intent(Home.this,Saved_Item.class);
                    startActivity(intent);
                }
            }
        });
//signout
        signin_lo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent intent=new Intent(Home.this,Enter_OTP.class);
//                PreferenceManager_ASOS.setcamefrom("Home");
//                intent.putExtra("from_intent","Home");
                startActivity(intent);
//                finish();
            }
        });
//floating
        floatingActionButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                    Intent intent=new Intent(getApplicationContext(),Bag.class);
                    startActivity(intent);
            }
        });

        wallet.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                WALLET_GETDATA();

            }
        });


    }

    //signout
    public void SIGNOUT()
    {
        signout_lo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final ProgressDialog progress = new ProgressDialog(Home.this);
                progress.setTitle("Signing out");
                progress.setMessage("Please wait...");
                progress.show();

                Thread timer = new Thread()
                {
                    public void run()
                    {
                        try
                        {
                            sleep(1000);
                            progress.cancel();
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                        finally
                        {
                            PreferenceManager_ASOS.SetUserID("");
                            Intent i = new Intent(Home.this, Splash_Screen.class);
//                            i.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                            startActivity(i);
                            finish();
                        }

                    }

                };
                timer.start();
            }
        });

    }

    public void CLOSE_NAV_DRAWER()
    {
            myDrawerLayout.closeDrawer(GravityCompat.START);
    }


    public void WALLET_GETDATA()
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "get_wallet_info");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            JSONObject object2=new JSONObject() ;
            object.put("options", object2);
            object2.put("action", "get_info");

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(Home.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("Invalid User ID"))
                    {
                        Log.e("WALLET_res", "BLANK DATA");

                        Toast.makeText(getApplicationContext(),"Unable to load wallet details, please try again later",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Log.e("WALLET_res", response);
                        try
                        {
                            JSONObject jsonObject=new JSONObject(response);
                            JSONObject jsonObject1=jsonObject.getJSONObject("wallet_info");

                            String api_wallet_disc = jsonObject1.getString("wallet_limit");
                            String api_wallet_amount=jsonObject1.getString("wallet_amount");

//                            PreferenceManager_ASOS.setWalletAmt(api_wallet_amount);
                            Wallet_BottomSheetDialog(api_wallet_amount);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("WALLET_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    private void Wallet_BottomSheetDialog(String walletamt)
    {
        final Dialog dialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.asos_wallet_dialogue, null);

        LinearLayout walletamtlo=view.findViewById(R.id.walletamtloid);
        LinearLayout emptywalletlo=view.findViewById(R.id.emptywalletloid);
        TextView tvwalletamt=view.findViewById(R.id.tvwalletamtid);

        double walletamt_double=Double.parseDouble(walletamt);
        if(walletamt_double<1)//if wallet is empty
        {
            emptywalletlo.setVisibility(View.VISIBLE);
            walletamtlo.setVisibility(View.GONE);
        }
        else
        {
            emptywalletlo.setVisibility(View.GONE);
            walletamtlo.setVisibility(View.VISIBLE);
            tvwalletamt.setText(String.valueOf("₹ "+walletamt_double));
        }

        dialog.setCancelable(true);
        dialog.setContentView(view);
        dialog.show();
    }

    private void Share_Earn_BottomSheetDialog()
    {
        final Dialog dialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.asos_share_earn_dialogue, null);

        final ImageButton shareearnbt=view.findViewById(R.id.shareearnbtid);
        shareearnbt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SHAREEARN();
            }
        });

        dialog.setCancelable(true);
        dialog.setContentView(view);
        dialog.show();
    }

    public void SHAREEARN()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
            else
            {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out the app at:\n"+""+"https://searchus.in/share_and_earn.php?id="+""+PreferenceManager_ASOS.GetUserID());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        }

    }

    public boolean checkPermissionREAD_EXTERNAL_STORAGE
            (
                    final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)

            {
                Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_SHORT).show();

                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE))
                {
                    showDialog("External storage required", context,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                    Toast.makeText(getApplicationContext(),"Done1",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    Toast.makeText(getApplicationContext(),"Done2",Toast.LENGTH_SHORT).show();
                }
                return false;
            }
            else
            {
                return true;
            }

        }
        else
        {
            return true;
        }
    }

    public void showDialog(final String msg, final Context context,
                           final String permission)
    {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[] { permission },
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String[] permissions, int[] grantResults) {
//        switch (requestCode)
//        {
//            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
//                {
//                    // do your stuff
//                }
//                else
//                {
//                    Toast.makeText(Home.this, "Permission required",
//                            Toast.LENGTH_SHORT).show();
//                }
//                break;
//            default:
//                super.onRequestPermissionsResult(requestCode, permissions,
//                        grantResults);
//        }
//    }


    private void Img_Option_Dialog()
    {
        final Dialog dialog = new Dialog(this);
        View view = getLayoutInflater().inflate(R.layout.asos_imageoption_dialogue, null);

        final ImageView ivcamera=view.findViewById(R.id.ivcameraid);
        final ImageView ivgallery=view.findViewById(R.id.ivgalleryid);


        ivcamera.setOnClickListener(new View.OnClickListener() //FROM CAMERA
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_CAMERA_PERMISSION_CODE);
                    }
                    else
                    {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        CameraUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "abc" +
                                String.valueOf(System.currentTimeMillis()) + ".jpg"));

//                        finalpath= String.valueOf(CameraUri);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, CameraUri);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                }
            }
        });

        ivgallery.setOnClickListener(new View.OnClickListener() //FROM GALLERY
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                RESULT_LOAD_IMAGE);
                    }
                    else
                    {
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(i, RESULT_LOAD_IMAGE);
                    }
                }

            }
        });


        dialog.setCancelable(true);
        dialog.setContentView(view);
        dialog.show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) //CAMERA
            {

                picturePath=CameraUri.getPath().toString();
                Log.e("abc",picturePath);
                iv.setImageURI(Uri.parse(picturePath));
//                bitmap = (Bitmap) data.getExtras().get("data");
                bitmap=BitmapFactory.decodeFile(picturePath);
//                imgtoStr(bitmap);
                iv.setImageBitmap(bitmap);
            }
            else  if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) //GALLERY
            {
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                Log.e("abc",picturePath);
                cursor.close();

                bitmap=BitmapFactory.decodeFile(picturePath);
//                imgtoStr(bitmap);
                iv.setImageBitmap(bitmap);
            }

        }

//    private String imgtoStr(Bitmap bitmap)
//    {
//        ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
//        byte[] imagebyte=outputStream.toByteArray();
//        String encodeimg=Base64.encodeToString(imagebyte,Base64.DEFAULT);
//        Log.e("encodeimg",encodeimg);
//        return encodeimg;
//    }

//    @SuppressWarnings("deprecation")
//    private String uploadFile()
//    {
//        String responseString = null;
//        String ServerUrl="https://searchus.in/image_upload_api.php";
//
//        HttpClient httpclient = new DefaultHttpClient();
//        HttpPost httppost = new HttpPost(ServerUrl);
//
//        try {
//            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
//                    new AndroidMultiPartEntity.ProgressListener() {
//
//                        @Override
//                        public void transferred(long num) {
////                            publishProgress((int) ((num / (float) totalSize) * 100));
//                        }
//                    });
//
//                String uploadfileimg = picturePath;
//                File sourceFile = new File(uploadfileimg);
//                entity.addPart("filename", new FileBody(sourceFile));
//                entity.addPart("auth_key", new StringBody("1e04c781b3875c38ed853407a6d345e59b4d2f76bbb68e9528a55674225b6f1d"));
//                entity.addPart("type", new StringBody("app_feedback_image"));
//                entity.addPart("user_type", new StringBody("ecommerce"));
//
//                httppost.setEntity(entity);
//
//            // Making server call
//            HttpResponse response = httpclient.execute(httppost);
//            HttpEntity r_entity = response.getEntity();
//
//            int statusCode = response.getStatusLine().getStatusCode();
//            if (statusCode == 200)
//            {
//                // Server response
//                responseString = EntityUtils.toString(r_entity);
//
//                if (responseString != null)
//                {
//                    try
//                    {
//                        JSONObject jbj = new JSONObject(responseString);
//                        finalpath = jbj.getString("path");
//                    }
//                    catch (JSONException e)
//                    {
//                        e.printStackTrace();
//                    }
//                }
//
//                Log.d("imgpath", responseString + "");
//
//
//            }
//            else
//            {
//                responseString = "Error occurred! Http Status Code: "
//                        + statusCode;
//            }
//
//        }
//        catch (ClientProtocolException e)
//        {
//            responseString = e.toString();
//        }
//        catch (IOException e)
//        {
//            responseString = e.toString();
//        }
//
//        return responseString;
//
//    }

    // convert from bitmap to byte array
    public byte[] getBytesFromBitmap(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);
        return stream.toByteArray();
    }


    public String ABC()
    {
        String ServerUrl="https://searchus.in/image_upload_api.php";

        final ProgressDialog progressDialog=new ProgressDialog(Home.this,R.style.ProgressDialogStyle);
        progressDialog.setTitle("Sending feedback");
        progressDialog.setMessage("Please wait ...");
        progressDialog.show();

        MultipartRequest request = new MultipartRequest(ServerUrl, null,
                new Response.Listener<NetworkResponse>()
                {
                    @Override
                    public void onResponse(NetworkResponse response)
                    {
                        progressDialog.dismiss();

                        try
                        {
                           json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                           Log.e("NetworkResponse",""+json);

                            try
                            {
                                JSONObject jsonObject=new JSONObject(String.valueOf(json));
                                Log.e("jsonObject",""+jsonObject);
                                String status=jsonObject.getString("status");
                                finalpath=jsonObject.getString("path");
                                if(status.equals("success"))
                                {
                                    FEEDBACK_GETDATA(finalpath);


                                }
                                else
                                {
                                    final Snackbar snackbar = Snackbar.make(parentlo, "Unable to sent feedback, try again later....!!!", Snackbar.LENGTH_SHORT);
                                    View snackBarView = snackbar.getView();
                                    snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                                    snackbar.show();
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }


                        } catch (UnsupportedEncodingException e)
                        {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {

                    }
                });


        request.addPart(new MultipartRequest.FilePart("filename",null,picturePath,getBytesFromBitmap(bitmap)));
        request.addPart(new MultipartRequest.FormPart("auth_key","1e04c781b3875c38ed853407a6d345e59b4d2f76bbb68e9528a55674225b6f1d"));
        request.addPart(new MultipartRequest.FormPart("type","app_feedback_image"));
        request.addPart(new MultipartRequest.FormPart("user_type","ecommerce"));

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
        return json;
    }


    public void FEEDBACK_GETDATA(String finalpath)
    {
        try
        {
//            Thread thread = new Thread(new Runnable()
//            {
//                public void run()
//                {
//                    try
//                    {
//                        if(!picturePath.equals(""))
////                        {
////                            ABC();
////                        }
////                        else
////                        {
////                            Toast.makeText(getApplicationContext(),"Unable to upload image",Toast.LENGTH_SHORT).show();
////                        }
////                    }
//                    catch (Exception e)
//                    {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//            thread.start();
            RequestQueue requestQueue = Volley.newRequestQueue(this);

//            JSONObject object=new JSONObject() ;
//            JSONObject object1=new JSONObject() ;
//            object.put("type", "user_feedback_process");
//            object.put("action", "get_user_address");
//            object.put("user_id", PreferenceManager_ASOS.GetUserID());
//            object.put("options", object1);
//            object1.put("message", msg);
//            object1.put("image_path", finalpath);

            JSONObject object=new JSONObject() ;
            JSONObject object1=new JSONObject() ;
            object.put("type", "user_feedback_process");
            object.put("action", "set_feedback");
            object.put("user_id", PreferenceManager_ASOS.GetUserID());
            object.put("options", object1);
            object1.put("message", msg);
            object1.put("image_path", finalpath);

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(Home.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Sending feedback");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();
                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("Invalid Data Found"))
                    {
                        Log.e("FEEDBACK_res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("FEEDBACK_res", response);

                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            String status=jsonObject.getString("status");
                            if(status.equals("Done"))
                            {
                                final Snackbar snackbar = Snackbar.make(parentlo, "Feedback has been sent successfully....!!!", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mblue));
                                snackbar.show();
                            }
                            else
                            {
                                final Snackbar snackbar = Snackbar.make(parentlo, "Unable to sent feedback, try again later....!!!", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.mred));
                                snackbar.show();
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }


                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("ADDRESS_error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    private void Feedback_BottomSheetDialog()
    {
        final Dialog dialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.asos_feedback_dialogue, null);

        iv=view.findViewById(R.id.ivid);
        final TextFieldBoxes tffeedback=view.findViewById(R.id.tffeedbackid);
        final ExtendedEditText edfeedback=view.findViewById(R.id.edfeedbackid);
        Button feedbackbt=view.findViewById(R.id.feedbackbtid);
        feedbackbt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(edfeedback.length()==0)
                {
                    Toast.makeText(getApplicationContext(), "Please add your feedback", Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (myDrawerLayout.isDrawerOpen(GravityCompat.START))
                    {
                        myDrawerLayout.closeDrawer(GravityCompat.START);
                    }

                    msg=edfeedback.getText().toString();
//                    Toast.makeText(getApplicationContext(), ""+edfeedback.getText().toString(), Toast.LENGTH_LONG).show();
                    dialog.dismiss();

//                    FEEDBACK_GETDATA();

                    if(finalpath.equals("") || finalpath.equals(null))
                    {
                        ABC();
                    }
                    else
                    {
                        ABC();
                    }

                }
            }
        });
        iv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Img_Option_Dialog();
            }
        });

        dialog.setCancelable(true);
        dialog.setContentView(view);
        dialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
//                Toast.makeText(this, "Camera permission granted", Toast.LENGTH_LONG).show();

                Intent cameraIntent = new
                        Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
            else
            {
                Toast.makeText(this, "Camera permission required", Toast.LENGTH_LONG).show();
            }

        }
        else if(requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
//                // do your stuff
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out the app at:\n"+""+"https://searchus.in/share_and_earn.php?id="+""+PreferenceManager_ASOS.GetUserID());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

//                SHAREEARN();
            }
            else
            {
                Toast.makeText(Home.this, "Storage permission required",
                        Toast.LENGTH_SHORT).show();
            }
        }


    }


    private BroadcastReceiver dataChangeReceiver= new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            // update your listview
        }
    };


    @Override
    public void onBackPressed()
    {
        if (myDrawerLayout.isDrawerOpen(GravityCompat.START))
        {
            myDrawerLayout.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
            moveTaskToBack(true);
        }
    }

//    @Override
//    protected void onPause()
//    {
//        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(dataChangeReceiver);
//    }

    @Override
    protected void onResume()
    {
        super.onResume();
        //BAG COUNT CODE
        ArrayList<mycart> cart_count = new ArrayList<mycart>();
        Main_impl impl = new Main_impl(Home.this);
        cart_count = impl.getUser();
        tv_bagcount.setText(String.valueOf(cart_count.size()));

        SHOW_HIDE();
    }
}
