package com.example.searchus.asos.Impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.example.searchus.asos.Db.DatabaseHelper;
import com.example.searchus.asos.MODELS.mycart;

import java.util.ArrayList;

/**
 * Created by DELL on 03/24/2016.
 */
public class Main_impl implements DBOperation {

    SQLiteDatabase db;

    public Main_impl(Context context) {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        try {
            db = databaseHelper.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Main_impl() {

    }


    @Override
    public long insert(Object object) {

        mycart details = (mycart) object;
        long result = db.insert(details.TableName(), null, this.getContentvalues(object));
        return result;
    }

    @Override
    public int update(Object object) {
        mycart details = (mycart) object;
        String[] whereArgs = {String.valueOf(details.getId())};
        return updateall(object, "id = ?", whereArgs);
    }

    private int updateall(Object object, String string, String[] whereArgs) {
        mycart table = (mycart) object;
        int result = db.update(table.TableName(),
                this.getContentvalues(object), string, whereArgs);

        return result;
        // TODO Auto-generated method stub

    }

    public ArrayList<mycart> getSinlgeEntry() {

        ArrayList<mycart> studentList = new ArrayList<mycart>();


        try {
            Cursor c = db.rawQuery(
                    "SELECT product_id,product_name,qty FROM Details", null);

            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    mycart table = new mycart();
                    table.setProduct_id(c.getString(c.getColumnIndex("product_id")));
                    table.setProduct_name(c.getString(c.getColumnIndex("product_name")));
                    table.setQty(c.getString(c.getColumnIndex("qty")));

                    Log.e("ke1", c.getString(c.getColumnIndex("product_id")) + "");
                    studentList.add(table);
                } while (c.moveToNext());
                c.close();
            } else {
                // Util.customLog("getAll FavValue - No value found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentList;


    }

    @Override
    public int delete(Object object) {
        mycart details = (mycart) object;
        String[] whereArgs = {String.valueOf(details.getId())};
        int result = db.delete(details.TableName(), "id = ?", whereArgs);
        // Util.customLog("Delete Result - " + result);
        return result;

    }

    @Override
    public ContentValues getContentvalues(Object object) {
        mycart details = (mycart) object;

        ContentValues contentValues = new ContentValues();
        contentValues.put("product_id", details.getProduct_id());
        contentValues.put("product_name", details.getProduct_name());
        contentValues.put("product_price", details.getPrice());
        contentValues.put("product_desc", details.getDescription());
        contentValues.put("product_spec", details.getSpecification());
        contentValues.put("discount", details.getDiscount());
        contentValues.put("avalibility", details.getAvailability());
        contentValues.put("img", String.valueOf(details.getImg()));
        contentValues.put("qty", details.getQty());
        contentValues.put("amount", details.getAmount());
        contentValues.put("stts", details.getStts());
        contentValues.put("availqty", details.getAvailqty());
        contentValues.put("variant", details.getVariant());
        contentValues.put("note", details.getNote());

        return contentValues;
    }

    public boolean isExist(String product_id) {
        Cursor cursor = db.rawQuery("SELECT product_id FROM Details WHERE product_id = '" + product_id
                + "'", null);
        boolean exist = (cursor.getCount() > 0);
        cursor.close();
        db.close();
        return exist;
    }

    public void delete_record(int id) {
        db.delete(DatabaseHelper.TABLE_NAME, "id=" + "'" + id + "'", null);
    }

    public void delete_table() {
        db.delete(DatabaseHelper.TABLE_NAME, null, null);
    }

//    public List<CategoryData> getUserDetails(String EMAIL) {
//        List<CategoryData> studentList = new ArrayList<CategoryData>();
//
//        String[] columnArray = {"id,FIRST_NAME,LAST_NAME,EMAIL,NUMBER,PASSWORD "};
//        Cursor c;
//        try {
//            c = db.query("Details", columnArray, "EMAIL=" + "'" + EMAIL
//                    + "'", null, null, null, null);
//            if (c.getCount() > 0) {
//                c.moveToFirst();
//                do {
//                    CategoryData table = new CategoryData();
//                    table.setId(c.getInt(c.getColumnIndex("id")));
//                    table.setFirstname(c.getString(c.getColumnIndex("FIRST_NAME")));
//                    table.setLastname(c.getString(c.getColumnIndex("LAST_NAME")));
//                    table.setEmail(c.getString(c.getColumnIndex("EMAIL")));
//                    table.setNumber(c.getString(c.getColumnIndex("NUMBER")));
//                    table.setPassword(c.getString(c.getColumnIndex("PASSWORD")));
//
//                    Log.e("ke1", c.getString(c.getColumnIndex("EMAIL")) + "");
//                    studentList.add(table);
//                } while (c.moveToNext());
//                c.close();
//            } else {
//                // Util.customLog("getAll FavValue - No value found");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return studentList;
//    }
//
//    public List<CategoryData> getUserDetails1(String EMAIL) {
//        List<CategoryData> studentList = new ArrayList<CategoryData>();
//
//        String[] columnArray = {"id,FIRST_NAME,LAST_NAME,EMAIL,NUMBER,PASSWORD "};
//        Cursor c;
//        try {
//            c = db.query("Details", columnArray, "EMAIL =" + "'" + EMAIL
//                    + "'", null, null, null, null);
//            if (c.getCount() > 0) {
//                c.moveToFirst();
//                do {
//                    CategoryData table = new CategoryData();
//                    table.setId(c.getInt(c.getColumnIndex("id")));
//                    table.setFirstname(c.getString(c.getColumnIndex("FIRST_NAME")));
//                    table.setLastname(c.getString(c.getColumnIndex("LAST_NAME")));
//                    table.setEmail(c.getString(c.getColumnIndex("EMAIL")));
//                    table.setNumber(c.getString(c.getColumnIndex("NUMBER")));
//                    table.setPassword(c.getString(c.getColumnIndex("PASSWORD")));
//
//                    Log.e("ke1", c.getString(c.getColumnIndex("EMAIL")) + "");
//                    studentList.add(table);
//                } while (c.moveToNext());
//                c.close();
//            } else {
//                // Util.customLog("getAll FavValue - No value found");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return studentList;
//    }

    public ArrayList<mycart> getUser() {
        ArrayList<mycart> studentList = new ArrayList<mycart>();

        String[] columnArray = {"id,product_id,product_name,product_price,product_desc,product_spec,discount,avalibility,img,qty,amount,stts,availqty,variant,note"};
        Cursor c;
        try {
            c = db.query("Details", columnArray, null, null, null, null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    mycart table = new mycart();
                    table.setId(c.getInt(c.getColumnIndex("id")));
                    table.setProduct_id(c.getString(c.getColumnIndex("product_id")));
                    table.setProduct_name(c.getString(c.getColumnIndex("product_name")));
                    table.setPrice(c.getString(c.getColumnIndex("product_price")));
                    table.setDescription(c.getString(c.getColumnIndex("product_desc")));
                    table.setSpecification(c.getString(c.getColumnIndex("product_spec")));
                    table.setDiscount(c.getString(c.getColumnIndex("discount")));
                    table.setAvailability(c.getString(c.getColumnIndex("avalibility")));
                    table.setImg(c.getString(c.getColumnIndex("img")));
                    table.setQty(c.getString(c.getColumnIndex("qty")));
                    table.setAmount(c.getString(c.getColumnIndex("amount")));
                    table.setStts(c.getString(c.getColumnIndex("stts")));
                    table.setAvailqty(c.getString(c.getColumnIndex("availqty")));
                    table.setVariant(c.getString(c.getColumnIndex("variant")));
                    table.setNote(c.getString(c.getColumnIndex("note")));

//                    Log.e("ke1", c.getString(c.getColumnIndex("product_id")) + "");
                    studentList.add(table);
                } while (c.moveToNext());
                c.close();
            } else {
                // Util.customLog("getAll FavValue - No value found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentList;
    }

}
