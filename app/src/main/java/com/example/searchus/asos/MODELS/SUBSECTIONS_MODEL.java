package com.example.searchus.asos.MODELS;

import java.util.ArrayList;

public class SUBSECTIONS_MODEL
{

    public SUBSECTIONS_MODEL(String product_id, String product_spec, String product_name, String product_price, String product_desc, String product_keywords, String quantity, String final_Price, ArrayList<RecentView_Varient_MODEL> recentView_varient_models, ArrayList<String> img, String avalibility, String product_Favourite_Status, String purchase_price, String product_code, String note) {
        this.product_id = product_id;
        this.product_spec = product_spec;
        this.product_name = product_name;
        this.product_price = product_price;
        this.product_desc = product_desc;
        this.product_keywords = product_keywords;
        this.quantity = quantity;
        Final_Price = final_Price;
        this.recentView_varient_models = recentView_varient_models;
        this.img = img;
        this.avalibility = avalibility;
        Product_Favourite_Status = product_Favourite_Status;
        this.purchase_price = purchase_price;
        this.product_code = product_code;
        this.note = note;
    }

    String product_id,product_spec,product_name,product_price,product_desc,product_keywords,quantity,Final_Price;
    ArrayList<RecentView_Varient_MODEL> recentView_varient_models;
    ArrayList<String>img;
    String avalibility,Product_Favourite_Status,purchase_price,product_code,note;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_spec() {
        return product_spec;
    }

    public void setProduct_spec(String product_spec) {
        this.product_spec = product_spec;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_desc() {
        return product_desc;
    }

    public void setProduct_desc(String product_desc) {
        this.product_desc = product_desc;
    }

    public String getProduct_keywords() {
        return product_keywords;
    }

    public void setProduct_keywords(String product_keywords) {
        this.product_keywords = product_keywords;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getFinal_Price() {
        return Final_Price;
    }

    public void setFinal_Price(String final_Price) {
        Final_Price = final_Price;
    }

    public ArrayList<RecentView_Varient_MODEL> getRecentView_varient_models() {
        return recentView_varient_models;
    }

    public void setRecentView_varient_models(ArrayList<RecentView_Varient_MODEL> recentView_varient_models) {
        this.recentView_varient_models = recentView_varient_models;
    }

    public ArrayList<String> getImg() {
        return img;
    }

    public void setImg(ArrayList<String> img) {
        this.img = img;
    }

    public String getAvalibility() {
        return avalibility;
    }

    public void setAvalibility(String avalibility) {
        this.avalibility = avalibility;
    }

    public String getProduct_Favourite_Status() {
        return Product_Favourite_Status;
    }

    public void setProduct_Favourite_Status(String product_Favourite_Status) {
        Product_Favourite_Status = product_Favourite_Status;
    }

    public String getPurchase_price() {
        return purchase_price;
    }

    public void setPurchase_price(String purchase_price) {
        this.purchase_price = purchase_price;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
