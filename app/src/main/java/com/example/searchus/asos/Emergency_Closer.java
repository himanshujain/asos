package com.example.searchus.asos;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Emergency_Closer extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_emergency__closer);


        String getemergency_closer_notes=getIntent().getStringExtra("emergency_closer_notes").toString();
        TextView emergency_closer_notes=findViewById(R.id.emergency_closer_notesid);
        emergency_closer_notes.setText(getemergency_closer_notes);
    }
}
