package com.example.searchus.asos.MODELS;

import java.util.ArrayList;

public class MyOrder_Prod_Detail_Model
{
    public MyOrder_Prod_Detail_Model(String product_id, String product_name, String variant_name, String quantity, String price, String amount, String saler_status, String customer_status, ArrayList<String> prods_img_jsonArray) {
        Product_id = product_id;
        Product_name = product_name;
        this.variant_name = variant_name;
        Quantity = quantity;
        Price = price;
        Amount = amount;
        Saler_status = saler_status;
        Customer_status = customer_status;
        Prods_img_jsonArray = prods_img_jsonArray;
    }

    String Product_id,Product_name,variant_name,Quantity,Price,Amount,Saler_status,Customer_status;
    ArrayList<String> Prods_img_jsonArray;


    public String getProduct_id() {
        return Product_id;
    }

    public void setProduct_id(String product_id) {
        Product_id = product_id;
    }

    public String getProduct_name() {
        return Product_name;
    }

    public void setProduct_name(String product_name) {
        Product_name = product_name;
    }

    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getSaler_status() {
        return Saler_status;
    }

    public void setSaler_status(String saler_status) {
        Saler_status = saler_status;
    }

    public String getCustomer_status() {
        return Customer_status;
    }

    public void setCustomer_status(String customer_status) {
        Customer_status = customer_status;
    }

    public ArrayList<String> getProds_img_jsonArray() {
        return Prods_img_jsonArray;
    }

    public void setProds_img_jsonArray(ArrayList<String> prods_img_jsonArray) {
        Prods_img_jsonArray = prods_img_jsonArray;
    }
}
