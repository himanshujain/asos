package com.example.searchus.asos.MODELS;

import java.io.Serializable;

public class LAYER_2_MODEL implements Serializable
{
    public LAYER_2_MODEL(String id, String parent_id, String category_name, String image_path, String child, String notes) {
        this.id = id;
        this.parent_id = parent_id;
        this.category_name = category_name;
        this.image_path = image_path;
        this.child = child;
        this.notes = notes;
    }

    String id,
            parent_id,
            category_name,
            image_path,
            child,
            notes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
