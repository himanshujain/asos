package com.example.searchus.asos;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_CoNNectioN;
import com.example.searchus.asos.CONNECTiON_PAGE.PaperCart_HEADER;
import com.example.searchus.asos.Classes.ItemClickListener;
import com.example.searchus.asos.Classes.No_Internet;
import com.example.searchus.asos.MODELS.LAYER_2_MODEL;
import com.example.searchus.asos.TAB_FRAG.CAT_TAB_FRAG;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SubCategory_Layer_2 extends AppCompatActivity
{


    RecyclerView Subcategory_layer2_RecyclerView;
    ArrayList<LAYER_2_MODEL> Subcat_array=new ArrayList<>();

    LinearLayout nonet_lo;
    ScrollView mainlo;

    Toolbar toolbar;
    TextView toolbarcatname,note;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asos_activity_sub_category__layer_2);

        //Calling Checking Internet connection function
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        nonet_lo=findViewById(R.id.nonet_lo_id);
        mainlo=findViewById(R.id.mainlo_id);
        toolbarcatname=findViewById(R.id.tvtoolbarcatnameid);
        note=findViewById(R.id.noteid);

        String getpname= getIntent().getStringExtra("catname");
        toolbarcatname.setText(getpname);

        String getnote=getIntent().getStringExtra("getnote").toString();
        Log.e("getnote", getnote);
        if(getnote.equals(""))
        {
            note.setVisibility(View.GONE);
        }
        else
        {
            note.setVisibility(View.VISIBLE);
            note.setText(getnote);
        }

    }

    // Checking Internet connection function
    public class NetworkChangeReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(final Context context, final Intent intent)
        {
            if(No_Internet.getInstance(getApplicationContext()).isNetworkAvailable(context))
            {
//                Toast.makeText(getApplicationContext(),"YES",Toast.LENGTH_SHORT).show();
                toolbar.setVisibility(View.VISIBLE);
                nonet_lo.setVisibility(View.GONE);
                mainlo.setVisibility(View.VISIBLE);


                SUB_CATEGORY_LAYER_2_GETDATA();

            }
            else
            {
//                Toast.makeText(getApplicationContext(),"NO",Toast.LENGTH_SHORT).show();
                toolbar.setVisibility(View.GONE);
                nonet_lo.setVisibility(View.VISIBLE);
                mainlo.setVisibility(View.GONE);

            }

        }


    }


    private void SUB_CATEGORY_LAYER_2_GETDATA()
    {
        Subcategory_layer2_RecyclerView = (RecyclerView) findViewById(R.id.subcategory_layer2_recycler_view);
        LinearLayoutManager trending_LayoutManager2 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        trending_LayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        Subcategory_layer2_RecyclerView.setLayoutManager(trending_LayoutManager2);
        Subcategory_layer2_RecyclerView.setNestedScrollingEnabled(false);

        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            JSONObject object=new JSONObject() ;
            object.put("type", "get_product_category");
            object.put("category", "levelwise");
            JSONObject object1=new JSONObject() ;
            object.put("options", object1);
            object1.put("level", "2");
            object1.put("category_id",  getIntent().getStringExtra("catid").toString().trim());
            object1.put("row_offset", "0");
            object1.put("row_count", "10");

            final String requestBody = object.toString();

            final ProgressDialog progressDialog=new ProgressDialog(SubCategory_Layer_2.this,R.style.ProgressDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait ...");
            progressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaperCart_CoNNectioN.LOGINAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();
                    Subcat_array.clear();

                    String getresponce = response.replace("\"", "");

                    if(getresponce.equals("No Data Found"))
                    {
                        Log.e("SUB_CAT_L_2 res", "BLANK DATA");
                    }
                    else
                    {
                        Log.e("SUB_CAT_L_2 res", response);

                        try
                        {
                            JSONObject jsonObject=new JSONObject(String.valueOf(response));
                            JSONArray jsonArray=jsonObject.getJSONArray("data");
                            Gson gson = new Gson();
                            for (int j = 0; j < jsonArray.length(); j++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                Subcat_array.add(gson.fromJson(String.valueOf(jsonObject1), LAYER_2_MODEL.class));


                            }

                            Subcategory_layer2_RecyclerView.setAdapter(new SUB_CATEGORY_LAYER_2_GETDATA_ADP(getApplicationContext(),Subcat_array));

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }


                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("SUB_CAT_L_2 error", error.toString());
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    params.put("authentication", PaperCart_HEADER.KEY);

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError
                {
                    try
                    {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    }
                    catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            requestQueue.add(stringRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }


    public class SUB_CATEGORY_LAYER_2_GETDATA_ADP extends RecyclerView.Adapter<SUB_CATEGORY_LAYER_2_GETDATA_ADP.ViewHolder>
    {
        ArrayList<LAYER_2_MODEL> subcat_array;
        Context context;

        public SUB_CATEGORY_LAYER_2_GETDATA_ADP(Context context, ArrayList<LAYER_2_MODEL> subcat_array)
        {
            super();
            this.context = context;
            this.subcat_array = subcat_array;
        }

        @Override
        public SUB_CATEGORY_LAYER_2_GETDATA_ADP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asos_big_sub_category_lvitems, parent, false);
            SUB_CATEGORY_LAYER_2_GETDATA_ADP.ViewHolder viewHolder = new SUB_CATEGORY_LAYER_2_GETDATA_ADP.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(SUB_CATEGORY_LAYER_2_GETDATA_ADP.ViewHolder holder, int position)
        {

            Picasso.with(context)
                    .load(PaperCart_CoNNectioN.IMGPATH+subcat_array.get(position).getImage_path())
                    .into(holder.imgThumbnail);

            holder.subcatname.setText(subcat_array.get(position).getCategory_name());

            holder.setClickListener(new ItemClickListener()
            {
                @Override
                public void onClick(View view, int position, boolean isLongClick)
                {

                    if(subcat_array.get(position).getChild().equals("0"))
                    {
                        String subid=subcat_array.get(position).getId();
                        String subcatname=subcat_array.get(position).getCategory_name();
                        String getnote=subcat_array.get(position).getNotes();
                        Log.e("subid", subcat_array.get(position).getChild());
                        Intent intent=new Intent(SubCategory_Layer_2.this,Product_Listing.class);
                        intent.putExtra("catid",subid);
                        intent.putExtra("subcatname",subcatname);
                        intent.putExtra("CATEGORY_NAME",subcatname);
                        intent.putExtra("from_intent","from_category");
                        intent.putExtra("getnote",getnote);
                        startActivity(intent);

                    }
                    else
                    {
                        String subcatid=subcat_array.get(position).getId();
                        String subcatname=subcat_array.get(position).getCategory_name();
                        String getnote=subcat_array.get(position).getNotes();
                        Log.e("subid", subcat_array.get(position).getId());
                        Intent intent=new Intent(SubCategory_Layer_2.this,Category_Layer_3.class);
                        intent.putExtra("subcatid",subcatid);
                        intent.putExtra("subcatname",subcatname);
                        intent.putExtra("getnote",getnote);
                        startActivity(intent);
                    }

                }
            });

        }

        @Override
        public int getItemCount()
        {
            return subcat_array.size();
        }

        public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            public ImageView imgThumbnail;
            public TextView subcatname;
            LinearLayout click_layout;
            private ItemClickListener clickListener;

            public ViewHolder(View itemView)
            {
                super(itemView);
                imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
                subcatname = (TextView) itemView.findViewById(R.id.tvproductnameid);
                click_layout = (LinearLayout) itemView.findViewById(R.id.click_layout_id);
                itemView.setOnClickListener(this);
            }


            public void setClickListener(ItemClickListener itemClickListener)
            {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        //Calling Checking Internet connection function
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        NetworkChangeReceiver receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
