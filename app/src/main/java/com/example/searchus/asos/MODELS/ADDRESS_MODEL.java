package com.example.searchus.asos.MODELS;

import java.io.Serializable;

public class ADDRESS_MODEL implements Serializable
{
    public ADDRESS_MODEL(String address_id, String name, String address, String country, String state, String city, String pincode, String phone, String near_by) {
        this.address_id = address_id;
        this.name = name;
        this.address = address;
        this.country = country;
        this.state = state;
        this.city = city;
        this.pincode = pincode;
        this.phone = phone;
        this.near_by = near_by;
    }

    String address_id,
            name,
            address,
            country,
            state,
            city,
            pincode,
            phone,
            near_by;


    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNear_by() {
        return near_by;
    }

    public void setNear_by(String near_by) {
        this.near_by = near_by;
    }
}
